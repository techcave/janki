
usage:
	@echo 
	@echo "  .: Project builder usage :."
	@echo
	@echo "  dev            - build development version, no crontabs"
	@echo "  prod           - build production version with crontabs"
	@echo
	@echo "  clean          - rm py[co] files"
	@echo "  dist           - !!! rm eggs, parts, downloads, bootstrap.py"
	@echo "  compile        - compile .py files"
	@echo

system:
	@mkdir -p ~/logs
	@mkdir -p var/log
	@mkdir -p downloads
	@mkdir -p eggs
	@mkdir -p parts
	@mkdir -p develop-eggs
	if [ ! -f local.cfg ]; then echo "\n\tYOU MUST cp deploy/local.cfg-dist local.cfg && nano local.cfg\n";return 1; fi


extractstatic:
	@tar -C backend/ -xvf static.tar backend/static/

bootstrap.py :
	wget http://downloads.buildout.org/2/bootstrap.py
	python bootstrap.py -c dev.cfg

bin/buildout : bootstrap.py
	@mkdir -p etc
	@mkdir -p var/log

dev: system bin/buildout compile
	python bin/buildout -c dev.cfg
	@if [ ! -f backend/conf/development.py ]; then cp -i templates/development.py backend/conf/; echo " .: created backend/conf/development.py"; fi
	@echo
	@echo " .: DEVELOPMENT configuration was built, so NO CRON here"
	@echo

prod: system bin/buildout compile
	python bin/buildout -c prod.cfg
	@if [ ! -f backend/conf/production.py ]; then cp -i templates/production.py backend/conf/; echo " .: created backend/conf/production.py";  fi
	@echo
	@echo " .: PRODUCTION configuration was built, with CRON"
	@echo

clean:
	find . -name '*.py[oc~]'| xargs rm -f

dist: clean
	rm -rf bin develop-eggs eggs parts .installed.cfg downloads bootstrap.py

compile:
	python -c 'import compileall;  compileall.compile_dir("./backend/", force=True, quiet=True);'
	@echo

configure:
	@if [ ! -f backend/conf/development.py ]; then cp -i templates/development.py backend/conf/; echo " .: created backend/conf/development.py"; fi
	@if [ ! -f backend/conf/production.py ]; then cp -i templates/production.py backend/conf/; echo " .: created backend/conf/production.py";  fi

css:
	bin/compass compile backend --force -c deploy/production.rb

deployall:
	fab dev deploy; fab prod deploy;
