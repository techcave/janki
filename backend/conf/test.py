# -*- coding: utf-8 -*-
import os
import logging
import sys
from development import *

# for nose
#os.environ['REUSE_DB'] = "1"

logging.disable(logging.CRITICAL)

DEBUG = False

USE_TZ = True

SOUTH_TESTS_MIGRATE = False

DATABASES = {
    'default': {
        'NAME': os.path.join(ROOT, '..', '..', 'data', 'unittest.db'),
        'ENGINE': 'django.db.backends.sqlite3',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

TEMPLATE_DEBUG = DEBUG