# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import os
import sys
import raven
from decimal import Decimal as D
from datetime import datetime
from django_utils.datetimestub import DatetimeStub
from rebateceo.settings import *
import djcelery
djcelery.setup_loader()

#################################################################
class FakeDatetime(DatetimeStub):
    class datetime(DatetimeStub.datetime):
        year = 2015
        month = 1
        day = 1

# sys.modules['datetime'] = FakeDatetime()
#################################################################

os.environ['REUSE_DB'] = "1"

ROOT = os.path.dirname(os.path.abspath(__file__))
MEDIA_ROOT = os.path.join(ROOT, "media")
MEDIA_URL = "/media/"

STATIC_ROOT = os.path.join(ROOT, "static")
STATIC_URL = "/static/"

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(ROOT, "assets"),
)

SECRET_KEY = ''

ROOT_URLCONF = 'backend.urls'

# Custom apps
INSTALLED_APPS += (
    'backend.tools',
)


RAVEN_CONFIG = {
    'dsn': 'http://b0f6b4873dd94a45b0bc9b2dac6a4047:fa3e72ed841948cc9744a7b7c053aa6d@sentry.sizeof.pl/62',
    # 'release': raven.fetch_git_sha(os.path.dirname(os.path.dirname(__file__)))
}

WSGI_APPLICATION = 'ws4redis.django_runserver.application'

# ===== WEB SOCKETS =====
WEBSOCKET_URL = '/ws/'
WS4REDIS_PREFIX = 'ws'
WS4REDIS_EXPIRE = None
WS4REDIS_HEARTBEAT = '--heartbeat--'

#
# http://jira.techcave.pl:8080/browse/FL-27
#
REBATECEO_NEWS_SYNC_DATA_URL = 'https://chjanki.pl/json/'
REBATECEO_PROMOTIONS_SYNC_DATA_URL = 'https://chjanki.pl/json/'
REBATECEO_CATEGORIES_SYNC_DATA_URL = ''
REBATECEO_SHOPS_SYNC_DATA_URL = ''


# Languages
LANGUAGES = (
    ('en', 'English'),
    ('pl', 'Polish'),
)

PARLER_DEFAULT_LANGUAGE_CODE = 'pl'

# Translations
PARLER_LANGUAGES = {
    1: (
        {'code': 'pl'},
        {'code': 'en'},
    ),
    'default': {
        'fallbacks': ['pl'],  # defaults to PARLER_DEFAULT_LANGUAGE_CODE
        'hide_untranslated': False,  # the default; let .active_translations() return fallbacks too.
    }
}



API_USER = 'api'
API_PASSWORD = 'A1x84kmfx03A'

# Should be overwrite in prod/dev settings

CACHE_FOLDER = os.path.join(os.path.dirname(ROOT), 'var/cache/')
CACHE_CHECK_FILE = os.path.join(CACHE_FOLDER, 'shops.json')


# CACHE_LINKS = [
#     {'name': 'shops',
#      'link': 'http://basis.techcave.pl/api/shop/list/'},
#     {'name': 'categories',
#      'link': 'http://basis.techcave.pl/api/shop/category/list/'},
#     {'name': 'news',
#      'link': 'http://basis.techcave.pl/api/news/list/'},
#     {'name': 'promotions',
#      'link': 'http://basis.techcave.pl/api/promotion/list/'},
#     {'name': 'tags',
#      'link': 'http://basis.techcave.pl/api/tags/list/'},
#     {'name': 'points',
#      'link': 'http://basis.techcave.pl/api/map/point/list/'},
#     {'name': 'edges',
#      'link': 'http://basis.techcave.pl/api/map/edge/list/'},
# ]

