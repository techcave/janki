# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import reversion
import urllib2
from datetime import datetime
from urlparse import urlparse
from django.db import connections, transaction
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.management import BaseCommand
from optparse import make_option


class Command(BaseCommand):
    help = u'Migruje dane starego lokalizatora do nowej struktury'
    option_list = BaseCommand.option_list + (
        make_option('--hard-reset',
            action='store_true',
            dest='hard_reset',
            default=False,
            help='Reset all data'),
    )


    def get_old(self, table):
        def dictfetchall(cursor):
            "Returns all rows from a cursor as a dict"
            desc = cursor.description
            return [
                    dict(zip([col[0] for col in desc], row))
                    for row in cursor.fetchall()
            ]

        cursor = connections['old'].cursor()
        cursor.execute("SELECT * FROM {}".format(table))
        return dictfetchall(cursor)

    @transaction.atomic()
    @reversion.create_revision()
    def handle(self, *args, **options):
        from rebateceo.contrib.map.models import Type, Edge, Point
        from rebateceo.contrib.shop.models import Shop, Category
        from rebateceo.contrib.tags.models import Tag

        # import reversion
        # from  reversion.models import Version, Revision
        # with reversion.revision:
        # Points
        # print "\n\nPoints\n"
        # for row in self.get_old('points'):
        #     type, created = Type.objects.get_or_create(id=row['type'], title="Type #{}".format(row['type']))
        #     point, created = Point.objects.get_or_create(id=row['point_id'], x=row['x'], y=row['y'],
        #                                                  level_id=row['level'], type=type)
        #
        #     print point, created
        #
        # # Edges
        # print "\n\nEdges\n"
        # for row in self.get_old('edge'):
        #     try:
        #         point_a = Point.objects.get(pk=row['point_id'])
        #     except Point.DoesNotExist:
        #         point_a = None
        #
        #     try:
        #         point_b = Point.objects.get(pk=row['destination'])
        #     except Point.DoesNotExist:
        #         point_b = None
        #
        #     if point_a:
        #         edge, created = Edge.objects.get_or_create(
        #             point_a=point_a,
        #             point_b=point_b,
        #             length=row['length'],
        #             id=row['uid'],
        #         )
        #         print edge, created
        #
        # # Categories
        # print "Categories\n"
        # for row in self.get_old('categories'):
        #     category, created = Category.objects.get_or_create(id=row['category_id'], name=row['name'], visible=row['visible'])
        #     category.save()
        #     print category, created
        def _get_image(shop, field, value):

            if not value:
                return

            try:
                url = 'http://nigger.techcave.pl:7070/lomianki' + value
            except:
                pass
            else:
                try:
                    img_temp = NamedTemporaryFile(delete=True)
                    req = urllib2.Request(url)
                    img_temp.write(urllib2.urlopen(req).read())
                except:
                    pass
                else:
                    img_temp.flush()
                    image_name = urlparse(url).path.split('/')[-1]
                    field.save(image_name, File(img_temp), save=True)
                    shop.save()
                    print "\tImage: ", url, field
            return field

        # Shops
        print "\n\nShops\n"
        for row in self.get_old('shops'):

            # try:
            #     point = Point.objects.get(pk=row['point_id'])
            # except Point.DoesNotExist:
            #     pass
            # else:
                shop, created = Shop.objects.get_or_create(
                    name=row['name'],
                    description=row['description'],
                    #point=point,
                    level=row['level'],
                    lottery=row['lottery'] or False,
                    locale_id=row['shop_id']
                )

                if row['archived'] == 1:
                    shop.deleted = True
                    shop.archived = True
                    shop.deleted_at = datetime.now()

                shop.external_id = row['lomianki_shop_id']
                shop.phone = row['phone_number']
                shop.archived = row['archived']
                shop.front = None
                shop.logo = None
                shop.save()

                shop_front = _get_image(shop, shop.front, row['shop_image'])
                shop_logo = _get_image(shop, shop.logo, row['logo'])


                try:
                    category = Category.objects.get(pk=row['category_id'])
                except Category.DoesNotExist:
                    pass
                else:
                    shop.categories.add(category)

                shop.save()
                print shop, shop.pk, created, '\n\n'

        # ShopTags
        # print "\n\nShop Tags\n"
        # for row in self.get_old('shop_tag'):
        #
        #     try:
        #         shop = Shop.objects.get(pk=row['shop_id'])
        #     except Shop.DoesNotExist:
        #         print row['shop_id']
        #     else:
        #
        #         print shop

            # try:
            #     shop_tag, created = Shop.objects.get(
            #         pk=Shop.objects.get(pk=row['shop_id']),
            #     )
            # except Shop.DoesNotExist, e:
            #     print "Shop.DoesNotExist: {}".format(row['shop_id'])
            # else:
            #     Shop.objects.get(pk=row['shop_id'])
            #     print shop_tag, created+