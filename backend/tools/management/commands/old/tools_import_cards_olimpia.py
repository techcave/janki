
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime
from optparse import make_option
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
        csv => Numer karty,Status karty,Data aktywacji
    """
    help = u'Importuje karty z olimpii'

    def handle(self, *args, **options):
        from rebateceo.contrib.card.models import Card, Activation

        file = open(args[0]).readlines()
        for line in file[1:]:
            row = line.split(',')
            number = row[0]
            status = row[1]
            activation_date = row[2].strip()

            card, created = Card.objects.get_or_create(number=number)
            card.created = datetime.now()
            card.save()
            print "\tcard: ", card, created

            if activation_date:
                activation, created = Activation.objects.get_or_create(card=card, type=Activation.OPERATOR,
                                                                       operator_id=1, status=True)
                if created:
                    activation.date = activation_date
                    activation.details = "Activated by import"
                    activation.save()

                print "\tactivation: ", activation, created

            print ""

        print "Imported or updated %s cards" % len(Card.objects.all())