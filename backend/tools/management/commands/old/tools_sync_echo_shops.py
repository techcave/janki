# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Importuje sklepy z bazy lomianki'

    def dictfetchall(self, cursor):
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop

        # print ("Deleting current shops")
        # count = Shop.objects.all().count()
        # Shop.objects.all().delete()
        # print ("...deleted: %s\n" % count)

        # print ("Reseting sequence")
        # Shop.objects.raw("""
        #     BEGIN;
        #         SELECT setval(pg_get_serial_sequence('"shop_shop"','id'), coalesce(max("id"), 1),
        #         max("id") IS NOT null) FROM "shop_shop";
        #     COMMIT;
        # """)
        # print ("...Done\n")

        print ("Connecting to remote db server")
        cursor = connections['lomianki'].cursor()
        cursor.execute("SELECT * FROM shops WH")
        shops = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(shops))

        print ("Create local objects")
        for shop in shops:
            object, created = Shop.objects.get_or_create(pk=shop['shop_id'])
            object.name = shop['name']
            object.description = shop['description']
            object.category_id = shop['category_id']
            object.point_id = shop['point_id']
            object.level = shop['level']
            object.uid = shop['uid']
            object.search_name = shop['search_name']
            object.lottery = shop['lottery']
            object.save()

            print object.pk, ', is new?: ' + str(created)

        print ("...created or updated: %s objects\n" % Shop.objects.all().count())
