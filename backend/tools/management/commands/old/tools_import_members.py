# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import barcode
from optparse import make_option
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator


class Command(BaseCommand):
    help = u'Importuje uczestników'

    option_list = BaseCommand.option_list + (
        make_option('--hard-reset',
                    action='store_true',
                    dest='hard_reset',
                    default=False,
                    help='Reset all cards'),
    )

    GENDER_MAP = {
        'm': 1,
        'k': 2
    }

    def handle(self, *args, **options):
        from rebateceo.contrib.member.models import Member
        from rebateceo.contrib.card.models import Card, Activation

        if options.get('hard_reset'):
            Member.objects.all().delete()
            print "All Members have been cleaned"
            return

        file = open(args[0]).readlines()
        for line in file:
            row = line.split(',')
            number = '000' + row[0]
            first_name = row[1]
            last_name = row[2]
            email = row[3]
            mobile_number = row[4]
            phone = row[5]
            gender = self.GENDER_MAP[row[6]]
            street = row[8]
            house_number = row[9]
            postal = row[9]
            city = row[10]

            # Card
            card, created = Card.objects.get_or_create(number=number)
            print "Card: ", card, created

            # Activation
            activation, created = Activation.objects.get_or_create(card=card, type=Activation.OPERATOR, status=True)
            activation.details = "Activated during import"
            activation.save()
            print "\tActivation: ", activation, created

            # Member
            member, created = Member.objects.get_or_create(card=card)
            member.first_name = first_name
            member.last_name = last_name
            member.email = email
            member.mobile_number = mobile_number
            member.phone = phone
            member.gender = gender
            member.street = street
            member.street = street
            member.number = house_number
            member.postal = postal
            member.city = city
            member.save()
            print "\tMember: ", member, created
            print ""

        print "All cards in db: %s" % len(Card.objects.all())
        print "All Members in db: %s" % len(Member.objects.all())