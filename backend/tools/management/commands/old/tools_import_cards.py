# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import barcode
from optparse import make_option
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator


class Command(BaseCommand):
    help = u'Importuje karty'

    option_list = BaseCommand.option_list + (
        make_option('--hard-reset',
            action='store_true',
            dest='hard_reset',
            default=False,
            help='Reset all cards'),
    )

    def handle(self, *args, **options):
        from rebateceo.contrib.card.models import Card

        def get_ean(code):
            EAN = barcode.get_barcode_class('ean13')
            codebase = "%s" % str(code).zfill(12)
            return EAN13_validator(str(EAN(codebase)))

        if options.get('hard_reset'):
            Card.objects.all().delete()
            print "All cards have been cleaned"
            return

        file = open(args[0]).readlines()
        for line in file:
            row = line.split(';')
            number = row[0]
            card, created = Card.objects.get_or_create(number=number)
            card.created = "%s %s" % (row[1], str(row[2]).replace('\n', ''))
            card.save()
            print card, created

        print "Imported or updated %s cards" % len(Card.objects.all())