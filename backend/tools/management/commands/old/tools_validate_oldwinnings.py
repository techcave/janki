# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator
from django_utils.generators import get_EAN13
from django.core.exceptions import ValidationError
from backend.lottery.models import Winning


class Command(BaseCommand):
    help = u''

    def handle(self, *args, **options):
        i = 0
        for item in Winning.objects.all():
            try:
                if not item.code:
                    raise ValidationError('Empty')
                EAN13_validator(item.code)
            except ValidationError, e:
                item.notes = item.code
                item.code = get_EAN13(item.pk)
                item.save()
            else:
                if not item.created_at:
                    item.created_at = item.draw.scan.created
                    item.updated_at = datetime.datetime.now()
                    item.save()

                #if not item.draw.created_at:
                item.draw.created_at = item.draw.scan.created
                item.draw.updated_at = datetime.datetime.now()
                item.draw.save()
        print i