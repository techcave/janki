# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Scala stare kategorie'

    def dictfetchall(self, cursor):

        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop, Category

        print ("Deleting current categories")
        count = Category.objects.all().count()
        Category.objects.all().delete()
        print ("...deleted: %s\n" % count)

        cursor = connections['lomianki'].cursor()
        cursor.execute("SELECT * FROM categories WH")
        categories = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(categories))

        for category in categories:
            object, create = Category.objects.get_or_create(pk=category['category_id'])
            print create
            object.name = category['name'];
            object.save();

            print category



        print ("...created or updated: %s objects\n" % Category.objects.all().count())