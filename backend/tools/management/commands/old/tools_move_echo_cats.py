# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Importuje sklepy z bazy lomianki'

    def dictfetchall(self, cursor):
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Category

        print ("Connecting to db server")
        cursor = connections['lomianki'].cursor()
        cursor.execute("SELECT * FROM categories")
        data = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(data))

        for cat in data:
            try:
                category = Category.objects.get(id=int(cat['category_id']))
            except Category.DoesNotExist:
                Category.objects.create(id=int(cat['category_id']), name=cat['name'])
            else:
                category.name = cat['name']
                category.save()