# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Scala stare kategorie'

    def dictfetchall(self, cursor):

        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.map.models import Edge, Point

        print ("Counting existing Edges")
        count = Edge.objects.all().count()
        print ("...counted: %s\n" % count)

        cursor = connections['lomianki'].cursor()
        cursor.execute("SELECT * FROM edge WH")
        edges = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(edges))

        # Edges
        for edge in edges:
            object, created = Edge.objects.get_or_create(pk=edge['uid'], length=edge['length'],
                                                         point_a=Point.objects.get(pk=edge['point_id']),
                                                         point_b=Point.objects.get(pk=edge['destination']))

            print object, created