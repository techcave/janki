# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from optparse import make_option
from django.core.management import BaseCommand
from backend.lottery.models import Lottery, Prize


class Command(BaseCommand):
    help = u'Importuje nagrody do systemu'
    option_list = BaseCommand.option_list + (
        make_option('--cleanup-gte',
            dest='cleanup_gte',
            default=False,
            help='Remove prizes from date'),
    )

    def handle(self, *args, **options):

        ## RESET !!!!!!
        cleanup_gte = options.get('cleanup_gte')
        if cleanup_gte:
            prizes = Prize.objects.filter(date_time__gte=cleanup_gte).all()
            print "Found %s prizes" % prizes.count()
            print "All prizes from date_time gte %s have been removed" % cleanup_gte
            return

        file = open(args[0]).readlines()

        i = 100
        for row in file[1:]:
            item = row.split(',')
            date_time = item[0] + ' ' + str(datetime.datetime.strptime(item[1], '%I:%M:%S %p'))[11:]
            prize_value = str(item[2])
            is_material_prize = False
            prize, created = Prize.objects.get_or_create(
                lottery=Lottery.objects.get_current(),
                date_time=date_time,
                number=i,
                amount=prize_value if not is_material_prize else 0,
            )
            i += 1
            print created, prize
