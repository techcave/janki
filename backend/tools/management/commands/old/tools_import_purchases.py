
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from optparse import make_option
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = u'Importuje transakcje'

    option_list = BaseCommand.option_list + (
        make_option('--hard-reset',
            action='store_true',
            dest='hard_reset',
            default=False,
            help='Reset all cards'),
    )

    def handle(self, *args, **options):
        from rebateceo.contrib.card.models import Card
        from rebateceo.contrib.purchase.models import Purchase
        from rebateceo.contrib.receipt.models import Receipt
        from rebateceo.contrib.shop.models import Shop

        if options.get('hard_reset'):
            Purchase.objects.all().delete()
            print "All purchases have been cleaned"
            return

        file = open(args[0]).readlines()
        for line in file[1:]:
            row = line.split(';')
            number = row[0]
            purchase_date = row[1]
            shop_name = row[2]
            receipt_number = row[3]
            amount = row[4]

            card, created = Card.objects.get_or_create(number=number)
            if created:
                card.created = datetime.datetime.now()
            print 'card: ', card, created

            shop, created = Shop.objects.get_or_create(name=shop_name.strip().decode('utf-8').upper())
            print 'shop: ', shop, created

            receipt, created = Receipt.objects.get_or_create(shop=shop, purchase_date=purchase_date,
                                                             number=receipt_number, amount=amount)
            print 'receipt: ', receipt, created

            purchase, created = Purchase.objects.get_or_create(card=card, receipt=receipt, register_date=purchase_date, 
                                                               notes="Imported from old system", operator_id=1)
            print 'purchase: ', purchase, created
            print '-------------------'

        print "Imported or updated %s purchases" % len(Purchase.objects.all())