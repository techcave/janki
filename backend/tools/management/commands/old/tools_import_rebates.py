# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from optparse import make_option
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = u'Importuje rabaty'

    def handle(self, *args, **options):
        from rebateceo.contrib.rebate.models import Rebate
        from datetime import datetime

        file = open(args[0]).readlines()
        #shop_id							daily_limit	global_limit

        from dateutil import parser
        count = 0
        for row in file[1:]:
            item = row.split(';')
            shop_id = item[0].strip()
            if shop_id not in [None, ''] and len(item) == 9:
                shop_id = int(item[0])
                name = item[1]
                content = item[2]
                show_date_from = item[3]
                show_date_to = item[4]
                valid_date_from = item[5]
                valid_date_to = item[6]
                if shop_id and show_date_from and show_date_to:
                    item, created = Rebate.objects.get_or_create(
                        shop_id=shop_id,
                        content=unicode(content.decode("utf8")),
                        show_date_from=parser.parse(show_date_from + " 00:00:00"),
                        show_date_to=parser.parse(show_date_to + " 00:00:00"),
                        valid_date_from=parser.parse(valid_date_from + " 00:00:00"),
                        valid_date_to=parser.parse(valid_date_to + " 00:00:00")
                    )

                    print created, item
                    count += 1

        print count