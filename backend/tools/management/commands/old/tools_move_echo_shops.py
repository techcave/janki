# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Importuje sklepy z bazy lomianki'

    def dictfetchall(self, cursor):
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop, Category
        from rebateceo.contrib.map.models import Point

        print ("Connecting to db server")
        cursor = connections['lottery_v3'].cursor()
        cursor.execute("SELECT * FROM shop_shop")
        shops = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(shops))

        print ("Create or update shops objects")
        for shop in shops:
            object, created = Shop.objects.get_or_create(pk=shop['id'])

            if shop['category_id']:
                try:
                    category = Category.objects.get(id=shop['category_id'])
                except Category.DoesNotExist:
                    pass
                else:
                    object.category = category
                    try:
                        object.save()
                    except Exception, e:
                        object.category = None
                        print e

            object.name = shop['name']
            if shop['point_id']:
                point, created = Point.objects.get_or_create(pk=shop['point_id'], level_id=shop['level'], x=-1, y=-1, type=-1)
                object.point = point
            object.imported_at = shop['imported_at']
            object.description = shop['description']
            object.save()
        print ("...created or updated: %s objects\n" % Shop.objects.all().count())