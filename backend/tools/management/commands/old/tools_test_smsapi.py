
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.core.management import BaseCommand
from smsapi.client import SmsAPI
from smsapi.responses import ApiError


class Command(BaseCommand):
    help = u'Test SMS-API'

    def handle(self, *args, **options):
        pass
        # https://ssl.smsapi.pl/sms.do?username=bartlomiej.zygmunt@techcave.pl&password=2bd0fe0a0d67c3d44ac12d0e0ca77fb1&from=Eco&to=48725247774&message=test%20wiadomosci

        # api = SmsAPI()
        #
        # api.set_username('bartomiej.zygmunt@techcave.pl')
        # api.set_password('2bd0fe0a0d67c3d44ac12d0e0ca77fb1', False)
        #
        # #sending SMS
        # try:
        #     api.service('sms').action('send')
        #
        #     api.set_content('Hello [%1%] [%2%]')
        #     api.set_params('name', 'last name')
        #     api.set_to('725247774')
        #     api.set_from('Info') #Pole nadawcy lub typ wiadomość 'ECO', '2Way'
        #
        #     result = api.execute()
        #
        #     for r in result:
        #         print r.id, r.points, r.status
        #
        # except ApiError, e:
        #     print '%s - %s' % (e.code, e.message)