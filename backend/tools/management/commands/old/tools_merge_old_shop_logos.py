# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Scala starą bazę'

    def dictfetchall(self, cursor):

        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop, Category
        from django.core.files.base import ContentFile


        cursor = connections['lomianki'].cursor()
        cursor.execute("SELECT * FROM shops WH")
        shops = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(shops))

        for shop in shops:
            try:
                with open('/home/techcave/htdocs/techcave/backend-rabatomat/lomianki/assets/logo/'+str(shop['shop_id'])+'.png') as f:
                    data = f.read()
                    object = Shop.objects.get(pk=shop['shop_id'])
                    object.logo.save(str(shop['shop_id'])+'.png', ContentFile(data))

            except Exception as e:
                print str(e);
