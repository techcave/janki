# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Scala stare punkty do nowych'
    db_alias = 'lomianki'

    def dictfetchall(self, cursor):
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop
        from rebateceo.contrib.map.models import Point, Type

        # Reset all shops
        Shop.objects.all().update(point=None)

        # Fetch old-db points
        cursor = connections[self.db_alias].cursor()
        cursor.execute("SELECT * FROM points WH")
        points = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(points))

        # Create new-db points
        for point in points:
#	    print point
#	    print Type.objects.get(pk=int(point['type']))
	    try:
	            object, created = Point.objects.get_or_create(pk=point['point_id'], x=point['x'], y=point['y'], level_id=point['level'])
	    except:
		  print point  		    
	    else:
		  object.type = Type.objects.get(pk=int(point['type']))
		  object.save()
		  print object, created


        # Fetch old-db shops
        cursor = connections[self.db_alias].cursor()
        cursor.execute("SELECT * FROM shops WH")
        shops = self.dictfetchall(cursor)
        print ("...shops: %s objects\n" % len(points))

        for shop in shops:
            try:
                object = Shop.objects.get(pk=shop['shop_id'])
                object.point = Point.objects.get(pk=shop['point_id'])
                object.save()
            except Exception:
                print "EXC:"
                print shop['shop_id']
