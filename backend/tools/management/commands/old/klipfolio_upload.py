# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import os
from django.conf import settings
from django.core.management import BaseCommand
from django.core.mail.message import EmailMessage
from klipfolio.api import KlipfolioClientAPI
from rebateceo.contrib.card.reports import get_activations_csv, get_scans_csv
from rebateceo.contrib.rebate.reports import get_coupons_csv
from rebateceo.contrib.shop.reports import get_shops_csv
from rebateceo.contrib.member.report import get_member_csv
from rebateceo.contrib.purchase.reports import get_purchases_csv
from rebateceo.contrib.lottery.report import get_winnings_csv


class Command(BaseCommand):
    help = u'Eksportuje wszystkie raporty do plikow i wysyla do klipfolio.'

    def handle(self, *args, **options):
        files = {
            'activations': get_activations_csv,
            'members': get_member_csv,
            'purchases': get_purchases_csv,
            'shops': get_shops_csv,
            'winnings': get_winnings_csv,
        }

        out = "Upload to klipfolio\n\n"
        for key in files:
            filename = os.path.join(os.path.dirname(settings.ROOT), 'data', 'klipfolio', '%s.csv' % key)
            with open(filename, 'w+') as file_:
                out += "Process file: %s\n" % filename
                func = files[key]
                content = str(func().getvalue())
                file_.write(content)

                # Run upload
                url = settings.KLIPFOLIO_API_UPLOADS[key]['url']

                client = KlipfolioClientAPI()
                response = client.post(url, filename)
                out += " - upload %s filename to %s\n" % (filename, url)
                out += " - result code=%s, Result response=%s\n\n" % (response.status_code, response.text)

        out += "End of upload"
        print out

        message = EmailMessage("[Echo] - Klipfolio upload results", out, 'monitor@techcave.pl',
                               settings.CSV_REPORT_RECEIVERS)
        message.send()