# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Scala starą bazę'

    def dictfetchall(self, cursor):

        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop, Category

        print ("Counting current shops")
        count = Shop.objects.all().count()
        print ("...counted: %s\n" % count)

        cursor = connections['lomianki'].cursor()
        cursor.execute("SELECT * FROM shops WH")
        shops = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(shops))

        #
        # ALTER TABLE shop_shop DROP CONSTRAINT category_id_refs_id_17742ece;

        # ALTER TABLE shop_shop
        #   ADD CONSTRAINT category_id_refs_id_17742ece FOREIGN KEY (category_id)
        #       REFERENCES shop_shop_categories (id) MATCH SIMPLE
        #       ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;

        for shop in shops:
            try:
                category = Category.objects.get(pk=shop['category_id'])
                object = Shop.objects.get(pk=shop['shop_id'])
                object.categories.add(category)
                object.category = category
                object.category_id = shop['category_id']
                object.point_id = shop['point_id']
                object.level = shop['level']
                object.uid = shop['uid']
                object.search_name = shop['search_name']
                object.lottery = shop['lottery']
                object.save()

                print str(object) + "\t" + str(shop['name'])
                print str(object.pk) + "\t" + str(shop['shop_id'])
                print str(object.category_id) + "\t" + str(shop['category_id'])


            except Exception as e:
                print str(e);
