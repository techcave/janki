# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.conf import settings
from django.core.management import BaseCommand
from backend.lottery.models import Winning
from django_ean13.validators import EAN13_validator
from django_utils.generators import get_EAN13
from django.core.exceptions import ValidationError


class Command(BaseCommand):
    help = u''

    def handle(self, *args, **options):
        from rebateceo.contrib.rebate.models import Coupon
        from rebateceo.contrib.card.models import Card
        from rebateceo.contrib.purchase.models import Purchase

        card = Card.objects.get(number=args[0], test=True)

        for x in Purchase.objects.filter(register_date__gte=settings.LOTTERY_START_DATE, card=card):
            print x.card, x.pk, x.register_date
            x.delete()

        print Coupon.objects.filter(card=card).count()
        for x in Coupon.objects.filter(card=card):
            print x.card, x.pk, x.created_at
            #x.delete()
