# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import random
from datetime import datetime
from optparse import make_option
from django.conf import settings
from dateutil.rrule import rrule, DAILY
from django.core.management import BaseCommand

from backend.lottery.models import Prize


class Command(BaseCommand):
    help = u'Generuje wszystkie nagrody'

    def handle(self, *args, **options):

        if options.get('hard_reset'):
            Prize.objects.all().delete()
            print "All Prizes have been cleaned"
            return

        start_date = settings.LOTTERY_START_DATE
        end_date = settings.LOTTERY_END_DATE
        excluded = settings.MALL_CLOSED_DAYS + (settings.LOTTERY_SPECIAL_DATE,)
        result, i, x19, x23, x7, x66 = {}, 0, 0, 0, 0, 0

        def _rand(from_hour, to_hour):
            hour = random.randrange(from_hour, to_hour)
            minute = random.randrange(0, 59)
            second = random.randrange(0, 59)
            if hour == 14 and minute > 50:
                minute = random.randrange(0, 50)
            return "%s:%s:%s" % (hour, minute, second)

        for dt in rrule(DAILY, dtstart=start_date, until=end_date):

            # Exclude closed days
            ommit = False
            for date in excluded:
                if date.strftime("%Y-%m-%d") == dt.strftime("%Y-%m-%d"):
                    ommit = True
                    continue

            if not ommit:
                result[dt] = {}

                weekday = dt.weekday()

                try:
                    d = datetime(year=dt.year, month=dt.month, day=dt.day)
                    custom_hours = settings.MALL_UNUSUAL_OPENING_HOURS[d]
                    from_hour = custom_hours[0][0]
                    to_hour = custom_hours[1][0]
                except KeyError:
                    from_hour = settings.MALL_OPENING_HOURS[weekday][0][0]
                    to_hour = settings.MALL_OPENING_HOURS[weekday][1][0]

                # Pon - Pn
                if weekday < 5:
                    items = []
                    for x in xrange(0, 19):
                        if x == 0:
                            step = 3
                        elif 1 <= x <= 4:
                            step = 4
                        else:
                            step = 5
                        items.append([_rand(from_hour, to_hour), settings.LOTTERY_PRIZES[step][0][1]])
                        x19 += 1
                    result[dt] = items

                else:
                    # Sb - Nd
                    items = []
                    for x in xrange(0, 23):
                        if x == 0:
                            step = 2
                        elif 1 <= x <= 2:
                            step = 3
                        elif 3 <= x <= 7:
                            step = 4
                        else:
                            step = 5
                        items.append([_rand(from_hour, to_hour), settings.LOTTERY_PRIZES[step][0][1]])
                        x23 += 1
                    result[dt] = items
                i += 1

        # Special event
        special_date = settings.LOTTERY_SPECIAL_DATE
        result[special_date] = {special_date.weekday(): {}}
        items = []
        for x in xrange(0, 7):
            items.append([_rand(16, 23), settings.LOTTERY_PRIZES[1][0]])
            x7 += 1
        result[special_date] = items

        items = []
        for x in xrange(0, 66):
            if x <= 1:
                step = 2
            elif 2 <= x <= 5:
                step = 3
            elif 6 <= x <= 15:
                step = 4
            else:
                step = 5
            items.append([_rand(9, 23), settings.LOTTERY_PRIZES[step][0][1]])
            x66 += 1
        result[special_date] += items

        #########################################################
        #import pprint
        #pprint.pprint(result)
        #print x19, x23, x7, x66, sum([x19, x23, x7, x66])

        out = """data,wylosowana godzina,rodzaj nagrody,lp\n"""
        i = 1
        for date, items in result.iteritems():
            for j, item in enumerate(items):
                d = datetime.strptime(item[0], "%H:%M:%S")
                prize = item[1]
                if type(prize) == list:
                    prize = prize[2]
                out += "{date},{time},{prize},{i}\n".format(date=date.strftime("%Y-%m-%d"),
                                                            time=d.strftime("%I:%M:%S %p"), prize=prize, i=i)
                i += 1
        print out
