# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    help = u'Aktualizuje datę starych kuponów'

    def dictfetchall(self, cursor):
        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def handle(self, *args, **options):
        from rebateceo.contrib.rebate.models import Coupon
        print ("Connecting to remote db server")
        cursor = connections['lottery_v3'].cursor()
        cursor.execute("SELECT * FROM rebate_coupon WH")
        coupons = self.dictfetchall(cursor)
        print ("...received: %s objects\n" % len(coupons))

        print ("Create local objects")
        for coupon in coupons:
            print coupon['id'], coupon['created']
            obj = Coupon.objects.get(pk=coupon['id'])
            obj.created_at = coupon['created']
            obj.save()