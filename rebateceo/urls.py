# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf.urls import *
from django.conf import settings
from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),

    # Project urls
    # url(r'^map/', include('rebateceo.contrib.map.urls', namespace='map')),
    # url(r'^demo/', include('rebateceo.contrib.demo.urls')),
    # url(r'^manager/', include('rebateceo.contrib.manager.urls', namespace='manager')),
    # url(r'^operator/', include('rebateceo.contrib.operator.urls', namespace='operator')),
    # url(r'^account/', include('rebateceo.contrib.account.urls')),
    # url(r'^rebate/', include('rebateceo.contrib.rebate.urls', namespace='rebate')),

    # url(r'^shops/', include('rebateceo.contrib.shop.urls', namespace='shop')),
    # url(r'^sms/', include('rebateceo.contrib.sms.urls', namespace='sms')),
    # url(r'^report/', include('rebateceo.contrib.report.urls', namespace='report')),
    # url(r'^purchase/', include('rebateceo.contrib.purchase.urls', namespace='purchase')),
    # url(r'^receipt/', include('rebateceo.contrib.receipt.urls', namespace='receipt')),
    # url(r'^campaigns/', include('rebateceo.contrib.campaigns.urls', namespace='campaigns')),
    # url(r'^qrcode/', include('rebateceo.contrib.urls', namespace='qrcode')),
    url(r'^tags/', include('rebateceo.contrib.tags.urls', namespace='tags')),
    url(r'^websocket/', include('rebateceo.contrib.websocket.urls', namespace='websocket')),
    url(r'^tracking/', include('rebateceo.contrib.tracking.urls', namespace='tracking')),
    url(r'^account/', include('userena.urls')),
    url(r'^messages/', include('userena.contrib.umessages.urls')),
    url(r'^api/', include('rebateceo.contrib.api.urls', namespace='api')),
    url(r'^api/auth/', include('rebateceo.contrib.authorize.api.urls', namespace='auth')),
    url(r'^api/docs/', include('rest_framework_swagger.urls')),
    url(r'^crossdomain.xml$', 'rebateceo.crossdomain.xml', name='crossdomain'),
    url(r'^$', include('rebateceo.contrib.dashboard.urls', namespace='dashboard')),
    url(r'^cache/', include('rebateceo.contrib.cache.urls', namespace='cache')),
    url(r'^promotions/', include('rebateceo.contrib.promotion.urls', namespace='promotions')),
    )


if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT}),
        (r'^static/(?P<path>.*)$', 'django.views.static.serve',
         {'document_root': settings.STATIC_ROOT}),
    )