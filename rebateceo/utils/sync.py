import datetime
import requests
import urllib2

from django.core.management import BaseCommand
from django.utils import translation

from rebateceo.utils import getimg2
from django.utils import timezone

class BCommand(BaseCommand):
    model = None
    image_model = None
    api_url = None
    fk_field = None
    image_path = ''

    def handle(self, *args, **options):
        print u"\n=== Start: %s ===\n" % timezone.now()
        self.image_model.objects.all().delete()
        self.model.objects.all().delete()

        user_agent = {
            'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36'
        }
        print "Getting Data from {}".format(self.api_url)
        response = requests.get(self.api_url, headers=user_agent).json()
        translation.activate('pl')
        for index, child in enumerate(response):
            pk = child.get('id')
            description = child.get('description')
            date_from = child.get('show_date_from')
            date_to = child.get('show_date_to')
            short = child.get('short')
            title = child.get('title')

            image = child.get('thumb_image')

            obj, created = self.model.objects.get_or_create(pk=pk)
            obj.description = description
            obj.date_from = date_from if date_from \
                else datetime.datetime.now()
            obj.date_to = date_to if date_to \
                else datetime.datetime.strptime('2020-12-31 23:59:59',
                                                '%Y-%m-%d %H:%M:%S')
            obj.short = short
            obj.title = title

            obj.save()

            print u'Object: {}. Created -> {}'.format(title.encode('ascii', 'replace'), created)

            if image:
                try:
                    image = image.replace('\\', '')
                    img, tmp = getimg2(image)
                except urllib2.HTTPError as e:
                    print
                    u'Image: ERROR: {}'.format(str(e), image)
                item = self.image_model()
                setattr(item, self.fk_field, obj)  # Set related item
                item.image.save(img, tmp, save=True)
                item.save()
                print u'\tImage: {}'.format(item.image.url)
        print u"\n=== End: {} ===\n".format(timezone.now())