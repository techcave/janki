# -*- coding: utf-8 -*-

import shutil
from datetime import datetime
from django.core.management.base import BaseCommand
from django.utils.html import strip_tags
from django.template.defaultfilters import slugify
import feedparser


class BaseSyncCommand(BaseCommand):
    """
        http://jira.techcave.pl:8080/browse/FL-27
    """
    model = None
    image_model = None
    api_url = None
    fk_field = None
    image_path = ''

    def html_decode(self, s):
        """
        Returns the ASCII decoded version of the given HTML string. This does
        NOT remove normal HTML tags like <p>.
        """
        html_codes = (
                ("'", '&#39;'),
                ("'", '&rsquo;'),
                ('"', '&quot;'),
                ('>', '&gt;'),
                ('<', '&lt;'),
                ('&', '&amp;'),
                ('ó', '&oacute;'),
                ('à', '&agrave;'),
                ('„', '&bdquo;'),
                ('”', '&rdquo;'),
            )
        for code in html_codes:
            s = s.replace(code[1], code[0])
        return s

    # Gets title from feed n
    def get_title(self, data, n):
        return strip_tags(data.entries[n].title)

    # Gets description from feed n
    def get_description(self, data, n):
        description = strip_tags(data.entries[n].description)
        return description

    # Get publication datetime.date object from feed n (as YYYY-MM-DD)
    def get_pubdate(self, data, n):
        pubdate = data.entries[0].published_parsed
        return datetime(pubdate[0], pubdate[1], pubdate[2]).date()

    def get_date_from(self, data, n):
        # try:
        #   created = data.entries[n].created
        # except AttributeError as e:
        #   created = ''
        pass

    def get_date_to(self, data, n):
        # try:
        #     created = data.entries[n].expires
        # except AttributeError as e:
        #     created = ''
        pass

    def handle(self, *args, **options):

        # Mark all as deleted
        self.model.objects.all().delete()
        self.image_model.objects.all().delete()

        try:
            shutil.rmtree(self.image_path)
        except OSError:
            pass

        # Data
        data = feedparser.parse(self.api_url)
        feeds_count = len(data.entries)
        for num in xrange(feeds_count):

            # date_from = self.get_date_from(data, num)
            date_from = ''
            # date_to = self.get_date_to(data, num)
            date_to = ''

            # Create item
            object, created = self.model.objects.get_or_create(
                pk=num,
                title=self.get_title(data, num),
                description=self.html_decode(self.get_description(data, num).encode('utf8'))
            )

            if date_from:
                object.date_from = date_from

            if date_to:
                object.date_to = date_to

            object.save()

            print u'Object: {} -> {}\n'.format(slugify(object), created)

