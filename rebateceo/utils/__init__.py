import requests

from django.core.files.base import File
from django.core.files.temp import NamedTemporaryFile
from django.template.defaultfilters import slugify


def getimg2(image_url):
    user_agent = {
        'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu '
                      'Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36 '
    }
    response = requests.get(image_url, headers=user_agent, verify=False).content
    tmp = NamedTemporaryFile(delete=True)
    tmp.write(response)
    tmp.flush()
    image = image_url.split('/')[-1]
    img_parts = image.split('.')
    img_name, img_extension = img_parts[0], 'png'
    return "{}.{}".format(slugify(img_name), img_extension), File(tmp)
