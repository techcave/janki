# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import textwrap
import os
from settings import MOBILE_IMAGE_SETTINGS, MEDIA_ROOT
from django.core.files.base import File as DjangoFile
from PIL import Image, ImageDraw, ImageFont


def generate_mobileimage(object, text, upload_to):
    if object.images.first():
        try:
            size = MOBILE_IMAGE_SETTINGS['IMAGE_SIZE']
            fname, fext = os.path.splitext(object.images.first().image.name)
            img = Image.open(object.images.first().image.path)
            img.thumbnail(size, Image.ANTIALIAS)
            img_width, img_height = img.size
            font = ImageFont.truetype(MOBILE_IMAGE_SETTINGS['FONT_PATH'], MOBILE_IMAGE_SETTINGS['FONT_SIZE'])

            if img_height != size[1] and text:
                back = Image.new("RGB", size, MOBILE_IMAGE_SETTINGS['BG_COLOR'])
                back.paste(img, (0, 0))
                img = back
                draw = ImageDraw.Draw(img)

                y_text = 0
                for line in adjust_text_length(text.capitalize(), font, size[1] - img_height - 10):
                    draw.text((5, 10 + img_height + y_text), line, font=font, fill=MOBILE_IMAGE_SETTINGS['FONT_COLOR'])
                    y_text += int(MOBILE_IMAGE_SETTINGS['LINE_HEIGHT'] * 1.3)

            dest_dir = os.path.join(MEDIA_ROOT, upload_to, 'mobile_image')
            if not os.path.isdir(dest_dir):
                os.makedirs(dest_dir)

            _file = 'generated_{}.jpg'.format(object.pk)
            filename = '{}/mobile_image/{}'.format(upload_to, _file)
            file_path = "{}".format(os.path.join(dest_dir, _file))
            print file_path
            if img_height > int(size[1] / 2):
                img.convert('RGB').save(file_path, 'JPEG', quality=95)
            else:
                img.convert('RGB').save(file_path, 'JPEG', quality=95)
            print "Created mobile image for " + object.__class__.__name__ + " object: " + str(object.pk)

            return DjangoFile(file(file_path, 'rb'), name=filename)

        except IOError:
            print "Could not process image: " + object.images.first().image.name


def adjust_text_length(text, font, max_height):
    is_bigger = True
    line_height = MOBILE_IMAGE_SETTINGS['LINE_HEIGHT']
    lines = []

    if max_height > line_height:
        while is_bigger:
            y_text = 0
            lines = textwrap.wrap(text, width=36)
            for line in lines:
                y_text += int(line_height * 1.3)
            if y_text > max_height:
                chars = len(text)
                if y_text > (2 * max_height):
                    text = smart_truncate(text, int(chars / 2))
                else:
                    text = smart_truncate(text, chars - 10)
            else:
                is_bigger = False
    return lines


def smart_truncate(content, length=100, suffix='...'):
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length + 1].split(' ')[0:-1]) + suffix