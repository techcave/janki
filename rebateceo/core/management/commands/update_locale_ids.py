# -*- coding: utf-8 -*-
import os
import datetime
from django.conf import settings
from django.core.management import BaseCommand
from rebateceo.contrib.shop.models import Shop

ROOT = settings.ROOT
path = lambda *a: os.path.join(ROOT, *a)
DATA = open(path(os.path.dirname(__file__), 'locale_ids.csv')).readlines()


class Command(BaseCommand):
    def handle(self, *args, **options):
        for item in DATA:
            item = item.split(';')
            shop_id = int(item[0])
            locale_id = str(item[1]).replace('"', '').replace('\n', '')
            shop = Shop.objects.get(id=shop_id)
            shop.locale_id = locale_id
            shop.save()
            print shop.id, shop.locale_id
