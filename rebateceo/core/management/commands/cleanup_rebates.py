# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from rebateceo.contrib.rebate.models import Rebate
from rebateceo.contrib.shop.models import Shop


class Command(BaseCommand):

    def handle(self, *args, **options):
        for rebate in Rebate.objects.all():
            try:
                rebate.shop
            except Shop.DoesNotExist:
                print "Delete " + str(rebate.pk)
                print rebate.delete()
                pass
            else:
                rebate.content = 'Rabat nr %s' % (rebate.pk, )
                rebate.save()
