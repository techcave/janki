# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections
from rebateceo.contrib.tags.models import Tag, OldShopTag, ShopTag
from rebateceo.contrib.shop.models import Shop


class Command(BaseCommand):

    def handle(self, *args, **options):
        for shop_tag in OldShopTag.objects.all():
            try:
                shop = Shop.objects.get(id=shop_tag.shop_id)
            except Shop.DoesNotExist:
                pass
            else:
                try:
                    tag = Tag.objects.get(id=shop_tag.tag_id)
                except Tag.DoesNotExist:
                    pass
                else:
                    try:
                        ShopTag.objects.create(shop_id=shop.id, tag_id=shop_tag.id)
                    except:
                        pass
