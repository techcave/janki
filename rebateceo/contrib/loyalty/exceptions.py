# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from rest_framework.exceptions import APIException
from rest_framework import status


class LoyaltyBaseException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {}
    extra_message = default_message
    data = {}
    code = None

    def __init__(self, *args, **kwargs):
        self.detail = {
            'code': self.code,
            'type': self.__class__.__name__,
            'messages': {}}

        api = self.default_detail.format(**kwargs)
        if api:
            self.detail['messages'].update({'api': api})

        # Default
        default = self.get_default_message(**kwargs)
        if default:
            self.detail['messages'].update({'default': default})

        # Extra
        extra = self.get_extra_message(**kwargs)
        if extra:
            self.detail['messages'].update({'extra': extra})

        # Data
        data = self.get_data(**kwargs)
        if data:
            self.detail.update({'data': data})

    def get_default_message(self, **kwargs):
        return self.default_message

    def get_extra_message(self, **kwargs):
        return self.extra_message

    def get_data(self, **kwargs):
        return self.data


class LoyaltyAPIException(LoyaltyBaseException):
    code = 'L001'


class LoyaltyScanIdRequired(LoyaltyAPIException):
    default_detail = 'Scan id is required!'
    code = 'L002'


class LoyaltySellerIdRequired(LoyaltyAPIException):
    default_detail = 'Seller id is required!'
    code = 'L003'


class LoyaltySellerDoesNotExist(LoyaltyAPIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Seller for id {seller_id} does not exist!'
    code = 'L004'


class LoyaltyAmountIsRequired(LoyaltyAPIException):
    default_detail = 'Amount is required!'
    code = 'L005'


class LoyaltyScanDoesNotExist(LoyaltyAPIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Scan for id {scan_id} does not exist!'
    code = 'L006'


class LoyaltyProgramIdRequired(LoyaltyAPIException):
    default_detail = 'Program id is required!'
    code = 'L007'


class LoyaltyProgramDoesNotExist(LoyaltyAPIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Program for id {program_id} does not exist!'
    code = 'L008'


class AmountAlreadyRegistered(LoyaltyAPIException):
    default_detail = 'Amount already registered for scan_id = {scan_id}!'
    code = 'L009'


class LoyaltyScanCardDoesNotExist(LoyaltyAPIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Card in scan({scan_id}) does not exist!'
    code = 'L010'
