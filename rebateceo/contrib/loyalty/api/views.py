# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.core.exceptions import MultipleObjectsReturned
from rebateceo.contrib.card.api.serializers import CardSerializer
from rebateceo.contrib.shop.models import Seller
from rebateceo.contrib.verifier.models import VerifierScan
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from .. import models, bases, exceptions
from . import serializers


class AddAmountView(bases.LoyaltyBaseAPIView, GenericAPIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = serializers.AmountPostSerializer
    model = models.Amount
    scan_id = None
    seller_id = None
    program_id = None
    program = None
    card = None
    scan = None
    amount = None
    seller = None
    detail = None

    def initial(self, request, *args, **kwargs):
        super(AddAmountView, self).initial(request, args, kwargs)

        # Check scan_id
        self.program_id = request.POST.get('program_id', request.GET.get('program_id', kwargs.get('program_id')))
        if not self.program_id:
            raise exceptions.LoyaltyProgramIdRequired()

        # Check scan
        try:
            self.program = models.Program.objects.get(pk=self.program_id)
        except models.Program.DoesNotExist:
            raise exceptions.LoyaltyProgramDoesNotExist(scan_id=self.scan_id)

        # Check scan_id
        self.scan_id = request.POST.get('scan_id', request.GET.get('scan_id', kwargs.get('scan_id')))
        if not self.scan_id:
            raise exceptions.LoyaltyScanIdRequired()

        # Already registered?
        try:
            self.model.objects.get(scan_id=self.scan_id)
        except models.Amount.DoesNotExist, e:
            pass
        except MultipleObjectsReturned, e:
            raise exceptions.AmountAlreadyRegistered(scan_id=self.scan_id)

        # Check scan
        try:
            self.scan = VerifierScan.objects.get(pk=self.scan_id)
        except VerifierScan.DoesNotExist:
            raise exceptions.LoyaltyScanDoesNotExist(scan_id=self.scan_id)
        else:
            self.card = self.scan.card
            if not self.card:
                raise exceptions.LoyaltyScanCardDoesNotExist(scan_id=self.scan_id)

        # Check seller_id
        self.seller_id = request.POST.get('seller_id', request.GET.get('seller_id', kwargs.get('seller_id')))
        if not self.seller_id:
            raise exceptions.LoyaltySellerIdRequired()

        # Check seller
        try:
            self.seller = Seller.objects.get(pk=self.seller_id)
        except Seller.DoesNotExist:
            raise exceptions.LoyaltySellerDoesNotExist(seller_id=self.seller_id)

        # Check amount
        self.amount = request.POST.get('amount', request.GET.get('amount', kwargs.get('amount')))
        if not self.amount:
            raise exceptions.LoyaltyAmountIsRequired()
        else:
            self.amount = float(str(self.amount).replace(',', '.'))

    def post(self, request, *args, **kwargs):
        instance = self.model.objects.create(
            program=self.program,
            seller=self.seller,
            scan=self.scan,
            card=self.card,
            amount=self.amount
        )
        return Response({
            'amount': serializers.AmountSerializer(instance).data,
            'card': CardSerializer(self.card).data
        })
