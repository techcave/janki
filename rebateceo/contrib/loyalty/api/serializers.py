# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework import serializers
from .. import models


class ProgramSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.Program
        fields = ['pk', 'title', 'description']


class AmountSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    points = serializers.SerializerMethodField()

    def get_points(self, instance):
        return instance.points

    class Meta:
        model = models.Amount
        fields = ['pk', 'scan', 'seller', 'card', 'amount', 'created_at', 'updated_at', 'points']


class AmountPostSerializer(serializers.Serializer):
    pk = serializers.ReadOnlyField()
    scan_id = serializers.CharField(max_length=255)
    seller_id = serializers.CharField(max_length=255)
    program_id = serializers.CharField(max_length=255)
    amount = serializers.CharField(max_length=255)

    class Meta:
        fields = ['pk', 'scan_id', 'seller_id', 'program_id', 'amount']