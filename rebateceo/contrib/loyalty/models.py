# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.db import models


class ProgramManager(models.Manager):
    pass


class Amount(models.Model):
    scan = models.ForeignKey('verifier.VerifierScan', verbose_name=u'Skan', related_name='amounts')
    program = models.ForeignKey('loyalty.Program', verbose_name=u'Program', related_name='amounts')
    seller = models.ForeignKey('shop.Seller', verbose_name=u'Sprzedawca', related_name='amounts')
    card = models.ForeignKey('card.Card', verbose_name=u'Karta', related_name='amounts')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True)

    def __unicode__(self):
        return "%s - %s" % (self.amount, self.points)

    @property
    def points(self):
        return int(int(self.amount) / 100)

    class Meta:
        verbose_name = u"Amount"
        verbose_name_plural = u"Amounts"
        ordering = ('pk', )


class Program(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = ProgramManager()

    class Meta:
        verbose_name = u"Program"
        verbose_name_plural = u"Programs"

    def __unicode__(self):
        return u"%s" % (unicode(self.title),)

