# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.views.generic import TemplateView
from django.db.models.fields import FieldDoesNotExist
from django.utils.translation import ugettext_lazy as _

from datetime import date, datetime
from rebateceo.contrib.profile.models import Session
from .models import Click, Display, Event, Printout


class TrackingSummary(TemplateView):
    template_name = 'tracking/summary.html'

    def get_context_data(self, **kwargs):
        context = super(TrackingSummary, self).get_context_data(**kwargs)

        today = date.today()
        summary_data = []

        for item in [Click, Display, Printout, Event]:

            objects = item.objects.all()
            object_data = {
                'name': _(item.__name__),
                'total': objects.count(),
                'today': '-',
                'last': '-'
            }

            try:
                item._meta.get_field('event')
                object_data['today'] = objects.filter(event__created_at__year=today.year,
                                                      event__created_at__month=today.month,
                                                      event__created_at__day=today.day).count()
                object_data['last'] = objects and objects.order_by('-event__created_at')[0].event.created_at or '-'

            except FieldDoesNotExist:
                object_data['today'] = objects.filter(created_at__year=today.year, created_at__month=today.month,
                                                      created_at__day=today.day).count()
                object_data['last'] = objects and objects.order_by('-created_at')[0].created_at or '-'

            summary_data += [object_data]

        session_objects = Session.objects.all()

        session_total = {
            'stand': _("All sessions"),
            'total': session_objects.count(),
            'today': session_objects.filter(updated_at__year=today.year, updated_at__month=today.month,
                                            updated_at__day=today.day).count(),
            'last': session_objects and session_objects.order_by('-updated_at')[0].updated_at or '-'
        }

        stand_data = []

        stands = session_objects.filter(stand__isnull=False).values('stand__ip_address').distinct()
        for item in stands:
            stand = session_objects.filter(stand__ip_address=item['stand__ip_address'])
            stand_data += [{
                'stand': item['stand__ip_address'],
                'total': stand.count(),
                'today': stand.filter(updated_at__year=today.year, updated_at__month=today.month,
                                      updated_at__day=today.day).count(),
                'last': stand and stand.order_by('-updated_at')[0].updated_at or '-'
            }]

        mobile_data = []

        os_types = session_objects.filter(os_info__isnull=False).exclude(os_info__exact='').values(
            'os_info').distinct().order_by('os_info')
        for os_item in os_types:
            item = session_objects.filter(os_info=os_item['os_info'])
            app_versions = item.values('app_version').distinct().order_by('app_version')
            app_version_list = []
            for app_version_item in app_versions:
                item_app = item.filter(app_version=app_version_item['app_version'])
                app_version_list += [{
                    'app_version': _('App version: ') + app_version_item['app_version'],
                    'total': item_app.count(),
                    'today': item_app.filter(updated_at__year=today.year, updated_at__month=today.month,
                                             updated_at__day=today.day).count(),
                    'last': item_app and item_app.order_by('-updated_at')[0].updated_at or '-'
                }]

            mobile_data += [{
                'os_info': os_item['os_info'],
                'app_versions': app_version_list,
                'total': item.count(),
                'today': item.filter(updated_at__year=today.year, updated_at__month=today.month,
                                     updated_at__day=today.day).count(),
                'last': item and item.order_by('-updated_at')[0].updated_at or '-'
            }]

        context.update({
            'session_total': session_total,
            'summary_data': summary_data,
            'stand_data': stand_data,
            'mobile_data': mobile_data,
            'last_refresh': datetime.now().time()

        })
        return context
