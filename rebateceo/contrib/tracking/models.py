# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.db import models
from picklefield.fields import PickledObjectField
from rebateceo.contrib.shop.models import Shop as Tenant
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType


class Event(models.Model):
    session = models.ForeignKey('profile.Session')
    tenant = models.ForeignKey(Tenant, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    extra_data = PickledObjectField(blank=True, null=True)

    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = generic.GenericForeignKey()

    def __unicode__(self):
        return str(self.pk)


class Click(models.Model):
    event = models.ForeignKey(Event)
    screen_name = models.CharField(max_length=255)
    button_name = models.CharField(max_length=255)

    def __unicode__(self):
        return str(self.pk)


class Printout(models.Model):
    event = models.ForeignKey(Event)
    filename = models.CharField(max_length=255)

    def __unicode__(self):
        return str(self.pk)


class Display(models.Model):
    event = models.ForeignKey(Event)
    screen_name = models.CharField(max_length=255)
    duration = models.PositiveIntegerField()

    def __unicode__(self):
        return str(self.pk)