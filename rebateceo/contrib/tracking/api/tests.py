from django.contrib.auth.models import User
from rebateceo.contrib.profile.models import Profile, Session
from rest_framework.reverse import reverse

from rebateceo.contrib.profile.factories import SessionFactory
from ...api.tests import APITestBase


class TrackingTest(APITestBase):
    """
    bin/backend-test test rebateceo.contrib.tracking.api.tests.TrackingTest
    """

    def setUp(self):
        super(TrackingTest, self).setUp()
        self.session = SessionFactory()

    def test_create(self):
        data = {
            'event.session': self.session.pk,
            'screen_name': 'xxx',
            'button_name': 'yyy'
        }
        response = self.client.post(reverse('api:tracking:click-create'), data, **self.headers)
        self.assertEqual(response.status_code, 201)
