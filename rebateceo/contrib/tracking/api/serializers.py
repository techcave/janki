# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.forms.models import model_to_dict
from rest_framework import serializers
from .. import models


class EventSerializer(serializers.ModelSerializer):
    content_object = serializers.SerializerMethodField()
    extra_data = serializers.CharField(allow_null=True, required=False,
                                       help_text="Additional important data (JSON required)")

    class Meta:
        model = models.Event
        fields = ['pk', 'session', 'tenant', 'content_type', 'extra_data', 'object_id', 'content_object', 'created_at']

    def get_content_object(self, instance):
        return model_to_dict(instance.content_object,
                             exclude=['image', 'result_img', 'success_banner', 'logo', 'front', 'icon', 'mobile_image',
                                      'photo', 'stand_icon', 'mobile_icon']) \
            if instance.content_object else None


class ClickSerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        model = models.Click
        fields = ['pk', 'event', 'screen_name', 'button_name']

    def create(self, validated_data):
        return models.Click.objects.create(
            screen_name=validated_data.get('screen_name'),
            button_name=validated_data.get('button_name'),
            event=models.Event.objects.create(**validated_data.get('event'))
        )


class PrintoutSerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        model = models.Printout
        fields = ['pk', 'event', 'filename']

    def create(self, validated_data):
        return models.Printout.objects.create(
            filename=validated_data.get('filename'),
            event=models.Event.objects.create(**validated_data.get('event'))
        )


class DisplaySerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        model = models.Display
        fields = ['pk', 'event', 'screen_name', 'duration']

    def create(self, validated_data):
        return models.Display.objects.create(
            screen_name=validated_data.get('screen_name'),
            duration=validated_data.get('duration'),
            event=models.Event.objects.create(**validated_data.get('event'))
        )
