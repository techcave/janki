# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib import admin
from . import models


class EventAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'session', 'tenant', 'created_at', 'content_type', 'object_id', 'content_object',
                    'extra_data',)


class ClickAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'event', 'screen_name', 'button_name',)


class DisplayAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'event', 'screen_name', 'duration',)


class PrintoutAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'event', 'filename')


admin.site.register(models.Event, EventAdmin)
admin.site.register(models.Click, ClickAdmin)
admin.site.register(models.Display, DisplayAdmin)
admin.site.register(models.Printout, PrintoutAdmin)