from django.conf.urls import patterns, url
from django.views.decorators.cache import cache_page
from . import views


urlpatterns = patterns('',
    url(r'^', cache_page(60*60)(views.TrackingSummary.as_view()), name='summary')
)
