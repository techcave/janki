from ...api.tests import APITestBase
from rest_framework.reverse import reverse
from rest_framework import status


class AuthorizeTest(APITestBase):
    """
    bin/backend-test test rebateceo.contrib.authorize.api.tests.AuthorizeTest
    """

    def setUp(self):
        super(AuthorizeTest, self).setUp()

    def test_register_by_mobile(self):
        data = {
            'mobile_uuid': 12345678
        }

        # 201
        response = self.client.post(reverse('auth:register-by-mobile'), data, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # 200
        response = self.client.post(reverse('auth:register-by-mobile'), data, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
