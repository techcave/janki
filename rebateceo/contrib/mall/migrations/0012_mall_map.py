# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0006_map_is_current'),
        ('mall', '0011_auto_20150322_2223'),
    ]

    operations = [
        migrations.AddField(
            model_name='mall',
            name='map',
            field=models.ForeignKey(blank=True, to='map.Map', null=True),
            preserve_default=True,
        ),
    ]
