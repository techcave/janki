# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0017_auto_20150519_0849'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghourlabel',
            options={'ordering': ['ordering'], 'verbose_name': 'Opening hour label', 'verbose_name_plural': 'Opening hour labels'},
        ),
    ]
