# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0005_auto_20150312_2158'),
    ]

    operations = [
        migrations.AddField(
            model_name='openinghour',
            name='notes',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='openinghour',
            name='from_time',
            field=models.CharField(max_length=64),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='openinghour',
            name='to_time',
            field=models.CharField(max_length=64),
            preserve_default=True,
        ),
    ]
