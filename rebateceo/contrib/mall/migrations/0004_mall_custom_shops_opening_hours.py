# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_auto_20150309_1616'),
        ('mall', '0003_auto_20150312_2146'),
    ]

    operations = [
        migrations.AddField(
            model_name='mall',
            name='custom_shops_opening_hours',
            field=models.ManyToManyField(to='shop.Shop', null=True, blank=True),
            preserve_default=True,
        ),
    ]
