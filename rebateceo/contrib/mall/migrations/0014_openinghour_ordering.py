# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0013_mall_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='openinghour',
            name='ordering',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
