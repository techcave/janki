# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpeningHour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(help_text='Day or days, e.g: pn-wt', max_length=64)),
                ('from_time', models.TimeField()),
                ('to_time', models.TimeField()),
                ('mall', models.ForeignKey(to='mall.Mall')),
            ],
            options={
                'verbose_name': 'Opening hour',
                'verbose_name_plural': 'Opening hours',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='openinghour',
            unique_together=set([('mall', 'key', 'from_time', 'to_time')]),
        ),
    ]
