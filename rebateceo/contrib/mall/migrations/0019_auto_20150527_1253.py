# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0018_auto_20150519_0857'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mall',
            options={'verbose_name': 'Mall', 'verbose_name_plural': 'Malls'},
        ),
    ]
