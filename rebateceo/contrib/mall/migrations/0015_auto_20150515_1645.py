# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0014_openinghour_ordering'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'ordering': ['ordering'], 'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
    ]
