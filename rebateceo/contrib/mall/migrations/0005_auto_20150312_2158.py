# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0004_mall_custom_shops_opening_hours'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mall',
            old_name='custom_shops_opening_hours',
            new_name='shops_opening_hours',
        ),
    ]
