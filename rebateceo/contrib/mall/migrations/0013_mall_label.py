# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0012_mall_map'),
    ]

    operations = [
        migrations.AddField(
            model_name='mall',
            name='label',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
