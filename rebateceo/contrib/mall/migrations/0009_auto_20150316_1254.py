# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0008_auto_20150316_1226'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpeningHourLabel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='Day or days, e.g: pn-wt', unique=True, max_length=64)),
            ],
            options={
                'verbose_name': 'Opening hour label',
                'verbose_name_plural': 'Opening hour labels',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='openinghour',
            name='label',
            field=models.ForeignKey(blank=True, to='mall.OpeningHourLabel', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='openinghour',
            unique_together=set([('mall', 'label', 'value')]),
        ),
        migrations.RemoveField(
            model_name='openinghour',
            name='key',
        ),
    ]
