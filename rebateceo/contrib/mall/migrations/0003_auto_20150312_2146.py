# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.mall.models


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0002_auto_20150309_1251'),
    ]

    operations = [
        migrations.AddField(
            model_name='mall',
            name='policy_file',
            field=models.FileField(max_length=255, null=True, upload_to=rebateceo.contrib.mall.models.upload_to, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='rules_file',
            field=models.FileField(max_length=255, null=True, upload_to=rebateceo.contrib.mall.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
