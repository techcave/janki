# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.mall.models


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0010_remove_openinghour_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='mall',
            name='description',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='email',
            field=models.EmailField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='front',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.mall.models.upload_to, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='infopoint',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='logo',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.mall.models.upload_to, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='phone',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mall',
            name='www',
            field=models.URLField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
