# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0015_auto_20150515_1645'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'ordering': ['label__ordering'], 'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
        migrations.AlterModelOptions(
            name='openinghourlabel',
            options={'ordering': ['ordering'], 'verbose_name': 'Opening hour label', 'verbose_name_plural': 'Opening hour labels'},
        ),
        migrations.RemoveField(
            model_name='openinghour',
            name='ordering',
        ),
        migrations.AddField(
            model_name='openinghourlabel',
            name='ordering',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
