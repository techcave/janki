from django.conf import settings
from rest_framework.reverse import reverse
from ..factories import MallFactory
from ...api.tests import APITestBase

MALL_ID = getattr(settings, 'MALL_ID', -1)


class MallTest(APITestBase):

    def test_list(self):
        limit = 3
        items = [MallFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:mall:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Mall list test failed")

    def test_current(self):
        mall = MallFactory(pk=MALL_ID)
        response = self.client.get(reverse('api:mall:current'), {}, **self.headers)
        self.assertEqual(response.data['is_current'], True, "Mall current test failed")

    def test_current_label(self):
        mall = MallFactory(pk=MALL_ID)
        response = self.client.get(reverse('api:mall:current'), {}, **self.headers)
        self.assertIsNotNone(response.data['label'], "Mall current test failed")