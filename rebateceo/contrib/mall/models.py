# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
import reversion
from django.conf import settings
from collections import OrderedDict
from django.utils.translation import gettext as _
from django.db import models


MALL_ID = getattr(settings, 'MALL_ID', -1)


def upload_to(instance, filename):
    return '/'.join(['mall', str(filename)])


class MallManager(models.Manager):
    def current(self):
        current = None
        for obj in self.all():
            if obj.is_current:
                current = obj
                break
        return current


class Mall(models.Model):
    name = models.TextField()
    label = models.TextField(blank=True, null=True)
    rules_file = models.FileField(max_length=255, upload_to=upload_to, blank=True, null=True)
    policy_file = models.FileField(max_length=255, upload_to=upload_to, blank=True, null=True)
    description = models.TextField(blank=True, null=True,)
    infopoint = models.TextField(blank=True, null=True,)
    phone = models.CharField(max_length=255, blank=True, null=True)
    www = models.URLField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=True, null=True)
    logo = models.ImageField(upload_to=upload_to, blank=True, null=True)
    front = models.ImageField(upload_to=upload_to, blank=True, null=True)
    shops_opening_hours = models.ManyToManyField('shop.Shop', blank=True, null=True)
    map = models.ForeignKey('map.Map', blank=True, null=True)
    objects = MallManager()

    class Meta:
        verbose_name = _('Mall')
        verbose_name_plural = _('Malls')

    def __unicode__(self):
        return u"%s" % self.name

    @property
    def is_current(self):
        return self.pk == MALL_ID

    @property
    def opening_hours(self):
        from rebateceo.contrib.shop.api.serializers import ShopOpeingHoursSerializer
        shops = ShopOpeingHoursSerializer(self.shops_opening_hours.all(), many=True).data

        mall = OrderedDict([(x.label.name, x.value) for x in  self.openinghour_set.all().order_by('label__ordering')])

        labels = OpeningHourLabel.objects.all().values_list('name', flat=True)

        return {
            'labels': labels,
            'mall': mall,
            'shops': shops
        }


class OpeningHourLabel(models.Model):
    name = models.CharField(max_length=64, help_text='Day or days, e.g: pn-wt', unique=True)
    ordering = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        verbose_name = _('Opening hour label')
        verbose_name_plural = _('Opening hour labels')
        ordering = ['ordering']

    def __unicode__(self):
        return u"%s" % self.name


class OpeningHour(models.Model):
    mall = models.ForeignKey(Mall)
    label = models.ForeignKey(OpeningHourLabel, blank=True, null=True)
    value = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        verbose_name = _('Opening hour')
        verbose_name_plural = _('Opening hours')
        unique_together = ('mall', 'label', 'value')
        ordering = ['label__ordering']

    def __unicode__(self):
        return u"{0}: {1}".format(self.label, self.value)


reversion.register(Mall)
reversion.register(OpeningHour)