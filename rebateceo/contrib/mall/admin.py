# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.admin import ModelAdmin, site
from django.contrib.admin.options import TabularInline
from .models import Mall, OpeningHourLabel, OpeningHour


class OpeningHourInline(TabularInline):
    model = OpeningHour
    extra = 7


class MallAdmin(ModelAdmin):
    inlines = [OpeningHourInline]
    list_display = ('name', 'label', 'id', 'rules_file', 'policy_file', 'description', 'infopoint', 'phone', 'www', 'email',
                    'logo', 'front', 'get_opening_hours', 'map')
    filter_horizontal = ('shops_opening_hours',)

    def get_opening_hours(self, instance):
        mall = instance.opening_hours.get('mall')
        return u"</br>".join([u'{0}: {1}'.format(label, val) for label, val in mall.items()])
    get_opening_hours.allow_tags = True


class OpeningHourLabelAdmin(ModelAdmin):
    list_display = ('name', 'ordering')

site.register(Mall, MallAdmin)
site.register(OpeningHourLabel, OpeningHourLabelAdmin)