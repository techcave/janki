from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from . import models
from . import factories


class ShopAdminTest(TestCase):

    def setUp(self):
        username = 'test_user'
        pwd = 'secret'

        self.u = User.objects.create_user(username, '', pwd)
        self.u.is_staff = True
        self.u.is_superuser = True
        self.u.save()

        self.assertTrue(self.client.login(username=username, password=pwd),
            "Logging in user %s, pwd %s failed." % (username, pwd))

        models.Shop.objects.all().delete()

    def tearDown(self):
        self.client.logout()
        self.u.delete()

    def test_item_ok(self):
        self.assertEquals(models.Shop.objects.count(), 0)
        post_data = {
            'title': u'Test OK',

            'openinghour_set-INITIAL_FORMS': u'0',
            'openinghour_set-TOTAL_FORMS': u'7',
            'openinghour_set-MIN_NUM_FORMS': u'0',
            'openinghour_set-MAX_NUM_FORMS': u'1000',

            'link_set-INITIAL_FORMS': u'0',
            'link_set-TOTAL_FORMS': u'7',
            'link_set-MIN_NUM_FORMS': u'0',
            'link_set-MAX_NUM_FORMS': u'1000',

            'video_set-INITIAL_FORMS': u'0',
            'video_set-TOTAL_FORMS': u'7',
            'video_set-MIN_NUM_FORMS': u'0',
            'video_set-MAX_NUM_FORMS': u'1000',
        }

        response = self.client.post(reverse('admin:shop_shop_add'), post_data)
        self.assertEqual(response.status_code, 200)

    def test_admin_list(self):
        item_1 = factories.ShopFactory()
        self.assertEquals(models.Shop.objects.count(), 1)

        response = self.client.get(reverse('admin:shop_shop_changelist'))
        self.assertEqual(response.status_code, 200)