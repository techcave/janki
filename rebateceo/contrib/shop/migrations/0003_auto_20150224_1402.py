# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20150223_0049'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='main_category',
            field=models.ForeignKey(related_name='shops', blank=True, to='shop.Category', null=True),
            preserve_default=True,
        ),
        migrations.AlterModelTable(
            name='seller',
            table=None,
        ),
    ]
