# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0021_auto_20150326_1031'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'ordering': ['ordering'], 'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
        migrations.AddField(
            model_name='openinghour',
            name='ordering',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
