# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0016_auto_20150316_1254'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='openinghour',
            name='notes',
        ),
    ]
