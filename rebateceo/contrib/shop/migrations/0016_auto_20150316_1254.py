# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mall', '0009_auto_20150316_1254'),
        ('shop', '0015_auto_20150316_1226'),
    ]

    operations = [
        migrations.AddField(
            model_name='openinghour',
            name='label',
            field=models.ForeignKey(related_name='shop_opeinghourlabel', blank=True, to='mall.OpeningHourLabel', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='openinghour',
            unique_together=set([('shop', 'label', 'value')]),
        ),
        migrations.RemoveField(
            model_name='openinghour',
            name='key',
        ),
    ]
