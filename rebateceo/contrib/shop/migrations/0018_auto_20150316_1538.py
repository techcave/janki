# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0017_remove_openinghour_notes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='type',
            field=models.CharField(max_length=64, choices=[('www', 'www'), ('facebook', 'facebook'), ('twitter', 'twitter'), ('youtube', 'youtube'), ('vimeo', 'vimeo'), ('pinitup', 'pinitup'), ('gplus', 'gplus'), ('instagram', 'instagram')]),
            preserve_default=True,
        ),
    ]
