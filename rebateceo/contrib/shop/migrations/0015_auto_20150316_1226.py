# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_auto_20150316_1218'),
    ]

    operations = [
        migrations.RenameField(
            model_name='openinghour',
            old_name='from_time',
            new_name='value',
        ),
        migrations.AlterUniqueTogether(
            name='openinghour',
            unique_together=set([('shop', 'key', 'value')]),
        ),
        migrations.RemoveField(
            model_name='openinghour',
            name='to_time',
        ),
    ]
