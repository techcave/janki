# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_auto_20150309_1616'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
        migrations.AddField(
            model_name='openinghour',
            name='notes',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='openinghour',
            name='from_time',
            field=models.CharField(max_length=64),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='openinghour',
            name='to_time',
            field=models.CharField(max_length=64),
            preserve_default=True,
        ),
    ]
