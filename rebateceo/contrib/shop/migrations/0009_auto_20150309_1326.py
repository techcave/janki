# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0008_auto_20150309_1326'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpeningHour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(help_text='Day or days, e.g: pn-wt', max_length=64)),
                ('from_time', models.TimeField()),
                ('to_time', models.TimeField()),
                ('shop', models.ForeignKey(to='shop.Shop')),
            ],
            options={
                'verbose_name': 'Shop opening hour',
                'verbose_name_plural': 'Shop opening hours',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='shopopeninghour',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='shopopeninghour',
            name='shop',
        ),
        migrations.DeleteModel(
            name='ShopOpeningHour',
        ),
        migrations.AlterUniqueTogether(
            name='openinghour',
            unique_together=set([('shop', 'key', 'from_time', 'to_time')]),
        ),
    ]
