# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.shop.models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20150302_0024'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='logo',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='mobile_icon',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='stand_icon',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
