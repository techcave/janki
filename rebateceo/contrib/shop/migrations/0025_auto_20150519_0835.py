# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0024_auto_20150518_1650'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'ordering': ['label__ordering'], 'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
        migrations.RemoveField(
            model_name='openinghour',
            name='ordering',
        ),
    ]
