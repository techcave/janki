# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0032_auto_20170330_1602'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_code', models.CharField(max_length=15, verbose_name='Language', db_index=True)),
                ('name', models.TextField()),
                ('master', models.ForeignKey(related_name='translations', editable=False, to='shop.Category', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'shop_category_translation',
                'db_tablespace': '',
                'default_permissions': (),
                'verbose_name': 'Category Translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='categorytranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.RemoveField(
            model_name='category',
            name='name',
        ),
    ]
