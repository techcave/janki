# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0033_auto_20170403_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='categories',
            field=models.ManyToManyField(to='shop.Category', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='tags',
            field=models.ManyToManyField(to='tags.Tag', blank=True),
            preserve_default=True,
        ),
    ]
