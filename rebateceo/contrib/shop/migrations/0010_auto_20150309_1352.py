# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0009_auto_20150309_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='main_category',
            field=models.ForeignKey(related_name='shops', to='shop.Category', null=True),
            preserve_default=True,
        ),
    ]
