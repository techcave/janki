# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0020_auto_20150324_1633'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['-ordering'], 'verbose_name': 'Category', 'verbose_name_plural': 'Kategorie'},
        ),
    ]
