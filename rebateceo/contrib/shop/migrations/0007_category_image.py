# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.shop.models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0006_remove_category_logo'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
