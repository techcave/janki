# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0031_auto_20170330_1300'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShopTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_code', models.CharField(max_length=15, verbose_name='Language', db_index=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(null=True, blank=True)),
                ('master', models.ForeignKey(related_name='translations', editable=False, to='shop.Shop', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'shop_shop_translation',
                'db_tablespace': '',
                'default_permissions': (),
                'verbose_name': 'Sklep Translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='shoptranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
