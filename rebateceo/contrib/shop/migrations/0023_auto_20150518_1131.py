# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0022_auto_20150515_1645'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'ordering': ['-ordering'], 'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
    ]
