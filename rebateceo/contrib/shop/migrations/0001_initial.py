# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.shop.models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '__first__'),
        ('map', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(db_index=True)),
                ('visible', models.NullBooleanField()),
                ('color', models.CharField(help_text='Fill six digits without #. E.g: FFEEAA', max_length=6, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(null=True, verbose_name='Deleted at', blank=True)),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Kategorie',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=150, verbose_name='Imi\u0119')),
                ('last_name', models.CharField(max_length=150, verbose_name='Nazwisko')),
                ('email', models.EmailField(max_length=75, null=True, verbose_name='Email', blank=True)),
                ('mobile_number', models.CharField(max_length=20, null=True, blank=True)),
                ('uuid', django_extensions.db.fields.UUIDField(null=True, editable=False, blank=True, unique=True, verbose_name='Kod UUID', db_index=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(help_text='Je\u015bli zaznaczysz kupon zostanie oznaczony jako usuni\u0119ty', null=True, verbose_name='Podaj dat\u0119 usuni\u0119cia rekordu', blank=True)),
            ],
            options={
                'db_table': 'shop_seller',
                'verbose_name': 'Seller',
                'verbose_name_plural': 'Sprzedawcy',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('external_id', models.IntegerField(null=True, blank=True)),
                ('name', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('description', models.TextField(db_index=True, null=True, blank=True)),
                ('locale_id', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('phone', models.CharField(max_length=255, null=True, blank=True)),
                ('www', models.URLField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=255, null=True, blank=True)),
                ('level', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('local_nr', models.CharField(db_index=True, max_length=255, null=True, blank=True)),
                ('logo', models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True)),
                ('front', models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True)),
                ('has_icon', models.BooleanField(default=False)),
                ('archived', models.BooleanField(default=False)),
                ('lottery', models.BooleanField(default=False)),
                ('deleted', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(null=True, verbose_name='Deleted at', blank=True)),
                ('categories', models.ManyToManyField(to='shop.Category', null=True, blank=True)),
                ('point', models.ForeignKey(blank=True, to='map.Point', null=True)),
                ('tags', models.ManyToManyField(to='tags.Tag', null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Sklep',
                'verbose_name_plural': 'Sklepy',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='seller',
            name='shop',
            field=models.ForeignKey(related_name='sellers', blank=True, to='shop.Shop', null=True),
            preserve_default=True,
        ),
    ]
