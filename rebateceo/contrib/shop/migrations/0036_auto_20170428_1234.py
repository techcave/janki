# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0035_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Stworzony'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='deleted_at',
            field=models.DateTimeField(null=True, verbose_name=b'Usuni\xc4\x99ty', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name=b'Zaktualizowany'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Stworzony'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='deleted_at',
            field=models.DateTimeField(null=True, verbose_name=b'Usuni\xc4\x99ty', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name=b'Zaktualizowany'),
            preserve_default=True,
        ),
    ]
