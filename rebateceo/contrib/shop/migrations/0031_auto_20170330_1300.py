# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0030_auto_20150713_1435'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='shop',
            options={'ordering': ('translations__name',), 'verbose_name': 'Sklep', 'verbose_name_plural': 'Sklepy'},
        ),
        migrations.RemoveField(
            model_name='shop',
            name='description',
        ),
        migrations.RemoveField(
            model_name='shop',
            name='name',
        ),
    ]
