# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.shop.models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0018_auto_20150316_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='mobile_image',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.shop.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
