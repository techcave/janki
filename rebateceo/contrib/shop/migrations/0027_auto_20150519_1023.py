# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0026_auto_20150519_0848'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='openinghour',
            options={'ordering': ['label__ordering'], 'verbose_name': 'Opening hour', 'verbose_name_plural': 'Opening hours'},
        ),
    ]
