# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0010_auto_20150309_1352'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=64, choices=[('www', 'www'), ('facebook', 'facebook'), ('twitter', 'twitter'), ('youtube', 'youtube'), ('vimeo', 'vimeo'), ('pinitup', 'pinitup'), ('gplus', 'gplus')])),
                ('url', models.CharField(max_length=255)),
                ('shop', models.ForeignKey(to='shop.Shop')),
            ],
            options={
                'verbose_name': 'Shop link',
                'verbose_name_plural': 'Shop links',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='link',
            unique_together=set([('shop', 'url')]),
        ),
    ]
