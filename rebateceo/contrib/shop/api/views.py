# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf import settings
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from ..models import Shop, Category
from .serializers import (ShopSerializer, ShopForMobileSerializer, CategorySerializer, CategoryWithShopsFKSerializer,
                          CategoryWithShopsManySerializer, ShopSimpleSerializer, ShopForSelfkioskSerializer,
                          ShopForVerifierSerializer, ShopPointUpdateSerializer)

SHOP_MANAGER_EXCLUDE_ITEMS_FOR_SELFKIOSK = getattr(settings, 'SHOP_MANAGER_EXCLUDE_ITEMS_FOR_SELFKIOSK', [])


#
# Mixins
#
class AuthMixin(object):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)


class SortedListView(ListAPIView):
    def sort_queryset(self, queryset):
        order_by = self.request.GET.get('order_by')
        queryset = queryset.order_by(order_by) if order_by else queryset
        return queryset

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)

    def filter_queryset(self, queryset):
        return self.sort_queryset(queryset)


class CategoryListView(AuthMixin, SortedListView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ShopDetailView(AuthMixin, RetrieveAPIView):
    serializer_class = ShopSerializer
    model = Shop


class ShopForMobileDetailView(ShopDetailView):
    serializer_class = ShopForMobileSerializer


class ShopListView(AuthMixin, SortedListView):
    serializer_class = ShopSerializer
    queryset = Shop.objects.not_archived().distinct()

    def filter_queryset(self, queryset):
        query = self.request.GET.get('query')
        print(queryset)
        if query:
            queryset = queryset. \
                filter(name__icontains=query)
        return self.sort_queryset(queryset)


class ShopForMobileListView(ShopListView):
    serializer_class = ShopForMobileSerializer


class ShopForMobileByCategoryListView(ShopForMobileListView):
    def filter_queryset(self, queryset):
        return queryset.filter(categories__in=self.kwargs.get('category_id'))


class ShopSimpleListView(AuthMixin, SortedListView):
    serializer_class = ShopSimpleSerializer
    queryset = Shop.objects.select_related('main_category',
                                           'point',
                                           'point__type').prefetch_related('tags',
                                                                           'categories',
                                                                           'translations',
                                                                           'point__edges_a').exclude(archived=True)


class ShopForSelfKioskListView(ShopListView):
    serializer_class = ShopForSelfkioskSerializer

    def get_queryset(self):
        return self.model.objects.active().distinct('name')

    def filter_queryset(self, queryset):
        # https://pm.sizeof.pl/issues/4234
        return queryset.exclude(name__in=SHOP_MANAGER_EXCLUDE_ITEMS_FOR_SELFKIOSK)


class ShopForVerifierListView(ShopListView):
    serializer_class = ShopForVerifierSerializer


class CategoryListWithShopsFKView(CategoryListView):
    serializer_class = CategoryWithShopsFKSerializer


class CategoryListWithShopsManyView(CategoryListView):
    serializer_class = CategoryWithShopsManySerializer


class ShopPointUpdateView(AuthMixin, UpdateAPIView):
    serializer_class = ShopPointUpdateSerializer
    model = Shop
