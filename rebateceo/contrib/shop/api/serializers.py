# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf import settings
from django.contrib.sites.models import Site
from rest_framework import serializers
# from rebateceo.contrib.trans.models import Translation
from rebateceo.contrib.map.models import Point
from rebateceo.contrib.map.api.serializers import PointSerializer

from parler_rest.serializers import TranslatedFieldsField

from ..models import Shop, Category, Seller, Link, Video



class BaseSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    trans = serializers.SerializerMethodField()

    # def get_trans(self, instance):
    #     trans = {
    #         "name_en": None,
    #         "name_pl": None,
    #         "description_en": None,
    #         "description_pl": None
    #     }
    #     for x in Translation.objects.for_object(instance):
    #         trans.update({"%s_%s" % (x.field_name, x.lang): x.value})
    #     return trans


class CategorySerializer(BaseSerializer):
    translations = TranslatedFieldsField(shared_model=Category)

    class Meta:
        model = Category
        fields = ['id', 'translations', 'visible', 'color', 'ordering', 'stand_icon', 'mobile_icon', 'image',
                  'mobile_image']


class LinkSerializer(BaseSerializer):

    class Meta:
        model = Link
        fields = ['id', 'type', 'url']


class VideoSerializer(BaseSerializer):

    class Meta:
        model = Video
        fields = ['id', 'url']


class ShopSerializer(BaseSerializer):
    pk = serializers.ReadOnlyField()
    point = PointSerializer()
    logo_url = serializers.SerializerMethodField()
    front_url = serializers.SerializerMethodField()
    floor = serializers.SerializerMethodField()
    has_verifier = serializers.SerializerMethodField()
    link_set = LinkSerializer(many=True)
    video_set = VideoSerializer(many=True)
    has_verifier = serializers.SerializerMethodField()
    translations = TranslatedFieldsField(shared_model=Shop)

    class Meta:
        model = Shop
        fields = ['id', 'translations', 'point', 'locale_id', 'email', 'www', 'phone', 'logo_url', 'front_url', 'tags',
                  'has_icon', 'main_category', 'categories', 'contest_set', 'description', 'archived', 'created_at',
                  'updated_at', 'external_id', 'level', 'local_nr', 'lottery', 'has_verifier', 'floor', 'opening_hours',
                  'link_set', 'video_set']

    def _get_site_url(self, instance):
        try:
            site_url = self.context['request'].GET.get('site_url', None)
        except KeyError:
            site_url = None
        override = site_url not in [None, 'None']
        return site_url if override else "http://%s" % Site.objects.get_current()

    def get_logo_url(self, instance):
        return "%s%s" % (self._get_site_url(instance), instance.logo.url) if instance.logo else ''

    def get_front_url(self, instance):
        return "%s%s" % (self._get_site_url(instance), instance.front.url) if instance.front else ''

    def get_floor(self, instance):
        try:
            point = instance.point
        except Point.DoesNotExist:
            return ''
        else:
            return point.level_id if point else ''

    def get_has_verifier(self, instance):
        return instance.has_verifier


class ShopOpeingHoursSerializer(ShopSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Shop
        fields = ['id', 'name', 'opening_hours',]

class CategoryWithShopsFKSerializer(BaseSerializer):
    shops = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ['id', 'name', 'visible', 'shops']

    def get_shops(self, instance):
        return ShopSerializer(instance.shop_category.not_archived(), many=True).data


class CategoryWithShopsManySerializer(BaseSerializer):
    shops = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ['id', 'name', 'visible', 'shops']

    def get_shops(self, instance):
        return ShopSerializer(instance.shop_set.not_archived(), many=True).data


class ShopForMobileSerializer(ShopSerializer):
    #categories = CategorySerializer(many=True)

    class Meta:
        model = Shop
        fields = ['id', 'name', 'floor', 'email', 'www', 'phone', 'logo_url', 'categories', 'description', 'opening_hours'] #'trans']


class ShopForVerifierSerializer(ShopSerializer):

    class Meta:
        model = Shop
        fields = ['id', 'name', 'floor', 'email', 'www', 'phone', 'logo_url', 'has_verifier']


class ShopSimpleSerializer(ShopSerializer):
    pk = serializers.ReadOnlyField()
    point = PointSerializer()
    logo_url = serializers.SerializerMethodField()
    front_url = serializers.SerializerMethodField()
    floor = serializers.SerializerMethodField()
    translations = TranslatedFieldsField(shared_model=Shop)

    class Meta:
        model = Shop
        fields = ['id', 'translations', 'point', 'locale_id', 'email', 'www', 'phone', 'logo_url', 'front_url', 'tags',
                  'has_icon', 'main_category', 'categories', 'level', 'local_nr', 'lottery', 'floor']

    def _get_site_url(self, instance):
        try:
            site_url = self.context['request'].GET.get('site_url', None)
        except KeyError:
            site_url = None
        override = site_url not in [None, 'None']
        return site_url if override else "http://%s" % Site.objects.get_current()

    def get_logo_url(self, instance):
        return "%s%s" % (self._get_site_url(instance), instance.logo.url) if instance.logo else ''

    def get_front_url(self, instance):
        return "%s%s" % (self._get_site_url(instance), instance.front.url) if instance.front else ''

    def get_floor(self, instance):
        try:
            point = instance.point
        except Point.DoesNotExist:
            return ''
        else:
            return point.level_id if point else ''


class ShopForSelfkioskSerializer(ShopSerializer):

    class Meta:
        model = Shop
        fields = ['id', 'name', 'has_verifier']


class ShopPointUpdateSerializer(serializers.ModelSerializer):
    """Designed for map:editor"""
    pk = serializers.ReadOnlyField()
    point_id = serializers.IntegerField(allow_null=True)

    class Meta:
        model = Shop
        fields = ['pk', 'point_id']


class SellerSerializer(BaseSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Seller
        fields = ['pk', 'id', 'shop', 'first_name', 'last_name', 'email', 'mobile_number', 'uuid']