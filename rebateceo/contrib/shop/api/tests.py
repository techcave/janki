from rest_framework.reverse import reverse
from ..factories import ShopFactory, CategoryFactory
from ...api.tests import APITestBase
from .. import models


class ShopTest(APITestBase):
    def setUp(self):
        super(ShopTest, self).setUp()
        models.Category.objects.all().delete()
        models.Shop.objects.all().delete()

    def test_list(self):
        [ShopFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:shop:list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Shop list test failed")

    def test_list_simple(self):
        [ShopFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:shop:list-simple'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Shop list simple test failed")

    def test_list_mobile(self):
        [ShopFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:shop:list-mobile'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Shop list mobile test failed")


class CategoryTest(APITestBase):
    def test_list(self):
        [CategoryFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:shop:category-list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Category list test failed")