# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf.urls import patterns, url
from .views import (ShopDetailView, ShopListView, CategoryListView, ShopSimpleListView, ShopPointUpdateView,
                    ShopForMobileListView, ShopForMobileByCategoryListView, ShopForSelfKioskListView,
                    ShopForVerifierListView, CategoryListWithShopsFKView, CategoryListWithShopsManyView)


urlpatterns = patterns('',
    # Shop custom lists
    url(r'^list/simple/$', ShopListView.as_view(), name='list-simple'),
    url(r'^list/$', ShopSimpleListView.as_view(), name='list'),
    url(r'^list/mobile/$', ShopForMobileListView.as_view(), name='list-mobile'),
    url(r'^list/verifier/$', ShopForVerifierListView.as_view(), name='list-for-verifier'),
    url(r'^list/selfkiosk/$', ShopForSelfKioskListView.as_view(), name='list-for-selfkiosk'),
    url(r'^list/mobile/category/(?P<category_id>\d+)/$', ShopForMobileByCategoryListView.as_view(),
        name='list-by-category-for-mobile'),

    # Shop custom details
    url(r'^(?P<pk>\d+)/$', ShopDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/point/update/$', ShopPointUpdateView.as_view(), name='point-update'),
    url(r'^(?P<pk>\d+)/mobile/$', ShopForMobileListView.as_view(), name='detail-for-mobile'),

    # Categories
    url(r'^category/list/$', CategoryListView.as_view(), name='category-list'),
    url(r'^category/list/shops-fk/$', CategoryListWithShopsFKView.as_view(), name='list-with-shops-fk'),
    url(r'^category/list/shops-many/$', CategoryListWithShopsManyView.as_view(), name='list-with-shops-many'),
)