# -*- coding: utf-8 -*-
"""
@license: MIT License
"""
import csv
from django.core.management import BaseCommand
from rebateceo.contrib.shop.models import Shop


class Command(BaseCommand):
    """
        Imports translations of names and descriptions for shops
    """
    def handle(self, *args, **options):
        help = 'Imports translations for shop names and descriptions from file'

        # TODO: Delete all old shop translations
        # TODO: Reset sequence for shop_shop_translation_id_seq

        with open(args[0], 'rb') as csvfile:
            data = csv.reader(csvfile, delimiter=',')

            for counter, item in enumerate(data):
                if counter == 0:
                    continue

                shop_id = item[0]
                shop_name = item[1]
                shop_description_pl = item[2]

                try:
                    shop_description_en = item[4]
                except IndexError:
                    shop_description_en = '-'

                try:
                    shop = Shop.objects.get(id=shop_id)
                except:
                    shop = None

                if shop:
                    print('{} - {}'.format(shop_id, shop_name))

                    shop.set_current_language('pl')
                    shop.name = shop_name
                    shop.description = shop_description_pl

                    shop.set_current_language('en-us')
                    shop.name = shop_name
                    shop.description = shop_description_en

                    shop.save()
