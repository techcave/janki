# -*- coding: utf-8 -*-
"""
@author: Jakub Pelczar
@license: MIT License
"""
import csv
from django.core.management import BaseCommand
from rebateceo.contrib.shop.models import Shop
from rebateceo.contrib.tags.models import Tag


class Command(BaseCommand):
    """Imports tags and sets tags to shop
    """
    def handle(self, *args, **options):
        help = 'Imports tags for shops from file'
        with open(args[0], 'rb') as csvfile:
            data = csv.reader(csvfile, delimiter=',', quotechar='"')

            for row in data:
                try:
                    shop = Shop.objects.get(pk=int(row[0]))
                except Shop.DoesNotExist:
                    print "No such shop: " + row[1]
                    shop = None
                else:
                    for tag in row[2].split(";"):
                        if tag != "":
                            tag_object, created = Tag.objects.get_or_create(name=tag)
                            if created:
                                print "Tag: " + tag_object.name + " created!"
                            shop.tags.add(tag_object)

                    shop.save()
