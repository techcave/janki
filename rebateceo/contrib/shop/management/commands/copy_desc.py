from django.core.management.base import BaseCommand
from ... import models


class Command(BaseCommand):
    model = models.Shop

    def handle(self, *args, **options):
        shops = self.model.objects.all().distinct()
        for shop in shops:
            shop.set_current_language('pl')
            desc = shop.description
            shop.set_current_language('en')
            if not shop.description:
                shop.description = desc
                shop.save()
                print('description overwritten en in {}'.format(shop.id))
