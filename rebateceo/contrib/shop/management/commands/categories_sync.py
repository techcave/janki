# -*- coding: utf-8 -*-
# !/usr/bin/env python

import json
import os
import requests
import shutil
import sys
import urllib2
import requests

from PIL import Image
from urllib2 import HTTPError


from django.utils.html import strip_tags
from datetime import datetime
from django.core.management.base import BaseCommand
from django.conf import settings
from django.template.defaultfilters import slugify

from rebateceo.contrib.shop import models

from logbook import Logger, StreamHandler


try:
    _API_URL = settings.REBATECEO_CATEGORIES_SYNC_DATA_URL
except AttributeError:
    raise Warning('REBATECEO_CATEGORIES_SYNC_DATA_URL setting is required!')


class Command(BaseCommand):
    """
        http://jira.techcave.pl:8080/browse/FL-27
    """
    model = models.Category
    api_url = _API_URL
    fk_field = 'categories'
    image_path = os.path.join(settings.MEDIA_ROOT, 'categories')

    StreamHandler(sys.stdout).push_application()
    log = Logger('categories_sync')

    def handle(self, *args, **options):

        # Mark all as deleted
        self.model.objects.all().delete()

        # Data
        user_agent = {
            'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36'
        }
        response = requests.get(self.api_url, headers=user_agent).content
        data = json.loads(response)
        for counter, child in enumerate(data):
            child = child.get('industry')
            id = child.get('id')
            name = child.get('name')

            object, created = self.model.objects.get_or_create(id=id)

            object.name = name
            object.set_current_language(settings.PARLER_DEFAULT_LANGUAGE_CODE)

            object.save()

            self.log.info(u'[{}] Object: {} -> {}'.format(counter+1, unicode(slugify(object)), created))