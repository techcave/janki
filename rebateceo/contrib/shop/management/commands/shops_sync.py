# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import json
import os
import requests
import shutil
import sys
import urllib2
import requests

from PIL import Image
from urllib2 import HTTPError


from django.utils.html import strip_tags
from datetime import datetime
from django.core.management.base import BaseCommand
from django.utils.html import escape
from django.conf import settings
from django.template.defaultfilters import striptags
from django.template.defaultfilters import slugify

# from rebateceo.utils import getimg2
from rebateceo.contrib.shop import models

# from bs4 import BeautifulSoup as Soup
from logbook import Logger, StreamHandler


try:
    _API_URL = settings.REBATECEO_SHOPS_SYNC_DATA_URL
except AttributeError:
    raise Warning('REBATECEO_SHOPS_SYNC_DATA_URL setting is required!')


class Command(BaseCommand):
    """
        http://jira.techcave.pl:8080/browse/FL-27
    """
    model = models.Shop
    api_url = _API_URL
    fk_field = 'shops'
    image_path = os.path.join(settings.MEDIA_ROOT, 'shops')

    StreamHandler(sys.stdout).push_application()
    log = Logger('shops_sync')

    def handle(self, *args, **options):

        # Mark all as deleted
        self.model.objects.all().delete()

        # Data
        user_agent = {
            'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36'
        }
        response = requests.get(self.api_url, headers=user_agent).content
        data = json.loads(response)
        for counter, child in enumerate(data):
            id = child.get('id')
            title = child.get('fullname')
            content = child.get('description')
            url = child.get('website')
            phone = child.get('phone')
            local_id = child.get('number')
            logo = child.get('logo')
            email = child.get('email')
            category = models.Category.objects.get(
                id=child.get('industry').get('id'))

            # Create item
            object, created = self.model.objects.get_or_create(id=id)
            object.name = title
            object.description = content
            object.www = url
            object.phone = phone
            object.locale_id = local_id
            object.email = email
            object.logo = logo
            object.categories.add(category)
            object.set_current_language(settings.PARLER_DEFAULT_LANGUAGE_CODE)

            object.save()

            self.log.info(u'[{}] Object: {} -> {}'.format(counter+1, unicode(slugify(object)), created))