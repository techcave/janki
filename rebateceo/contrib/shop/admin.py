# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import reversion
from django.conf import settings
from django.contrib.admin import ModelAdmin, site
from django.contrib.admin.options import TabularInline
from django.contrib.sites.models import Site
from django.utils.translation import gettext as _
# from adminsortable.admin import SortableAdminMixin, nie dziala z dj>=1.7?
from .models import Shop, Category, Seller, OpeningHour, Link, Video
from parler.admin import TranslatableAdmin
from django.utils.translation import activate
from parler.cache import get_cached_translated_field


class CategoryAdmin(reversion.admin.VersionAdmin, TranslatableAdmin):
    list_display = ('pk', 'name', 'all_languages_column', 'color', 'ordering', 'get_color', 'image_preview',
                    'mobile_image_preview', 'stand_icon_preview', 'mobile_icon_preview')
    list_editable = ('color', 'ordering')
    search_fields = ('pk', 'color')

    def get_color(self, obj):
        return "<span style='background-color: #%s; width: 100px; height: 15px; display: block;'/>" % \
               obj.color if obj.color else ''
    get_color.allow_tags = True
    get_color.short_description = _('Color preview')

    def mobile_image_preview(self, instance):
        site_url = "http://%s" % Site.objects.get_current()
        return "<img src='%s%s' width='150'/>" % (site_url, instance.mobile_image.url) if instance.mobile_image else '-'
    mobile_image_preview.allow_tags = True

    def image_preview(self, instance):
        site_url = "http://%s" % Site.objects.get_current()
        return "<img src='%s%s' width='150'/>" % (site_url, instance.image.url) if instance.image else '-'
    image_preview.allow_tags = True

    def stand_icon_preview(self, instance):
        site_url = "http://%s" % Site.objects.get_current()
        return "<img src='%s%s' width='150'/>" % (site_url, instance.stand_icon.url) if instance.stand_icon else '-'
    stand_icon_preview.allow_tags = True

    def mobile_icon_preview(self, instance):
        site_url = "http://%s" % Site.objects.get_current()
        return "<img src='%s%s' width='150'/>" % (site_url, instance.mobile_icon.url) if instance.mobile_icon else '-'
    mobile_icon_preview.allow_tags = True


class OpeningHourInline(TabularInline):
    model = OpeningHour
    extra = 7


class LinkInline(TabularInline):
    model = Link
    extra = 5


class VideoInline(TabularInline):
    model = Video
    extra = 5


def copy_translations(modeladmin, request, queryset):
    for shop in queryset:
        activate('pl')
        name_pl = shop.name
        for language in settings.LANGUAGES:

            if language[0] != 'pl' and not get_cached_translated_field(shop, 'name', language[0]):
                activate(language[0])
                shop.name = name_pl;
                shop.save()
        activate('pl')

copy_translations.short_description = "Copy title to other languages"

class ShopAdmin(reversion.admin.VersionAdmin, TranslatableAdmin):
    actions = [copy_translations]
    list_max_show_all = 750
    inlines = [OpeningHourInline, LinkInline, VideoInline]

    list_display = ('pk', 'name', 'all_languages_column', 'main_category', 'get_categories', 'get_tags_exsist',
                    'logo_img', 'front_img', 'has_icon', 'archived', 'lottery', 'created_at', 'updated_at', 'point_id',
                    'locale_id')
    search_fields = ('translations__name', 'point__pk', 'main_category__translations__name', 'external_id',
                     'translations__description', 'locale_id', 'phone', 'www', 'email', 'level', 'local_nr')
    list_editable = ('has_icon', 'archived',)
    list_filter = ('categories', 'has_icon', 'archived')

    raw_id_fields = ('point', )

    def point_id(self, instance):
        return u"{}".format(instance.point.id if instance.point else '-')

    point_id.allow_tags = True

    def get_tags_exsist(self, instance):
        return u"+" if instance.tags.exists() else '-'

    get_tags_exsist.allow_tags = True

    def get_opening_hours(self, instance):
        return u"</br>".join([u'{0}: {1}'.format(key, val) for key, val in instance.opening_hours.items()]) \
            if instance.opening_hours else '-'
    get_opening_hours.allow_tags = True

    def get_links(self, instance):
        return u"</br>".join([u'{0}: {1}'.format(x.type, x.url) for x in instance.link_set.all()])
    get_links.allow_tags = True

    def get_video(self, instance):
        return u"</br>".join([u'{0}'.format(x.url) for x in instance.video_set.all()])
    get_video.allow_tags = True

    def get_categories(self, obj):
        return u", ".join([unicode(x) for x in obj.categories.all()])
    get_categories.short_description = "Categories n2m"

    def get_tags(self, obj):
        return u", ".join([unicode(x) for x in obj.tags.all()])
    get_tags.short_description = "Tags n2m"

    def logo_img(self, instance):
        site_url = "http://%s" % Site.objects.get_current()
        return "<img src='%s%s' width='150'/>" % (site_url, instance.logo.url) if instance.logo else '-'
    logo_img.allow_tags = True

    def front_img(self, instance):
        site_url = "http://%s" % Site.objects.get_current()
        return "<img src='%s%s' width='150'/>" % (site_url, instance.front.url) if instance.front else '-'
    front_img.allow_tags = True


class SellerAdmin(reversion.admin.VersionAdmin, ModelAdmin):
    list_display = ('pk', 'shop', 'first_name', 'last_name', 'email', 'mobile_number', 'uuid', 'qrcode')
    search_fields = ('pk', 'shop__translations__name', 'email', 'mobile_number', 'uuid')
    list_filter = ('shop', )

    def qrcode(self, obj):
        return '<a href="{src}" target="_blank"><img src="{src}" /></a><br><input value="{qrcode_str}"/>'.\
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)
    qrcode.allow_tags = True


site.register(Shop, ShopAdmin)
site.register(Category, CategoryAdmin)
site.register(Seller, SellerAdmin)