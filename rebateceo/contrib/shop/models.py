# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
import datetime
from collections import OrderedDict

import reversion
from django.utils.translation import gettext as _
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django_extensions.db.fields import UUIDField

from parler.managers import TranslationManager
from parler.models import TranslatableModel, TranslatedFields

from rebateceo.contrib.map.models import Point
from rebateceo.contrib.tags.models import Tag
from rebateceo.contrib.mall.models import OpeningHourLabel
from rebateceo.contrib.bookmark.handlers import library as bookmarks

from parler.cache import get_cached_translated_field
from parler.signals import post_translation_save


QRCODE_STR = getattr(settings, 'QRCODE_STR', "{type};{uuid}")
QRCODE_IMG = getattr(settings, 'QRCODE_IMG', "http://chart.apis.google.com/chart?cht=qr&chs={size}x{size}&chl={qrcode_str}")
SHOP_MANAGER_EXCLUDE_ITEMS = getattr(settings, 'SHOP_MANAGER_EXCLUDE_ITEMS', [])


def upload_to(instance, filename):
    return u'/'.join(['shop', str(filename)])


class CategoryManager(TranslationManager):
    def get_queryset(self):
        qs = super(CategoryManager, self).get_queryset()
        return qs.exclude(deleted_at__isnull=False)


class Category(TranslatableModel):
    translations = TranslatedFields(
        name=models.TextField(),
    )
    visible = models.NullBooleanField()
    color = models.CharField(max_length=6, blank=True, null=True, help_text='Fill six digits without #. E.g: FFEEAA')
    stand_icon = models.ImageField(upload_to=upload_to, blank=True, null=True)
    mobile_icon = models.ImageField(upload_to=upload_to, blank=True, null=True)
    image = models.ImageField(upload_to=upload_to, blank=True, null=True)
    mobile_image = models.ImageField(upload_to=upload_to, blank=True, null=True)

    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    deleted_at = models.DateTimeField(verbose_name=_('Deleted at'), blank=True, null=True)
    ordering = models.PositiveIntegerField(default=0, blank=False, null=False)
    objects = CategoryManager()

    def __unicode__(self):
        return u"%s" % self.safe_translation_getter('name')

    def delete(self, using=None):
        self.deleted_at = datetime.datetime.now()
        self.save()

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['-ordering', ]


class ShopManager(TranslationManager):
    def get_queryset(self):
        qs = super(ShopManager, self).get_queryset()
        return qs.exclude(deleted=True)

    def deleted(self, **kwargs):
        qs = super(ShopManager, self).get_queryset()
        return qs(deleted=True)\
            .filter(**kwargs)

    def not_deleted(self, **kwargs):
        qs = super(ShopManager, self).get_queryset()
        return qs.filter(deleted=False)\
            .filter(**kwargs)

    def archived(self, **kwargs):
        return self.filter(archived=True)\
            .filter(**kwargs)

    def not_archived(self, **kwargs):
        return self.filter(archived=False)\
            .filter(**kwargs)

    def active(self):
        excluded_names = SHOP_MANAGER_EXCLUDE_ITEMS
        qs = self.all()
        for excluded_name in excluded_names:
            qs = qs.exclude(name__icontains=excluded_name)
        return qs


class Shop(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=255),
        description=models.TextField(blank=True, null=True)
    )
    external_id = models.IntegerField(blank=True, null=True)
    point = models.ForeignKey(Point, blank=True, null=True, db_index=True)
    locale_id = models.CharField(max_length=255, blank=True, null=True, db_index=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    www = models.URLField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=True, null=True)
    level = models.CharField(max_length=255, blank=True, null=True, db_index=True)
    local_nr = models.CharField(max_length=255, blank=True, null=True, db_index=True)
    logo = models.ImageField(upload_to=upload_to, blank=True, null=True)
    front = models.ImageField(upload_to=upload_to, blank=True, null=True)
    has_icon = models.BooleanField(default=False)
    main_category = models.ForeignKey(Category, blank=False, null=True, related_name='shops')
    categories = models.ManyToManyField(Category, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    archived = models.BooleanField(default=False)
    lottery = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    deleted_at = models.DateTimeField(verbose_name=_('Deleted at'), blank=True, null=True)
    objects = ShopManager()

    class Meta:
        verbose_name = u"Sklep"
        verbose_name_plural = u"Sklepy"

    def __unicode__(self):
        return u"%s" % self.safe_translation_getter('name')

    def delete(self, using=None):
        self.deleted = True
        self.archived = True
        self.deleted_at = datetime.datetime.now()
        self.save()

    def get_receipts(self, **kwargs):
        return self.receipt.filter(**kwargs)

    def get_receipt_count(self):
        return self.get_receipts().count()

    def get_receipt_values(self):
        return sum([r.amount for r in self.get_receipts()])

    @property
    def opening_hours(self):
        items = OrderedDict([(x.label.name, x.value) for x in self.openinghour_set.all().order_by('label__ordering')])
        return items if items else None

    @property
    def has_verifier(self):
        return True if len(self.verifiers.all()) > 0 else False

def translation_copy(sender, instance, **kwargs):
    shop = instance.master
    if not get_cached_translated_field(shop, 'name', 'en'):
        shop.set_current_language('en')
        shop.name = get_cached_translated_field(shop, 'name', 'pl')
        shop.save()
    shop.set_current_language('pl')
post_translation_save.connect(translation_copy, sender=Shop)


class SellerManager(models.Manager):
    def get_queryset(self):
        qs = super(SellerManager, self).get_queryset()
        return qs.exclude(deleted_at__isnull=False)

    def assigned(self):
        qs = super(SellerManager, self).get_queryset()
        return qs.exclude(shop__isnull=True)


class Seller(models.Model):
    QRCODE_TYPE = 'seller'
    QRCODE_SIZE = 200

    shop = models.ForeignKey('shop.Shop', related_name='sellers', blank=True, null=True, db_index=True)
    first_name = models.CharField(max_length=150, verbose_name=u"Imię")
    last_name = models.CharField(max_length=150, verbose_name=u"Nazwisko")
    email = models.EmailField(verbose_name=u"Email", blank=True, null=True)
    mobile_number = models.CharField(max_length=20,  blank=True, null=True)
    uuid = UUIDField(verbose_name=u"Kod UUID", blank=True, null=True, unique=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(verbose_name=u'Podaj datę usunięcia rekordu',
                                      help_text='Jeśli zaznaczysz kupon zostanie oznaczony jako usunięty',
                                      blank=True, null=True)

    objects = SellerManager()

    @property
    def qrcode_str(self):
        return QRCODE_STR.format(type=self.QRCODE_TYPE, uuid=self.uuid)

    @property
    def qrcode_img(self):
        return QRCODE_IMG.format(size=200, qrcode_str=self.qrcode_str)

    def __unicode__(self):
        return u"%s %s" % (self.first_name, self.last_name)

    def delete(self, using=None):
        self.deleted_at = datetime.datetime.now()
        self.save()

    class Meta:
        verbose_name = _('Seller')
        verbose_name_plural = _('Sellers')


class OpeningHour(models.Model):
    shop = models.ForeignKey(Shop)
    label = models.ForeignKey(OpeningHourLabel, related_name='shop_opeinghourlabel', blank=True, null=True)
    value = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        verbose_name = _('Opening hour')
        verbose_name_plural = _('Opening hours')
        unique_together = ('shop', 'label', 'value')
        ordering = ['label__ordering']

    def __unicode__(self):
        return u"{0}: {1}".format(self.label, self.value)


class Link(models.Model):
    TYPES = (
        ('www', 'www'),
        ('facebook', 'facebook'),
        ('twitter', 'twitter'),
        ('youtube', 'youtube'),
        ('vimeo', 'vimeo'),
        ('pinitup', 'pinitup'),
        ('gplus', 'gplus'),
        ('instagram', 'instagram'),
    )
    shop = models.ForeignKey(Shop)
    type = models.CharField(max_length=64, choices=TYPES)
    url = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Shop link')
        verbose_name_plural = _('Shop links')
        unique_together = ('shop', 'url')

    def __unicode__(self):
        return u"{0}: {1}".format(self.type, self.url)


class Video(models.Model):
    shop = models.ForeignKey(Shop)
    url = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Shop video')
        verbose_name_plural = _('Shop videos')
        unique_together = ('shop', 'url')

    def __unicode__(self):
        return u"{0}: {1}".format(self.url)


reversion.register(Shop)
reversion.register(Category)
reversion.register(Seller)
reversion.register(OpeningHour)
reversion.register(Link)
reversion.register(Video)


bookmarks.register(Shop)
bookmarks.register(Category)
