# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse
from .models import Seller


class SellerMenuMixin(object):
    model = Seller

    def get_context_data(self, *args, **kwargs):
        context = super(SellerMenuMixin, self).get_context_data(*args, **kwargs)
        context['active'] = 'shop'
        return context

    def get_success_url(self):
        return reverse('shop:seller_list')


class SellerCreateView(SellerMenuMixin, CreateView):
    template_name = 'shop/seller_create.html'


class SellerUpdateView(SellerMenuMixin, UpdateView):
    template_name = 'shop/seller_update.html'


class SellerDeleteView(SellerMenuMixin, DeleteView):
    template_name = 'shop/seller_delete.html'


class SellerListView(SellerMenuMixin, ListView):
    template_name = 'shop/seller_list.html'
