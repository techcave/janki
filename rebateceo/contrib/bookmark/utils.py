# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""

from django.utils.functional import memoize
from django.contrib.contenttypes.models import ContentType

_get_content_type_for_model_cache = {}


def get_content_type_for_model(model):
    return ContentType.objects.get_for_model(model)


get_content_type_for_model = memoize(get_content_type_for_model,
                                     _get_content_type_for_model_cache, 1)


def get_templates(instance, key, name, base='bookmark'):
    """
    Return a list of template names based on given *instance* and
    bookmark *key*.

    For example, if *name* is 'form.html'::

        bookmark/[app_name]/[model_name]/[key]/form.html
        bookmark/[app_name]/[model_name]/form.html
        bookmark/[app_name]/[key]/form.html
        bookmark/[app_name]/form.html
        bookmark/[key]/form.html
        bookmark/form.html
    """
    app_label = instance._meta.app_label
    module_name = instance._meta.module_name
    return [
        '%s/%s/%s/%s/%s' % (base, app_label, module_name, key, name),
        '%s/%s/%s/%s' % (base, app_label, module_name, name),
        '%s/%s/%s/%s' % (base, app_label, key, name),
        '%s/%s/%s' % (base, app_label, name),
        '%s/%s/%s' % (base, key, name),
        '%s/%s' % (base, name),
    ]
