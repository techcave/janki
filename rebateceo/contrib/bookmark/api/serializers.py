# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework import serializers
from django.forms.models import model_to_dict
from django.contrib.contenttypes.models import ContentType
from ..models import Bookmark


class BookmarkCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bookmark


class BookmarkSimpleSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Bookmark
        fields = ['pk', 'content_type', 'object_id', 'key', 'user', 'created_at']


class BookmarkSerializer(BookmarkSimpleSerializer):
    content_object = serializers.SerializerMethodField()

    class Meta(BookmarkSimpleSerializer.Meta):
        fields = BookmarkSimpleSerializer.Meta.fields + ['content_object']

    def get_content_object(self, instance):
        try:
            content_object = instance.content_object
        except (AttributeError, ContentType.DoesNotExist) as e:
            return {'error': str(e)}
        else:
            return model_to_dict(content_object,
                                 exclude=['image', 'result_img', 'success_banner', 'logo', 'front', 'icon',
                                          'mobile_image',
                                          'photo', 'stand_icon', 'mobile_icon'])
