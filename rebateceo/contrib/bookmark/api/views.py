# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework.serializers import Serializer
from django.contrib.contenttypes.models import ContentType
from django.forms.models import model_to_dict
from rebateceo.contrib.bookmark.handlers import library as bookmarks
from rest_framework.generics import GenericAPIView, CreateAPIView, ListAPIView, DestroyAPIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .serializers import BookmarkCreateSerializer, BookmarkSerializer
from ..models import Bookmark


def get_registry():
    ctypes = []
    for model in bookmarks._registry:
        ctypes.append(model_to_dict(ContentType.objects.get_for_model(model)))
    return ctypes


class BookmarkListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = BookmarkSerializer
    queryset = Bookmark.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class BookmarkListForUserView(BookmarkListView):
    authentication_classes = (TokenAuthentication,) # !!

    def filter_queryset(self, queryset):
        return queryset.filter(user_id=self.kwargs.get('user_id'))


class BookmarkCreateView(CreateAPIView):
    authentication_classes = (TokenAuthentication,) # !!
    permission_classes = (IsAuthenticated,)
    serializer_class = BookmarkCreateSerializer


class BookmarkDestroyView(DestroyAPIView):
    authentication_classes = (TokenAuthentication,) # !!
    permission_classes = (IsAuthenticated,)
    serializer_class = BookmarkSerializer
    queryset = Bookmark.objects.all()


class BookmarkRegistryView(GenericAPIView):
    class DummySerializer(Serializer):
        pass

    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = DummySerializer

    def get(self, *args, **kwargs):
        return Response(get_registry())
