from rest_framework.reverse import reverse
from ..factories import BookmarkFactory
from ...api.tests import APITestBase


class BookmarkTest(APITestBase):

    def test_list(self):
        limit = 3
        [BookmarkFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:bookmark:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Bookmark list test failed")