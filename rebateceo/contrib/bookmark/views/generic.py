# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""

"""
Class based generic views.
These views are only available if you are using Django >= 1.3.
"""
from django.contrib.auth.models import User
from django.views.generic.detail import DetailView
from rebateceo.contrib.bookmark.handlers import library


class BookmarksMixin(object):
    """
    Mixin for bookmark class based views.
    Views subclassing this class must implement the *get_bookmark* method.

    .. py:attribute:: context_bookmark_name

        The name of context variable containing bookmark.
        Default is *'bookmark'*.

    .. py:attribute:: key

        The bookmark key to use for retreiving bookmark.
        Default is *None*.

    .. py:attribute:: reversed_order

        If True, bookmark are ordered by creation date descending.
        Default is True.
    """
    context_bookmark_name = 'bookmark'
    template_name_suffix = '_bookmark'
    key = None
    reversed_order = True

    def get_context_bookmark_name(self, obj):
        """
        Get the variable name to use for the bookmark.
        """
        return self.context_bookmark_name

    def get_key(self, obj):
        """
        Get the key to use to retreive bookmark.
        If the key is None, use all keys.
        """
        return self.key

    def order_is_reversed(self, obj):
        """
        Return True to sort bookmark by creation date descending.
        """
        return self.reversed_order

    def get_context_data(self, **kwargs):
        context = super(BookmarksMixin, self).get_context_data(**kwargs)
        context_bookmark_name = self.get_context_bookmark_name(self.object)
        key = self.get_key(self.object)
        is_reversed = self.order_is_reversed(self.object)
        bookmark = self.get_bookmark(self.object, key, is_reversed)
        context[context_bookmark_name] = bookmark
        return context

    def get_bookmark(self, obj, key, is_reversed):
        """
        Must return a bookmark queryset.
        """
        raise NotImplementedError


class BookmarksForView(BookmarksMixin, DetailView):
    """
    Can be used to retreive and display a list of bookmark of a given object.

    This class based view accepts all the parameters that can be passed
    to *django.views.generic.detail.DetailView*.

    For example, you can add in your *urls.py* a view displaying all
    bookmark of a single active article::
    
        from bookmark.views.generic import BookmarksForView
        
        urlpatterns = patterns('',
            url(r'^(?P<slug>[-\w]+)/bookmark/$', BookmarksForView.as_view(
                queryset=Article.objects.filter(is_active=True)),
                name="article_bookmark"),
        )

    You can also manage bookmark order (default is by date descending) and
    bookmark keys, in order to retreive only bookmark for a given key, e.g.::

        from bookmark.views.generic import BookmarksForView
        
        urlpatterns = patterns('',
            url(r'^(?P<slug>[-\w]+)/bookmark/$', BookmarksForView.as_view(
                model=Article, key='mykey', reversed_order=False),
                name="article_bookmark"),
        )
    
    Two context variables will be present in the template:
        - *object*: the bookmarked article
        - *bookmark*: all the bookmark of that article
        
    The default template suffix is ``'_bookmark'``, and so the template
    used in our example is ``article_bookmark.html``.


    """

    def get_bookmark(self, obj, key, is_reversed):
        """
        Return a queryset of bookmark of *obj*.
        """
        lookups = {'instance': obj, 'reversed': is_reversed}
        if key is not None:
            lookups['key'] = key
        return library.backend.filter(**lookups)


class BookmarksByView(BookmarksMixin, DetailView):
    """
    Can be used to retreive and display a list of bookmark saved by a
    given user.

    This class based view accepts all the parameters that can be passed
    to *django.views.generic.detail.DetailView*, with an exception:
    it is not mandatory to specify the model or queryset used to
    retreive the user (*django.contrib.auth.models.User* model is used
    by default).

    For example, you can add in your *urls.py* a view displaying all
    bookmark by a single active user::
    
        from bookmark.views.generic import BookmarksByView
        
        urlpatterns = patterns('',
            url(r'^(?P<pk>\d+)/bookmark/$', BookmarksByView.as_view(
                queryset=User.objects.filter(is_active=True)),
                name="user_bookmark"),
        )
    
    You can also manage bookmark order (default is by date descending) and
    bookmark keys, in order to retreive only bookmark for a given key, e.g.::

        from bookmark.views.generic import BookmarksByView
        
        urlpatterns = patterns('',
            url(r'^(?P<pk>\d+)/bookmark/$', BookmarksByView.as_view(
                key='mykey', reversed_order=False),
                name="user_bookmark"),
        )

    Two context variables will be present in the template:
        - *object*: the user
        - *bookmark*: all the bookmark saved by that user
        
    The default template suffix is ``'_bookmark'``, and so the template
    used in our example is ``user_bookmark.html``.
    """
    model = User

    def get_bookmark(self, obj, key, is_reversed):
        """
        Return a queryset of bookmark saved by *obj* user.
        """
        lookups = {'user': obj, 'reversed': is_reversed}
        if key is not None:
            lookups['key'] = key
        return library.backend.filter(**lookups)
