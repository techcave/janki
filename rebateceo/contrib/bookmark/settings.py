# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""

from django.conf import settings

# default bookmark model (if None, *bookmark.backends.ModelBackend* is used)
# to use MongoDB backend you can just write::
# GENERIC_BOOKMARKS_BACKEND = 'bookmark.backends.MongoBackend'
BACKEND = getattr(settings, 'GENERIC_BOOKMARKS_BACKEND', None)

# default key to use for bookmark when there is only one bookmark-per-content
DEFAULT_KEY = getattr(settings, 'GENERIC_BOOKMARKS_DEFAULT_KEY', 'main')

# querystring key that can contain the url of the redirection 
# performed after adding or removing bookmark
NEXT_QUERYSTRING_KEY = getattr(settings,
                               'GENERIC_BOOKMARKS_NEXT_QUERYSTRING_KEY', 'next')

# set to False if you want to globally disable bookmark deletion
CAN_REMOVE_BOOKMARKS = getattr(settings,
                               'GENERIC_BOOKMARKS_CAN_REMOVE_BOOKMARKS', True)

# mongodb backend connection parameters
# if the instance of MongoDB is executed in localhost without authentication 
# you can just write::
# GENERIC_BOOKMARKS_MONGODB = {"NAME": "bookmark"}
MONGODB = getattr(settings, "GENERIC_BOOKMARKS_MONGODB", {
    'NAME': '',
    'USERNAME': '',
    'PASSWORD': '',
    'PARAMETERS': {},
})
