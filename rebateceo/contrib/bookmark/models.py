# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""

import string

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User

from rebateceo.contrib.bookmark import managers


class Bookmark(models.Model):
    """
    A user's bookmark for a content object.

    This is only used if the current backend stores bookmark in the database
    using Django models.

    .. py:attribute:: content_type

        the bookmarked instance content type

    .. py:attribute:: object_id

        the bookmarked instance id

    .. py:attribute:: content_object
        
        the bookmarked instance

    .. py:attribute:: key

        the bookmark key

    .. py:attribute:: user

        the user who bookmarked the instance 
        (as a fk to *django.contrib.auth.models.User*)

    .. py:attribute:: created_at

        the bookmark creation datetime
    """
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    key = models.CharField(max_length=128)

    user = models.ForeignKey(User, blank=True, null=True,
                             related_name='bookmark')

    created_at = models.DateTimeField(auto_now_add=True)

    # manager
    objects = managers.BookmarksManager()

    class Meta:
        unique_together = ('content_type', 'object_id', 'key', 'user')

    def __unicode__(self):
        return u'Bookmark for %s by %s' % (self.content_object, self.user)


# IN BULK SELECT QUERIES

def annotate_bookmark(queryset_or_model, key, user, attr='is_bookmarked'):
    """
    Annotate *queryset_or_model* with bookmark, in order to retreive from
    the database all bookmark values in bulk.
    
    The first argument *queryset_or_model* must be, of course, a queryset
    or a Django model object. The argument *key* is the bookmark key.
    
    The bookmark are filtered using given *user*.
    
    A boolean is inserted in an attr named *attr* (default='is_bookmarked')
    of each object in the generated queryset.
    
    Usage example::
    
        for article in annotate_bookmark(Article.objects.all(), 'favourite',
            myuser, attr='has_a_bookmark'):
            if article.has_a_bookmark:
                print u"User %s likes article %s" (myuser, article)
    """
    from rebateceo.contrib.bookmark import utils
    # getting the queryset
    if isinstance(queryset_or_model, models.base.ModelBase):
        queryset = queryset_or_model.objects.all()
    else:
        queryset = queryset_or_model
    # preparing arguments for *extra* query
    opts = queryset.model._meta
    content_type = utils.get_content_type_for_model(queryset.model)
    mapping = {
        'bookmark_table': Bookmark._meta.db_table,
        'model_table': opts.db_table,
        'model_pk_name': opts.pk.name,
        'content_type_id': content_type.pk,
    }
    # building base query
    template = """
    SELECT id FROM ${bookmark_table} WHERE 
    ${bookmark_table}.object_id = ${model_table}.${model_pk_name} AND 
    ${bookmark_table}.content_type_id = ${content_type_id} AND
    ${bookmark_table}.user_id = %s AND
    ${bookmark_table}.key = %s
    """
    select = {attr: string.Template(template).substitute(mapping)}
    return queryset.extra(select=select, select_params=[user.pk, key])


# ABSTRACT MODELS

class BookmarkedModel(models.Model):
    """
    Mixin for bookmarkable models.

    Models subclassing this abstract model gain a *bookmark* attribute
    allowing accessto the reverse generic relation 
    to the *bookmark.models.Bookmark*.
    """
    bookmark = generic.GenericRelation(Bookmark)

    class Meta:
        abstract = True 
