# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import reversion
import datetime

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from rebateceo.contrib.rebate.models import Rebate


def granular_now(n=None):
    """
    A datetime.now look-alike that returns times rounded to a five minute
    boundary. This helps the backend database to optimize/reuse/cache its
    queries by not creating a brand new query each time.

    Also useful if you are using johnny-cache or a similar queryset cache.
    """
    if n is None:
        n = datetime.datetime.now()
    return datetime.datetime(n.year, n.month, n.day, n.hour, (n.minute // 5) * 5)


def format_date(d, if_none=''):
    """
    Format a date in a nice human readable way: Omit the year if it's the current
    year. Also return a default value if no date is passed in.
    """

    if d is None: return if_none

    now = datetime.now()
    fmt = (d.year == now.year) and '%d.%m' or '%d.%m.%Y'
    return d.strftime(fmt)


class CampaignManager(models.Manager):

    def active(self):
        queryset = self.filter(active=True)
        return queryset


class Campaign(models.Model):
    STATE_BEFORE = 'BEFORE'
    STATE_DURING = 'DURING'
    STATE_AFTER = 'AFTER'
    STATES_CHOICES = (
        (STATE_BEFORE, STATE_BEFORE),
        (STATE_DURING, STATE_DURING),
        (STATE_AFTER, STATE_AFTER),
    )

    title = models.CharField(_('title'), max_length=255)
    slug = models.SlugField(max_length=255)
    active = models.BooleanField(default=False)
    publication_date = models.DateTimeField(_('publication date'))
    publication_end_date = models.DateTimeField(_('publication end date'), blank=True, null=True)
    rules_desc = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    rebates = models.ManyToManyField(Rebate, blank=True, null=True)
    objects = CampaignManager()

    def __unicode__(self):
        return unicode(self.title)

    class Meta:
        verbose_name = u"Campaign"
        verbose_name_plural = u"Campaigns"
        ordering = ('pk', )

    @property
    def state(self):
        from_date = self.publication_date
        to_date = self.publication_end_date
        now = timezone.now()
        if from_date and not to_date and from_date <= now:
            return self.STATE_DURING
        elif from_date and to_date:
            if from_date <= now <= to_date:
                return self.STATE_DURING
            elif now >= to_date:
                return self.STATE_AFTER
        return self.STATE_BEFORE

    def save(self, *args, **kwargs):
        if self.publication_date:
            self.publication_date = granular_now(self.publication_date)
        if self.publication_end_date:
            self.publication_end_date = granular_now(self.publication_end_date)
        super(Campaign, self).save(*args, **kwargs)

    def admin_datepublisher(self):
        return u'%s &ndash; %s' % (
            format_date(self.publication_date),
            format_date(self.publication_end_date, '&infin;'),
        )
    admin_datepublisher.allow_tags = True
    admin_datepublisher.short_description = _('visible from-to')


reversion.register(Campaign)