from rest_framework.reverse import reverse
from ..factories import CampaignFactory
from ...api.tests import APITestBase


class CampaignTest(APITestBase):
    def test_list(self):
        """
        - bin/backend-test test rebateceo.contrib.rebate.api.tests.RebateTest.test_list
        - bin/py.test -v rebateceo/contrib/rebate --nomigrations --reuse-db
        """
        [CampaignFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:campaign:list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Campaign list test failed")
