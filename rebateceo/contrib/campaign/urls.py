from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
       url(r'^list/$', views.CampaignListView.as_view(), name='list'),
       url(r'^create/$', views.CampaignCreateView.as_view(), name='create'),
       url(r'^update/(?P<pk>[0-9]+)/$', views.CampaignUpdateView.as_view(), name='update'),
       url(r'^delete/(?P<pk>[0-9]+)/$', views.CampaignDeleteView.as_view(), name='delete'),
)
