# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse
from django_tables2_reports.config import RequestConfigReport as RequestConfig
from django_tables2_reports.utils import create_report_http_response
from . import models, filters as _filters, tables


class CampaignMenuMixin(object):
    model = models.Campaign

    def get_context_data(self, *args, **kwargs):
        context = super(CampaignMenuMixin, self).get_context_data(*args, **kwargs)
        context['active'] = 'campaign'
        return context

    def get_success_url(self):
        return reverse('campaign:list')


class RebateCreateView(CampaignMenuMixin, CreateView):
    template_name = 'campaign/create.html'


class RebateUpdateView(CampaignMenuMixin, UpdateView):
    template_name = 'campaign/update.html'


class RebateDeleteView(CampaignMenuMixin, DeleteView):
    template_name = 'campaign/delete.html'


class RebateListView(CampaignMenuMixin, ListView):
    template_name = 'campaign/rebate_list.html'

    def get_context_data(self, **kwargs):
        context = super(RebateListView, self).get_context_data(**kwargs)

        filters = _filters.CampaignFilter(self.request.GET, queryset=models.Campaign.objects.active())
        queryset = filters.qs
        table = tables.CampaignTable(queryset)
        table_to_report = RequestConfig(self.request, paginate={"per_page": 25}).configure(table)
        if table_to_report:
            return create_report_http_response(table_to_report, self.request)
        context.update({
            'table': table,
            'filters': filters,
            'active': 'rebate:list',
            'active_sub': 'rebate:list',
            'current_path': self.request.get_full_path()
        })
        return context
