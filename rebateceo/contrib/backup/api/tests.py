from django.conf import settings
from rest_framework.reverse import reverse
from ...api.tests import APITestBase


# class BackupTest(APITestBase):
#     def test_backup(self):
#         response = self.client.get(reverse('api:backup:download'), {}, **self.headers)
#
#         self.assertEquals(
#             response.get('Content-Disposition'),
#             "attachment; filename=media.zip"
#         )