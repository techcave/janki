# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib import messages
from django.utils.safestring import mark_safe
from django.contrib.admin import ModelAdmin, site, TabularInline

import datetime
from . import models, forms


class SubRebateInlineMixin(TabularInline):
    formset = forms.SubRebateFormset
    fk_name = 'rebate'
    extra = 1


class UpsellingInline(SubRebateInlineMixin):
    model = models.Rebate.upselling.through
    form = forms.UpsellingForm


class CrossellingInline(SubRebateInlineMixin):
    model = models.Rebate.crosselling.through
    form = forms.CrossellingForm


class RebateImageInlines(TabularInline):
    model = models.RebateImage
    form = forms.RebateImageForm
    extra = 5


class RebateAdmin(ModelAdmin):
    form = forms.RebateAdminForm
    list_display = (
        'shop', 'get_image', 'get_mobile_image', 'title', 'type', 'segment', 'content', 'updated_at', 'created_at',
        'show_date_from', 'show_date_to', 'valid_date_from', 'valid_date_to', 'daily_limit', 'reservation_limit_days',
        'remaining_daily_limit', 'global_limit', 'remaining_global_limit', 'min_purchase_amount', 'exclusive',
        'product_range', 'archived_at')
    list_editable = ('content',)
    list_filter = ('shop', 'show_date_from', 'show_date_to', 'valid_date_from', 'valid_date_to', 'exclusive')
    readonly_fields = ['deleted_at', 'imported_at', 'import_name']
    search_fields = ('shop__name', 'title', 'content', 'show_date_from', 'show_date_to', 'valid_date_from',
                     'valid_date_to')
    inlines = [RebateImageInlines, UpsellingInline, CrossellingInline]
    actions = ['make_deleted']

    def get_image(self, record):
        image = record.images.first()
        return mark_safe("<img src='{url}' width='270' />".format(url=image)) if image else '-'

    def get_mobile_image(self, record):
        return mark_safe("<img src='{url}' />".format(url=record.mobile_image.url)) if record.mobile_image else '-'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return []
        return super(RebateAdmin, self).get_inline_instances(request, obj)

    def get_upselling(self, obj):
        return "<br/><br/>".join([str(x) for x in obj.upselling.active()])

    get_upselling.allow_tags = True

    def get_crosselling(self, obj):
        return "<br/><br/>".join([str(x) for x in obj.crosselling.active()])

    get_crosselling.allow_tags = True

    def get_actions(self, request):
        actions = super(RebateAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def make_deleted(modeladmin, request, queryset):
        queryset.update(deleted_at=datetime.datetime.now())
        messages.success(request, 'Selected items has been marked as deleted.')

    make_deleted.short_description = "Mark selected items as deleted (safe method)"

    def save_model(self, request, obj, form, change):
        copy_dates = form.cleaned_data.get('copy_dates')
        if copy_dates:
            obj.valid_date_from = obj.show_date_from
            obj.valid_date_to = obj.show_date_to
        obj = super(RebateAdmin, self).save_model(request, obj, form, change)
        return obj


class CouponAdmin(ModelAdmin):
    list_display = ('pk', 'origin', 'code', 'rebate', 'qrcode', 'was_used', 'created_at', 'updated_at',
                    'reservation_date_to', 'parent')
    list_filter = ('created_at', 'origin')
    list_editable = ('was_used',)
    readonly_fields = ('code', 'uuid')
    search_fields = ('code', 'rebate__title', 'rebate__content', 'rebate__shop__name', 'code')

    def qrcode(self, obj):
        return '<a href="{src}" target="_blank"><img src="{src}" /></a><br><input value="{qrcode_str}"/>'. \
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)

    qrcode.allow_tags = True


class TypeAdmin(ModelAdmin):
    list_display = ('title', 'symbol', 'description', 'created_at', 'updated_at')
    list_filter = ('created_at',)


site.register(models.Rebate, RebateAdmin)
site.register(models.Coupon, CouponAdmin)
site.register(models.Type, TypeAdmin)
