# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import random

import factory
import datetime
from factory import django
from rebateceo.contrib.profile.factories import SessionFactory
from django.utils.translation import ugettext_lazy as _
from rebateceo.contrib.shop.factories import ShopFactory
from . import models


class RebateFactory(django.DjangoModelFactory):
    FACTORY_FOR = models.Rebate

    shop = factory.LazyAttribute(lambda o: ShopFactory())
    content = factory.Sequence(lambda n: _('Rebate test nr %s' % n))
    show_date_from = datetime.datetime(2015, 1, 28, 0, 0, 0)
    show_date_to = datetime.datetime(2030, 4, random.randrange(1, 30), 0, 0, 0)
    valid_date_from = datetime.datetime(2015, 3, 28, 0, 0, 0)
    valid_date_to = datetime.datetime(2030, random.randrange(4, 6), random.randrange(1, 30), 0, 0, 0)
    daily_limit = factory.LazyAttribute(lambda o: unicode(random.randrange(5, 20)))
    global_limit = factory.LazyAttribute(lambda o: unicode(random.randrange(10, 200)))


class CouponFactory(django.DjangoModelFactory):
    FACTORY_FOR = models.Coupon

    rebate = factory.LazyAttribute(lambda o: RebateFactory())
    session = factory.LazyAttribute(lambda o: SessionFactory())
    code = factory.Sequence(lambda n: "200000000%s" % str(n).zfill(3))
    uuid = factory.Sequence(lambda n: "xxx-yyy-zzz-aaa-bbb-%s" % str(n).zfill(3))
