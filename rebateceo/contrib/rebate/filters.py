# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: SIZEOF | Web applications
@license: MIT License
@contact: contact@sizeof.pl
"""
import django_filters
from django.contrib.auth import get_user_model
from decimal import Decimal
from django.core.validators import EMPTY_VALUES
from django.utils import six
from django.utils.translation import ugettext as _
from django.forms.widgets import CheckboxSelectMultiple
from django import forms
from rebateceo.contrib.member.models import Member
from rebateceo.contrib.shop.models import Category, Shop
from rebateceo.contrib.account.models import UserProfile


class RebateFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(label=_('title'), lookup_type='icontains')
    content = django_filters.CharFilter(label=_('Content'), lookup_type='icontains')
    shop = django_filters.ModelChoiceFilter(label=_('Shop'), queryset=Shop.objects.active())