# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import csv
import StringIO
from django.utils.encoding import smart_str
from .models import Coupon


def get_coupons_csv(queryset=None):
    queryset = queryset or Coupon.objects.all().select_related('card').select_related('rebate').order_by('-created_at')
    csvfile = StringIO.StringIO()
    csvwriter = csv.writer(csvfile, delimiter=';', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect=csv.excel)

    csvwriter.writerow([
        smart_str(u"ID"),
        smart_str(u"Card Number"),
        smart_str(u"Verify Code"),
        smart_str(u"Created date"),
        smart_str(u"Created time"),
        smart_str(u"Rebate info"),
        smart_str(u"Coupon UUID"),
        smart_str(u"Verification"),
        smart_str(u"Verification amount"),
    ])

    for obj in queryset:
        csvwriter.writerow([
            smart_str(obj.pk),
            smart_str(obj.card.number if obj.card else ''),
            smart_str(obj.code),
            smart_str(obj.created_at)[0:10],
            smart_str(obj.created_at)[11:19],
            smart_str(obj.rebate),
            smart_str(obj.qrcode_str),
            smart_str(obj.verification.created if obj.verification else '-'),
            smart_str(obj.verification.purchase.receipt.amount if obj.verification else ''),
        ])

    return csvfile