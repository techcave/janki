from django.conf.urls import patterns, url

from .views import RebateListView, RebateCreateView, RebateDeleteView, RebateUpdateView

urlpatterns = patterns('',
                       url(r'^list/$', RebateListView.as_view(), name='list'),
                       url(r'^create/$', RebateCreateView.as_view(), name='create'),
                       url(r'^update/(?P<pk>[0-9]+)/$', RebateUpdateView.as_view(), name='update'),
                       url(r'^delete/(?P<pk>[0-9]+)/$', RebateDeleteView.as_view(), name='delete'),
)
