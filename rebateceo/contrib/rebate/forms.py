# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.forms import widgets
from django import forms
from django.forms.models import BaseInlineFormSet
from rebateceo.core.widgets import AdminImageWidget
from rebateceo.contrib.shop.models import Shop
from .models import Rebate, RebateImage


class RebateImageForm(forms.ModelForm):
    class Meta:
        model = RebateImage
        exclude = []
        widgets = {
            'image': AdminImageWidget,
        }


class RebateAdminForm(forms.ModelForm):
    copy_dates = forms.BooleanField(required=False, help_text='Copy dates from "show" to "valid"')

    class Meta:
        model = Rebate
        exclude = []
        fields = ['shop', 'title', 'content', 'type', 'segment', 'show_date_from', 'show_date_to', 'copy_dates',
                  'valid_date_from', 'valid_date_to', 'daily_limit', 'global_limit', 'min_purchase_amount',
                  'exclusive', 'product_range', 'reservation_limit_days', 'program']

    def __init__(self, *args, **kwargs):
        super(RebateAdminForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

        # Type
        self.fields['type'].required = True

        # Shop
        self.fields['shop'].queryset = Shop.objects.not_archived()

        # Dates
        now = datetime.datetime.now()
        self.fields['show_date_from'].initial = datetime.datetime(2014, 12, 31, 0, 0, 0)
        self.fields['show_date_to'].initial = datetime.datetime(2014, 12, 31, 23, 59, 59)
        self.fields['valid_date_from'].initial = datetime.datetime(2014, 12, 31, 0, 0, 0)
        self.fields['valid_date_to'].initial = datetime.datetime(2014, 12, 31, 23, 59, 59)

        # if not instance.pk:
        # pass
        # else:
        #     self.fields['upselling'].queryset = Rebate.objects.upselling_for_shop(instance.shop)
        #     self.fields['crosselling'].queryset = Rebate.objects.display()


class SubRebateFormset(BaseInlineFormSet):
    def _construct_form(self, i, **kwargs):
        return super(SubRebateFormset, self)._construct_form(i, parent=self.instance)


class UpsellingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        parent = kwargs.pop('parent', None)
        super(UpsellingForm, self).__init__(*args, **kwargs)

        self.fields['child'].queryset = Rebate.objects.none()
        if parent:
            self.fields['child'].queryset = Rebate.objects.active().filter(shop=parent.shop).exclude(pk=parent.pk)


class CrossellingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        parent = kwargs.pop('parent', None)
        super(CrossellingForm, self).__init__(*args, **kwargs)

        self.fields['child'].queryset = Rebate.objects.none()
        if parent:
            self.fields['child'].queryset = Rebate.objects.active().exclude(shop=parent.shop)
