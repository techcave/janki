# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from settings import ROOT, path
from django.core.management import BaseCommand
from ...models import Rebate

DEMO_PATH = path(ROOT, '..', 'data', 'import')
file = path(DEMO_PATH, 'lista_kuponow_rabatowych_042014_TEMPLATE.csv')
RABATES = open(file).readlines()


class Command(BaseCommand):
    help = u'Importuje rabaty z pliku'

    def handle(self, *args, **options):
        raise Warning("Uzyj import_rebates_v2")

        print ("Create Rebates from file: %s" % file)
        for row in RABATES[1:]:
            rebate = row.split(';')
            Rebate.objects.get_or_create(**{
                'shop_id': int(rebate[0]),
                'content': rebate[1],
                'show_date_from': rebate[2],
                'show_date_to': rebate[3],
                'valid_date_from': rebate[4],
                'valid_date_to': rebate[5],
                'daily_limit': int(rebate[6]),
                'global_limit': int(rebate[7]),
            })
        print ("...got or created : %s objects\n" % Rebate.objects.all().count())
