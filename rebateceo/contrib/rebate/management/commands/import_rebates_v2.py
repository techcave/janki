# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime, date
from settings import ROOT, path
from django.core.management import BaseCommand
from rebateceo.contrib.shop.models import Shop
from ...models import Rebate

DATA_PATH = path(ROOT, '..', 'data', 'import')


class Command(BaseCommand):
    help = u'Importuje rabaty z pliku csv (rozdzielany ":")'

    def handle(self, *args, **options):

        file = str(args[0])
        RABATES = open(path(DATA_PATH, file)).readlines()

        # Clear rebates
        Rebate.objects.all().delete()

        def format_date(value):
            try:
                result = datetime.strptime(value, "%d.%m.%Y").date() if value.strip() else value
            except ValueError:
                print value
            else:
                return result

        def get_restrictions(item):
            try:
                result = str(item[7])
            except IndexError:
                print item
            else:
                return result

        # Create rabates
        for row in RABATES[1:]:
            item = row.split(';')

            rebate = Rebate.objects.create(
                shop=Shop.objects.get(pk=int(item[0])),
                content=str(item[2]),
                restrictions=get_restrictions(item)
            )

            try:
                if format_date(item[3]):
                    rebate.show_date_from = format_date(item[3])
            except IndexError:
                pass

            try:
                if format_date(item[4]):
                    rebate.show_date_to = format_date(item[4])
            except IndexError:
                pass

            try:
                if format_date(item[5]):
                    rebate.valid_date_from = format_date(item[5])
            except IndexError:
                pass

            try:
                if format_date(item[6]):
                    rebate.valid_date_to = format_date(item[6])
            except IndexError:
                pass

            rebate.save()

        print ("...created : %s objects\n" % Rebate.objects.all().count())
