# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import json
from django.db import models
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from .models import Rebate
from rebateceo.contrib.rebate.api.serializers import RebateSerializer
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage

REBATE_USE_REDIS_PUBLISHER = getattr(settings, 'REBATE_USE_REDIS_PUBLISHER', True)


def rebate_save(sender, instance, created, **kwargs):
    # Remove from output json
    del kwargs['signal']

    RedisPublisher(facility='rebate', broadcast=True).publish_message(
        RedisMessage(json.dumps({
                                    'sender': {
                                        'module': sender.__module__,
                                        'class': sender.__name__
                                    },
                                    'instance': RebateSerializer(instance).data,
                                    'created': created,
                                    'kwargs': kwargs
                                }, cls=DjangoJSONEncoder))
    )


if REBATE_USE_REDIS_PUBLISHER:
    models.signals.post_save.connect(rebate_save, sender=Rebate)
