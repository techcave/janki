��    /      �  C                   (  1   ,     ^     e     s     {     �  -   �     �     �  
   �     �     �  
                  !     -     :     @     L  Q   X     �     �     �     �     �     �     �     �     �  !   �           %     4     A     G     L  
   R     ]     c     j     z     �     �  �  �     F     W  2   `     �     �     �     �     �  2   �  '   	     )	  	   0	     :	     G	  	   M	     W	     ^	     q	     �	     �	     �	     �	  Z   �	     
     
     $
     *
     8
     ?
  
   N
     Y
     `
  )   h
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          "                                                                              
          	          +   $              *       )       !   (                             #   ,   '         "                -                 .   /   &      %    Add new rebate All Are you sure that you want to delete this record? Cancel Clear filters Content Copied rebate Coupon Coupon can not be displayed for current date. Coupon can't be printed.  %s Coupons Created at Daily limit Delete Deleted at Edit Expiration days Filter list Global limit Image Imported at Media group Model import error! Check LOTTERY_BACKEND_MODEL settings for lottery model class. Origin Program Rebate Rebate edition Rebates Rebates list Restrictions Save Segment Select segment if type is private Shop Show date from Show date to Title Type Types Updated at Usage Usages Valid date from Valid date to Verification code title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-02 19:15+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Dodaj nowy rabat Wszystko Czy jesteś pewny, że chcesz usunąć ten rekord? Anuluj Wyczyść filtry Zawartość Skopiowany rabat Kupon Nie można wyświetlić kuponu dla aktualnej daty. Kupon nie może zostać wydrukowany. %s Kupony Stworzony Limit dzinny Usuń Usunięty Edytuj Dni do wyczerpania Lista filtrów Limit całościowy Obraz Zaimportowany Grupa mediów Błąd importu modelu! Sprawdź ustawienie LOTTERY_BACKEND_MODEL dla klasy modelu loterii  Pochodzenie Program Rabat Edycja rabatu Rabaty Lista rabatów Restrykcje Zapisz Segment Wybierz segment jeżeli typ jest prywatny Sklep Pokazuj datę od Pokazuj datę do Tytuł Typ Typy Zaktualizowany Użycie Zastosowania Waliduj datę od Waliduj datę do Kod weryfikacyjny tytuł 