# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0003_auto_20150309_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rebate',
            name='verification_type',
            field=models.CharField(blank=True, max_length=16, null=True, verbose_name='Verification type', choices=[(b'', b'Without verification'), (b'pin', b'Verification by PIN'), (b'qrcode', b'Verification by QRCode')]),
            preserve_default=True,
        ),
    ]
