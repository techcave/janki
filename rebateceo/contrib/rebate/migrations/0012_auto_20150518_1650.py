# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0011_auto_20150518_1131'),
    ]

    operations = [
        migrations.RenameField(
            model_name='rebate',
            old_name='only_for_vip',
            new_name='exclusive',
        ),
    ]
