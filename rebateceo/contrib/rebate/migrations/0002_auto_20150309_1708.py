# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='rebate',
            name='min_purchase_amount',
            field=models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True, verbose_name='Min purchase amount', db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rebate',
            name='only_for_vip',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rebate',
            name='product_range',
            field=models.TextField(null=True, verbose_name='Product range', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rebate',
            name='verification_type',
            field=models.TextField(default=0, verbose_name='Verification type', choices=[(0, b'Without verification'), (1, b'Verification by PIN'), (2, b'Verification by QRCode')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rebate',
            name='restrictions',
            field=models.TextField(null=True, verbose_name='Restrictions', blank=True),
            preserve_default=True,
        ),
    ]
