# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import rebateceo.contrib.rebate.models
import django_ean13.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20150302_0024'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('origin', models.CharField(db_index=True, max_length=16, null=True, blank=True)),
                ('code', django_ean13.fields.EAN13Field(null=True, max_length=13, blank=True, help_text='Type an EAN13 number', unique=True, verbose_name='Verification code', db_index=True)),
                ('uuid', django_extensions.db.fields.UUIDField(null=True, editable=False, blank=True, unique=True, db_index=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('was_used', models.BooleanField(default=False)),
                ('parent', models.ForeignKey(related_name='coupons', blank=True, to='rebate.Coupon', null=True)),
            ],
            options={
                'ordering': ['-created_at'],
                'verbose_name': 'Coupon',
                'verbose_name_plural': 'Coupons',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Crosselling',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rebate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField(db_index=True, null=True, blank=True)),
                ('content', models.TextField(null=True, verbose_name='Content', blank=True)),
                ('image', models.ImageField(upload_to=rebateceo.contrib.rebate.models.upload_to, null=True, verbose_name='Image', blank=True)),
                ('show_date_from', models.DateTimeField(db_index=True, null=True, verbose_name='Show date from', blank=True)),
                ('show_date_to', models.DateTimeField(db_index=True, null=True, verbose_name='Show date to', blank=True)),
                ('valid_date_from', models.DateTimeField(db_index=True, null=True, verbose_name='Valid date from', blank=True)),
                ('valid_date_to', models.DateTimeField(db_index=True, null=True, verbose_name='Valid date to', blank=True)),
                ('daily_limit', models.IntegerField(null=True, verbose_name='Daily limit', blank=True)),
                ('global_limit', models.IntegerField(null=True, verbose_name='Global limit', blank=True)),
                ('restrictions', models.CharField(max_length=255, null=True, verbose_name='Restrictions', blank=True)),
                ('import_name', models.CharField(max_length=100, null=True, verbose_name='Import name', blank=True)),
                ('imported_at', models.DateTimeField(null=True, verbose_name='Imported at', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(null=True, verbose_name='Deleted at', blank=True)),
                ('crosselling', models.ManyToManyField(related_name='rebates_crosseling', null=True, through='rebate.Crosselling', to='rebate.Rebate', blank=True)),
                ('shop', models.ForeignKey(related_name='rebates', on_delete=django.db.models.deletion.DO_NOTHING, verbose_name='Shop', to='shop.Shop')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Rebate',
                'verbose_name_plural': 'Rebates',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Upselling',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('expiration_days', models.IntegerField(default=0, verbose_name='Expiration days')),
                ('child', models.ForeignKey(related_name='upselling_rebates', verbose_name='Rebate', to='rebate.Rebate')),
                ('rebate', models.ForeignKey(related_name='upselling_mtm', to='rebate.Rebate')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='rebate',
            name='upselling',
            field=models.ManyToManyField(related_name='rebates_upselling', null=True, through='rebate.Upselling', to='rebate.Rebate', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='crosselling',
            name='child',
            field=models.ForeignKey(related_name='crosselling_rebates', verbose_name='Rebate', to='rebate.Rebate'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='crosselling',
            name='rebate',
            field=models.ForeignKey(related_name='crosseling_mtm', to='rebate.Rebate'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='coupon',
            name='rebate',
            field=models.ForeignKey(related_name='coupons', to='rebate.Rebate'),
            preserve_default=True,
        ),
    ]
