# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0008_rebate_archived_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rebate',
            name='archived_at',
            field=models.DateTimeField(help_text=b'Used in mobile app', null=True, verbose_name='Archived at', blank=True),
            preserve_default=True,
        ),
    ]
