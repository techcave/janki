# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0010_remove_rebate_archived_at'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rebate',
            options={'ordering': ('-updated_at',), 'verbose_name': 'Rebate', 'verbose_name_plural': 'Rebates'},
        ),
        migrations.AddField(
            model_name='rebate',
            name='type',
            field=models.CharField(default=b'regular', max_length=7, choices=[(b'vip', 'VIP'), (b'regular', 'Regular'), (b'private', 'Private')]),
            preserve_default=True,
        ),
    ]
