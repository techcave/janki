# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.rebate.models


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0004_auto_20150312_1238'),
    ]

    operations = [
        migrations.CreateModel(
            name='RebateImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=rebateceo.contrib.rebate.models.upload_to)),
                ('rebate', models.ForeignKey(to='rebate.Rebate')),
            ],
            options={
                'verbose_name': 'Rebate image',
                'verbose_name_plural': 'Rebate images',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='rebate',
            name='image',
        ),
    ]
