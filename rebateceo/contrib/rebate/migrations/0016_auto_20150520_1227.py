# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0015_campaign'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='campaign',
            name='rebates',
        ),
        migrations.DeleteModel(
            name='Campaign',
        ),
    ]
