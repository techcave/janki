# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0016_auto_20150520_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='rebate',
            name='daily_limit_valid',
            field=models.IntegerField(null=True, verbose_name='Daily limit valid', blank=True),
            preserve_default=True,
        ),
    ]
