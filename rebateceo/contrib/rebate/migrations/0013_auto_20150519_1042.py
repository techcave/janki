# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0016_auto_20150519_1023'),
        ('rebate', '0012_auto_20150518_1650'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rebate',
            name='type',
        ),
        migrations.AddField(
            model_name='rebate',
            name='segment',
            field=models.ForeignKey(related_name='rebates', blank=True, to='profile.Segment', help_text='Select segment if type is private', null=True, verbose_name='Segment'),
            preserve_default=True,
        ),
    ]
