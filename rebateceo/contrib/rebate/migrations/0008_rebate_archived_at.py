# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0007_rebate_mobile_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='rebate',
            name='archived_at',
            field=models.DateTimeField(help_text=b'deleted_at = archived_at', null=True, verbose_name='Archived at', blank=True),
            preserve_default=True,
        ),
    ]
