# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0009_auto_20150413_1016'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rebate',
            name='archived_at',
        ),
    ]
