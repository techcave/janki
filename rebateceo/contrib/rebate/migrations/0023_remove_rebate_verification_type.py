# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0022_auto_20150527_1621'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rebate',
            name='verification_type',
        ),
    ]
