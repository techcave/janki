# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0017_rebate_daily_limit_valid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rebate',
            name='daily_limit_valid',
        ),
        migrations.AddField(
            model_name='rebate',
            name='reservation_limit_days',
            field=models.IntegerField(null=True, verbose_name='Reservation limit days', blank=True),
            preserve_default=True,
        ),
    ]
