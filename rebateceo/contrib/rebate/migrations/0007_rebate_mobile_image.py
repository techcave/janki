# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.rebate.models


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0006_coupon_session'),
    ]

    operations = [
        migrations.AddField(
            model_name='rebate',
            name='mobile_image',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.rebate.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
