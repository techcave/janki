# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0002_auto_20150525_0952'),
        ('rebate', '0019_coupon_reservation_date_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='card',
            field=models.ForeignKey(related_name='coupons', blank=True, to='card.Card', null=True),
            preserve_default=True,
        ),
    ]
