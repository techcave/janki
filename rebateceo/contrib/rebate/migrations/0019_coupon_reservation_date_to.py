# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0018_auto_20150526_1145'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='reservation_date_to',
            field=models.DateTimeField(null=True, verbose_name='Reservation date to', blank=True),
            preserve_default=True,
        ),
    ]
