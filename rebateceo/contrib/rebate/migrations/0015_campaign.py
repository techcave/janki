# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rebate', '0014_auto_20150519_1043'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('slug', models.SlugField(max_length=255)),
                ('active', models.BooleanField(default=False)),
                ('publication_date', models.DateTimeField(verbose_name='publication date')),
                ('publication_end_date', models.DateTimeField(null=True, verbose_name='publication end date', blank=True)),
                ('rules_desc', models.TextField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('rebates', models.ManyToManyField(to='rebate.Rebate', null=True, blank=True)),
            ],
            options={
                'ordering': ('pk',),
                'verbose_name': 'Campaign',
                'verbose_name_plural': 'Campaigns',
            },
            bases=(models.Model,),
        ),
    ]
