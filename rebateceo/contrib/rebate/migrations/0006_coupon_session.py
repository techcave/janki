# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0009_auto_20150319_1021'),
        ('rebate', '0005_auto_20150312_2219'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='session',
            field=models.ForeignKey(related_name='coupons', blank=True, to='profile.Session', null=True),
            preserve_default=True,
        ),
    ]
