# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0009_auto_20150527_1621'),
        ('rebate', '0021_auto_20150527_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rebate',
            name='program',
            field=models.ForeignKey(blank=True, to='loyalty.Program', help_text=b'Use for verifier verification', null=True),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Program',
        ),
    ]
