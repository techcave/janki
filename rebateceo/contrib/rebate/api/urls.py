# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
   url(r'^list/$', views.RebateListView.as_view(), name='list'),
   url(r'^list/mobile/$', views.RebateListForMobileView.as_view(), name='list'),
   url(r'^list/for-mobile/$', views.RebateListForMobileView.as_view(), name='list-for-mobile'),
   url(r'^list/shop/(?P<shop_id>\d+)/$', views.RebateListForShopView.as_view(), name='rebate-list-for-shop'),
   url(r'^coupon/(?P<uuid>.+)/check/$', views.CouponCheckView.as_view(), name='coupon-check'),
   url(r'^coupon/create/$', views.CouponCreateView.as_view(), name='coupon-create'),
   url(r'^coupon/list/$', views.CouponListView.as_view(), name='coupon-list'),
   # url(r'^coupon/list/for-profile/(?P<uuid>.+)/list/$', views.CouponListForProfileListView.as_view(),
   #     name='coupon-list-for-profile'),
)