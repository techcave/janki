from rebateceo.contrib.profile.factories import SessionFactory
from rest_framework.reverse import reverse
from ..factories import RebateFactory, CouponFactory
from ...api.tests import APITestBase
from .. import models


class RebateBaseTest(APITestBase):
    def setUp(self):
        super(RebateBaseTest, self).setUp()
        models.Rebate.objects.all().delete()
        models.Coupon.objects.all().delete()


class RebateTest(RebateBaseTest):
    """
    bin/backend-test test rebateceo.contrib.rebate.api.tests.RebateTest
    bin/py.test -v rebateceo/contrib/rebate --nomigrations --reuse-db
    """
    def test_list(self):
        [RebateFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:rebate:list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Rebate list test failed")
        self.assertEqual(response.data[0]['mobile_image'], None, "Rebate mobile image field test failed")

    def test_list_for_mobile(self):
        [RebateFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:rebate:list-for-mobile'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Rebate list for mobile test failed")

    def test_list_for_mobile_with_coupons(self):
        CouponFactory(rebate=RebateFactory())
        response = self.client.get(reverse('api:rebate:list-for-mobile'), {}, **self.headers)
        self.assertIsInstance(response.data[0]['coupons'][0]['verified'], bool, "Rebate mobile coupons field test failed")


class CouponTest(RebateBaseTest):
    """
    bin/backend-test test rebateceo.contrib.rebate.api.tests.CouponTest
    """
    def test_list(self):
        [CouponFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:rebate:coupon-list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Coupon list test failed")

    # def test_list_for_profile(self):
    #     items = [CouponFactory() for x in range(0, self.limit)]
    #     response = self.client.get(reverse('api:rebate:coupon-listfor-profile', kwargs={'uuid': }), {}, **self.headers)
    #     self.assertEqual(len(response.data), self.limit, "Rebate coupon list test failed")

    def test_create(self):
        session, rebate = SessionFactory(), RebateFactory(reservation_limit_days=2)
        data = {
            'session_id': session.pk,
            'rebate_id': rebate.pk,
            'origin': 'mobile'
        }
        response = self.client.post(reverse('api:rebate:coupon-create'), data, **self.headers)
        self.assertIsNotNone(response.data.get('reservation_date_to'), "'reservation_date_to' test failed")
        self.assertIsInstance(response.data.get('bookmark'), dict, "'reservation_date_to' test failed")
        self.assertEqual(response.status_code, 201, "Rebate coupon create test failed")

    def test_check(self):
        uuid = 'ds932kdsl320sd-023'
        CouponFactory(uuid=uuid)
        response = self.client.get(reverse('api:rebate:coupon-check', kwargs={'uuid': uuid}), {}, **self.headers)
        self.assertEqual(response.status_code, 200, "Rebate coupon check test failed")

