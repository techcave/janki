# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime
from django.contrib.sites.models import Site
from rebateceo.contrib.bookmark.api.serializers import BookmarkSerializer
from rebateceo.contrib.card.api.serializers import CardSerializer
from rest_framework import serializers

from django.forms.models import model_to_dict
from rebateceo.contrib.shop.api.serializers import ShopSerializer
from ..models import Rebate, Coupon, RebateImage, Type


class RebateImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = RebateImage


class TypeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Type
        fields = ['pk', 'title', 'symbol']


class RebateSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    shop = ShopSerializer()
    type = TypeSerializer()
    images = RebateImageSerializer(many=True)
    shop_id = serializers.SerializerMethodField()
    shop_name = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    dates = serializers.SerializerMethodField()

    class Meta:
        model = Rebate
        fields = ['pk', 'shop', 'show_date_from', 'show_date_to', 'valid_date_from',
                  'valid_date_from', 'valid_date_to', 'show_date_from', 'valid_date_to', 'deleted_at',
                  'daily_limit', 'global_limit', 'remaining_daily_limit', 'remaining_global_limit', 'title', 'type',
                  'segment', 'min_purchase_amount', 'exclusive', 'product_range', 'created_at',
                  'updated_at', 'archived_at', 'content', 'mobile_image', 'images', 'shop', 'only_for_vip',
                  'shop_id', 'shop_name', 'image_url', 'dates']

    def get_dates(self, instance):
        fields = ['valid_date_from', 'valid_date_to', 'show_date_from', 'show_date_to']
        result = {}
        for field in fields:
            date = getattr(instance, field)
            if date:
                result.update({field: date.strftime("%a, %Y %B %d, %H:%m")})
        return result

    def get_shop_name(self, instance):
        return instance.shop.name

    def get_shop_id(self, instance):
        return instance.shop.pk

    def get_remaining_daily_limit(self, instance):
        return instance.remaining_daily_limit

    def get_remaining_global_limit(self, instance):
        return instance.remaining_global_limit

    def get_image_url(self, instance):
        current_site = Site.objects.get_current()
        image = instance.images.first()
        return "http://%s%s" % (current_site, image.image.url) if image else ''


class CouponForMobileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = ['pk', 'verified']


class RebateForMobileSerializer(RebateSerializer):
    coupons = CouponForMobileSerializer(many=True)

    class Meta(RebateSerializer.Meta):
        fields = RebateSerializer.Meta.fields + ['coupons']


class CouponSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    rebate = RebateSerializer()
    card = CardSerializer()
    verification = serializers.SerializerMethodField()
    upselling = serializers.SerializerMethodField()
    crosselling = serializers.SerializerMethodField()
    qrcode_str = serializers.CharField(max_length=255)
    qrcode_img = serializers.CharField(max_length=255)
    verification_type = serializers.SerializerMethodField()

    class Meta:
        model = Coupon
        fields = ['pk', 'rebate', 'qrcode_str', 'qrcode_img', 'session', 'card', 'code', 'created_at', 'updated_at',
                  'reservation_date_to', 'uuid', 'verification', 'upselling', 'crosselling', 'verification_type']

    def get_verification(self, instance):
        return model_to_dict(instance.verification) if instance.verification else {}

    def get_verification_type(self, instance):
        return instance.verification_type

    def get_upselling(self, instance):
        obj = instance.upselling
        return RebateSerializer(obj.child).data if obj else None

    def get_crosselling(self, instance):
        obj = instance.crosselling
        return RebateSerializer(obj.child).data if obj else None


class CouponCreateSerializer(serializers.Serializer):
    session_id = serializers.CharField(max_length=255)
    rebate_id = serializers.IntegerField()
    origin = serializers.CharField(max_length=255)

    class Meta:
        fields = ['session_id', 'rebate_id', 'origin']
