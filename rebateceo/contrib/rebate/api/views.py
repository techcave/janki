# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.core.exceptions import ValidationError
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rebateceo.contrib.bookmark.models import Bookmark

from rest_framework.generics import ListAPIView
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated

from django_utils.generators import get_EAN13
from django_utils.exceptions import InputValidationError

from rebateceo.contrib.bookmark import utils as bookmark_utils
from rebateceo.contrib.bookmark.api.serializers import BookmarkSimpleSerializer
from rebateceo.contrib.profile.models import Session
from ..validators import rebate_exists_validator
from .. import models
from . import exceptions, serializers


class RebateListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)  # IsLocalizatorPermission)
    serializer_class = serializers.RebateSerializer
    model = models.Rebate

    def get_queryset(self):
        return self.model.objects.display()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)

    def filter_queryset(self, queryset):
        query = self.request.GET.get('query')
        if query:
            queryset = queryset.filter(title__icontains=query)
        return queryset


class RebateListForMobileView(RebateListView):
    serializer_class = serializers.RebateForMobileSerializer

    def get_queryset(self):
        return self.model.objects.active()


class RebateListByGroupView(RebateListView):
    def filter_queryset(self, qs):
        return qs.filter(group__symbol__in=self.kwargs.get('symbol').split(','))


class RebateListForShopView(RebateListView):
    def filter_queryset(self, queryset):
        return queryset.filter(shop__pk=self.kwargs.get('shop_id'))


class CouponCreateView(GenericAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.CouponCreateSerializer
    model = models.Coupon
    origin = None
    rebate = None
    stand = None
    card_id = None
    card = None
    scan = None

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            self.validate(request, *args, **kwargs)
        except InputValidationError, e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        else:
            # Check limits
            if self.rebate.can_create and not (self.card and self.card.has_printed_discount_today):

                instance = self.model.objects.create(
                    origin=self.origin,
                    rebate=self.rebate,
                    session=self.session,
                    reservation_date_to=self.rebate.reservation_date_to
                )

                instance.code = get_EAN13(instance.pk)  # Update code
                instance.save()

                # https://pm.sizeof.pl/issues/5653
                response = serializers.CouponSerializer(instance=instance).data
                if hasattr(instance, '_bookmark'):
                    response.update({
                        'bookmark': BookmarkSimpleSerializer(instance=instance._bookmark).data
                    })

                return Response(response, status=201)
            else:
                raise exceptions.CannotGenerateCoupon(reason=unicode(self.rebate.reason))

    def validate(self, request, *args, **kwargs):

        # Check source scan
        session_id = request.POST.get('session_id')
        if session_id == '' or int(session_id) == 0:
            # TODO: use exceptions
            raise InputValidationError(u'Brak wymaganego pola session_id')
        else:
            try:
                self.session = Session.objects.get(pk=session_id)
            except Session.DoesNotExist:
                # TODO: use exceptions
                raise InputValidationError(u'Nie znaleziono sesji dla podanego id')

        # Origin
        self.origin = request.POST.get('origin')
        if not self.origin:
            # TODO: use exceptions
            raise InputValidationError(u'Brak wymaganego pola origin (rabatomat|mobile)')

        # Origin
        rebate_id = request.POST.get('rebate_id')
        if rebate_id == '' or int(rebate_id) == 0:
            # TODO: use exceptions
            raise InputValidationError(u'Brak wymaganego pola rebate_id')

        # Check rebate exists
        try:
            rebate_id = request.POST.get('rebate_id')
            self.rebate = rebate_exists_validator(rebate_id)
        except ValidationError, e:
            # TODO: use exceptions
            raise InputValidationError(u'%s' % unicode(e.message))
        else:
            # Check limits
            if self.rebate.global_limit and self.rebate.remaining_global_limit == 0:
                raise exceptions.RebateGlobalLimitOverflow(type='global', limit=self.rebate.global_limit)

            if self.rebate.daily_limit and self.rebate.remaining_daily_limit == 0:
                raise exceptions.RebateDailyLimitOverflow(type='daily', limit=self.rebate.daily_limit)


class CouponListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.CouponSerializer
    queryset = models.Coupon.objects.all()


class CouponCheckView(RetrieveAPIView):
    model = models.Coupon
    serializer_class = serializers.CouponSerializer
    queryset = models.Coupon.objects.all()
    lookup_field = 'uuid'
    
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        result = serializer.data

        try:
            bookmark = Bookmark.objects.get(content_type=bookmark_utils.get_content_type_for_model(instance.rebate),
                                            object_id=instance.rebate.pk, user=instance.session.profile.user)
        except Bookmark.DoesNotExist:
            bookmark = None
        else:
            bookmark = BookmarkSimpleSerializer(instance=bookmark).data

        result.update({'bookmark': bookmark})
        return Response(result)


class CouponForProfileListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.CouponSerializer

    def get_queryset(self):
        raise NotImplementedError()


# class CouponListForCardView(ListAPIView):
#     # authentication_classes = (TokenAuthentication,)
#     # permission_classes = (IsAuthenticated,)
#     serializer_class = serializers.CouponSerializer
#     model = models.Coupon
#     card = None
#
#     def list(self, request, *args, **kwargs):
#         try:
#             self.validate(request, *args, **kwargs)
#         except InputValidationError, e:
#             return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
#         else:
#             return super(CouponListForCardView, self).list(request, args, kwargs)
#
#     def filter_queryset(self, queryset):
#         return queryset.filter(card=self.card)
#
#     def validate(self, request, *args, **kwargs):
#
#         # Check number
#         number = kwargs.get('number')
#         if not number:
#             raise InputValidationError(u'[0004] Brak numeru karty')
#
#         # Check number
#         try:
#             EAN13_validator(number)
#         except ValidationError, e:
#             raise InputValidationError(u'[0005] %s' % unicode(e.message))
#
#         for item in CARD_VALIDATORS:
#             try:
#                 card = item[0](self.code_value)
#             except ValidationError, e:
#                 raise item[1](code_type=self.code_type, code_value=self.code_value)
#         self.card = card
#
#         # Check card exists
#         try:
#             self.card = card_exists_validator(number)
#         except ValidationError, e:
#             raise InputValidationError(u'[0007] %s' % unicode(e.message))
