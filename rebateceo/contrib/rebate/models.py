# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
import itertools
import reversion

from datetime import timedelta
from rebateceo.contrib.bookmark.models import Bookmark
from rebateceo.contrib.loyalty.models import Program
from rebateceo.contrib.verifier.models import Verification
from django.db.models import Q
from django.db import models
from django.conf import settings
from django.utils import timezone

from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
from django_ean13.fields import EAN13Field
from django.db.models.signals import post_save
from rebateceo.contrib.shop.models import Shop
from rebateceo.contrib.bookmark.handlers import library as bookmarks
from rebateceo.utils.mobileimages_helper import generate_mobileimage
from rebateceo.contrib.profile.models import Segment
from rebateceo.contrib.bookmark import utils as bookmark_utils


QRCODE_STR = getattr(settings, 'QRCODE_STR', "{type};{uuid}")
QRCODE_IMG = getattr(settings, 'QRCODE_IMG', "http://chart.apis.google.com/chart?cht=qr&chs={size}x{size}&chl={qrcode_str}")
REBATE_ORDERING_FIELD = getattr(settings, 'REBATE_ORDERING_FIELD', ("-updated_at",))
REBATE_RESERVATION_LIMIT_DAYS = getattr(settings, 'REBATE_RESERVATION_LIMIT_DAYS', 2)


def upload_to(instance, filename):
    return u'/'.join(['rebates', unicode(filename)])


class RebateManager(models.Manager):
    def get_queryset(self):
        qs = super(RebateManager, self).get_queryset()
        return qs.exclude(deleted_at__isnull=False)

    def deleted(self, **kwargs):
        return self.filter(deleted_at__isnull=False) \
            .filter(**kwargs)

    def active(self, **kwargs):
        return self.filter(deleted_at__isnull=True) \
            .filter(**kwargs)

    def display(self, for_date=None, **kwargs):
        date = for_date or datetime.datetime.now()
        date_filter = Q(show_date_from__lte=date) & (Q(show_date_to__isnull=True) | Q(show_date_to__gt=date))
        queryset = self.filter(date_filter)

        return queryset


class UpsellingManager(models.Manager):
    def active(self, for_date, **kwargs):
        return self.all()

    def active_for_coupon(self, coupon):
        return self.active()


class Upselling(models.Model):
    rebate = models.ForeignKey('rebate.Rebate', related_name='upselling_mtm')
    child = models.ForeignKey('rebate.Rebate', related_name='upselling_rebates', verbose_name=_('Rebate'))
    expiration_days = models.IntegerField(verbose_name=_('Expiration days'), default=0)
    objects = UpsellingManager()


class CrossellingManager(models.Manager):
    def active(self, for_date, **kwargs):
        return self.all()


class Crosselling(models.Model):
    rebate = models.ForeignKey('rebate.Rebate', related_name='crosseling_mtm')
    child = models.ForeignKey('rebate.Rebate', related_name='crosselling_rebates', verbose_name=_('Rebate'))
    objects = CrossellingManager()


class TypeManager(models.Manager):
    pass


class Type(models.Model):
    VIP_SYMBOL = 'vip'

    title = models.CharField(max_length=255)
    symbol = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = TypeManager()

    class Meta:
        verbose_name = _("Type")
        verbose_name_plural = _("Types")

    def __unicode__(self):
        return u"%s" % (unicode(self.title), )


class Rebate(models.Model):
    VERIFICATION_TYPE_NONE = ''
    VERIFICATION_TYPE_PIN = 'pin'
    VERIFICATION_TYPE_QRCODE = 'qrcode'
    VERIFICATION_TYPES = (
        (VERIFICATION_TYPE_NONE, 'Without verification'),
        (VERIFICATION_TYPE_PIN, 'Verification by PIN'),
        (VERIFICATION_TYPE_QRCODE, 'Verification by QRCode'),
    )

    shop = models.ForeignKey(Shop, verbose_name=_('Shop'), related_name='rebates', on_delete=models.DO_NOTHING,
                             db_index=True)
    title = models.TextField(blank=True, null=True, db_index=True)
    type = models.ForeignKey(Type, verbose_name=_('Type'), related_name='rebates', blank=True, null=True)
    segment = models.ForeignKey(Segment, verbose_name=_('Segment'), related_name='rebates', blank=True, null=True,
                                help_text=_('Select segment if type is private'))
    program = models.ForeignKey(Program, blank=True, null=True, help_text="Use for verifier verification")
    mobile_image = models.ImageField(upload_to=upload_to, blank=True, null=True)
    content = models.TextField(verbose_name=_('Content'), blank=True, null=True)
    show_date_from = models.DateTimeField(verbose_name=_('Show date from'), blank=True, null=True, db_index=True)
    show_date_to = models.DateTimeField(verbose_name=_('Show date to'), blank=True, null=True, db_index=True)
    valid_date_from = models.DateTimeField(verbose_name=_('Valid date from'), blank=True, null=True, db_index=True)
    valid_date_to = models.DateTimeField(verbose_name=_('Valid date to'), blank=True, null=True, db_index=True)
    reservation_limit_days = models.IntegerField(verbose_name=_('Reservation limit days'), blank=True, null=True)
    daily_limit = models.IntegerField(verbose_name=_('Daily limit'), blank=True, null=True)
    global_limit = models.IntegerField(verbose_name=_('Global limit'), blank=True, null=True)
    restrictions = models.TextField(verbose_name=_('Restrictions'), blank=True, null=True)
    min_purchase_amount = models.DecimalField(verbose_name=_('Min purchase amount'), decimal_places=2, max_digits=10,
                                              db_index=True, blank=True, null=True)
    # verification_type = models.CharField(verbose_name=_('Verification type'), max_length=16, choices=VERIFICATION_TYPES, blank=True, null=True)
    exclusive = models.BooleanField(default=False)
    product_range = models.TextField(verbose_name=_('Product range'), blank=True, null=True)
    import_name = models.CharField(verbose_name=_('Import name'), max_length=100, blank=True, null=True)
    imported_at = models.DateTimeField(verbose_name=_('Imported at'), blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    deleted_at = models.DateTimeField(verbose_name=_('Deleted at'), blank=True, null=True)
    upselling = models.ManyToManyField("self", blank=True, null=True, symmetrical=False,
                                       through=Upselling, related_name='rebates_upselling')
    crosselling = models.ManyToManyField("self", blank=True, null=True, symmetrical=False,
                                         through=Crosselling, related_name='rebates_crosseling')
    objects = RebateManager()

    class Meta:
        verbose_name = _("Rebate")
        verbose_name_plural = _("Rebates")
        ordering = REBATE_ORDERING_FIELD

    def __unicode__(self):
        if not self.pk:
            return '<new rebate>'
        return u"#%s: %s / %s" % (self.pk, unicode(self.shop), unicode(self.title or self.content or '(without text)'))

    def delete(self, using=None):
        self.deleted_at = datetime.datetime.now()
        self.save()

    @property
    def reservation_date_to(self):
        return datetime.datetime.now() + timedelta(days=self.reservation_limit_days or REBATE_RESERVATION_LIMIT_DAYS)

    @property
    def can_display(self):
        if self.show_date_from and self.show_date_to:
            return True if self.show_date_from < timezone.now() < self.show_date_to else False
        else:
            return True

    @property
    def is_valid(self):
        if self.valid_date_from and self.valid_date_to:
            now = datetime.datetime.now()
            return True if self.valid_date_from < now < self.valid_date_to else False
        else:
            return True

    @property
    def can_create(self):
        return self.can_display  # and not self.exhausted

    @property
    def exhausted(self):
        return not self.remaining_daily_limit or not self.remaining_global_limit

    @property
    def only_for_vip(self):
        return True if self.type and self.type.symbol == Type.VIP_SYMBOL else False

    @property
    def remaining_daily_limit(self):
        if not self.daily_limit:
            return 0

        now = datetime.datetime.now()
        today_count = self.coupons.filter(created_at__year=now.year,
                                          created_at__month=now.month,
                                          created_at__day=now.day).count()
        limit = self.daily_limit - today_count
        return limit if limit >= 0 else 0

    @property
    def remaining_global_limit(self):
        if not self.global_limit:
            return 0

        limit = self.global_limit - self.coupons.count()
        return limit if limit >= 0 else 0

    @property
    def reason(self):
        if not self.can_display:
            return _('Coupon can not be displayed for current date.')
        if self.exhausted:
            if not self.remaining_daily_limit:
                return _('Daily limit reached')
            if not self.remaining_global_limit:
                return _('Global limit reached')

    @property
    def is_upselling(self):
        return self.upselling_rebates.all().exists()

    @property
    def is_crosseling(self):
        return self.crosselling_rebates.all().exists()

    @property
    def images(self):
        return self.rebateimage_set.all()

    @property
    def archived_at(self):
        return self.show_date_to + datetime.timedelta(seconds=1) if not self.can_display else None


class RebateImage(models.Model):
    rebate = models.ForeignKey(Rebate)
    image = models.ImageField(upload_to=upload_to)

    class Meta:
        verbose_name = _('Rebate image')
        verbose_name_plural = _('Rebate images')

    def __unicode__(self):
        return self.image.url


class CouponManager(models.Manager):
    def get_queryset(self):
        qs = super(CouponManager, self).get_queryset()
        return qs.exclude(rebate__deleted_at__isnull=False)

    def avail(self):
        qs = super(CouponManager, self).get_queryset()
        return qs


class Coupon(models.Model):
    ORIGIN_RABATOMAT = 'rabatomat'
    ORIGIN_MOBILE = 'mobile'
    ORIGIN_WEBSITE = 'website'
    ORIGIN_VERIFIER = 'verifier'

    QRCODE_TYPE = 'coupon'
    QRCODE_SIZE = 200

    session = models.ForeignKey('profile.Session', related_name="coupons", null=True, blank=True)
    card = models.ForeignKey('card.Card', related_name="coupons", null=True, blank=True)
    rebate = models.ForeignKey('rebate.Rebate', related_name='coupons', db_index=True)
    parent = models.ForeignKey('self', related_name='coupons', blank=True, null=True)
    origin = models.CharField(blank=True, null=True, max_length=16, db_index=True)
    code = EAN13Field(verbose_name=_('Verification code'), null=True, blank=True, db_index=True)
    uuid = UUIDField(db_index=True, unique=True, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    reservation_date_to = models.DateTimeField(verbose_name=_('Reservation date to'), blank=True, null=True)

    was_used = models.BooleanField(default=False)
    objects = CouponManager()

    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')
        ordering = ['-created_at']

    def __unicode__(self):
        return u"Coupon #%s from %s" % (self.pk, unicode(self.rebate))

    @property
    def is_used(self):
        return True if self.verification and not self.upselling else False

    @property
    def verified(self):
        return self.is_used

    @property
    def upselling(self):
        upselling = {}
        offers = self.rebate.upselling.through.objects.active(self.created_at)
        for offer in offers:
            coupons = Coupon.objects.filter(parent=self, rebate=offer)
            if coupons:
                continue
            else:
                upselling = offer
                break
        return upselling

    @property
    def crosselling(self):
        return self.rebate.crosselling.through.objects.active(self.created_at).first()

    @property
    def verification(self):
        try:
            return self.verifications.get(status=True)
        except Verification.DoesNotExist:
            return None

    @property
    def verification_required(self):
        return False if self.verification else True

    @property
    def verification_type(self):
        return 'code_amount' if self.rebate.program else 'tap'

    @property
    def qrcode_str(self):
        return QRCODE_STR.format(type=self.QRCODE_TYPE, uuid=self.uuid)

    @property
    def qrcode_img(self):
        return QRCODE_IMG.format(size=200, qrcode_str=self.qrcode_str)

    @property
    def get_card(self):
        return self.session.profile.card_set.first() if self.session.profile else None

    @classmethod
    def get_grouped_stats(cls):
        purchases = cls.objects.all().order_by('created_at')
        grouped = itertools.groupby(purchases, lambda x: x.created_at.strftime("%Y-%m-%d"))
        return [(day, len(list(by_this_day))) for day, by_this_day in grouped]


# Signals
def coupon_post_save(sender, instance, created, **kwargs):
    """Create a user profile when a new user account is created"""
    if created == True:
        # https:/
        # /pm.sizeof.pl/issues/5653
        if instance.origin == Coupon.ORIGIN_MOBILE:
            instance._bookmark, created = Bookmark.objects.get_or_create(
                content_type=bookmark_utils.get_content_type_for_model(instance.rebate),
                object_id=instance.rebate.pk,
                user=instance.session.profile.user
            )
            instance._bookmark.key = 'from-{}-coupon-{}'.format(instance.origin, instance.pk)
            instance._bookmark.save()


def _handle_image_signal(sender, instance, created, **kwargs):
    mobile_image = generate_mobileimage(instance, instance.content, upload_to='rebate')
    instance._meta.model.objects.filter(pk=instance.pk) \
        .update(mobile_image=mobile_image)


def rebate_post_save(sender, instance, created, **kwargs):
    _handle_image_signal(sender, instance, created, **kwargs)


def rebateimage_post_save(sender, instance, created, **kwargs):
    _handle_image_signal(sender, instance.rebate, created, **kwargs)

post_save.connect(coupon_post_save, sender=Coupon)
post_save.connect(rebate_post_save, sender=Rebate)
post_save.connect(rebateimage_post_save, sender=RebateImage)

bookmarks.register(Rebate)

reversion.register(Rebate)
reversion.register(RebateImage)
