# -*- coding: utf-8 -*-
# !/usr/bin/env python

"""
Adapted from django-constance, which itself was adapted from django-adminfiles.
"""

import os
import sys

here = os.path.dirname(os.path.abspath(__file__))
parent = os.path.dirname(here)
sys.path[0:0] = [here, parent]

from django.conf import settings

settings.configure(
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'memory',  # Or path to database file if using sqlite3.
            'USER': 'sizeof',  # Not used with sqlite3.
            'PASSWORD': 'sizeof',  # Not used with sqlite3.
            'HOST': '',  # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',  # Set to empty string for default. Not used with sqlite3.
        }
    },

    INSTALLED_APPS=[
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.humanize',
        'django.contrib.admin',
        'rabatomat_rebates.rebate',
    ],
    ROOT_URLCONF='rabatomat_rebates.rebate.tests.test_urls',
    SECRET_KEY="it's a secret to everyone",
    SITE_ID=1,
)

from django.test.runner import DiscoverRunner


def main():
    runner = DiscoverRunner(failfast=True, verbosity=1)
    failures = runner.run_tests(['loyalty'], interactive=True)
    sys.exit(failures)


if __name__ == '__main__':
    main()