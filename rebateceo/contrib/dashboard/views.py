# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
# from rebateceo.contrib.adv.models import Banner
# from rebateceo.contrib.rebate.models import Rebate, Coupon
# from rebateceo.contrib.card.models import Card, Scan
from rebateceo.contrib.stand.models import Stand
# from rebateceo.contrib.loyalty.models import Program, Amount
# from rebateceo.contrib.verifier.models import Verifier
# from rebateceo.contrib.member.models import Member
from rebateceo.contrib.shop.models import Shop, Category
# from rebateceo.contrib.receipt.models import Receipt
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage


class IndexView(TemplateView):
    template_name = 'dashboard/index.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):

        # Publish redis message
        RedisPublisher(facility='rebateceo', broadcast=True).publish_message(RedisMessage('RebateCEO say hello!'))
        return super(IndexView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update({
            'stats': [
                # [_('Loyalty Cards'), 'red-intense', 'fa fa-bar-chart-o', len(Card.objects.all().values('pk'))],
                # [_('Card Scans'), 'green-haze', 'fa fa-sign-in', len(Scan.objects.all().values('pk'))],
                # [_('Registered Amounts'), 'blue-madison', 'fa fa-barcode',
                #     '%s' % sum(Amount.objects.values_list('amount', flat=True))],
                # [_('Registered Amounts'), 'blue-madison', 'fa fa-barcode',
                #      '%s' % sum(Receipt.objects.values_list('amount', flat=True))],
                # [_('Registered Members'), 'red-intense', 'fa fa-smile-o', len(Member.objects.all().values('pk'))],
                # [_('Loyalty Programs'), 'purple-plum', 'fa fa-heart-o', len(Program.objects.all().values('pk'))],
                # [_('Promotions'), 'blue-madison', 'fa fa-certificate', len(Rebate.objects.all().values('pk'))],
                # [_('Registered Coupons'), 'green', 'fa fa-shopping-cart', len(Coupon.objects.all().values('pk'))],
                [_('Tenants'), 'green-haze', 'fa fa-bar-chart-o', len(Shop.objects.all().values('pk'))],
                [_('Shops Categories'), 'blue-madison', 'fa fa-bar-chart-o', len(Category.objects.all().values('pk'))],
                [_('Stands'), 'blue-madison', 'fa fa-desktop', len(Stand.objects.all().values('pk'))],
                # [_('Advertising Banners'), 'blue-madison', 'fa fa-picture-o', len(Banner.objects.all().values('pk'))],
                # [_('Verifiers'), 'blue-madison', 'fa fa-retweet', len(Verifier.objects.all().values('pk'))]
            ]
        })
        return context
