import base64
from rest_framework.reverse import reverse
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User


class APITestBase(TestCase):
    def setUp(self):
        self.limit = 2
        self.username = 'foo'
        self.password = 'bar'
        self.user = User.objects.create_user(self.username, 'foo@bar.de', self.password)
        self.headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode('{0}:{1}'.format(self.username, self.password))
        }
        self.client = Client()


class APITest(APITestBase):
    """
    bin/backend-test test rebateceo.contrib.api.tests.APITest
    """
    def test_info__GET(self):
        response = self.client.get(reverse('api:info'), {}, **self.headers)
        self.assertIsInstance(response.data, dict, "API GET Info test failed")

    def test_info__POST(self):
        response = self.client.post(reverse('api:info'), {}, **self.headers)
        self.assertIsInstance(response.data, dict, "API POST Info test failed")

    def test_ping__POST(self):
        response = self.client.post(reverse('api:ping'), {}, **self.headers)
        self.assertIsInstance(response.data, dict, "API PING test failed")

    def test_code_list(self):
        response = self.client.get(reverse('api:code-list'), {}, **self.headers)
        self.assertIsInstance(response.data, list, "API code list test failed")

    def test_contentype_list(self):
        response = self.client.get(reverse('api:contenttype-list'), {}, **self.headers)
        self.assertIsInstance(response.data, list, "API code list test failed")
