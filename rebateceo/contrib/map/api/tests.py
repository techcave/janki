from rest_framework.reverse import reverse
from ..factories import PointFactory, MapFactory, LocaleFactory, EdgeFactory, TypeFactory
from ...api.tests import APITestBase


class PointTest(APITestBase):
    def test_list(self):
        limit = 100
        items = [PointFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:map:point-list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Point list test failed")


class MapTest(APITestBase):
    def test_list(self):
        limit = 2
        items = [MapFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:map:map-list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Map list test failed")


class LocaleTest(APITestBase):
    def test_list(self):
        limit = 2
        items = [LocaleFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:map:locale-list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Locale list test failed")


class EdgeTest(APITestBase):
    def test_list(self):
        limit = 20
        items = [EdgeFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:map:edge-list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Edge list test failed")


class TypeTest(APITestBase):
    def test_list(self):
        limit = 20
        items = [TypeFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:map:type-list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Type list test failed")