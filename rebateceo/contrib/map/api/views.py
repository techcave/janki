# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import json
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.generics import (ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView,
                                     RetrieveAPIView, GenericAPIView)
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rebateceo.contrib.api.permissions import IsLocalizatorPermission, ApiIsEnabledPermission
from ..models import Point, Edge, Map, Locale, Type
from .serializers import (PointSerializer, PointModelSerializer, EdgeSerializer, LocaleSerializer, PointBulkSerializer,
                          LocaleBulkSerializer, MapSerializer, TypeSerializer)

from django.http import JsonResponse
from django.views.generic import View


class AuthMixin(object):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

#
# Point
#
class PointMixin(object):
    serializer_class = PointSerializer
    model = Point


class PointListView(PointMixin, ListAPIView):
    queryset = Point.objects.select_related('type').prefetch_related('edges_a')

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class PointBulkCreateView(AuthMixin, GenericAPIView):
    """This view uses custom serializer!!"""
    serializer_class = PointBulkSerializer
    model = Point

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.POST.get('data'))
        except ValueError:
            raise ValidationError('"data" param is correct json?!')
        else:
            items = []
            for item in data:
                instance, created = self.model.objects.get_or_create(**item)
                item = PointSerializer(instance=instance).data
                item.update({'_created': created})
                items.append(item)
            return Response(items)


class PointCreateView(AuthMixin, CreateAPIView):
    """This view uses custom serializer!!"""
    serializer_class = PointModelSerializer
    model = Point


class PointUpdateView(AuthMixin, UpdateAPIView):
    """This view uses custom serializer!!"""
    serializer_class = PointModelSerializer
    model = Point


class PointDeleteView(PointMixin, AuthMixin, DestroyAPIView):
    pass


class PointDetailView(PointMixin, AuthMixin, RetrieveAPIView):
    pass

#
# Edges
#
class EdgeMixin(object):
    serializer_class = EdgeSerializer
    model = Edge


class EdgeListView(EdgeMixin, ListAPIView):
    queryset = Edge.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class EdgeSimpleListView(View):

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)

    def get(self, request, *args, **kwargs):
        from django.conf import settings
        settings.DEBUG = True
        from django.db import connection

        edges = Edge.objects.select_related('point_a', 'point_b')

        result = []
        for edge in edges:
            item = {'pk': edge.pk}
            item['point_a'] = edge.point_a.pk
            item['point_b'] = edge.point_b.pk
            item['b_to_a_relation'] = edge.b_to_a_relation if edge.b_to_a_relation else None
            item['length'] = edge.length if edge.length else None
            item['direction'] = edge.direction if type(edge.direction) == int else None
            result.append(item)
        # print(len(connection.queries))
        return JsonResponse(result, safe=False)


class EdgeCreateView(EdgeMixin, AuthMixin, CreateAPIView):
    pass


class EdgeUpdateView(EdgeMixin, AuthMixin, UpdateAPIView):
    pass


class EdgeDeleteView(EdgeMixin, AuthMixin, DestroyAPIView):
    pass


class EdgeDetailView(EdgeMixin, AuthMixin, RetrieveAPIView):
    pass


#
# Locale
#
class LocaleMixin(object):
    serializer_class = LocaleSerializer
    model = Locale


class LocaleListView(LocaleMixin, ListAPIView):
    queryset = Locale.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class LocaleCreateView(LocaleMixin, AuthMixin, CreateAPIView):
    pass


class LocaleBulkCreateView(AuthMixin, GenericAPIView):
    """This view uses custom serializer!!"""
    serializer_class = LocaleBulkSerializer
    model = Locale

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = set(request.POST.get('data').split(','))
        if not data:
            raise ValidationError('"data" param is required!')
        items = []
        for item in data:
            instance, created = self.model.objects.get_or_create(symbol=item.strip())
            item = LocaleSerializer(instance=instance).data
            item.update({'_created': created})
            items.append(item)
        return Response(items)


class LocaleUpdateView(LocaleMixin, AuthMixin, UpdateAPIView):
    pass


class LocaleBulkUpdateView(AuthMixin, GenericAPIView):
    """This view uses custom serializer!!"""
    serializer_class = LocaleBulkSerializer
    model = Locale

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.POST.get('data'))
        except ValueError:
            raise ValidationError('"data" param is correct json?!')
        else:
            items = []
            for item in data:
                instance = self.model.objects.get(symbol=item.get('symbol'))
                for point in item.get('points'):
                    instance.points.add(Point.objects.get(pk=point))
                instance.save()
                item = LocaleSerializer(instance=instance).data
                items.append(item)
            return Response(items)


class LocaleDeleteView(LocaleMixin, AuthMixin, DestroyAPIView):
    pass


class LocaleDetailView(LocaleMixin, AuthMixin, RetrieveAPIView):
    pass


#
# Map
#
class MapMixin(object):
    queryset = Map.objects.all()
    serializer_class = MapSerializer
    model = Map


class MapListView(MapMixin, AuthMixin, ListAPIView):

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class MapCreateView(MapMixin, AuthMixin, CreateAPIView):
    pass


class MapUpdateView(MapMixin, AuthMixin, UpdateAPIView):
    pass


class MapDeleteView(MapMixin, AuthMixin, DestroyAPIView):
    pass


class MapDetailView(MapMixin, AuthMixin, RetrieveAPIView):
    pass


#
# Type
#
class TypeMixin(object):
    serializer_class = TypeSerializer
    model = Type


class TypeListView(TypeMixin, ListAPIView):
    queryset = Type.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class TypeCreateView(TypeMixin, AuthMixin, CreateAPIView):
    pass


class TypeUpdateView(TypeMixin, AuthMixin, UpdateAPIView):
    pass


class TypeDeleteView(TypeMixin, AuthMixin, DestroyAPIView):
    pass


class TypeDetailView(TypeMixin, AuthMixin, RetrieveAPIView):
    pass