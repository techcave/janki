# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf.urls import patterns, url
from .views import (PointListView, PointCreateView, PointDeleteView, PointDetailView, PointUpdateView,
                    PointBulkCreateView, EdgeListView, EdgeSimpleListView, EdgeCreateView, EdgeDeleteView, EdgeDetailView, EdgeUpdateView,
                    LocaleListView, LocaleCreateView, LocaleBulkCreateView, LocaleBulkUpdateView, LocaleDeleteView,
                    LocaleDetailView, LocaleUpdateView, TypeListView, TypeCreateView, TypeDeleteView, TypeDetailView,
                    TypeUpdateView, MapListView, MapCreateView, MapDeleteView, MapDetailView, MapUpdateView,)
from django.views.decorators.csrf import csrf_exempt


urlpatterns = patterns('',
    # Points editor
    url(r'^point/list/$', PointListView.as_view(), name='point-list'),
    url(r'^point/create/$', PointCreateView.as_view(), name='point-create'),
    url(r'^point/bulk-create/$', PointBulkCreateView.as_view(), name='point-bulk-create'),
    url(r'^point/(?P<pk>\d+)/delete/$', PointDeleteView.as_view(), name='point-delete'),
    url(r'^point/(?P<pk>\d+)/detail/$', PointDetailView.as_view(), name='point-detail'),
    url(r'^point/(?P<pk>\d+)/update/$', PointUpdateView.as_view(), name='point-update'),
    # Edges editor
    url(r'^edge/list/simple/$', EdgeListView.as_view(), name='edge-simple'),
    url(r'^edge/list/$', csrf_exempt(EdgeSimpleListView.as_view()), name='edge-list'),
    url(r'^edge/create/$', EdgeCreateView.as_view(), name='edge-create'),
    url(r'^edge/(?P<pk>\d+)/delete/$', EdgeDeleteView.as_view(), name='edge-delete'),
    url(r'^edge/(?P<pk>\d+)/detail/$', EdgeDetailView.as_view(), name='edge-detail'),
    url(r'^edge/(?P<pk>\d+)/update/$', EdgeUpdateView.as_view(), name='edge-update'),
    # Locales editor
    url(r'^locale/list/$', LocaleListView.as_view(), name='locale-list'),
    url(r'^locale/create/$', LocaleCreateView.as_view(), name='locale-create'),
    url(r'^locale/bulk-create/$', LocaleBulkCreateView.as_view(), name='locale-bulk-create'),
    url(r'^locale/bulk-update/$', LocaleBulkUpdateView.as_view(), name='locale-bulk-update'),
    url(r'^locale/(?P<pk>\d+)/delete/$', LocaleDeleteView.as_view(), name='locale-delete'),
    url(r'^locale/(?P<pk>\d+)/detail/$', LocaleDetailView.as_view(), name='locale-detail'),
    url(r'^locale/(?P<pk>\d+)/update/$', LocaleUpdateView.as_view(), name='locale-update'),
    # Types editor
    url(r'^type/list/$', TypeListView.as_view(), name='type-list'),
    url(r'^type/create/$', TypeCreateView.as_view(), name='type-create'),
    url(r'^type/(?P<pk>\d+)/delete/$', TypeDeleteView.as_view(), name='type-delete'),
    url(r'^type/(?P<pk>\d+)/detail/$', TypeDetailView.as_view(), name='type-detail'),
    url(r'^type/(?P<pk>\d+)/update/$', TypeUpdateView.as_view(), name='type-update'),
    # Map editor
    url(r'^map/list/$', MapListView.as_view(), name='map-list'),
    url(r'^map/create/$', MapCreateView.as_view(), name='map-create'),
    url(r'^map/(?P<pk>\d+)/delete/$', MapDeleteView.as_view(), name='map-delete'),
    url(r'^map/(?P<pk>\d+)/detail/$', MapDetailView.as_view(), name='map-detail'),
    url(r'^map/(?P<pk>\d+)/update/$', MapUpdateView.as_view(), name='map-update'),
)