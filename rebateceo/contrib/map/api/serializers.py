# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from itertools import chain
from rest_framework import serializers
from ..models import Point, Edge, Type, Map, Locale, MapSource, MapSymbol


class EdgeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Edge
        fields = ['pk', 'point_a', 'point_b', 'b_to_a_relation', 'length', 'direction']


class PointSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    edges_a = EdgeSerializer(many=True)

    class Meta:
        model = Point
        fields = ['pk', 'x', 'y', 'level_id', 'type', 'edges_a']


class PointModelSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Point
        fields = ['pk', 'x', 'y', 'level_id', 'type']


class PointBulkSerializer(serializers.Serializer):
    data = serializers.CharField()

    class Meta:
        fields = ['data']


class TypeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Type
        fields = ['pk', 'title', 'symbol']


class MapSymbolSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = MapSymbol
        fields = ['pk', 'symbol', 'description', 'created_at', 'updated_at']


class MapSourceSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = MapSource
        fields = ['pk', 'symbol_key', 'file', 'description', 'created_at', 'updated_at']


class MapSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    mapsource_set = MapSourceSerializer(many=True)

    class Meta:
        model = Map
        fields = ['pk', 'name', 'description', 'created_at', 'updated_at', 'mapsource_set', 'mall_set']


class LocaleSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Locale
        fields = ['pk', 'symbol', 'points']


class LocaleBulkSerializer(serializers.Serializer):
    data = serializers.CharField()

    class Meta:
        fields = ['data']


