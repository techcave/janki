# -*- coding: utf-8 -*-
import os
import datetime
import random
import factory
from decimal import Decimal
from django.conf import settings
from django.contrib.auth.models import User, Group
from factory import django
from factory import fuzzy
from .models import Point, Type, Map, Locale, Edge


class TypeFactory(django.DjangoModelFactory):
    FACTORY_FOR = Type

    title = factory.Sequence(lambda n: 'type #%s' % n)


class MapFactory(django.DjangoModelFactory):
    FACTORY_FOR = Map

    name = factory.Sequence(lambda n: 'map-name-%s' % n)


class PointFactory(django.DjangoModelFactory):
    FACTORY_FOR = Point

    x = factory.LazyAttribute(lambda o: unicode(random.randrange(5, 2000)))
    y = factory.LazyAttribute(lambda o: unicode(random.randrange(5, 2000)))
    level_id = factory.LazyAttribute(lambda o: unicode(random.randrange(5, 200)))
    type = factory.LazyAttribute(lambda o: TypeFactory())


class LocaleFactory(django.DjangoModelFactory):
    FACTORY_FOR = Locale

    symbol = factory.Sequence(lambda n: 'locale %s' % n)


class EdgeFactory(django.DjangoModelFactory):
    FACTORY_FOR = Edge

    point_a = factory.LazyAttribute(lambda o: PointFactory())
    point_b = factory.LazyAttribute(lambda o: PointFactory())
    length = factory.Sequence(lambda n: Decimal(random.randrange(1, 1000)))