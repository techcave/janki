
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import os
import datetime
from optparse import make_option
from django.core.management import BaseCommand
from ...models import Type


class Command(BaseCommand):
    help = u'Importuje typy punktow'

    option_list = BaseCommand.option_list + (
        make_option('--hard-reset',
            action='store_true',
            dest='hard_reset',
            default=False,
            help='Reset all cards'),
    )

    def handle(self, *args, **options):
        if options.get('hard_reset'):
            Type.objects.all().delete()
            print "All types have been cleaned"
            return

        try:
            filepath = args[0]
        except IndexError:
            filepath = os.path.dirname(__file__) + '/../../fixtures/types.csv'

        file = open(filepath).readlines()
        for line in file[1:]:
            row = line.split(';')
            symbol = row[0]
            id = row[1]
            title = "Type #{}".format(symbol)

            item, created = Type.objects.get_or_create(id=id)
            item.title = title
            item.symbol = symbol
            item.save()
            print 'type: ', item, created

        print "Imported or updated %s items" % len(Type.objects.all())