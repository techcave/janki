# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0006_map_is_current'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='type',
            options={'ordering': ['pk'], 'verbose_name': 'Type', 'verbose_name_plural': 'Types'},
        ),
    ]
