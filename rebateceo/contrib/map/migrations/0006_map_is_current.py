# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0005_auto_20150327_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='is_current',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
