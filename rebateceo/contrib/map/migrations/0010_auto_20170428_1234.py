# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0009_auto_20170315_1110'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='type',
            options={'ordering': ['pk'], 'verbose_name': 'Typ', 'verbose_name_plural': 'Typy'},
        ),
    ]
