# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0002_pointdescription'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='type',
            options={'ordering': ['pk'], 'verbose_name': 'Typ', 'verbose_name_plural': 'Typy'},
        ),
    ]
