# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.map.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Edge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('b_to_a_relation', models.SmallIntegerField(null=True, blank=True)),
                ('length', models.IntegerField()),
                ('direction', models.SmallIntegerField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Edge',
                'verbose_name_plural': 'Edges',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Locale',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('symbol', models.CharField(unique=True, max_length=255, db_index=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Locale',
                'verbose_name_plural': 'Locales',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Map',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('swf', models.FileField(upload_to=rebateceo.contrib.map.models.upload_to)),
                ('description', models.TextField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Map',
                'verbose_name_plural': 'Maps',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('x', models.IntegerField()),
                ('y', models.IntegerField()),
                ('level_id', models.SmallIntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Point',
                'verbose_name_plural': 'Points',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=255)),
                ('icon', models.ImageField(null=True, upload_to=rebateceo.contrib.map.models.upload_to, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Type',
                'verbose_name_plural': 'Types',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='point',
            name='type',
            field=models.ForeignKey(blank=True, to='map.Type', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='locale',
            name='points',
            field=models.ManyToManyField(to='map.Point', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='edge',
            name='point_a',
            field=models.ForeignKey(related_name='edges_a', to='map.Point'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='edge',
            name='point_b',
            field=models.ForeignKey(related_name='edges_b', to='map.Point'),
            preserve_default=True,
        ),
    ]
