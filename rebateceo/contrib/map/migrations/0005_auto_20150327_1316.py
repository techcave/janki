# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.map.models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0004_auto_20150312_1238'),
    ]

    operations = [
        migrations.CreateModel(
            name='MapSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=rebateceo.contrib.map.models.upload_to)),
                ('description', models.TextField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('map', models.ForeignKey(blank=True, to='map.Map', null=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Map Source',
                'verbose_name_plural': 'Map Sources',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MapSymbol',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('symbol', models.CharField(unique=True, max_length=64)),
                ('description', models.TextField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Map Symbol',
                'verbose_name_plural': 'Map Symbols',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='mapsource',
            name='symbol',
            field=models.ForeignKey(blank=True, to='map.MapSymbol', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='mapsource',
            unique_together=set([('map', 'symbol')]),
        ),
        migrations.RemoveField(
            model_name='map',
            name='swf',
        ),
        migrations.AddField(
            model_name='map',
            name='name',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='type',
            name='symbol',
            field=models.CharField(max_length=255, unique=True, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='map',
            name='description',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
