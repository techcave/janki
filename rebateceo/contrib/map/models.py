# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
import reversion
from django.utils.encoding import python_2_unicode_compatible, smart_unicode
from django.conf import settings
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.db import models


def upload_to(instance, filename):
    return '/'.join(['map', str(filename)])


@python_2_unicode_compatible
class Type(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=True)
    symbol = models.CharField(max_length=255, unique=True, blank=True, null=True)
    icon = models.ImageField(upload_to=upload_to, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"{} - {}".format(self.pk, self.symbol)

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Type')
        verbose_name_plural = _('Types')
        ordering = ['pk']


@python_2_unicode_compatible
class Point(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    level_id = models.SmallIntegerField()
    type = models.ForeignKey(Type, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.pk) + ':' + str(self.x) + ',' + str(self.y) + ',' + str(self.level_id) + ',' + str(self.type)

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Point')
        verbose_name_plural = _('Points')
        ordering = ['pk']


@python_2_unicode_compatible
class PointDescription(models.Model):
    point = models.ForeignKey(Point)
    value = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.point

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Point Description')
        verbose_name_plural = _('Points Descriptions')


class Edge(models.Model):
    point_a = models.ForeignKey(Point, related_name='edges_a', db_index=True)
    point_b = models.ForeignKey(Point, related_name='edges_b', db_index=True)
    b_to_a_relation = models.SmallIntegerField(blank=True, null=True)
    length = models.IntegerField()
    direction = models.SmallIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        verbose_name = _('Edge')
        verbose_name_plural = _('Edges')


@python_2_unicode_compatible
class Locale(models.Model):
    symbol = models.CharField(max_length=255, unique=True, db_index=True)
    points = models.ManyToManyField(Point, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.symbol

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Locale')
        verbose_name_plural = _('Locales')
        ordering = ['pk']


@python_2_unicode_compatible
class Map(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_current = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Map')
        verbose_name_plural = _('Maps')
        ordering = ['pk']


@python_2_unicode_compatible
class MapSymbol(models.Model):
    symbol = models.CharField(max_length=64, unique=True)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.symbol)

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Map Symbol')
        verbose_name_plural = _('Map Symbols')
        ordering = ['pk']


@python_2_unicode_compatible
class MapSource(models.Model):
    map = models.ForeignKey(Map, blank=True, null=True)
    symbol = models.ForeignKey(MapSymbol, blank=True, null=True)
    file = models.FileField(upload_to=upload_to)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}: {}".format(str(self.symbol), str(self.file))

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Map Source')
        verbose_name_plural = _('Map Sources')
        ordering = ['pk']
        unique_together = ['map', 'symbol']

    @property
    def symbol_key(self):
        return self.symbol.symbol


reversion.register(Type)
reversion.register(Point)
reversion.register(Map)
reversion.register(MapSymbol)
reversion.register(MapSource)
reversion.register(Locale)
reversion.register(Edge)
