# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django import forms
from django.contrib import messages
from django.conf import settings
from django.contrib import admin
from . import models
from .forms import TypeFormAdmin


class EdgeAdmin(admin.ModelAdmin):
    list_display = ('point_a', 'point_b', 'b_to_a_relation', 'length', 'direction', 'id',)
    list_filter = ('direction',)
    search_fields = ('point_a', 'point_b', 'b_to_a_relation', 'length', 'direction', 'id',)


class PointAdmin(admin.ModelAdmin):
    list_display = ('id', 'x', 'y', 'level_id', 'type')
    list_filter = ('level_id', 'type')
    search_fields = ('id', 'x', 'y', 'level_id', 'type')


class LocaleAdmin(admin.ModelAdmin):
    list_display = ('symbol',)
    search_fields = ('id', 'symbol')


class MapSourceInline(admin.options.TabularInline):
    model = models.MapSource
    extra = 5


class MapAdmin(admin.ModelAdmin):
    inlines = [MapSourceInline]
    list_display = ('name', 'id', 'description', 'created_at', 'updated_at', 'is_current', 'get_sources')
    search_fields = ('name', 'description')

    def get_sources(self, instance):
        return u"</br>".join(['{0}: <a href="{1}" target="_blank">{1}</a>'.format(x.symbol, x.file.url) for x in instance.mapsource_set.all()])
    get_sources.allow_tags = True


class MapSymbolAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'description', 'created_at', 'updated_at')
    search_fields = ('symbol', 'description')
    list_filter = ('symbol',)


class MapSourceAdmin(admin.ModelAdmin):
    list_display = ('file', 'symbol', 'description', 'created_at', 'updated_at')
    search_fields = ('file', 'symbol', 'description')
    list_filter = ('symbol',)


class TypeAdmin(admin.ModelAdmin):
    form = TypeFormAdmin
    list_display = ('id', 'title', 'symbol')
    search_fields = ('title',)

    def save_model(self, request, obj, form, change):
        custom_id = form.cleaned_data.get('custom_id')
        if custom_id:
            obj.pk = custom_id
            obj.created_at = datetime.datetime.today()
        obj = super(TypeAdmin, self).save_model(request, obj, form, change)
        return obj


admin.site.register(models.Type, TypeAdmin)
admin.site.register(models.Map, MapAdmin)
admin.site.register(models.MapSymbol, MapSymbolAdmin)
admin.site.register(models.MapSource, MapSourceAdmin)
admin.site.register(models.Locale, LocaleAdmin)
admin.site.register(models.Edge, EdgeAdmin)
admin.site.register(models.Point, PointAdmin)
