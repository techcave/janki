# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from rest_framework import serializers
from ..models import Contest, Banner


class BannerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Banner


    # def to_representation(self, obj):
    #     return obj.contest.state


class ContestSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    banner_set = serializers.SerializerMethodField() #BannerSerializer(many=True)
    now = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()

    class Meta:
        model = Contest
        fields = ['pk', 'title', 'slug', 'image', 'publication_date', 'publication_end_date', 'active', 'state',
                  'banner_set', 'rules_file', 'rules_desc', 'success_banner', 'min_amount', 'now']

    def get_banner_set(self, object):
        queryset = Banner.objects.all().filter(contest=object, for_state=object.state)
        serializer = BannerSerializer(instance=queryset, many=True, context=self.context)
        return serializer.data

    # def get_fields(self, *args, **kwargs):
    #     fields = super(ContestSerializer, self).get_fields(*args, **kwargs)
    #     fields['banner_set'].queryset = self.context['view'].request.user#None#permitted_objects(self.context['view'].request.user, fields['purchaser'].queryset)
    #     return fields
    #
    def __init__(self, *args, **kwargs):
        super(ContestSerializer, self).__init__(*args, **kwargs)
        self.fields['banner_set'].queryset = None #User._default_manager.filter(pk=request_user.pk)


    def get_now(self, record):
        return str(datetime.datetime.now())

    def get_state(self, record):
        return record.state