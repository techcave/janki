import datetime
from rest_framework.reverse import reverse
from rebateceo.contrib.api import codes
from django.utils import timezone
from ..factories import ContestFactory
from ...api.tests import APITestBase


# # Create a constant with a fixed value for utcnow
# NOW = timezone.now()
#
# # Define a test method to replace datetime.datetime.utcnow
# def now_fixed_value():
#     return datetime.datetime(2017, 4, 1, 0, 0, 0)


class ContestTest(APITestBase):

    # def setUp(self):
    #     self.real_now = datetime.datetime.now()
    #     timezone.now = now_fixed_value
    #
    # def tearDown(self):
    #     # Undo the monkey patch and replace the real version of utcnow
    #     timezone.now = self.real_now

    def test_list(self):
        limit = 3
        items = [ContestFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:contest:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Contest list test failed")

    def test_current(self):
        contest = ContestFactory(active=True)
        response = self.client.get(reverse('api:contest:current'), {}, **self.headers)
        self.assertIsNotNone(response.data['now'])

    def test_current_not_found(self):
        contest = ContestFactory(active=False)
        response = self.client.get(reverse('api:contest:current'), {}, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.CURRENT_CONTEST_NOT_FOUND)

    # def test_current_STATE_BEFORE(self):
    #     contest = ContestFactory(active=False)
    #     response = self.client.get(reverse('api:contest:current'), {}, **self.headers)
    #     self.assertEqual(response.data['exception']['code'], codes.CURRENT_CONTEST_NOT_FOUND)

    # def test_current_STATE_DURING(self):
    #     contest = ContestFactory(active=False)
    #     response = self.client.get(reverse('api:contest:current'), {}, **self.headers)
    #     self.assertEqual(response.data['exception']['code'], codes.CURRENT_CONTEST_NOT_FOUND)
    #
    # def test_current_STATE_AFTER(self):
    #     contest = ContestFactory(active=False)
    #     response = self.client.get(reverse('api:contest:current'), {}, **self.headers)
    #     self.assertEqual(response.data['exception']['code'], codes.CURRENT_CONTEST_NOT_FOUND)
