# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib import messages
from django.contrib.admin import site, ModelAdmin
from django.contrib.admin.options import TabularInline
from . import forms, models


class BannerInlines(TabularInline):
    model = models.Banner
    form = forms.BannerForm
    extra = 5


class ContestAdmin(ModelAdmin):
    inlines = [BannerInlines]
    list_display = ['title', 'active', 'publication_date', 'publication_end_date',
                    'admin_image', 'rules_file', 'rules_desc', 'min_amount']
    list_editable = ['active', 'publication_date', 'publication_end_date',]
    list_filter = ['active', 'publication_date', 'publication_end_date']
    prepopulated_fields = {"slug": ("title",)}

    def save_model(self, request, obj, form, change):
        if form.cleaned_data.get('active'):
            exists = self.model.objects.active().first()
            if exists:
                if exists.pk != obj.pk:
                    obj.active = False
                    messages.error(request, u"UWAGA! Tylko jeden konkurs może byc aktywny w tej wersji systemu!")
        obj.save()
        return obj


site.register(models.Contest, ContestAdmin)
