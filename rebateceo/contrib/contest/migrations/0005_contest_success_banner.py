# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0004_contest_created_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='success_banner',
            field=models.ImageField(help_text=b'Background for results after the user action', upload_to=b'', blank=True),
            preserve_default=True,
        ),
    ]
