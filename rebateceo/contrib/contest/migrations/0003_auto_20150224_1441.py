# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.contest.models


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0002_auto_20150224_1438'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='banner',
            name='rules_desc',
        ),
        migrations.RemoveField(
            model_name='banner',
            name='rules_file',
        ),
        migrations.AddField(
            model_name='contest',
            name='rules_desc',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contest',
            name='rules_file',
            field=models.FileField(max_length=255, null=True, upload_to=rebateceo.contrib.contest.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
