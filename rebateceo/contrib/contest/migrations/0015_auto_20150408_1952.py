# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0014_banner_for_state'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='banner',
            options={'ordering': ['for_state'], 'verbose_name': 'Banner', 'verbose_name_plural': 'Banner'},
        ),
    ]
