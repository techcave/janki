# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.contest.models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0003_auto_20150224_1402'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.FileField(max_length=255, upload_to=rebateceo.contrib.contest.models.upload_to)),
            ],
            options={
                'verbose_name': 'Banner',
                'verbose_name_plural': 'Banner',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('slug', models.SlugField(max_length=255)),
                ('image', models.FileField(max_length=255, null=True, upload_to=rebateceo.contrib.contest.models.upload_to, blank=True)),
                ('active', models.BooleanField(default=False)),
                ('publication_date', models.DateTimeField(default=rebateceo.contrib.contest.models.granular_now, verbose_name='publication date')),
                ('publication_end_date', models.DateTimeField(help_text='Leave empty if the entry should stay active forever.', null=True, verbose_name='publication end date', blank=True)),
                ('shops', models.ManyToManyField(to='shop.Shop', null=True, blank=True)),
            ],
            options={
                'ordering': ('pk',),
                'verbose_name': 'Contest',
                'verbose_name_plural': 'Contests',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='banner',
            name='contest',
            field=models.ForeignKey(to='contest.Contest'),
            preserve_default=True,
        ),
    ]
