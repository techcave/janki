# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0003_auto_20150224_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 26, 10, 35, 20, 735352), verbose_name='Created at', auto_now_add=True),
            preserve_default=False,
        ),
    ]
