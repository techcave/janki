# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0013_remove_contest_is_current'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='for_state',
            field=models.CharField(default=b'BEFORE', max_length=120, choices=[(b'BEFORE', b'BEFORE'), (b'DURING', b'DURING'), (b'AFTER', b'AFTER')]),
            preserve_default=True,
        ),
    ]
