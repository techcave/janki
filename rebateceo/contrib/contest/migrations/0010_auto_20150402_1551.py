# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0009_contest_min_amount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='publication_date',
            field=models.DateTimeField(verbose_name='publication date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contest',
            name='publication_end_date',
            field=models.DateTimeField(null=True, verbose_name='publication end date', blank=True),
            preserve_default=True,
        ),
    ]
