# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.contest.models


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0007_auto_20150303_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='success_banner',
            field=models.ImageField(help_text=b'Background for results after the user action', max_length=255, upload_to=rebateceo.contrib.contest.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
