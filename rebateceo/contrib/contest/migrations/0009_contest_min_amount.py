# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0008_auto_20150330_2002'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='min_amount',
            field=models.DecimalField(decimal_places=2, max_digits=10, blank=True, help_text='inclusive', null=True, verbose_name='Min amount'),
            preserve_default=True,
        ),
    ]
