# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0012_contest_is_current'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contest',
            name='is_current',
        ),
    ]
