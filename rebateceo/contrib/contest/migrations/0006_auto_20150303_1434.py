# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0005_contest_success_banner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contest',
            name='success_banner',
            field=models.FileField(help_text=b'Background for results after the user action', upload_to=b'', blank=True),
            preserve_default=True,
        ),
    ]
