# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.contest.models


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='rules_desc',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='banner',
            name='rules_file',
            field=models.FileField(max_length=255, null=True, upload_to=rebateceo.contrib.contest.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
