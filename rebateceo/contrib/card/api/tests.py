from rest_framework.reverse import reverse
from .. import factories, models
from ...api.tests import APITestBase


class CardTest(APITestBase):
    """
    bin/backend-test test rebateceo.contrib.card.api.tests.CardTest
    """
    def setUp(self):
        super(CardTest, self).setUp()
        models.Scan.objects.all().delete()
        models.Card.objects.all().delete()
        models.Type.objects.all().delete()

    def test_card_list(self):
        [factories.CardFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:card:list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Card list test failed")

    def test_type_list(self):
        limit = 2
        [factories.TypeFactory() for x in range(0, limit)]
        response = self.client.get(reverse('api:card:type-list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Card type list test failed")

    def test_scan_without_device(self):
        # TODO: assertRaises
        response = self.client.post(reverse('api:card:scan'), {}, **self.headers)
        self.assertEqual(response.status_code, 400)

    def test_scan_stand_does_not_exists(self):
        # TODO: assertRaises
        data = {
            'device': 'xcsdfs-rwedfsdf'
        }
        response = self.client.post(reverse('api:card:scan'), data, **self.headers)
        self.assertEqual(response.status_code, 404)
