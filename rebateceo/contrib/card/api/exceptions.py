# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework import status
from rebateceo.contrib.api.exceptions import APIRebateceoException
from django.utils.translation import ugettext_lazy as _


class CardAPIException(APIRebateceoException):
    code = 'R000'


class DeviceIsRequired(APIRebateceoException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Device param is required')
    code = 'R002'


class StandDoesNotExist(APIRebateceoException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Stand device for deviceid {device} does not exist!')
    code = 'R003'


class ScanCodeValueRequired(APIRebateceoException):
    default_detail = _('Scan "code" param is required')
    code = 'R004'


class ScanCodeBaseException(APIRebateceoException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {
        'title': _('upss... unknown code'),
        'body': _('only codes from loyalty card\n'
                  'or vouchers are valid.\n'
                  'Try again...')
    }
    code = 'R005'


class ScanCodeValueError(ScanCodeBaseException):
    default_detail = _('Wrong format for scan "code"!')
    code = 'R006'


class ScanCodeTypeUnrecognized(ScanCodeBaseException):
    default_detail = _('Unrecognized scan code-type ({code_type})!')
    code = 'R007'


class CardIsNotEAN13(ScanCodeBaseException):
    default_detail = _('Code {code_value} for {code_type} is not EAN13!')
    code = 'R008'


class CardDoesNotExist(ScanCodeBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Card for code {code_value} does not exist!')
    code = 'R009'


class ScanHasNoMethod(ScanCodeBaseException):
    default_detail = _('Scan has no method "{func_name}"')
    code = 'R010'


class CardForCodeDoesNotExist(ScanCodeBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Card for code {code} does not exist!')
    code = 'R011'


class VallsurAPIException(ScanCodeBaseException):
    default_detail = _('Vallsur API exception: {mess} => {url}')
    code = 'R012'


class MemberFromAPIUpdateException(ScanCodeBaseException):
    default_detail = _('Error on Member updating: {mess}')
    code = 'R013'

    def get_data(self, **kwargs):
        return {'member_data': kwargs.get('member_data')}


class VallsurAPITimeoutException(ScanCodeBaseException):
    default_detail = _('Vallsur API timeout exception: {mess} => {url}')
    code = 'R014'


class CardIsNotOlimpia(ScanCodeBaseException):
    default_detail = _('Code {code_value} for {code_type} is Olimpia Card!')
    code = 'R015'


class CardIsNotValid(ScanCodeBaseException):
    default_detail = _('Code {code_value} for {code_type} is not valid!')
    code = 'R016'


class LotteryFinished(APIRebateceoException):
    default_detail = _('Lottery has been finished!')
    code = 'R017'


class LotteryTemporaryDisabled(APIRebateceoException):
    default_detail = _('Lottery has been temporary disabled!')
    code = 'R018'


class CannotScanForNow(APIRebateceoException):
    default_detail = _('Scanning backed has been disabled by program rules!')
    code = 'R019'


class CardHasNotMember(APIRebateceoException):
    default_detail = _('Card has not a member!')
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {
        'title': _(u'Card inactive'),
        'body': _(u'There are two methods to register your card')
    }
    extra_message = {
        u'Send <strong>SMS</strong> with your card number to <strong>608 631 933<strong>',
        u'Fill in form in <strong>Information Office</strong>'
    }
    code = 'R020'


class APIIsDisabled(APIRebateceoException):
    default_detail = _('API is disabled by application regulation rules (see regulation file)!')
    code = 'R021'


class ScanDoesNotExist(APIRebateceoException):
    default_detail = _('Scan for {scan_id} does not exist')
    code = 'R022'


class ScanIdIsRequired(APIRebateceoException):
    default_detail = _('Scan id is required')
    code = 'R023'


class CardHasNoAttempts(CardAPIException):
    default_detail = _('Card {card} has no attempts')
    code = 'R023'


class CardHasBeenBlocked(APIRebateceoException):
    default_detail = _('Card has been blocked!')
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {
        'title': _(u'Card blocked'),
        'body': _(u'More info in Information Office')
    }
    code = 'R024'
