# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django_utils.exceptions import InputValidationError
from rebateceo.contrib.rebate.models import Coupon
from rebateceo.contrib.shop.models import Seller
from rest_framework.generics import GenericAPIView
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rebateceo.contrib.stand.models import Stand
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rebateceo.contrib.profile.models import Session
from . import serializers, exceptions
from .. import models


class CardListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.CardSerializer
    queryset = models.Card.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class TypeListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.TypeSerializer
    queryset = models.Type.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class ScanView(GenericAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.ScanPostSerializer

    # Fields
    device = None
    stand = None
    scan = None
    code = None
    card = None
    code_type = None
    code_value = None
    force_code_type = None

    def _resolve_code(self, request, *args, **kwargs):
        # Check code
        code = request.POST.get('code')
        if not code:
            raise exceptions.ScanCodeValueRequired()
        if ';' in code:
            code = code.replace(';', '%3B')
        try:
            code_type, code_value = code.split('%3B')  # ;
        except ValueError:
            if self.force_code_type:
                return self.force_code_type, code
            raise exceptions.ScanCodeValueError()
        else:
            if code_type not in [models.Card.QRCODE_TYPE, Coupon.QRCODE_TYPE, Seller.QRCODE_TYPE]:
                raise exceptions.ScanCodeTypeUnrecognized(code_type=code_type)
        return code_type, code_value

    def initial(self, request, *args, **kwargs):
        super(ScanView, self).initial(request, args, kwargs)
        session = Session.objects.create(stand=self.stand, request=request.DATA)
        self.scan = models.Scan.objects.create(session=session)

    def validate(self, request, *args, **kwargs):
        # Check device
        self.device = request.POST.get('device')
        if not self.device:
            raise exceptions.DeviceIsRequired()

        # Check stand
        try:
            self.stand = Stand.objects.get(device=self.device)
        except Stand.DoesNotExist:
            raise exceptions.StandDoesNotExist(device=self.device)

        # Check code
        self.code = request.POST.get('code')
        if not self.code:
            raise exceptions.ScanCodeValueRequired()

        # Resolve code
        self.code_type, self.code_value = self._resolve_code(request, args, kwargs)
        try:
            self.card = models.Card.objects.get(number=self.code_value)
        except models.Card.DoesNotExist:
            raise exceptions.CardDoesNotExist(code_value=self.code_value)
        else:
            self.scan.session.profile = self.card.profile
            self.scan.card = self.card

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            self.validate(request, *args, **kwargs)
        except Exception, e:
            self.scan.session.response = dict(e.__dict__)
            self.scan.session.save()
            raise e
        else:

            # Update status
            self.scan.status = True
            self.scan.save()

            # Prepare response
            response = {
                'scan': serializers.ScanSerializer(instance=self.scan, context={'request': self.request}).data,
                'extra': {}
            }

            # Update session info
            self.scan.session.response = response
            self.scan.session.save()

            return Response(response)
