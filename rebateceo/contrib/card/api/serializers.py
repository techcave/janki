# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rebateceo.contrib.profile.api.serializers import SessionSerializer, ProfileSerializer
from rest_framework import serializers
from .. import models
from . import exceptions


class TypeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.Type
        fields = ['pk', 'title', 'slug', 'image']

        
class CardSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    profile = ProfileSerializer()
    qrcode_str = serializers.CharField(max_length=255)
    qrcode_img = serializers.CharField(max_length=255)
    points = serializers.SerializerMethodField()
    code = serializers.SerializerMethodField()
    is_active = serializers.SerializerMethodField()

    class Meta:
        model = models.Card
        fields = ['pk', 'number', 'code', 'qrcode_str', 'qrcode_img', 'points', 'is_active', 'profile']

    def get_points(self, instance):
        return instance.points

    def get_code(self, instance):
        return instance.number

    def get_is_active(self, instance):
        return True


class ScanPostSerializer(serializers.Serializer):
    device = serializers.CharField(max_length=255)
    code = serializers.CharField(max_length=255)
        
    class Meta:
        fields = ['code', 'device']

    def validate_device(self, value):
        if not value:
            raise exceptions.DeviceIsRequired()


class ScanSerializer(serializers.Serializer):
    pk = serializers.ReadOnlyField()
    card = CardSerializer()
    session = SessionSerializer()

    class Meta:
        models = models.Scan
        fields = ['pk', 'session', 'card', 'created_at', 'updated_at', 'status']
