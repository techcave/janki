# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rebateceo.contrib.purchase.models import Purchase
import reversion
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from picklefield.fields import PickledObjectField


QRCODE_STR = getattr(settings, 'QRCODE_STR', "{type};{uuid}")
QRCODE_IMG = getattr(settings, 'QRCODE_IMG', "http://chart.apis.google.com/chart?cht=qr&chs={size}x{size}&chl={qrcode_str}")


def upload_to(instance, filename):
    return '/'.join(['cards', str(filename)])


class Status(models.Model):
    symbol = models.CharField(max_length=18, unique=True, db_index=True)
    title = models.CharField(max_length=18, unique=True, db_index=True)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True, blank=True, null=True)

    class Meta:
        ordering = ('-symbol',)
        verbose_name = u"Status"
        verbose_name_plural = u"Statuses"

    def __unicode__(self):
        return self.symbol


class Type(models.Model):
    PROFILE = 'profile'

    title = models.CharField(_('title'), max_length=255)
    slug = models.SlugField(max_length=255)
    symbol = models.SlugField(max_length=255, blank=True, null=True)
    image = models.FileField(max_length=255, upload_to=upload_to)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True, blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Type"
        verbose_name_plural = u"Types"

    @property
    def image_link(self):
        return '<a href="%s" target="_blank"><img src="%s"></a>' % (self.image.url, self.image.url) if self.image else '-'

    def admin_image(self):
        return self.image_link
    admin_image.allow_tags = True
    admin_image.short_description = _('image')


class CardManager(models.Manager):
    def for_number(self, number):
        try:
            card = self.get(number=number)
        except Card.DoesNotExist:
            return None
        else:
            return card


class Card(models.Model):
    QRCODE_TYPE = 'card'
    QRCODE_SIZE = 200

    type = models.ForeignKey(Type, blank=True, null=True)
    profile = models.ForeignKey('profile.Profile', blank=True, null=True)
    number = models.CharField(max_length=128, blank=True, null=True, db_index=True,
                              help_text='Pozostaw pusty jeśli wybrałeś Profil powyżej. '
                                        'Numer zostanie nadany automatycznie.')
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    status = models.ForeignKey(Status, blank=True, null=True)
    test = models.NullBooleanField()
    objects = CardManager()

    @property
    def qrcode_str(self):
        return QRCODE_STR.format(type=self.QRCODE_TYPE, uuid=self.number)

    @property
    def qrcode_img(self):
        return QRCODE_IMG.format(size=200, qrcode_str=self.qrcode_str)

    @property
    def is_blocked(self):
        return self.status == Status.objects.get(symbol='blocked')

    @property
    def current_purchases(self):
        return Purchase.objects.current_for_user(user=self.profile.user)

    @property
    def points(self):
        sum = 0
        if self.amounts:
            for amount in self.amounts.all():
                sum += amount.points
        return sum

    @property
    def attempts(self):
        attempts = 0
        for purchase in self.current_purchases:
            attempts += purchase.attempts
        avail = attempts - 0 #  self.current_draws.count() ## TODO
        return {
            'all': attempts,
            'avail': 0 if avail < 0 else avail
        }

    def __unicode__(self):
        return str(self.number)


class Scan(models.Model):
    session = models.ForeignKey('profile.Session', related_name='sessions', blank=True, null=True)
    card = models.ForeignKey(Card, related_name='scans', blank=True, null=True)
    status = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Scan')
        verbose_name_plural = _('Scans')
        ordering = ['-pk']


# Signals
def card_post_save(sender, instance, created, **kwargs):
    if instance.profile:
        instance._meta.model.objects.filter(pk=instance.pk) \
            .update(number=instance.profile.pk)

post_save.connect(card_post_save, sender=Card)


# Reversion
reversion.register(Card)
