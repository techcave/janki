# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.card.models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0016_auto_20150519_1023'),
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(db_index=True, max_length=128, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('test', models.NullBooleanField()),
                ('profile', models.ForeignKey(blank=True, to='profile.Profile', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('symbol', models.CharField(unique=True, max_length=18, db_index=True)),
                ('title', models.CharField(unique=True, max_length=18, db_index=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at', null=True)),
            ],
            options={
                'ordering': ('-symbol',),
                'verbose_name': 'Status',
                'verbose_name_plural': 'Statuses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('slug', models.SlugField(max_length=255)),
                ('image', models.FileField(max_length=255, upload_to=rebateceo.contrib.card.models.upload_to)),
                ('description', models.TextField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at', null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at', null=True)),
            ],
            options={
                'verbose_name': 'Type',
                'verbose_name_plural': 'Types',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='card',
            name='status',
            field=models.ForeignKey(blank=True, to='card.Status', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='card',
            name='type',
            field=models.ForeignKey(blank=True, to='card.Type', null=True),
            preserve_default=True,
        ),
    ]
