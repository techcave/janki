# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0016_auto_20150519_1023'),
        ('card', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Scan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.BooleanField(default=False)),
                ('card', models.ForeignKey(related_name='scans', blank=True, to='card.Card', null=True)),
                ('session', models.ForeignKey(related_name='sessions', blank=True, to='profile.Session', null=True)),
            ],
            options={
                'ordering': ['-pk'],
                'verbose_name': 'Scan',
                'verbose_name_plural': 'Scans',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='card',
            name='number',
            field=models.CharField(help_text=b'Pozostaw pusty je\xc5\x9bli wybra\xc5\x82e\xc5\x9b Profil powy\xc5\xbcej. Numer zostanie nadany automatycznie.', max_length=128, null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
    ]
