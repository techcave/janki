# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from rebateceo.contrib.profile.models import Profile
from . import models


LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT = getattr(settings, 'LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT', None)
LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT = getattr(settings, 'LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT', None)
LOTTERY_DAILY_PURCHASES_LIMIT = getattr(settings, 'LOTTERY_DAILY_PURCHASES_LIMIT', None)


def card_exists_validator(value):
    card = models.Card.objects.for_number(value)
    if not card:
        raise ValidationError(_(u'Card with number %s does not exist in data base') % value)
    if card.is_blocked:
        raise ValidationError(_(u'Card with number %s has been blocked!') % value)
    return card


def card_exists_validator_for_code(value):
    card = models.Card.objects.for_code(value)
    if not card:
        raise ValidationError(_(u'Card with code %s does not exist in data base') % value)
    return card


def card_member_validator(card):
    if not card.member:
        raise ValidationError(_(u'Card with number %s does not have user assigned.') % card.number)


def card_activation_validator(card):
    if card.activation:
        raise ValidationError(_(u'Card with number %s has already been activated') % (card.number,))


def card_for_mobile_validator(current_member, value):
    member = Profile.objects.for_mobile(value)
    # activations = Activation.objects.filter(mobile_number=value, status=True)
    # activation = activations[0] if activations else None
    if member and current_member != member:
        raise ValidationError(_(u'This phone number has already different card assigned'))


def card_for_email_validator(current_member, value):
    member = Profile.objects.for_email(value)
    if member and current_member != member:
        raise ValidationError(_(u'This address has already different card assigned'))


def card_for_userdata_validator(current_member, cleanded_data):
    req_fields = ['first_name', 'last_name', 'city', 'number', 'street', 'postal']
    data = {}
    for f in req_fields:
        if f in cleanded_data and cleanded_data[f]:
            data.update({f: cleanded_data[f]})

    if len(data) == len(req_fields):
        member = Profile.objects.for_data(data)
        if member and current_member != member:
            raise ValidationError(_(u'For fields (Name, Surname, Street, House No, Postal Code, City) is different card registered'))


def card_from_olimpia(card):
    try:
        if len(str(card)) != 8:
            raise ValidationError(_(u'This card is not associated with Olimpia Loyality Programme!'))
    except AttributeError:
        raise ValidationError(_(u'Invalid card number!'))


def card_purchase_limit_validator(card):
    now = datetime.datetime.now()
    monthly_purchases = card.purchases.filter(register_date__year=now.year, register_date__month=now.month)
    daily_purchases = monthly_purchases.filter(register_date__day=now.day)

    if LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT:
        if len(monthly_purchases) >= LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT:
            raise ValidationError(_(u'Cannnot register more than %s per month!') % LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT)

    if LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT:
        if len(daily_purchases) >= LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT:
            raise ValidationError(_(u'Cannnot register more than %s per day!') % LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT)


def card_daily_purchases_limit_validator(card, current_purchase_date):
    if LOTTERY_DAILY_PURCHASES_LIMIT:
        purchases = card.purchases.filter(receipt__purchase_date__year=current_purchase_date.year,
                                          receipt__purchase_date__month=current_purchase_date.month,
                                          receipt__purchase_date__day=current_purchase_date.day)
        if len(purchases) >= LOTTERY_DAILY_PURCHASES_LIMIT:
            raise ValidationError(_(u'Cannnot register more than %s per the same purchase date!') % LOTTERY_DAILY_PURCHASES_LIMIT)