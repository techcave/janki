# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import itertools
from decimal import Decimal as D
from datetime import date, datetime, time
from picklefield.fields import PickledObjectField
from django.db import models
from django.core.exceptions import FieldError
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.loading import get_model
from rabatomat_members.member.models import Member
from rabatomat_rebates.rebate.models import Coupon
from rabatomat_purchases.purchase.models import Purchase


QRCODE_STR = getattr(settings, 'QRCODE_STR', "{type};{uuid}")
QRCODE_IMG = getattr(settings, 'QRCODE_IMG', "http://chart.apis.google.com/chart?cht=qr&chs={size}x{size}&chl={qrcode_str}")
COUPON_HAS_PRINTED_TODAY = getattr(settings, 'COUPON_HAS_PRINTED_TODAY', False)
CHECK_CAN_WIN_MATERIAL_PRIZE = getattr(settings, 'CHECK_CAN_WIN_MATERIAL_PRIZE', False)

LOTTERY_BACKEND_MODEL = getattr(settings, 'LOTTERY_BACKEND_MODEL', ['lottery', 'Lottery'])
LOTTERY_BACKEND_WINNING_MODEL = getattr(settings, 'LOTTERY_BACKEND_WINNING_MODEL', ['lottery', 'Winning'])
LOTTERY_BACKEND_DRAW_MODEL = getattr(settings, 'LOTTERY_BACKEND_DRAW_MODEL', ['lottery', 'Draw'])

BARBURKA_YEAR = getattr(settings, 'BARBURKA_YEAR', 2014)
BARBURKA_MONTH = getattr(settings, 'BARBURKA_MONTH', 12)
BARBURKA_DAY = getattr(settings, 'BARBURKA_DAY', 4)
BARBURKA_MINIMUM_AMOUNT = getattr(settings, 'BARBURKA_MINIMUM_AMOUNT', D('50.00'))


class CardManager(models.Manager):

    def waiting(self):
        return self.all().filter(activations__isnull=True)

    def active(self):
        return self.all().filter(status__isnull=True, activations__isnull=False)

    def for_number(self, number):
        try:
            card = Card.objects.get(number=number)
        except Card.DoesNotExist:
            return None
        else:
            return card

    def for_code(self, code):
        try:
            card = Card.objects.get(code=code)
        except Card.DoesNotExist:
            return None
        else:
            return card


class Status(models.Model):
    symbol = models.CharField(max_length=18, unique=True, db_index=True)
    title = models.CharField(max_length=18, unique=True, db_index=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        app_label = 'card'
        ordering = ('-symbol',)

    def __unicode__(self):
        return self.symbol


class Card(models.Model):
    QRCODE_TYPE = 'card'
    QRCODE_SIZE = 200

    number = models.CharField(max_length=18, unique=True, db_index=True)
    code = models.CharField(max_length=128, blank=True, null=True)
    printed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    craeted_time = models.TimeField(blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)
    test = models.NullBooleanField()
    status = models.ForeignKey(Status, blank=True, null=True)
    objects = CardManager()

    class Meta:
        app_label = 'card'
        ordering = ('-printed',)

    def __unicode__(self):
        return self.code or self.number

    @property
    def qrcode_str(self):
        return QRCODE_STR.format(type=self.QRCODE_TYPE, uuid=self.number)

    @property
    def qrcode_img(self):
        return QRCODE_IMG.format(size=200, qrcode_str=self.qrcode_str)

    @property
    def activation(self):
        try:
            return self.activations.get(status=True)
        except Activation.DoesNotExist:
            return None

    @property
    def is_active(self):
        return True if self.activation else False

    @property
    def is_blocked(self):
        return self.status == Status.objects.get(symbol='blocked')

    @property
    def is_winning(self):
        now = datetime.now()
        today = date(year=now.year, month=now.month, day=now.day)
        return (self.winning_cards.date == today) if hasattr(self, 'winning_cards') else False

    @property
    def has_scanned_today(self):
        now = datetime.now()
        today_scans = self.scans.filter(status=True, created__year=now.year, created__month=now.month,
                                        created__day=now.day)
        return True if today_scans.count() > 1 else False

    @property
    def has_printed_discount_today(self):
        if COUPON_HAS_PRINTED_TODAY:
            now = datetime.now()
            today_prints = self.coupons.filter(created_at__year=now.year, created_at__month=now.month,
                                               created_at__day=now.day)
            return True if today_prints.count() > 1 else False
        else:
            return False

    @property
    def has_prize_today(self):
        winnings = self.winnings
        if winnings:
            now = datetime.now()
            try:
                today_winnings = winnings.filter(created_at__year=now.year, created_at__month=now.month, created_at__day=now.day)
            except FieldError:
                # For backward compatybility
                today_winnings = winnings.filter(created__year=now.year, created__month=now.month, created__day=now.day)
            return True if today_winnings else False
        return False

    @property
    def can_win_material_prize(self):
        if CHECK_CAN_WIN_MATERIAL_PRIZE:
            """Custom method for Olimpia barburka..."""
            now = datetime.now()
            purchases = self.purchases.filter(
                receipt__purchase_date__year=BARBURKA_YEAR,
                receipt__purchase_date__month=BARBURKA_MONTH,
                receipt__purchase_date__day=BARBURKA_DAY
            )

            # No barburka day
            if not (now.year, now.month, now.day) == (BARBURKA_YEAR, BARBURKA_MONTH, BARBURKA_DAY):
                return False
            elif sum([purchase.receipt.amount for purchase in purchases]) >= BARBURKA_MINIMUM_AMOUNT:
                return True
            else:
                return False
        return True

    @property
    def member(self):
        try:
            return self.members.get(card=self)
        except Member.DoesNotExist:
            return None

    @property
    def points(self):
        sum = 0
        if self.amounts:
            for amount in self.amounts.all():
                sum += amount.points
        return sum


    @property
    def draws(self):
        Draw = get_model(LOTTERY_BACKEND_DRAW_MODEL[0], LOTTERY_BACKEND_DRAW_MODEL[1])
        try:
            return Draw.objects.filter(scan__in=self.scans.all())
        except Draw.DoesNotExist:
            return None

    @property
    def winnings(self):
        Winning = get_model(LOTTERY_BACKEND_WINNING_MODEL[0], LOTTERY_BACKEND_WINNING_MODEL[1])
        try:
            return Winning.objects.filter(draw__in=self.draws.all())
        except Winning.DoesNotExist:
            return None

    @property
    def total_attempts(self):
        return sum([purchase.receipt.attempts for purchase in self.purchases.for_current_lottery()])

    @property
    def used_attempts(self):
        return len(self.current_draws)

    @property
    def current_purchases(self):
        return Purchase.objects.current_for_card(card=self)

    @property
    def current_attempts(self):
        return self.total_attempts - self.used_attempts

    @property
    def purchases_amount(self):
        return sum([purchase.receipt.amount for purchase in self.current_purchases])

    @property
    def current_draws(self):
        Draw = get_model(LOTTERY_BACKEND_DRAW_MODEL[0], LOTTERY_BACKEND_DRAW_MODEL[1])
        return Draw.objects.current_for_card(card=self)

    @property
    def winnings_count(self):
        return len(self.winnings)

    @property
    def winnings_amount(self):
        return sum([w.prize.amount for w in self.winnings])

    @property
    def attempts(self):
        attempts = 0
        for purchase in self.current_purchases:
            attempts += purchase.receipt.attempts
        avail = attempts - self.current_draws.count()
        return {
            'all': attempts,
            'avail': 0 if avail < 0 else avail
        }

    @classmethod
    def get_grouped_stats(cls, type=None):
        activations = cls.objects.all()
        grouped = itertools.groupby(activations, lambda x: x.created.strftime("%Y-%m-%d"))
        activations_by_day = [(day, len(list(activations_this_day))) for day, activations_this_day in grouped]
        return activations_by_day

    def save(self, *args, **kwargs):
        if self.created:
            self.created_time = self.created.time()
        return super(Card, self).save(*args, **kwargs)


class ActivationManager(models.Manager):
    def operator(self, **kwargs):
        return self.filter(type=Activation.OPERATOR).filter(**kwargs)

    def sms(self, **kwargs):
        return self.filter(type=Activation.SMS).filter(**kwargs)


class Activation(models.Model):
    SMS = 1
    OPERATOR = 2
    TYPE = (
        (SMS, 'SMS'),
        (OPERATOR, 'Operator'),
    )
    card = models.ForeignKey(Card, blank=True, null=True, related_name='activations')
    type = models.PositiveSmallIntegerField(choices=TYPE)
    operator = models.ForeignKey(User, blank=True, null=True, related_name='activations')
    mobile_number = models.CharField(max_length=15, blank=True, null=True, help_text=_('Help field for SMS activation'),
                                     db_index=True)
    sms_content = models.CharField(max_length=160, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    status = models.NullBooleanField()
    details = models.TextField(blank=True, null=True)
    args = PickledObjectField(blank=True, null=True)

    objects = ActivationManager()

    class Meta:
        app_label = 'card'
        verbose_name = _('Card activation')
        verbose_name_plural = _('Cards activations')
        ordering = ['-date']

    def __unicode__(self):
        return "%s, %s" % (self.date, self.get_type_display())

    @classmethod
    def get_grouped_stats(cls, type=None):
        if type == cls.OPERATOR:
            activations = cls.objects.operator()
        elif type == cls.SMS:
            activations = cls.objects.sms()
        else:
            activations = cls.objects.all()

        grouped = itertools.groupby(activations, lambda x: x.date.strftime("%Y-%m-%d"))
        activations_by_day = [(day, len(list(activations_this_day))) for day, activations_this_day in grouped]
        return activations_by_day


class Scan(models.Model):
    WITH_DISCOUNT = 'with_discount'
    WITHOUT_DISCOUNT = 'without_discount'

    stand = models.ForeignKey('stand.Stand', related_name='scans', blank=True, null=True)
    card = models.ForeignKey(Card, related_name='scans', blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    created_time = models.TimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    timestamp = models.CharField(max_length=32)
    status = models.BooleanField(default=False)
    details = models.TextField(blank=True, null=True)
    request = PickledObjectField(blank=True, null=True)
    response = PickledObjectField(blank=True, null=True)

    class Meta:
        app_label = 'card'
        verbose_name = _('Card scan')
        verbose_name_plural = _('Cards scans')
        ordering = ['-created']

    def __unicode__(self):
        return "#%s [%s] %s" % (self.pk, self.stand, self.created)

    @classmethod
    def get_grouped_stats(cls, type=None):
        if type == cls.WITH_DISCOUNT:
            # TODO: rename this stat type
            coupons = Coupon.objects.all()
            # scans = cls.objects.filter(card__in=Coupon.objects.all().values_list('card', flat=True)).distinct()
            scans = coupons  # hack for empty scan_id in Coupon model
            date_field = 'created_at'
        else:
            date_field = 'created'
            scans = cls.objects.all()

        grouped = itertools.groupby(scans, lambda x: getattr(x, date_field).strftime("%Y-%m-%d"))
        scans_by_day = [(day, len(list(scans_this_day))) for day, scans_this_day in grouped]
        return scans_by_day

    def save(self, *args, **kwargs):
        if self.created:
            self.created_time = self.created.time()
        return super(Scan, self).save(*args, **kwargs)