# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import barcodenumber
from django.test import TestCase
from rabatomat_stands.stand.models import Stand
from rabatomat.apps.demo.factories import CardsFactory
from rabatomat_cards.card.models import Card, Scan


class CardsBaseTest(TestCase):
    card_limit = 5

    def setUp(self):
        CardsFactory(self.card_limit)


class DjangoEAN13MassTest(CardsBaseTest):
    def test_mass(self):
        [self.assertTrue(barcodenumber.check_code('ean13', c.number), msg='Wrong EAN!') for c in Card.objects.all()]


class ScanTest(CardsBaseTest):
    fixtures = ['stands.json']

    def test_save_scan_log(self):
        stand = Stand.objects.get(device='6469819e-067c-11e3-8d96-001851c2bd4d', ip_address='10.4.34.31')
        card = Card.objects.all().order_by('?')[0]
        scan = Scan.objects.create(stand=stand, card=card, status=True, details='Test')
        self.assertIsInstance(scan.pk, int)