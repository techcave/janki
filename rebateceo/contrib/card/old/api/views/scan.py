# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from django_utils.utils import my_import
from rabatomat_members.member.api.serializers import MemberSerializer
from ..serializers import CardSerializer, ScanSerializer
from ..exceptions import LotteryFinished
from ..bases import ScanBaseAPIView
from ...models import Scan


# Load custom models for different lotteries
LOTTERY_MODEL_LOTTERY = getattr(settings, 'LOTTERY_MODEL_LOTTERY', 'rabatomat_lottery.lottery.models.Lottery')
Lottery = my_import(LOTTERY_MODEL_LOTTERY)


class ScanView(ScanBaseAPIView, GenericAPIView):
    lottery = None
    force_code_type = 'card'  # Backward

    def post(self, request, *args, **kwargs):
        response = {}
        scan = Scan.objects.create(card=self.card, request={
            'post': request.POST, 'get': request.GET,
        })

        # Valid input
        try:
            self.pre_validation(request, *args, **kwargs)
            self.validate(request, *args, **kwargs)
        except Exception, e:
            scan.details = dict(e.__dict__)
            scan.updated = datetime.datetime.now()
            scan.save()
            raise e
        else:

            # Update log
            scan.response = response
            scan.status = True
            scan.card = self.card
            scan.stand = self.stand
            scan.updated = datetime.datetime.now()
            scan.save()

            scan = ScanSerializer(instance=scan, context={'request': self.request}).data
            member = MemberSerializer(instance=self.card.member).data if self.card.member else {}
            card = CardSerializer(instance=self.card).data if self.card else {}

            response.update({
                'scan': scan,
                'member': member,
                'card': card,
                'extra': self.get_extra(scan, self.card)
            })

            return Response(response)

    def pre_validation(self, request, *args, **kwargs):
        self.lottery = Lottery.objects.get_current()
        if self.lottery.finished:
            raise LotteryFinished()
        #if not self.can_scan:
        #    raise CannotScanForNow()

    def get_extra(self, scan, card):
        member = card.member
        return {
            'has_scanned_today': card.has_scanned_today,
            'has_prize_today': card.has_prize_today,
            'has_printed_discount_today': card.has_printed_discount_today,
            'can_win_material_prize': card.can_win_material_prize,
            'member_attempts': self.get_attempts(member),
            'marketing_data': member.marketing_data if member else None,
            'marketing_email': member.marketing_email if member else None,
            'lottery': {
                'now': datetime.datetime.now(),
                'start_date': settings.LOTTERY_START_DATE,
                'end_date': settings.LOTTERY_END_DATE,
                'today_avail_prizes': self.lottery.today_avail_prizes
            }
        }

    def get_attempts(self, member):
        return self.lottery.get_member_attempts(member) if member else {
            'all': None,
            'avail': None
        }

