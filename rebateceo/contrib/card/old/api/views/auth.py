# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rabatomat_cards.card.api.serializers import CardSerializer
from rabatomat_members.member.api.serializers import MemberSerializer

from ..bases import ScanBaseAPIView
from ..serializers import (ScanSerializer)


class ScanView(ScanBaseAPIView, GenericAPIView):

    def post(self, request, *args, **kwargs):
        response = {}
        instance = self.model.objects.create(timestamp=self.get_timestamp(), request={
            'post': request.POST,
            'get': request.GET,
        })

        try:
            self.validate(request, *args, **kwargs)
        except Exception, e:
            instance.details = dict(e.__dict__)  # no nie wiem...
            instance.save()
            raise e
        else:
            # Update log
            instance.response = response
            instance.status = True
            instance.card = self.card
            instance.stand = self.stand
            instance.updated = datetime.datetime.now()
            instance.save()

            scan = ScanSerializer(instance=instance, context={'request': self.request}).data
            member = MemberSerializer(instance=self.card.member).data if self.card.member else {}
            card = CardSerializer(instance=self.card).data if self.card else {}

            response.update({
                'scan': scan,
                'member': member,
                'card': card,
                'extra': self.get_extra(instance, self.card)
            })

            return Response(response)

    def get_extra(self, scan, card):
        return {
            'has_scanned_today': card.has_scanned_today,
            'has_scanned_today': card.has_prize_today,
            'has_printed_discount_today': card.has_printed_discount_today,
            'lottery': {
                'now': datetime.datetime.now(),
                'start_date': settings.LOTTERY_START_DATE,
                'end_date': settings.LOTTERY_END_DATE,
            }
        }