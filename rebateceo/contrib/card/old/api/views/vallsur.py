# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
import barcode
import random
import simplejson
import urllib
import socket
import urllib2
from urllib2 import HTTPError
from django_ean13.validators import EAN13_validator
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rabatomat_members.member.models import Member
from rabatomat_cards.card.models import Card, Scan
from rabatomat_cards.card.api.serializers import CardSerializer
from rabatomat_members.member.api.serializers import MemberSerializer
from ..exceptions import MemberFromAPIUpdateException, VallsurAPIException, VallsurAPITimeoutException
from ..bases import ScanBaseAPIView
from ..serializers import (ScanSerializer)


GENDER = {'M': 1, 'V': 2}
VALLSUR_API_URL = getattr(settings, 'VALLSUR_API_URL', 'http://unbapi.rethinkcrm.es/api/')
VALLSUR_API_TOKEN = getattr(settings, 'VALLSUR_API_TOKEN', '390e8d76-943a-4215-b75a-2e5c90567bb0')


class ScanView(ScanBaseAPIView, GenericAPIView):
    force_code_type = 'card'

    def post(self, request, *args, **kwargs):
        response = {}
        instance = self.model.objects.create(timestamp=self.get_timestamp(), request={
            'post': request.POST,
            'get': request.GET,
        })

        try:
            self.validate(request, *args, **kwargs)
        except Exception, e:
            instance.details = dict(e.__dict__)  # no nie wiem...
            instance.save()
            raise e
        else:
            # Update log
            instance.response = response
            instance.status = True
            instance.card = self.card
            instance.stand = self.stand
            instance.updated = datetime.datetime.now()
            instance.save()

            scan = ScanSerializer(instance=instance, context={'request': self.request}).data
            member = MemberSerializer(instance=self.card.member).data if self.card.member else {}
            card = CardSerializer(instance=self.card).data if self.card else {}

            response.update({
                'scan': scan,
                'member': member,
                'card': card,
                'extra': self.get_extra(instance, self.card)
            })

            return Response(response)

    def get_extra(self, scan, card):
        return {
            'has_scanned_today': card.has_scanned_today,
            'has_prize_today': card.has_prize_today,
            'has_printed_discount_today': card.has_printed_discount_today
        }

    def validate_card(self, request, *args, **kwargs):

        # Check card in API
        api_url = VALLSUR_API_URL + 'checkcard/vallsur'
        params = {
            'token': VALLSUR_API_TOKEN,
            'cardCode': self.code_value,
            '_': random.random()
        }

        # Load from api
        url = "%s?%s" % (api_url, urllib.urlencode(params))
        request = urllib2.Request(url)
        request.get_method = lambda: 'GET'

        try:
            response = urllib2.urlopen(request, timeout=20)
        except socket.timeout, e:
            raise VallsurAPITimeoutException(url=url, mess=str(e))
        except HTTPError, e:
            raise VallsurAPIException(url=url, mess=str(e))
        else:
            response = response.read()
            data = simplejson.loads(response)
            if data.get('Id') == -1:
                mess = _('User not found for code="%s"') % self.code_value
                raise VallsurAPIException(url=url, mess=mess + ' (' + response + ')')
            else:
                # Card
                member_data = {}
                card, created = self.card_get_or_create(self.code_value)
                try:
                    birthdate = data.get('Birthdate')
                    if birthdate:
                        member_data.update({'birthdate': datetime.datetime.strptime(birthdate, '%m/%d/%Y %H:%M:%S %p')})

                    join_date = data.get('JoinDate')
                    if join_date:
                        member_data.update({'join_date': datetime.datetime.strptime(join_date, '%m/%d/%Y %H:%M:%S %p')})

                    mobile_number = data.get('Mobile')
                    if mobile_number:
                        member_data.update({'mobile_number': "+34%s" % mobile_number})

                    name = data.get('Name')
                    if name:
                        member_data.update({'name': name})

                    gender = data.get('Gender')
                    if gender:
                        member_data.update({'gender': GENDER[gender]})

                    age = data.get('Age')
                    if age:
                        member_data.update({'age': age})

                    phone = data.get('Phone')
                    if phone:
                        member_data.update({'phone': phone})

                    postal = data.get('PostalCode')
                    if postal:
                        member_data.update({'postal': postal})

                    email = data.get('Email')
                    if email:
                        member_data.update({'email': email})

                    # Comment
                    member_data.update({'comment': response})

                    # Create or update user
                    self.member_get_or_create_or_update(card=card, data=member_data)

                except Exception, e:
                    kwargs = {
                        'mess': str(e),
                        'data': data
                    }
                    if member_data:
                        kwargs.update({'member_data': member_data})
                    raise MemberFromAPIUpdateException(**kwargs)
                else:
                    self.card = card

    def card_get_or_create(self, code):
        card, created = Card.objects.get_or_create(code=code)
        if not card.number:
            EAN = barcode.get_barcode_class('ean13')
            codebase = str(card.pk).zfill(12)
            card.number = EAN13_validator(str(EAN(codebase)))
            card.save()
        return card, created

    def member_get_or_create_or_update(self, card, data):
        member, created = Member.objects.get_or_create(card=card)
        if not created:
            member.updated = datetime.datetime.now()
        for k, v in data.iteritems():
            setattr(member, k, v)
        member.save()
        return member, created