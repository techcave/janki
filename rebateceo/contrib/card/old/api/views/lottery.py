# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django_utils.exceptions import InputValidationError
from django.core.exceptions import ValidationError
from rabatomat_cards.card.validators import card_exists_validator
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status
from django_ean13.validators import EAN13_validator
from rabatomat_stands.stand.models import Stand
from rabatomat_lottery.lottery.models import Lottery
from ..serializers import ScanSerializer
from ...models import Scan


class ScanView(GenericAPIView):
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (IsAuthenticated,)
    serializer_class = ScanSerializer
    lottery = None
    card = None
    stand = None

    def post(self, request, *args, **kwargs):
        response = {}
        scan = Scan.objects.create(timestamp=self.get_timestamp(), request={
            'post': request.POST,
            'get': request.GET
        })

        # Valid input
        try:
            # Ustawia dodatkowo obiekty w polach klasy jesli sa poprawne
            self.validate(request, *args, **kwargs)
        except InputValidationError, e:
            scan.details = e
            scan.updated = datetime.datetime.now()
            scan.save()
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        else:

            # Draw!
            response.update(self.info(scan, self.card))

            # Update log
            scan.response = response
            scan.status = True
            scan.card = self.card
            scan.stand = self.stand
            scan.updated = datetime.datetime.now()
            scan.save()

            return Response(response)

    def info(self, scan, card):
        member = card.member
        return {
            'scan_id': scan.pk,
            'is_active': card.is_active,
            'has_scanned_today': card.has_scanned_today,
            'has_prize_today': card.has_prize_today,
            'winning': self.get_winning(card, scan),
            'member_id': member.pk if member else None,
            'marketing_data': member.marketing_data if member else None,
            'marketing_email': member.marketing_email if member else None,
            'has_printed_discount_today': card.has_printed_discount_today
        }

    def get_winning(self, card, scan):

        # ===== Sprawdzenie karty ======
        draw = card.draw(scan)
        # ==============================

        return {
            'id': draw.id,
            'code': draw.code,
            'card_number': card.number,
            'amount': draw.amount,
            'date': draw.date,
            'index': draw.index,
        } if draw else None

    def get_timestamp(self):
        tznow = timezone.now()
        return '{:%d-%m-%Y:%H:%M:%S}.{:03d}'.format(tznow, tznow.microsecond // 1000)

    def validate(self, request, *args, **kwargs):

        # Check rabatomat
        self.lottery = Lottery.objects.get_current()

        # Finished
        if self.lottery.finished:
            raise InputValidationError(_(u'[0001] Lottery has been finished!'))

        # Suspended
        if self.lottery.suspended:
            raise InputValidationError(_(u'[0002] Today lottery is paused or inactive'))

        # Check device
        device = request.POST.get('device')
        if not device:
            raise InputValidationError(_(u'[0003] No deviceId'))

        # Check number
        number = request.POST.get('number')
        if not number:
            raise InputValidationError(_(u'[0004] No card number'))

        # Check number
        try:
            EAN13_validator(number)
        except ValidationError, e:
            raise InputValidationError(u'[0005] %s' % unicode(e.message))

        # Check stand
        try:
            self.stand = Stand.objects.get(device=device)
        except Stand.DoesNotExist:
            raise InputValidationError(_(u'[0006] Device not found'))

        # Check card exists
        try:
            self.card = card_exists_validator(number)
        except ValidationError, e:
            raise InputValidationError(u'[0007] %s' % unicode(e.message))
