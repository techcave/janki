# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from datetime import timedelta

from django.test import TestCase
from rest_framework import status
from django_utils.testing import RequestFactory
from rabatomat_stands.stand.models import Stand
from rabatomat_cards.card.models import Card, Scan, Activation
from rabatomat_lottery.lottery.models import Lottery, WinningCard
from rabatomat_members.member.models import Member
from rabatomat.apps.demo.factories import LotteryFactory, WinningCardFactory, CardsFactory
from .views import ScanView


now = datetime.datetime.now()

class APITestBase(TestCase):
    device = '948f09ec-0353-11e3-9be5-001851c2bd4d'
    number = '8004260168878'

    def setUp(self):
        self.factory = RequestFactory()
        self.view = ScanView.as_view()
        self.path = '/'
        self.kwargs = {}
        self.data = {
            'number': '9903410003443',
            'device': '948f09ec-0353-11e3-9be5-001851c2bd4d',
        }

        # Factory
        LotteryFactory()
        CardsFactory(100)
        WinningCardFactory()

        # Create items for test
        self.stand = Stand.objects.create(ip_address='10.3.34.11', device=self.device)
        self.card = Card.objects.create(number=self.number)
        self.lottery = Lottery.objects.get_current()

    def test_scan_without_device(self):
        """
        Test bez podanego id urzadzenia
        """
        del self.data['device']
        request = self.factory.post(self.path, self.data)
        response = self.view(request, **self.kwargs)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_scan_with_wrong_device(self):
        """
        Test ze zlym id urzadzenia
        """
        self.data.update({
            'device': 'A948f09ecA948f09ec948f09ec948f09ec948f09ec948f09ecAAAAAAAAA',
        })
        request = self.factory.post(self.path, self.data)
        response = self.view(request, **self.kwargs)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_scan_without_number(self):
        """
        Test bez numeru karty
        """
        del self.data['number']
        request = self.factory.post(self.path, self.data)
        response = self.view(request, **self.kwargs)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_scan_with_wrong_number(self):
        """
        Test ze zlym numerem karty
        """
        self.data.update({
            'number': 'AAAAAAAAAAA',
        })
        request = self.factory.post(self.path, self.data)
        response = self.view(request, **self.kwargs)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_scan_with_no_card_in_database(self):
        """
        Test z dobrym numerem ale nie istnijacym w bazie
        """
        self.data.update({
            'number': '1004260168878',
        })
        request = self.factory.post(self.path, self.data)
        response = self.view(request, **self.kwargs)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_scan_ok(self):
        self.data.update({
            'number': self.number,
        })
        request = self.factory.post(self.path, self.data)
        response = self.view(request, **self.kwargs)
        self.assertContains(response, '"scan_id": 1')
        self.assertContains(response, '"member_id": null')

    def test_scan_with_member_has_approved_agreements(self):
        """
        Test ze zgodami
        """
        Member.objects.create(card=self.card, marketing_email=True, marketing_data=True)
        view = ScanView.as_view()

        self.data.update(**{'number': self.number})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)

        self.assertContains(response, '"marketing_email": true')
        self.assertContains(response, '"marketing_data": true')

    def test_scan_with_active_card(self):
        """
        Test skanowania karty aktywnej
        """
        Member.objects.create(card=self.card)
        Activation.objects.create(card=self.card, type=Activation.SMS, operator=None, mobile_number='487252477774',
                                  sms_content='test', status=True, details='test')
        view = ScanView.as_view()

        self.data.update(**{'number': self.number})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)
        self.assertContains(response, '"is_active": true')

    def test_scan_with_no_active_card(self):
        """
        Test skanowania karty nieaktywnej
        """
        Member.objects.create(card=self.card)
        Activation.objects.create(card=self.card, type=Activation.SMS, operator=None, mobile_number='487252477774',
                                  sms_content='test', status=False, details='test')
        view = ScanView.as_view()

        self.data.update(**{'number': self.number})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)
        self.assertContains(response, '"is_active": false')

    def test_scan_without_scanned_today(self):
        """
        Test czy karta byla dzis skanowana => NIE
        """
        view = ScanView.as_view()

        self.data.update(**{'number': self.number})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)
        self.assertContains(response, '"has_scanned_today": false')

    def test_scan_with_scanned_today(self):
        """
        Test czy karta byla skanowana => TAK
        """
        Scan.objects.create(timestamp='26-03-2014:20:47:27.288', request={}, status=False, card=self.card, created=now)
        Scan.objects.create(timestamp='26-03-2014:21:47:27.288', request={}, status=True, card=self.card, created=now + timedelta(seconds=50))
        Scan.objects.create(timestamp='26-03-2014:23:47:27.288', request={}, status=False, card=self.card, created=now + timedelta(seconds=60))
        Scan.objects.create(timestamp='26-03-2014:23:47:27.288', request={}, status=True, card=self.card, created=now + timedelta(seconds=100))

        view = ScanView.as_view()
        self.data.update(**{'number': self.card.number})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)
        self.assertContains(response, '"has_scanned_today": true')

    def test_scan_with_no_winning_card(self):
        """
        Test skanowania karty nie wygrywajacej danego dnia
        """
        view = ScanView.as_view()
        self.data.update({'number': '8004260168878'})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)
        self.assertContains(response, '"winning": null')

    def test_scan_with_winning_card(self):
        """
        Test skanowania karty wygrywajacej danego dnia
        """
        winning_card = WinningCard.objects.get(date__year=now.year, date__month=now.month, date__day=now.day)
        member = Member.objects.create(card=winning_card.card)
        Activation.objects.create(card=member.card, type=Activation.SMS, operator=None, mobile_number='487252477774',
                                  sms_content='test', status=True, details='test')

        view = ScanView.as_view()

        self.data.update({'number': winning_card.card.number})
        request = self.factory.post(self.path, self.data)
        response = view(request, **self.kwargs)
        self.assertContains(response, '"winning": {"date": "%s"' % winning_card.date)
