from django.conf.urls import *
from django.views.decorators.csrf import csrf_exempt
from .views import auth, lottery, vallsur, scan


urlpatterns = patterns('',
    url(r'^scan/lottery/$', csrf_exempt(lottery.ScanView.as_view()), name='scan-for-lottery'),
    url(r'^scan/auth/$', csrf_exempt(auth.ScanView.as_view()), name='scan-for-auth'),
    url(r'^scan/auth/vallsur/$', csrf_exempt(vallsur.ScanView.as_view()), name='scan-for-auth-vallsur'),
    url(r'^scan/$', csrf_exempt(scan.ScanView.as_view()), name='scan'),
)