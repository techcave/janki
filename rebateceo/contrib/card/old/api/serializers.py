# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework import serializers
from rabatomat_cards.card.models import Scan, Card


class CardSerializer(serializers.Serializer):
    pk = serializers.ReadOnlyField()
    number = serializers.CharField(max_length=255)
    qrcode_str = serializers.CharField(max_length=255)
    qrcode_img = serializers.CharField(max_length=255)
    points = serializers.SerializerMethodField('get_points')
    code = serializers.SerializerMethodField('get_code')
    is_active = serializers.SerializerMethodField('get_is_active')

    def get_points(self, instance):
        return instance.points

    def get_code(self, instance):
        return instance.code

    def get_is_active(self, instance):
        return instance.is_active

    class Meta:
        model = Card
        fields = ['pk', 'number', 'code', 'qrcode_str', 'qrcode_img', 'points', 'is_active']


class ScanPostSerializer(serializers.Serializer):
    pk = serializers.ReadOnlyField()
    code = serializers.CharField(max_length=255)
    device = serializers.CharField(max_length=255)

    class Meta:
        fields = ['pk', 'code', 'device']


class ScanSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    card = CardSerializer()
    #stand = StandSerializer()

    class Meta:
        model = Scan
        fields = ['pk', 'stand', 'card', 'created', 'status', 'details']