# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
# from rest_framework.authentication import TokenAuthentication
# from rest_framework.permissions import IsAuthenticated
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError
from rest_framework.views import APIView
from django_ean13.validators import EAN13_validator
from django_utils.utils import my_import
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from ..models import Scan
from .serializers import ScanPostSerializer
from .exceptions import (StandDoesNotExist, DeviceIsRequired, ScanCodeValueRequired, ScanCodeValueError,
                         ScanCodeTypeUnrecognized, CardIsNotEAN13, CardDoesNotExist, ScanHasNoMethod, CardHasNotMember)


# Validators!
CARD_VALIDATORS = getattr(settings, 'CARD_VALIDATORS', [(EAN13_validator, CardIsNotEAN13),
                                                        (card_exists_validator, CardDoesNotExist)])
validators = []
for validator in CARD_VALIDATORS:
    if type(validator[0]) == str and type(validator[1]) == str:
        validator = [my_import(validator[0]), my_import(validator[1])]
    validators.append(validator)
CARD_VALIDATORS = validators


class ScanBaseAPIView(APIView):
    SELFKIOSK = 'selfkiosk'
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated, IsLocalizatorPermission, ApiIsEnabledPermission)
    serializer_class = ScanPostSerializer
    model = Scan
    stand = None
    code_type = None
    code_value = None
    card = None
    coupon = None
    device = None
    force_code_type = None

    def initial(self, request, *args, **kwargs):
        super(ScanBaseAPIView, self).initial(request, args, kwargs)

        # Check device
        self.device = request.POST.get('device', request.GET.get('device', kwargs.get('device')))
        if not self.device:
            raise DeviceIsRequired()

        # Check verifier
        try:
            self.stand = Stand.objects.get(device=self.device)
        except Stand.DoesNotExist:
            raise StandDoesNotExist(device=self.device)

    def get_timestamp(self):
        tznow = timezone.now()
        return '{:%d-%m-%Y:%H:%M:%S}.{:03d}'.format(tznow, tznow.microsecond // 1000)

    def validate(self, request, *args, **kwargs):
        # Check code
        self.code_type, self.code_value = self._resolve_code(request, args, kwargs)
        func_name = 'validate_%s' % self.code_type
        try:
            return getattr(self, func_name)(request, *args, **kwargs)
        except AttributeError:
            raise ScanHasNoMethod(func_name=func_name)

    def _resolve_code(self, request, *args, **kwargs):
        # Check code
        code = request.POST.get('code')
        if not code:
            raise ScanCodeValueRequired()
        if ';' in code:
            code = code.replace(';', '%3B')
        try:
            code_type, code_value = code.split('%3B')  # ;
        except ValueError:
            if self.force_code_type:
                return self.force_code_type, code
            raise ScanCodeValueError()
        else:
            if code_type not in [Coupon.QRCODE_TYPE, Card.QRCODE_TYPE, Seller.QRCODE_TYPE]:
                raise ScanCodeTypeUnrecognized(code_type=code_type)
        return code_type, code_value

    # =========================
    # ===== HANDLE TYPES ======
    # =========================
    def validate_card(self, request, *args, **kwargs):
        card = None
        for item in CARD_VALIDATORS:
            try:
                card = item[0](self.code_value)
            except ValidationError, e:
                try:
                    _validator = item[1](code_type=self.code_type, code_value=self.code_value)
                except TypeError:
                    _validator = item[1](str(e))
                # Raise custom error
                raise _validator

        self.card = card

        if self.device == self.SELFKIOSK:
            # Raise exc for empty member
            if not self.card.member:
                raise CardHasNotMember()

            # Check limits
            try:
                card_purchase_limit_validator(self.card)
            except ValidationError, e:
                raise PurchaseRegisterLimitsException(mess=e.message)