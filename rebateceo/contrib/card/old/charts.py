# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import date, timedelta
from django_utils.utils import date_iterator, date_to_timestamp
from rabatomat_stats.stat.utils import ExportObject
from .models import Activation, Scan, Card


def calculate_activation_chart(from_date, to_date=None):
    if to_date is None:
        to_date = date.today()

    stats_sms = dict(Activation.get_grouped_stats(Activation.SMS))
    stats_operator = dict(Activation.get_grouped_stats(Activation.OPERATOR))
    stats_all = dict(Activation.get_grouped_stats())

    sms = []
    operator = []
    card_activations = []
    stats = []

    for day in date_iterator(from_date, to_date):
        timestamp = date_to_timestamp(day)

        sms_by_day = stats_sms.get(str(day), 0)
        operator_by_day = stats_operator.get(str(day), 0)
        stats_by_day = stats_all.get(str(day), 0)

        sms.append((timestamp, sms_by_day))
        operator.append((timestamp, operator_by_day))

        row = ExportObject()
        row.date = day
        row.count = stats_by_day
        stats.append(row)

        card_activations.append({'y': str(day).replace('%s-' % day.year, ''),
                                 'a': sms_by_day,
                                 'b': operator_by_day})

    return {'sms': sms, 'operator': operator, 'card_activations': card_activations, 'export_stats': stats}

def calculate_cards_chart(from_date=None, to_date=None):
    if to_date is None:
        to_date = date.today()

    if from_date is None:
        from_date = (date.today() - timedelta(days=30)).replace(day=1)

    stats_all = dict(Card.get_grouped_stats())

    sms = []
    operator = []
    card_activations = []
    stats = []

    for day in date_iterator(from_date, to_date):
        timestamp = date_to_timestamp(day)

        stats_by_day = stats_all.get(str(day), 0)


        row = ExportObject()
        row.date = day
        row.count = stats_by_day
        stats.append(row)

        card_activations.append({'y': str(day).replace('%s-' % day.year, ''),
                                 'a': stats_by_day
                                })

    return {'sms': sms, 'operator': operator, 'card_activations': card_activations, 'export_stats': stats}


def calculate_scan_chart(from_date, to_date=None):
    if to_date is None:
        to_date = date.today()

    stats_with_discount = dict(Scan.get_grouped_stats(Scan.WITH_DISCOUNT))
    stats_without_discount = dict(Scan.get_grouped_stats(Scan.WITHOUT_DISCOUNT))

    with_discount = []
    without_discount = []
    card_scans = []

    for day in date_iterator(from_date, to_date):
        timestamp = date_to_timestamp(day)

        with_discounts_by_day = stats_with_discount.get(str(day), 0)
        without_discounts_by_day = stats_without_discount.get(str(day), 0)

        with_discount.append((timestamp, with_discounts_by_day))
        without_discount.append((timestamp, without_discounts_by_day))

        card_scans.append({'y': str(day).replace('%s-' % day.year, ''),
                           'a': without_discounts_by_day,
                           'b': with_discounts_by_day})

    return {'with_discount': with_discount, 'without_discount': without_discount, 'card_scans': card_scans}