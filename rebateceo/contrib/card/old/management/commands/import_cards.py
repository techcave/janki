# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from settings import ROOT, path
from django.conf import settings
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator
from rabatomat_cards.card.models import Card
from rabatomat_lottery.lottery.models import WinningCard
from django.utils.translation import ugettext_lazy as _


DEMO_PATH = path(ROOT, '..', 'data', 'import')
CARDS = open(path(DEMO_PATH, 'kody_kart_dla_galerii_cho_seria_042014.csv')).readlines()


class Command(BaseCommand):
    help = _(u'Import all cards')

    def handle(self, *args, **options):
        if datetime.date.today() >= datetime.date(2014, 4, 1):
            raise _(u'Lottery in progress! Cannot import cards')

        if not settings.DEBUG:
            raise RuntimeWarning(_("Cannot import cards in PRODUCTION mode!"))

        WinningCard.objects.all().delete()
        Card.objects.all().delete()
        Card.objects.bulk_create([Card(number=EAN13_validator(number.strip())) for number in CARDS], batch_size=None)
        print _("Imported %s cards") % Card.objects.count()