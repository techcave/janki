# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from settings import ROOT, path
from django.conf import settings
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator
from rabatomat_cards.card.models import Card
from django.utils.translation import ugettext_lazy as _


DEMO_PATH = path(ROOT, '..', 'data', 'import')


class Command(BaseCommand):
    help = _(u'Import all cards')

    def handle(self, *args, **options):
        if datetime.date.today() >= datetime.date(2014, 4, 1):
            raise _(u'Lottery in progress! Cannot import cards')

#        if not settings.DEBUG:
#            raise RuntimeWarning("Nie mozna importować kart w trybie PRODUKCYJNYM !")

        file = str(args[0])
        CARDS = open(path(DEMO_PATH, file)).readlines()

        marked = 0
        for number in CARDS:
            card = Card.objects.get(number=EAN13_validator(number.strip()), printed=False)
            card.printed = True
            card.save()

        print _("%s cards marked as printed card") % marked
