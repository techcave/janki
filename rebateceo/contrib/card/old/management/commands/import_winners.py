# -*- coding: utf-8 -*-
import datetime
from settings import ROOT, path
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator
from rabatomat_cards.card.models import Card
from rabatomat_lottery.lottery.models import Lottery, WinningCard
from django.utils.translation import ugettext_lazy as _


DEMO_PATH = path(ROOT, '..', 'data', 'import')


class Command(BaseCommand):
    help = _(u'Import winning cards')

    def handle(self, *args, **options):
        if datetime.date.today() >= datetime.date(2014, 4, 1):
            raise _(u'Lottery in progress! Cannot import cards')

        file = str(args[0])
        WINNING_CARDS = open(path(DEMO_PATH, file)).readlines()

        lottery = Lottery.objects.get_current()
        WinningCard.objects.all().delete()

        for index, row in enumerate(WINNING_CARDS):
            item = row.split(';')
            number, date = item[0], item[1]
            card = Card.objects.get(number=EAN13_validator(number))
            data = {
                'index': index + 1,
                'lottery': lottery,
                'card': card,
                'date': str(date).strip()
            }
            item = WinningCard.objects.create(**data)
            print item

        print _("Imported %s winner card") % WinningCard.objects.count()
