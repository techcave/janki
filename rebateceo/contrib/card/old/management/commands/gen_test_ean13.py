# -*- coding: utf-8 -*-
import barcode
from django.core.management import BaseCommand
from django_ean13.validators import EAN13_validator
from rabatomat_cards.card.models import Card
from django.utils.translation import ugettext_lazy as _


class Command(BaseCommand):
    help = _(u'Generate codes for tests')

    def handle(self, *args, **options):
        EAN = barcode.get_barcode_class('ean13')
        for i in range(0, 1000):
            codebase = "5%s" % str(i).zfill(11)
            card, created = Card.objects.get_or_create(number=EAN13_validator(str(EAN(codebase))))
            print card, created