# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import csv
import StringIO
from django.utils.encoding import smart_str
from .models import Activation, Scan, Card
from django.utils.translation import ugettext as _


def get_activations_csv(queryset=None):
    queryset = queryset or Activation.objects.all().order_by('-date')
    csvfile = StringIO.StringIO()
    csvwriter = csv.writer(csvfile, delimiter=';', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect=csv.excel)

    csvwriter.writerow([
        _(u"ID"),
        _(u"Card Number"),
        _(u"Type"),
        _(u"Operator"),
        _(u"Source Mobile"),
        _(u"Status"),
        _(u"Status details"),
        _(u"Created date"),
        _(u"Created time"),
    ])

    for obj in queryset:
        csvwriter.writerow([
            smart_str(obj.pk),
            smart_str(obj.card.number if obj.card else ''),
            smart_str(obj.type),
            smart_str(obj.operator),
            smart_str(obj.mobile_number),
            smart_str(obj.status),
            smart_str(obj.details),
            smart_str(obj.date)[0:10],
            smart_str(obj.date)[11:19],
        ])

    return csvfile


def get_scans_csv(queryset=None):
    queryset = queryset or Scan.objects.all().order_by('-created')
    csvfile = StringIO.StringIO()
    csvwriter = csv.writer(csvfile, delimiter=';', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect=csv.excel)

    csvwriter.writerow([
        smart_str(_(u"ID")),
        smart_str(_(u"Card Number")),
        smart_str(_(u"Status")),
        smart_str(_(u"Status details")),
        smart_str(_(u"Created date")),
        smart_str(_(u"Created time")),
    ])

    for obj in queryset:
        csvwriter.writerow([
            smart_str(obj.pk),
            smart_str(obj.card.number if obj.card else ''),
            smart_str(obj.status),
            smart_str(obj.details),
            smart_str(obj.created)[0:10],
            smart_str(obj.created)[11:19],
        ])

    return csvfile

def get_cards_csv(queryset=None):
    queryset = queryset or Card.objects.all().order_by('-created')
    csvfile = StringIO.StringIO()
    csvwriter = csv.writer(csvfile, delimiter=';', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect=csv.excel)

    csvwriter.writerow([
        smart_str(_(u"ID")),
        smart_str(_(u"Card Number")),
        smart_str(_(u"Printed")),
        smart_str(_(u"Created date")),
        smart_str(_(u"Created time")),
        smart_str(_(u"Updated at date")),
        smart_str(_(u"Updated at time")),
        smart_str(_(u"Test")),
    ])

    for obj in queryset:
        csvwriter.writerow([
            smart_str(obj.pk),
            smart_str(obj.number),
            smart_str(obj.printed),
            smart_str(obj.created)[0:10],
            smart_str(obj.created)[11:19],
            smart_str(obj.updated_at)[0:10],
            smart_str(obj.updated_at)[11:19],
            smart_str(obj.test),
        ])

    return csvfile