# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.admin import ModelAdmin, site

from django_utils.admin import ReadonlyAdmin
from .models import Card, Activation, Scan, Status
from .forms import CardAdminForm


class CardAdmin(ModelAdmin):
    list_display = ('number', 'code', 'member', 'created', 'updated_at', 'test', 'status', 'qrcode_str')
    search_fields = ('number',)
    list_filter = ('printed', 'status')
    list_display_links = ('number',)
    form = CardAdminForm

    def qrcode_str(self, obj):
        return '<a href="{src}" target="_blank">{qrcode_str}</a>'.\
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)
    qrcode_str.allow_tags = True

    def qrcode(self, obj):
        return '<a href="{src}" target="_blank"><img src="{src}" /></a><br><input value="{qrcode_str}"/>'.\
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)
    qrcode.allow_tags = True


class ActivationAdmin(ModelAdmin):
    list_display = ('pk', 'card', 'type', 'date', 'status', 'operator', 'mobile_number', 'sms_content', 'get_details')
    list_filter = ('type', 'operator', 'date', 'status',)
    search_fields = ('card__number', 'type', 'operator__username', 'mobile_number', 'sms_content', 'date', 'status', 'details')
    #readonly_fields = ('card', 'type', 'operator', 'mobile_number', 'sms_content', 'date', 'status', 'details')

    def get_details(self, obj):
        return "<pre>%s</pre>" % obj.details
    get_details.allow_tags = True


class ScanAdmin(ModelAdmin):
    list_display = ('pk', 'card', 'stand', 'created', 'updated', 'timestamp', 'status', 'details', 'request', 'response')
    list_filter = ('stand', 'created', 'status',)
    search_fields = ('card__number', 'stand__device', 'created', 'updated', 'timestamp', 'status', 'details')
    readonly_fields = ('card', 'stand', 'created', 'updated', 'timestamp', 'status', 'details', 'request', 'response')


class StatusAdmin(ModelAdmin):
    list_display = ('pk', 'symbol', 'title', 'description')


site.register(Card, CardAdmin)
site.register(Activation, ActivationAdmin)
site.register(Scan, ScanAdmin)
site.register(Status, StatusAdmin)
