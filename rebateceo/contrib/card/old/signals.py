# -*- coding: utf-8 -*-
from django.db import models

from rabatomat_members.member.models import Member
from rabatomat_sms.sms.client import smsclient
from django.utils.translation import ugettext_lazy as _

from .models import Activation


def create_member_by_sms(sender, instance, created, **kwargs):
    """
    Create member if it not exists
    """
    if created and instance.type == Activation.SMS:
        if instance.card and instance.status:  # Rejestruj uczetnika tylko gdy aktywacja byla OK!
            member, created = Member.objects.get_or_create(card=instance.card)
            if created:
                member.mobile_number = instance.mobile_number
                member.save()

                # Send SMS to user
                smsclient.sendsms(member.mobile_number, _("Card with numer %s. was successfully registered. You can check your prize in Rabatomat.") % instance.card.number)


models.signals.post_save.connect(create_member_by_sms, sender=Activation)