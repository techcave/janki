# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.admin import site, ModelAdmin
from . import models


class CardAdmin(ModelAdmin):
    list_display = ('type', 'profile', 'number', 'created_at', 'updated_at', 'test', 'status', 'qrcode')
    search_fields = ('number',)
    list_display_links = ('number',)

    def qrcode_str(self, obj):
        return '<a href="{src}" target="_blank">{qrcode_str}</a>'.\
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)
    qrcode_str.allow_tags = True

    def qrcode(self, obj):
        return '<a href="{src}" target="_blank"><img src="{src}" /></a><br><input value="{qrcode_str}"/>'.\
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)
    qrcode.allow_tags = True


class StatusAdmin(ModelAdmin):
    list_display = ('pk', 'symbol', 'title', 'description')


class TypeAdmin(ModelAdmin):
    list_display = ['title', 'symbol', 'admin_image', 'description']
    prepopulated_fields = {"slug": ("title",)}
    list_per_page = 100


class ScanAdmin(ModelAdmin):
    list_display = ['pk', 'session', 'card', 'status']
    list_per_page = 100


site.register(models.Status, StatusAdmin)
site.register(models.Card, CardAdmin)
site.register(models.Type, TypeAdmin)
site.register(models.Scan, ScanAdmin)
