from rest_framework.reverse import reverse
from ..factories import StandFactory
from ...api.tests import APITestBase


class StandTest(APITestBase):
    def test_list(self):
        limit = 3
        items = [StandFactory() for x in range(0, limit)]

        response = self.client.get(reverse('api:stand:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Stand list test failed")