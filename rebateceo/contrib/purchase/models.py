# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import itertools
from decimal import Decimal as D

import reversion
from datetime import datetime
from django.utils import timezone
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


def upload_to(instance, filename):
    return '/'.join(['purchases', str(filename)])


class Amount(object):
    @staticmethod
    def to_decimal(amount):
        return D(str(amount).replace(',', '.'))


class Rule(object):
    def calc_from_multiplier(self, rules, amount):
        return int(Amount.to_decimal(amount) / Amount.to_decimal(rules[0][0]))

    def calc_from_thresholds(self, rules, amount):
        for rule in rules:
            if rule[0] <= Amount.to_decimal(amount) <= rule[1]:
                return rule[2]
        return 0

    def current(self, for_date=None):
        return settings.RECEIPT_CHANCES_RULES[0]
        # date = for_date if for_date else datetime.now()
        # for rule in settings.RECEIPT_CHANCES_RULES:
        #     if rule['from'] <= date <= rule['to']:
        #         return rule


class PurchaseManager(models.Manager):
    def current_for_user(self, user):
        return self.filter(user=user)


class Purchase(models.Model):
    ORIGIN_RABATOMAT = 'rab'
    ORIGIN_PANEL = 'pan'
    ORIGIN_MOBILE = 'mob'
    ORIGIN_WWW = 'www'
    ORIGIN_VERIFIER = 'ver'
    ORIGIN_SELFKIOSK = 'sk'
    ORIGIN = (
        (ORIGIN_PANEL, _("Panel Info")),
        (ORIGIN_RABATOMAT, _("Rabatomat")),
        (ORIGIN_MOBILE, _("Mobile")),
        (ORIGIN_WWW, _("Website")),
        (ORIGIN_VERIFIER, _("Verifier")),
        (ORIGIN_SELFKIOSK, _("Self-Kiosk")),
    )

    user = models.ForeignKey(User, verbose_name=_('User'), db_index=True, related_name='purchases')
    shop = models.ForeignKey('shop.Shop', verbose_name=_('Shop'), db_index=True)
    amount = models.DecimalField(verbose_name=_('Amount'), decimal_places=2, max_digits=10, db_index=True)
    number = models.CharField(verbose_name=_('Number'), max_length=64, db_index=True)
    origin = models.CharField(verbose_name=_('Origin'), choices=ORIGIN, default=ORIGIN_PANEL, blank=True, null=True,
                              max_length=3)
    photo = models.ImageField(verbose_name=_('Photo'), upload_to=upload_to, blank=True, null=True)
    purchase_date = models.DateTimeField(verbose_name=_('Purchase date'), default=datetime.now)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    operator = models.ForeignKey(User, verbose_name=_('Operator'), blank=True, null=True,
                                 related_name='registered_purchases')
    notes = models.TextField(verbose_name=_('Notes'), blank=True)

    objects = PurchaseManager()
    rule = Rule()

    class Meta:
        verbose_name = _("Purchase")
        verbose_name_plural = _("Purchases")

    def __unicode__(self):
        return u"%s - %s - (%s)" % (self.number, self.amount, self.shop)

    @staticmethod
    def chances(amount, for_date=None):
        rule = Purchase.rule.current(for_date)
        return getattr(Purchase.rule, rule.get('func'))(rule.get('rules'), amount)

    @property
    def attempts(self):
        return Purchase.chances(amount=self.amount, for_date=self.created_at)

    @classmethod
    def get_grouped_stats(cls, type=''):
        def group_result(items):
            if type == 'amount':
                return sum([i.amount for i in items])
            elif type == 'attempts':
                return sum([i.attempts for i in items])
            return len(list(items))

        items = cls.objects.all().order_by('created_at')
        grouped = itertools.groupby(items, lambda x: x.created_at.strftime("%Y-%m-%d"))
        return [(day, group_result(by_this_day)) for day, by_this_day in grouped]


reversion.register(Purchase)
