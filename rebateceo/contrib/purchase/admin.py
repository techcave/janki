# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import ModelAdmin, site
from .models import Purchase


class PurchaseAdmin(ModelAdmin):
    list_display = ('pk', 'user', 'shop', 'amount', 'number', 'purchase_date', 'origin', 'show_photo', 'created_at', 'operator', 'notes')
    list_filter = ('shop', 'purchase_date', 'origin', 'created_at')
    search_fields = ('number', 'amount', 'origin', 'purchase_date', 'created_at')
    #readonly_fields = ('shop', 'amount', 'number', 'purchase_date', 'photo', 'origin', 'created_at')

    def show_photo(self, instance):
        return "<a href='{url}' target='_blank'><img src='{url}' width='200' /></a>".format(url=instance.photo.url) \
            if instance.photo else '-'
    show_photo.allow_tags = True
    show_photo.short_description = _("Purchase Photo")


site.register(Purchase, PurchaseAdmin)