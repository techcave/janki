# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import csv
import StringIO
from django.utils.translation import ugettext as _
from django.utils.encoding import smart_str
from .models import Purchase


def get_purchases_csv(queryset=None):
    queryset = queryset or Purchase.objects.all().order_by('-created')
    csvfile = StringIO.StringIO()
    csvwriter = csv.writer(csvfile, delimiter=';', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect=csv.excel)

    csvwriter.writerow([
        u"ID",
        u"Shop",
        u"Amount",
        u"Number",
        u"Purchase date",
        u"Origin",
        u"Photo",
        u"Created date",
        u"Created time",
    ])

    for obj in queryset:
        csvwriter.writerow([
            smart_str(obj.pk),
            smart_str(obj.shop.name if obj.shop else ''),
            smart_str(obj.amount),
            smart_str(obj.number),
            smart_str(obj.purchase_date),
            smart_str(obj.origin),
            smart_str(obj.photo.url) if obj.photo else '',
            smart_str(obj.created)[0:10],
            smart_str(obj.created)[11:19],
        ])


    return csvfile