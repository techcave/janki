# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Maciej Urbanek, Jakub Pelczar
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from decimal import Decimal as D
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


VERIFIER_REWARD_MAP = getattr(settings, 'VERIFIER_REWARD_MAP', (
    (D('500'), D('300')),
    (D('100'), D('100')),
    (D('300'), D('500'))
))


class PrizeConfigNotDefined(Exception):
    pass


def get_reward(amount):
    """
    Calculates reward connected with given amount of money.

    :param amount: amount took into account in choosing the reward value
    :return: reward value
    """
    try:
        sorted_prizes = sorted(VERIFIER_REWARD_MAP)
        for (limit, reward) in sorted_prizes:
            if amount <= reward:
                return reward
        return sorted_prizes[-1][1]
    except Exception:
        raise PrizeConfigNotDefined(_('Define PRIZES config variable in format ( (limit1, prize1), (limit2, prize2), ... )'))