import os
import datetime
from decimal import Decimal as D
from django.core.files.base import File as DjangoFile
from rest_framework.reverse import reverse
from rebateceo.contrib.contest.factories import ContestFactory
from rebateceo.contrib.shop.models import Shop
from rebateceo.contrib.profile.models import Session

from ...api.tests import APITestBase
from ..factories import PurchaseFactory


class PurchaseTest(APITestBase):

    def setUp(self):
        super(PurchaseTest, self).setUp()

        self.contest = ContestFactory(
            pk=1, title='Contest', active=True,
            success_banner=DjangoFile(file(os.path.dirname(__file__) + '/../assets/success_banner.png', 'rb')))

        self.session = Session.objects.create()
        self.shop = Shop.objects.create(name='Contest')
        self.origin = 'mob'
        self.sample_data = {
            'user': self.user.pk,
            'shop': self.shop.pk,
            'amount': D('21.00'),
            'number': 1000,
            'origin': self.origin,
            'purchase_date': datetime.datetime(2015, 3, 3, 10, 0, 0),
            'photo': open(os.path.dirname(__file__) + '/../assets/300x400.gif', 'rb')
        }

    def test_list(self):
        limit = 3
        [PurchaseFactory(amount=10) for x in range(0, limit)]
        response = self.client.get(reverse('api:purchase:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Purchase list test failed")

    def test_create(self):
        response = self.client.post(reverse('api:purchase:create'), self.sample_data, **self.headers)
        self.assertIn(response.status_code, [200, 201])