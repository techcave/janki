# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime, timedelta
from rest_framework.exceptions import ValidationError
from rest_framework import serializers
from .. import models
from ..utils.reward_calulator import get_reward


class PurchaseCreateSerializer(serializers.ModelSerializer):
    extra_info = serializers.SerializerMethodField()

    class Meta:
        model = models.Purchase
        fields = ['pk', 'user', 'shop', 'amount', 'number', 'origin', 'purchase_date', 'photo', 'purchase_date',
                  'created_at', 'operator', 'notes', 'extra_info']

    def get_extra_info(self, purchase):
        card = purchase.user.profile.card_set.first()
        attempts = card.attempts
        all_purchases = purchase.user.purchases.all()
        delta = datetime.now() - timedelta(days=30)
        last_month_purchases = all_purchases.filter(created_at__gte=delta)
        attempts.update({
            'current': purchase.attempts,
        })

        return {
            'member': {
                'lottery_info': get_reward(purchase.amount),
                'attempts': attempts,
                'purchases': {
                    'all': len(all_purchases),
                    'last_month': len(last_month_purchases),
                }
            }
        }


class PurchaseSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.Purchase
        fields = ['pk', 'user', 'shop', 'amount', 'number', 'origin', 'purchase_date', 'photo', 'purchase_date',
                  'created_at', 'operator', 'notes']


class PurchaseChancesSerializer(serializers.Serializer):
    amount = serializers.CharField(required=True)

    def validate_amount(self, value):
        if not value:
            raise ValidationError('Amount is required!')
