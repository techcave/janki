# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework.generics import GenericAPIView, CreateAPIView, UpdateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from . import serializers
from ..models import Purchase


def get_chances(amount):
    return {
        'chances': Purchase.chances(amount),
        'points_map': Purchase.rule.current()
    }


class AuthMixin(object):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)


class PurchaseListView(AuthMixin, ListAPIView):
    serializer_class = serializers.PurchaseSerializer
    queryset = Purchase.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class PurchaseVerifyView(AuthMixin, GenericAPIView):
    serializer_class = serializers.PurchaseCreateSerializer

    def post(self, request, *args, **kwargs):
        response = {}
        amount = request.POST.get('amount')
        serializer = serializers.PurchaseCreateSerializer(data={
            'amount': amount,
            'shop': request.POST.get('shop'),
            'origin': request.POST.get('origin'),
            'number': request.POST.get('number')
        })

        # this raises exc if not valid
        if serializer.is_valid():
            response.update(get_chances(amount=amount))
        return Response(response)


class PurchaseCreateView(AuthMixin, CreateAPIView):
    serializer_class = serializers.PurchaseCreateSerializer


class PurchasePatchView(AuthMixin, UpdateAPIView):
    serializer_class = serializers.PurchaseSerializer
    model = Purchase


class PurchaseUpdateView(AuthMixin, UpdateAPIView):
    serializer_class = serializers.PurchaseSerializer
    model = Purchase
    object = None

    def post(self, request, *args, **kwargs):
        self.object = self.get_object_or_none()
        self.object.photo = request.FILES['photo']
        self.object.save()
        return Response(serializers.PurchaseSerializer(self.object).data)


class PurchaseChancesView(AuthMixin, GenericAPIView):
    serializer_class = serializers.PurchaseChancesSerializer

    def post(self, request, *args, **kwargs):
        amount = request.POST.get('amount')
        return Response(get_chances(amount=amount))
