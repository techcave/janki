# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchase', '0003_remove_purchase_register_date'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='purchase',
            unique_together=set([('user', 'shop', 'amount', 'number', 'purchase_date', 'origin')]),
        ),
    ]
