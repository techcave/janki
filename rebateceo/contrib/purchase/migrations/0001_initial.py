# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import rebateceo.contrib.purchase.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0003_auto_20150224_1402'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(verbose_name='Amount', max_digits=10, decimal_places=2, db_index=True)),
                ('number', models.CharField(db_index=True, max_length=64, null=True, verbose_name='Number', blank=True)),
                ('origin', models.CharField(default=b'pan', choices=[(b'pan', 'Panel Info'), (b'rab', 'Rabatomat'), (b'mob', 'Mobile'), (b'www', 'Website'), (b'ver', 'Verifier'), (b'sk', 'Self-Kiosk')], max_length=3, blank=True, null=True, verbose_name='Origin')),
                ('photo', models.ImageField(upload_to=rebateceo.contrib.purchase.models.upload_to, null=True, verbose_name='Photo', blank=True)),
                ('purchase_date', models.DateTimeField(default=datetime.datetime.now, verbose_name='Purchase date')),
                ('register_date', models.DateTimeField(default=datetime.datetime.now, verbose_name='Register date')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('notes', models.TextField(verbose_name='Notes', blank=True)),
                ('operator', models.ForeignKey(related_name='registered_purchases', verbose_name='Operator', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('shop', models.ForeignKey(verbose_name='Shop', to='shop.Shop')),
                ('user', models.ForeignKey(related_name='purchases', verbose_name='User', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Purchase',
                'verbose_name_plural': 'Purchases',
            },
            bases=(models.Model,),
        ),
    ]
