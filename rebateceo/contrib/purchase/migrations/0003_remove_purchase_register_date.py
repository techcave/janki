# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchase', '0002_auto_20150226_0830'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchase',
            name='register_date',
        ),
    ]
