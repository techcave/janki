# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchase', '0006_auto_20150527_1129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='origin',
            field=models.CharField(default=b'pan', choices=[(b'pan', 'Panel Info'), (b'rab', 'Rabatomat'), (b'mob', 'Mobile'), (b'www', 'Website'), (b'ver', 'Verifier'), (b'sk', 'Self-Kiosk')], max_length=3, blank=True, null=True, verbose_name='Origin'),
            preserve_default=True,
        ),
    ]
