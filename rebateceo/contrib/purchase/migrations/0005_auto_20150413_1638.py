# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchase', '0004_auto_20150226_0842'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='purchase',
            unique_together=set([]),
        ),
    ]
