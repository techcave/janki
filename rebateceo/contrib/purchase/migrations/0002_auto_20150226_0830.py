# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchase', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='number',
            field=models.CharField(default='b/d', max_length=64, verbose_name='Number', db_index=True),
            preserve_default=False,
        ),
    ]
