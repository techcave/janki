# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchase', '0005_auto_20150413_1638'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='origin',
            field=models.CharField(default=b'panel', choices=[(b'panel', 'Panel Info'), (b'rabatomat', 'Rabatomat'), (b'mobile', 'Mobile'), (b'website', 'Website'), (b'verifier', 'Verifier'), (b'selfkiosk', 'Self-Kiosk')], max_length=3, blank=True, null=True, verbose_name='Origin'),
            preserve_default=True,
        ),
    ]
