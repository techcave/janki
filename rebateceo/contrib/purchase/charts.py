# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from decimal import Decimal
from django.core.cache import cache
from django_utils.utils import date_iterator
from rebateceo.contrib.stat.utils import ExportObject
from .models import Purchase


def calculate_purchase_chart(date_from, date_to, type=''):
    key, lifetime = 'calculate_purchase_chart_%s_%s_%s' % (date_from, date_to, type), 60*15  # 15 minut
    result = cache.get(key)
    if not result:
        stats = dict(Purchase.get_grouped_stats(type=type))
        purchases_stats = []
        export_stats = []
        for day in date_iterator(date_from, date_to):
            stats_by_day = stats.get(str(day), 0)
            purchases_stats.append({'y': str(day).replace('%s-' % day.year, ''),
                                  'a': str(stats_by_day)})

            row = ExportObject()
            row.date = day
            row.value = stats_by_day if not isinstance(stats_by_day, Decimal) else str(stats_by_day).replace('.', ',')
            export_stats.append((day, row))
        result = {'stats': purchases_stats, 'export_stats': export_stats}
        cache.set(key, result, lifetime)
    return result
