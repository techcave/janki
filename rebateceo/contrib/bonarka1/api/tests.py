import os
import datetime
from django.core.files.base import File as DjangoFile
from rebateceo.contrib.shop.factories import ShopFactory
from rest_framework.reverse import reverse
from rebateceo.contrib.api import codes
from rebateceo.contrib.contest.models import Contest
from rebateceo.contrib.purchase.models import Purchase
from rebateceo.contrib.profile.factories import UserFactory, SessionFactory
from ..factories import bonarka1Factory
from ...api.tests import APITestBase


class bonarka1Test(APITestBase):
    """
    bin/backend-test test rebateceo.contrib.bonarka1.api.tests.bonarka1Test
    """

    def setUp(self):
        super(bonarka1Test, self).setUp()

        self.origin = 'mob'
        self.session = SessionFactory()
        self.shop = ShopFactory()
        self.contest = Contest.objects.create(
            pk=1,
            title='Contest',
            active=True,
            min_amount='50.00',
            publication_date=datetime.datetime(year=2015, month=2, day=1, hour=0, minute=0, second=0),
            publication_end_date=datetime.datetime(year=2030, month=2, day=1, hour=0, minute=0, second=0),
            success_banner=DjangoFile(file(os.path.dirname(__file__) + '/../assets/success_banner.png', 'rb')))
        self.contest.shops.add(self.shop)
        self.contest.save()

        self.data = {
            'contest_id': self.contest.pk,
            'session_id': self.session.pk,
            'purchase.user': self.user.pk,
            'purchase.shop': self.shop.pk,
            'purchase.amount': 51.00,
            'purchase.number': 1000,
            'purchase.origin': self.origin,
            'purchase.purchase_date': datetime.datetime(year=2017, month=2, day=1, hour=0, minute=0, second=0),
            'purchase.photo': open(os.path.dirname(__file__) + '/../assets/300x400.gif', 'rb')
        }

    def test_list(self):
        limit = 3
        [bonarka1Factory() for x in range(0, limit)]
        response = self.client.get(reverse('api:bonarka1:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit)

    def test_create(self):
        response = self.client.post(reverse('api:bonarka1:create'), self.data, **self.headers)
        self.assertEqual(response.data['pk'], 1)

    def test_create_with_less_min_amount(self):
        data = self.data
        data['purchase.amount'] = 40.00

        response = self.client.post(reverse('api:bonarka1:create'), data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_MINIMUM_AMOUNT_REQUIRED)

    def test_create_with_wrong_shop(self):
        data = self.data
        data['purchase.shop'] = ShopFactory(name='SampleShopWithoutContest').pk

        response = self.client.post(reverse('api:bonarka1:create'), data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_WRONG_SHOP)

    def test_create_with_wrong_purchase_date(self):
        data = self.data
        data['purchase.purchase_date'] = datetime.datetime(2020, 4, 1, 0, 0, 0)

        response = self.client.post(reverse('api:bonarka1:create'), data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_DATE_NOT_IN_RANGE)

    def test_already_exists_purchase(self):
        sample = {
            'user': self.user,
            'shop': self.shop,
            'amount': 53.00,
            'number': 100111111,
            'origin': self.origin,
            'purchase_date': datetime.datetime(year=2017, month=2, day=1, hour=0, minute=0, second=0),
        }

        exists = Purchase.objects.create(**sample)

        data = self.data
        data['purchase.user'] = self.user.pk
        data['purchase.shop'] = self.shop.pk
        data['purchase.amount'] = 53.00
        data['purchase.number'] = 100111111
        data['purchase.origin'] = self.origin
        data['purchase.purchase_date'] = datetime.datetime(year=2017, month=2, day=1, hour=0, minute=0, second=0)#datetime.datetime(2017, 4, 1, 0, 0, 0)

        response = self.client.post(reverse('api:bonarka1:create'), data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_ALREADY_EXISTS)

    def test_create_with_wrong_purchase_date(self):
        data = self.data
        data['purchase.purchase_date'] = datetime.datetime(1999, 4, 1, 0, 0, 0)

        response = self.client.post(reverse('api:bonarka1:create'), data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_DATE_NOT_IN_RANGE)

    def test_create_with_blocked_user(self):
        user = UserFactory()
        # Mark user as blocked
        user.profile.blocked_at = datetime.datetime(year=2017, month=2, day=1, hour=0, minute=0, second=0)
        user.profile.save()

        data = self.data
        data['purchase.user'] = user.pk

        response = self.client.post(reverse('api:bonarka1:create'), data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_USER_BLOCKED)

    def test_create_with_empty_publication_end_date(self):
        self.contest.publication_end_date = None
        self.contest.save()

        data = self.data
        data['purchase.purchase_date'] = datetime.datetime(1999, 4, 1, 0, 0, 0)
        response = self.client.post(reverse('api:bonarka1:create'), self.data, **self.headers)
        self.assertEqual(response.data['exception']['code'], codes.bonarka1_PURCHASE_DATE_NOT_IN_RANGE)