# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from decimal import Decimal
from django.utils import formats
from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework import validators
from rebateceo.contrib.contest.models import Contest
from rebateceo.contrib.contest.api.serializers import ContestSerializer
from rebateceo.contrib.purchase.models import Purchase
from rebateceo.contrib.profile.models import Session
from rebateceo.contrib.purchase.api.serializers import PurchaseSerializer
from ..models import bonarka1
from . import exceptions


class bonarka1Serializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    contest = ContestSerializer()
    purchase = PurchaseSerializer()

    class Meta:
        model = bonarka1
        fields = ['pk', 'contest', 'purchase', 'created_at']


class bonarka1CreateSerializer(serializers.ModelSerializer):
    session_id = serializers.IntegerField()
    contest_id = serializers.IntegerField()
    purchase = PurchaseSerializer()

    # Attr
    contest = None
    session = None

    class Meta:
        model = bonarka1
        fields = ['pk', 'session_id', 'contest_id', 'purchase', 'results']

    def validate_purchase(self, purchase):
        session_id = self.initial_data.get('session_id')
        contest_id = self.initial_data.get('contest_id')
        user = purchase.get('user')
        shop = purchase.get('shop')
        origin = purchase.get('origin')
        number = purchase.get('number')
        amount = Decimal(purchase.get('amount'))
        purchase_date = purchase.get('purchase_date')

        try:
            self.session = Session.objects.all().get(pk=session_id)
        except Session.DoesNotExist:
            raise serializers.ValidationError("Session for id={} not found or inaccessible!".format(session_id))

        try:
            self.contest = Contest.objects.active().get(pk=contest_id)
        except Contest.DoesNotExist:
            raise serializers.ValidationError("Contest for id={} not found or inaccessible!".format(contest_id))

        if not purchase_date:
            raise serializers.ValidationError(
                "purchase_date: %s" % _('This field is required.'))

        # User status
        if user.profile.blocked_at:
            raise exceptions.bonarka1PurchaseUserBlocked()

        # Amount
        limit = self.contest.min_amount
        if amount < limit:
            raise exceptions.bonarka1PurchaseMinimumAmountRequired(limit=limit, currency='PLN')

        # Shop not in contest
        if not self.contest.shops.filter(pk=shop.pk):
            raise exceptions.bonarka1PurchaseWrongShop()

        # Date check
        from_date = self.contest.publication_date
        to_date = self.contest.publication_end_date
        in_range = True
        if (from_date and to_date) and not (from_date < purchase_date < to_date):
            in_range = False
        elif (from_date and not to_date) and not (from_date < purchase_date):
            in_range = False
            to_date = datetime.datetime.now()

        if not in_range:
            raise exceptions.bonarka1PurchaseDateNotInRange(from_date=formats.date_format(from_date, "DATE_FORMAT"),
                                                            to_date=formats.date_format(to_date, "DATE_FORMAT"))
        # Purchase exists
        try:
            exist = Purchase.objects.get(shop=shop, number=number, origin=origin, purchase_date=purchase_date)
        except Purchase.DoesNotExist:
            pass
        else:
            raise exceptions.bonarka1PurchaseAlreadyExists()

        return purchase

    def create(self, validated_data):
        return bonarka1.objects.create(
            session=self.session,
            contest=self.contest,
            purchase=Purchase.objects.create(**validated_data.get('purchase'))
        )
