# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf import settings
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, permissions, status, response
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rebateceo.contrib.api.request import prepare_post
from ..models import bonarka1
from .serializers import bonarka1Serializer, bonarka1CreateSerializer

IS_LOCAL = getattr(settings, 'IS_LOCAL', False)

import logging
logger = logging.getLogger(__name__)


class bonarka1ListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = bonarka1Serializer
    queryset = bonarka1.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class bonarka1CreateView(generics.CreateAPIView):
    if not IS_LOCAL:
        authentication_classes = (TokenAuthentication,)
        permission_classes = (IsAuthenticated,)
    serializer_class = bonarka1CreateSerializer

    def post(self, request, *args, **kwargs):
        _request = request
        request = prepare_post(request)
        response = self.create(request, *args, **kwargs)

        logger.info(self.__class__, exc_info=True, extra={
            'request': request.DATA,
            'meta': request.META,
            'response': response.data
        })

        return response

