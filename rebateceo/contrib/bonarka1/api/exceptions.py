# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.forms.models import model_to_dict
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import APIException
from rest_framework import status
from rebateceo.contrib.api import codes, exceptions


class bonarka1PurchaseMinimumAmountRequired(exceptions.APIRebateceoException):
    code = codes.bonarka1_PURCHASE_MINIMUM_AMOUNT_REQUIRED
    default_detail = _(u'The minimum amount per transaction is {limit} {currency}')


class bonarka1PurchaseWrongShop(exceptions.APIRebateceoException):
    code = codes.bonarka1_PURCHASE_WRONG_SHOP
    default_detail = _(u'Error. The store does not participate in the Contest.')


class bonarka1PurchaseAlreadyExists(exceptions.APIRebateceoException):
    code = codes.bonarka1_PURCHASE_ALREADY_EXISTS
    default_detail = _(u'There is already a receipt for the given values! Registration is not possible! '
                       u'Please go to the Info Point')


class bonarka1PurchaseDateNotInRange(exceptions.APIRebateceoException):
    code = codes.bonarka1_PURCHASE_DATE_NOT_IN_RANGE
    default_detail = _(u'Purchases are accepted from the period from {from_date} to {to_date}')


class bonarka1PurchaseUserBlocked(exceptions.APIRebateceoException):
    code = codes.bonarka1_PURCHASE_USER_BLOCKED
    default_detail = _(u'Your participation in a contest is now impossible. Details on the banner competition')


