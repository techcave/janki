# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bonarka1', '0005_bonarka1_success_banner'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bonarka1',
            old_name='success_banner',
            new_name='generated_banner',
        ),
    ]
