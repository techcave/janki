# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.bonarka1.models


class Migration(migrations.Migration):

    dependencies = [
        ('bonarka1', '0004_bonarka1_session'),
    ]

    operations = [
        migrations.AddField(
            model_name='bonarka1',
            name='success_banner',
            field=models.ImageField(help_text=b'Generated IMAGE!!!', upload_to=rebateceo.contrib.bonarka1.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
