# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bonarka1', '0002_auto_20150303_1325'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bonarka1',
            name='results_banner',
        ),
    ]
