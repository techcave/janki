# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.bonarka1.models


class Migration(migrations.Migration):

    dependencies = [
        ('bonarka1', '0007_auto_20150319_1836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bonarka1',
            name='result_img',
            field=models.ImageField(help_text=b'Generated IMAGE!!!', max_length=255, upload_to=rebateceo.contrib.bonarka1.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
