# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0008_auto_20150313_1321'),
        ('bonarka1', '0003_remove_bonarka1_results_banner'),
    ]

    operations = [
        migrations.AddField(
            model_name='bonarka1',
            name='session',
            field=models.ForeignKey(blank=True, to='profile.Session', null=True),
            preserve_default=True,
        ),
    ]
