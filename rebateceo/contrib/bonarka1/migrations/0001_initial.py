# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contest', '0004_contest_created_at'),
        ('purchase', '0004_auto_20150226_0842'),
    ]

    operations = [
        migrations.CreateModel(
            name='bonarka1',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('contest', models.ForeignKey(to='contest.Contest')),
                ('purchase', models.ForeignKey(to='purchase.Purchase')),
            ],
            options={
                'verbose_name': 'lomianki contest (1)',
                'verbose_name_plural': 'lomianki contests (1)',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='bonarka1',
            unique_together=set([('contest', 'purchase')]),
        ),
    ]
