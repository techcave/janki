# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bonarka1', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bonarka1',
            name='results_banner',
            field=models.ImageField(help_text=b'Background for results after the purchase registration', upload_to=b'', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bonarka1',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated at', null=True),
            preserve_default=True,
        ),
    ]
