# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bonarka1', '0006_auto_20150319_1814'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bonarka1',
            old_name='generated_banner',
            new_name='result_img',
        ),
    ]
