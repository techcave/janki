# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import os
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.files import File
from django.db import models
from django.utils.translation import ugettext_lazy as _
from rebateceo.contrib.purchase.models import Purchase
from rebateceo.contrib.contest.models import Contest
from rebateceo.contrib.profile.models import Session


def upload_to(instance, filename):
    return u'/'.join(['contest', 'bonarka1', 'results', unicode(filename)])


class bonarka1(models.Model):
    contest = models.ForeignKey(Contest)
    purchase = models.ForeignKey(Purchase)
    session = models.ForeignKey(Session, blank=True, null=True)
    result_img = models.ImageField(max_length=255, blank=True, upload_to=upload_to, help_text='Generated IMAGE!!!')
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True, blank=True, null=True)

    def __unicode__(self):
        return u'{0} {0}'.format(str(self.contest), str(self.purchase))

    @property
    def results(self):
        result_img = self.render_image(self.points)
        result_img_url = "%s%s" % ("http://%s" % Site.objects.get_current(), result_img.url) if result_img else ''
        return {
            'points': self.points,
            'images': [
                result_img_url if result_img_url else 'b.d'
            ]
        }

    @property
    def points(self):
        return int(self.purchase.amount / 10)

    def render_image(self, points):
        fname, fext = os.path.splitext(self.contest.success_banner.url)
        img = Image.open(self.contest.success_banner.path)

        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype(os.path.dirname(__file__) + "/assets/ACaslonPro-Bold.ttf", 48)
        draw.text((50, 300), u"Uzyskałes {} pkt. ".format(points), (255, 0, 0), font=font)

        dest_dir = os.path.join(settings.MEDIA_ROOT, 'contest', 'bonarka1', 'img')
        if not os.path.isdir(dest_dir):
            os.makedirs(dest_dir)

        filename = 'points_{}_{}{}'.format(points, self.pk, fext)
        file_path = "{}".format(os.path.join(dest_dir, filename))
        img.save(file_path)

        # Store in DB
        self.result_img = File(open(file_path), name=filename)
        self.save()
        return self.result_img

    class Meta:
        verbose_name = u"lomianki contest (1)"
        verbose_name_plural = u"lomianki contests (1)"
        unique_together = ('contest', 'purchase')