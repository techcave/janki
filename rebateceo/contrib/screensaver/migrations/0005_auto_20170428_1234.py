# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('screensaver', '0004_auto_20170315_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='screensaver',
            name='show_date_from',
            field=models.DateTimeField(null=True, verbose_name=b'Pokazuj dat\xc4\x99 od', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='screensaver',
            name='show_date_to',
            field=models.DateTimeField(null=True, verbose_name=b'Pokazuj dat\xc4\x99 do', blank=True),
            preserve_default=True,
        ),
    ]
