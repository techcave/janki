# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('screensaver', '0005_auto_20170428_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='screensaver',
            name='show_date_from',
            field=models.DateTimeField(null=True, verbose_name='Show date from', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='screensaver',
            name='show_date_to',
            field=models.DateTimeField(null=True, verbose_name='Show date to', blank=True),
            preserve_default=True,
        ),
    ]
