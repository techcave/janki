# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.screensaver.models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Screensaver',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(null=True, upload_to=rebateceo.contrib.screensaver.models.upload_to, blank=True)),
                ('show_date_from', models.DateTimeField(null=True, verbose_name=b'Pokazuj dat\xc4\x99 od', blank=True)),
                ('show_date_to', models.DateTimeField(null=True, verbose_name=b'Pokazuj dat\xc4\x99 do', blank=True)),
                ('uuid', django_extensions.db.fields.UUIDField(editable=False, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
