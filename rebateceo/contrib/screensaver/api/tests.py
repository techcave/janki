from django.conf import settings
from rest_framework.reverse import reverse
from .. import factories
from ...api.tests import APITestBase


class ScreensaverTest(APITestBase):

    def _get_list(self, method):
        limit = 3
        items = [factories.ScrensaverFactory() for x in range(0, limit)]
        response = method(reverse('api:screensaver:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit)

    def test_list_GET(self):
        self._get_list(self.client.get)

    def test_list_POST(self):
        self._get_list(self.client.post)