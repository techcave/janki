from __future__ import unicode_literals
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import reversion
import datetime
from django.db.models import Q
from django.utils.translation import gettext as _
from django.db import models
from django.utils.text import slugify
from django_extensions.db.fields import UUIDField


def upload_to(instance, filename):
    return '/'.join(['screensaver', str(filename)])


class ScreensaverManager(models.Manager):
    def display(self, for_date=None, **kwargs):
        date = for_date or datetime.datetime.now()
        date_filter = Q(show_date_from__lte=date) & (Q(show_date_to__isnull=True) | Q(show_date_to__gt=date))
        queryset = self.filter(date_filter)
        return queryset


class Screensaver(models.Model):
    file = models.FileField(upload_to=upload_to, blank=True, null=True)
    show_date_from = models.DateTimeField(verbose_name=_('Show date from'), blank=True, null=True)
    show_date_to = models.DateTimeField(verbose_name=_('Show date to'), blank=True, null=True)
    uuid = UUIDField()
    objects = ScreensaverManager()

    def __unicode__(self):
        return str(self.file.url if self.file else self.pk)


reversion.register(Screensaver)