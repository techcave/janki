from django.core.management import BaseCommand


from rebateceo.contrib.cache.classes import CacheJSON
from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):
        for link in settings.CACHE_LINKS:
            print "Loading: {}".format(link['name'])
            cache = CacheJSON(settings.CACHE_FOLDER)
            cache.load(link['name'], link['link'], user=settings.API_USER, password=settings.API_PASSWORD)
            print "Done!"