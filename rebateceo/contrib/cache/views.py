# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Denys Rozlomii
@license: MIT License
@contact: denis.rozlomii@ims.fm
"""
import os
import time

from django.conf import settings
from django.core.management import call_command
from django.shortcuts import render
from django.http import Http404
from django.contrib.auth.decorators import login_required


def get_last_update(file):
    try:
        return time.ctime(os.path.getmtime(file))
    except OSError:
        return 'Cache didn\'t create'


@login_required
def cache_page(request):
    if request.method == 'GET':
        return render(request, 'cache_page.html',
                      {'last_update': get_last_update(settings.CACHE_CHECK_FILE)})
    else:
        raise Http404()


@login_required
def cache_refresh(request):
    if request.method == 'GET':
        call_command('sync_cache')
        return render(request, 'cache_page.html', {'sync': True})
    else:
        raise Http404()