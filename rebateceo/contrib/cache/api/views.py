# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Denys Rozlomii
@license: MIT License
@contact: denis.rozlomii@ims.fm
"""
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rebateceo.contrib.cache.classes import CacheJSON


class AuthMixin(object):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)


class CacheMixin(object):
    cache = CacheJSON(settings.CACHE_FOLDER)


class CacheShopList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('shops'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)


class CacheCategoryList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('categories'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)


class CacheNewsList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('news'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)


class CachePromotionList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('promotions'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)

class CachePointList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('points'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)

class CacheTagsList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('tags'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)

class CacheEdgeList(APIView, CacheMixin, AuthMixin):
    def get(self, request, *args, **kwargs):
        return Response(self.cache.get('edges'))

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)