


# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Denys Rozlomii
@license: MIT License
@contact: denys.rozlomii@ims.fm
"""

from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
    url(r'^shop/category/list/$', views.CacheCategoryList.as_view(), name='cache-categories-list'),
    url(r'^shop/list/$', views.CacheShopList.as_view(), name='cache-shops-list'),
    url(r'^news/list/$', views.CacheNewsList.as_view(), name='cache-news-list'),
    url(r'^promotion/list/$', views.CachePromotionList.as_view(), name='cache-promotions-list'),
    url(r'^tags/list/$', views.CacheTagsList.as_view(), name='cache-tags-list'),
    url(r'^map/point/list/$', views.CachePointList.as_view(), name='cache-point-list'),
    url(r'^map/edge/list/$', views.CacheEdgeList.as_view(), name='cache-edge-list'),
)
