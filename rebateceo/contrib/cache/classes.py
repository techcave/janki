# -*- coding: utf-8 -*-

import json
import os


class CacheJSON(object):
    """
    Class for caching json responses from the API.
    """
    def __init__(self, path_to_file=None):
        self._raw_data = None
        self._json_data = None
        self.path = path_to_file
        self.ext = 'json'

    def _remote_load(self, URL, method, **kwargs):
        from requests import get, post
        method = get if method.lower() == 'get' else post
        response = method(URL, auth=(kwargs.get('user'), kwargs.get('password')))
        self._raw_data = response.json()

    def _write_to_file(self, file_save):
        with open(os.path.join(self.path, file_save), "w") as file:
            json.dump(self._raw_data, file)

    def _read_from_file(self, name):
        with open(os.path.join(self.path, name), "r") as file:
           self._json_data = json.load(file)

    def get(self, name=None):
        self._read_from_file('.'.join([name, self.ext]))
        return self._json_data

    def load(self, file_save, URL, method="GET", user=None, password=None):
        self._remote_load(URL, method=method, user=user, password=password)
        self._write_to_file('.'.join([file_save, self.ext]))