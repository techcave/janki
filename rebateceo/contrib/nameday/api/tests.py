from rest_framework.reverse import reverse
from ..factories import NameDayFactory
from ...api.tests import APITestBase


class NameDayTest(APITestBase):
    def test_list(self):
        limit = 3
        [NameDayFactory(day=x, month=x+2) for x in range(1, limit+1)]

        response = self.client.get(reverse('api:nameday:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "NameDay list test failed")

    def test_list_by_month(self):
        limit = 3
        [NameDayFactory(day=x, month=x+2) for x in range(1, limit+1)]

        response = self.client.get(reverse('api:nameday:list-by-month', kwargs={"month": 3}), {}, **self.headers)
        self.assertEqual(len(response.data), 1, "NameDay by month list test failed")