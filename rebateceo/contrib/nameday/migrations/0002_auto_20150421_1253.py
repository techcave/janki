# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nameday', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nameday',
            options={'ordering': ('month',), 'verbose_name': 'Name Day'},
        ),
        migrations.AlterField(
            model_name='nameday',
            name='day',
            field=models.IntegerField(verbose_name='Day', db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='nameday',
            name='month',
            field=models.IntegerField(default=1, max_length=250, verbose_name='Month', db_index=True, choices=[(1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')]),
            preserve_default=True,
        ),
    ]
