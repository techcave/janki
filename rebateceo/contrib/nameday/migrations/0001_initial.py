# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NameDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.IntegerField(unique=True, verbose_name='Day', db_index=True)),
                ('month', models.IntegerField(default=1, max_length=250, verbose_name='Month', db_index=True, choices=[(1, 'Janurary'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')])),
                ('names', models.CharField(db_index=True, max_length=250, null=True, verbose_name='Names', blank=True)),
            ],
            options={
                'verbose_name': 'Name Day',
            },
            bases=(models.Model,),
        ),
    ]
