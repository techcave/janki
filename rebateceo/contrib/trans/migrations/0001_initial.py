# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('symbol', models.CharField(max_length=255, db_index=True)),
            ],
            options={
                'verbose_name': 'Group',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=255, db_index=True)),
                ('group', models.ForeignKey(to='trans.Group')),
            ],
            options={
                'verbose_name': 'Item',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField()),
                ('language_code', models.CharField(max_length=15, db_index=True)),
                ('master', models.ForeignKey(related_name='translations', editable=False, to='trans.Item', null=True)),
            ],
            options={
                'db_table': 'trans_item_translation',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Translation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('table_name', models.CharField(max_length=255, db_index=True)),
                ('field_name', models.CharField(max_length=255, db_index=True)),
                ('object_id', models.IntegerField(db_index=True)),
                ('lang', models.CharField(max_length=255, db_index=True)),
                ('value', models.TextField()),
            ],
            options={
                'verbose_name': 'Translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='itemtranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
