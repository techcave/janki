# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trans', '0007_auto_20170315_1110'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Translation',
        ),
        migrations.AlterModelOptions(
            name='group',
            options={'verbose_name': 'Grupa'},
        ),
        migrations.AlterModelOptions(
            name='itemtranslation',
            options={'default_permissions': (), 'verbose_name': 'Item Translation', 'managed': True},
        ),
        migrations.RemoveField(
            model_name='item',
            name='group',
        ),
        migrations.AddField(
            model_name='group',
            name='items',
            field=models.ManyToManyField(to='trans.Item', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='group',
            name='symbol',
            field=models.CharField(unique=True, max_length=255, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='itemtranslation',
            name='language_code',
            field=models.CharField(max_length=15, verbose_name='Language', db_index=True),
            preserve_default=True,
        ),
    ]
