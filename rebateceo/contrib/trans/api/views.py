# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rebateceo.contrib.api.request import prepare_post
from .serializers import ItemSerializer, GroupSerializer
from ..models import Item, Group


class AuthMixin(object):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)


class GroupCreateAPIView(AuthMixin, CreateAPIView):
    serializer_class = GroupSerializer

    def post(self, request, *args, **kwargs):
        return super(GroupCreateAPIView, self) \
            .post(prepare_post(request), args, kwargs)


class GroupListView(AuthMixin, ListAPIView):
    serializer_class = GroupSerializer
    queryset = Group.objects.all()

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)


class ItemCreateAPIView(AuthMixin, CreateAPIView):
    serializer_class = ItemSerializer

    def post(self, request, *args, **kwargs):
        return super(ItemCreateAPIView, self) \
            .post(prepare_post(request), args, kwargs)


class ItemListView(AuthMixin, ListAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)
