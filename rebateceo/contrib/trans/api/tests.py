from rest_framework.reverse import reverse
from ..factories import TransFactory
from ...api.tests import APITestBase


class TransTest(APITestBase):
    def test_list(self):
        limit = 100
        items = [TransFactory() for x in range(0, limit)]

        response = self.client.get(reverse('api:trans:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Trans list test failed")