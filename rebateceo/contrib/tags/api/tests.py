from rest_framework.reverse import reverse
from rebateceo.contrib.profile.factories import SessionFactory
from .. import models
from .. import factories
from ...api.tests import APITestBase


class TagTest(APITestBase):
    def test_list(self):
        limit = 3
        items = [factories.TagFactory() for x in range(0, limit)]

        response = self.client.get(reverse('api:tags:list'), {}, **self.headers)
        self.assertEqual(len(response.data), limit, "Tag list test failed")

    def test_click_create(self):
        tag = factories.TagFactory()
        session = SessionFactory()

        data = {
            'tag': tag.pk,
            'session': session.pk
        }
        response = self.client.post(reverse('api:tags:click-create'), data, **self.headers)
        self.assertEqual(response.data['pk'], 1)