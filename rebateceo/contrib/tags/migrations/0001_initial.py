# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('counter', models.BigIntegerField(null=True, blank=True)),
                ('lang', models.CharField(max_length=255, blank=True)),
                ('archived', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Tag',
            },
            bases=(models.Model,),
        ),
    ]
