# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0012_auto_20150414_2149'),
        ('tags', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TagClick',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('session', models.ForeignKey(to='profile.Session')),
                ('tag', models.ForeignKey(to='tags.Tag')),
            ],
            options={
                'verbose_name': 'Tag Click',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='tag',
            name='archived',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='counter',
        ),
        migrations.AlterField(
            model_name='tag',
            name='lang',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
