# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0004_auto_20150528_2254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='import',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Stworzony'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name=b'Zaktualizowany'),
            preserve_default=True,
        ),
    ]
