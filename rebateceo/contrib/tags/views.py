# -*- coding: utf-8 -*-
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, DetailView
from . import forms, models, tasks


class ImportCreateView(CreateView):
    form_class = forms.ImportForm
    template_name = 'tags/import.html'
    result = None

    def get_success_url(self):
        messages.success(self.request, u'Plik został dodany do importu. ID importu: #{}.'
                                       u'Uruchom import w tle za pomocą linków poniżej.'.format(self.object.pk))
        return reverse('tags:import-create')

    def get_form_kwargs(self):
        kwargs = super(CreateView, self).get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ImportCreateView, self).get_context_data(**kwargs)
        context.update({
            'objects': models.Import.objects.all()
        })
        return context


class ImportTaskView(DetailView):
    template_name = 'tags/import-task.html'
    model = models.Import

    def get(self, request, *args, **kwargs):
        response = super(ImportTaskView, self).get(request, *args, **kwargs)
        if not self.object.task:
            result = tasks.tags_import.delay(self.object.id)
            self.object.task = result
            self.object.save()
        else:
            messages.success(
                self.request, u'Ten plik został już dodany do '
                              u'kolejki o numerze: {}'.format(self.object.task))
        return response