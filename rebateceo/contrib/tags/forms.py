# -*- coding: utf-8 -*-
import csv
from django import forms
from django.utils.translation import ugettext_lazy as _
from . import models


class ImportForm(forms.ModelForm):
    file = forms.FileField(label=u'Plik csv rozdzielany ";" bez nagłówków. Tagi rozdzielane przecinkami. '
                                 u'Kolumny (id, shop, tags).')
    user = forms.IntegerField(widget=forms.HiddenInput())

    class Meta:
        model = models.Import
        fields = ['file', 'user']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ImportForm, self).__init__(*args, **kwargs)
        self.initial['user'] = self.request.user.pk

    def clean_user(self):
        return self.request.user

    def clean_file(self):
        file = self.cleaned_data['file']
        if not file.content_type in ['text/csv']:
            raise forms.ValidationError(_("The file type is not accepted. Use only CSV file"))
        else:
            reader = csv.reader(file, delimiter=';')
            try:
                map(lambda x: [x[0], x[1], x[2]], reader)
            except (csv.Error, IndexError):
                raise forms.ValidationError(u'Błędny format danych w pliku!')
        return file
