# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.db import connections
fr# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = u'Naprawia relacje tagi-sklepy'

    def handle(self, *args, **options):
        from rebateceo.contrib.shop.models import Shop
        from rebateceo.contrib.tags.models import Tag, ShopTag

        ShopTag.objects.all().delete()

        DATA = open(args[0]).readlines()
        for row in DATA:
            try:
                id, tag_id, shop_id = row.split(';')
                shop = Shop.objects.get(pk=shop_id)
                tag = Tag.objects.get(pk=tag_id)
                print shop, tag
            except:
                print row

from rebateceo.contrib.shop.models import Shop


class Command(BaseCommand):

    def handle(self, *args, **options):
        for tag in OldShopTag.objects.all():

            try:
                shop = Shop.objects.get(id=tag.shop_id)
            except Shop.DoesNotExist:
                pass
            else:
                pass