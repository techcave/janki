from django.core.management.base import BaseCommand
from rebateceo.contrib.shop import models
import csv
from django.conf import settings
from django.utils import translation


class Command(BaseCommand):

    modeltag = models.Tag
    modelshop = models.Shop

    def handle(self, *args, **options):
        self.modeltag.objects.all().delete()

        with open('tagim1marki.csv') as csvfile:
            translation.activate('pl')
            csv_reader = csv.reader(csvfile, delimiter=',')
            for row in csv_reader:
                if row[0]:
                    shop = self.modelshop.objects.get(id=row[0])
                    shop.set_current_language(settings.PARLER_DEFAULT_LANGUAGE_CODE)
                    for field in row[2].split(','):
                        tag = self.modeltag.objects.get_or_create(name=field.strip())
                        shop.tags.add(tag[0])
                        shop.save()
                        print '{} -> {}'.format(shop, tag)