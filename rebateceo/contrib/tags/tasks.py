from __future__ import absolute_import
import csv
from time import sleep

from celery import shared_task
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage

from rebateceo.contrib.shop.models import Shop

from .models import Tag, Import


@shared_task
def tags_import(id):
    object = Import.objects.get(pk=id)
    facility = 'import-task-for-{}'.format(object.pk)

    reader = csv.reader(object.file, delimiter=';')

    i = 1
    errors = []
    for row in reader:
        shop_id, name, tags = row

        try:
            shop = Shop.objects.get(pk=shop_id)
        except Shop.DoesNotExist:
            errors.append('Shop (%s) does not exist' % shop_id)
        else:
            for tag_name in tags.split(','):
                tag, created = Tag.objects.get_or_create(name=tag_name)
                shop.tags.add(tag)

        redis_message = """
        <div style='color: green;'>
            Shop id {shop_id} for line {line} of {lines} (<span class='percentage'>{percentage}</span>%%) was imported!
        </div>""".format(shop_id=shop_id, line=i, lines=reader.line_num, percentage=int(i / float(reader.line_num) * 100))

        if errors:
            redis_message += """
            <div style='color: red;'>Errors:<br>%s</div>""" % \
                             ('<br>'.join(errors),)
        if i == 1:
            sleep(1)
        RedisPublisher(facility=facility, broadcast=True) \
            .publish_message(RedisMessage(redis_message))

        i += 1

    if not reader.line_num:
        redis_message = """
        <div style='color: green;'>
            Completed! No new tags found (<span class='percentage'>100</span>%)
        </div>"""
        sleep(1)
        RedisPublisher(facility=facility, broadcast=True) \
            .publish_message(RedisMessage(redis_message))

    object.message = 'Completed! Imported %s/%s rows' % (reader.line_num - len(errors), reader.line_num)
    object.save()
