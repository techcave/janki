from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
    url(r'^create/$', views.ImportCreateView.as_view(), name='import-create'),
    url(r'^(?P<pk>\d+)/task/$', views.ImportTaskView.as_view(), name='import-task'),
)