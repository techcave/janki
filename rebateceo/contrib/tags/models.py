# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
import reversion
from django.conf import settings
from django.utils.translation import gettext as _
from django.db import models
from django.contrib.auth.models import User
from rebateceo.contrib.profile.models import Session


class Tag(models.Model):
    name = models.CharField(max_length=255)
    lang = models.CharField(max_length=255, blank=True, null=True)
    # created_at = models.DateTimeField(auto_now=True, auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Tag')

    def __unicode__(self):
        return self.name

    @property
    def counter(self):
        return self.tagclick_set.all()\
            .count()


class TagClick(models.Model):
    tag = models.ForeignKey(Tag)
    session = models.ForeignKey(Session)
    created_at = models.DateTimeField(auto_now=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Tag Click')

    def __unicode__(self):
        return u"{} {}".format(self.tag, self.session)


class Import(models.Model):
    user = models.ForeignKey(User)
    file = models.FileField(upload_to='imports')
    task = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    message = models.TextField(default='', blank=True)
    error = models.TextField(default='', blank=True)

    def __unicode__(self):
        return u"#{} ({})".format(self.pk, self.file)

    class Meta:
        ordering = ['-pk']


reversion.register(Tag)