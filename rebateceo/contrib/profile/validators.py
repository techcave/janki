# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT = getattr(settings, 'LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT', None)
LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT = getattr(settings, 'LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT', None)
LOTTERY_DAILY_PURCHASES_LIMIT = getattr(settings, 'LOTTERY_DAILY_PURCHASES_LIMIT', None)


def user_purchase_limit_validator(user):
    now = datetime.datetime.now()
    monthly_purchases = user.purchases.filter(created_at__year=now.year, created_at__month=now.month)
    daily_purchases = monthly_purchases.filter(created_at__day=now.day)

    if LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT:
        if len(monthly_purchases) >= LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT:
            raise ValidationError(_(u'Cannnot register more than %s per month!') % LOTTERY_MONTHLY_PURCHASE_REGISTER_LIMIT)

    if LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT:
        if len(daily_purchases) >= LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT:
            raise ValidationError(_(u'Cannnot register more than %s per day!') % LOTTERY_DAILY_PURCHASE_REGISTER_LIMIT)
