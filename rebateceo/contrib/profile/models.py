# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
import ast
import uuid
from picklefield.fields import PickledObjectField
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import UUIDField
from userena.models import UserenaLanguageBaseProfile
from userena.utils import user_model_label
from .signals import update_mobile_info_signal
from rebateceo.contrib.stand.models import Stand
from rebateceo.contrib.card.models import Card, Type


class SegmentManager(models.Manager):
    pass


class Segment(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = SegmentManager()

    def __unicode__(self):
        return u"%s" % (unicode(self.title),)


class Profile(UserenaLanguageBaseProfile):
    QRCODE_TYPE = 'profile'
    QRCODE_SIZE = 200

    GENDER_CHOICES = (
        (1, _('Male')),
        (2, _('Female')),
    )
    id = UUIDField(_('Profile UUID'), primary_key=True)
    User._meta.get_field("username").max_length = 255
    user = models.OneToOneField(user_model_label,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='profile')
    segment = models.ForeignKey('Segment', verbose_name=_(u"Segment"), related_name="profiles", blank=True, null=True)
    mobile_uuid = models.CharField(_('Mobile identifier (IMEI or iPhone UUID)'), max_length=250, blank=True)
    gender = models.PositiveSmallIntegerField(_('gender'),
                                              choices=GENDER_CHOICES,
                                              blank=True,
                                              null=True)
    website = models.URLField(_('website'), blank=True)
    location = models.CharField(_('location'), max_length=255, blank=True)
    birth_date = models.DateField(_('birth date'), blank=True, null=True)
    about_me = models.TextField(_('about me'), blank=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True, blank=True, null=True)
    blocked_at = models.DateTimeField(verbose_name=_('Blocked at'), blank=True, null=True)
    notes = models.TextField(_('Notes'), blank=True)
    channels = models.ManyToManyField('Channel', through='ProfileChannel', null=True, blank=True)

    @property
    def age(self):
        if not self.birth_date:
            return False
        else:
            today = datetime.date.today()
            # Raised when birth date is February 29 and the current year is not a
            # leap year.
            try:
                birthday = self.birth_date.replace(year=today.year)
            except ValueError:
                day = today.day - 1 if today.day != 1 else today.day + 2
                birthday = self.birth_date.replace(year=today.year, day=day)
            if birthday > today:
                return today.year - self.birth_date.year - 1
            else:
                return today.year - self.birth_date.year

    @property
    def rebates(self):
        from rebateceo.contrib.rebate.models import Rebate

        return Rebate.objects.all().filter(segment=self.segment, segment__isnull=False)


class Session(models.Model):
    user = models.ForeignKey(user_model_label, related_name='session', blank=True, null=True)
    stand = models.ForeignKey(Stand, related_name='session', blank=True, null=True)
    profile = models.ForeignKey(Profile, related_name='session', blank=True, null=True)
    request = PickledObjectField(blank=True, null=True)
    response = PickledObjectField(blank=True, null=True)
    data = PickledObjectField(blank=True, null=True)
    app_version = models.CharField(verbose_name=_('App version'), max_length=255, blank=True)
    device_info = models.CharField(verbose_name=_('Device info'), max_length=255, blank=True)
    os_info = models.CharField(verbose_name=_('OS info'), max_length=255, blank=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True, blank=True, null=True)

    def __unicode__(self):
        return "#{}: {}".format(self.pk, self.created_at)


class Channel(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class ProfileChannel(models.Model):
    NONE = 'none'
    NEVER = 'never'
    RARELY = 'rarely'
    NORMALLY = 'normally'
    OFTEN = 'often'

    FREQUENCY = (
        (NONE, _('None')),
        (NEVER, _('Never')),
        (RARELY, _('Rarely')),
        (NORMALLY, _('Normally')),
        (OFTEN, _('Often'))
    )

    profile = models.ForeignKey(Profile)
    channel = models.ForeignKey(Channel)
    frequency = models.CharField(
        max_length=255, choices=FREQUENCY, default=NONE)


# Signals
def user_post_save(sender, instance, created, **kwargs):
    """Create a user profile when a new user account is created"""
    if created == True:
        p = Profile()
        p.user = instance
        p.save()


def profile_post_save(sender, instance, created, **kwargs):
    if instance.card_set.count() == 0:
        type, _ = Type.objects.get_or_create(title="Profile QRCode Card", symbol=Type.PROFILE)
        c = Card()
        c.type = type
        c.profile = instance
        c.save()


def session_post_save(sender, instance, **kwargs):
    """Update device info after session record is modificated"""
    try:
        mobile_data = ast.literal_eval(instance.data)
        if isinstance(mobile_data, dict):
            if 'app_version' in mobile_data.keys():
                version = mobile_data['app_version']
                if version.startswith("lomianki"):
                    version = mobile_data['app_version'].split()
                    del version[-1]
                    version = ' '.join(version)
                    print version
                instance._meta.model.objects.filter(pk=instance.pk).update(app_version=version)
            if 'os_info' in mobile_data.keys():
                instance._meta.model.objects.filter(pk=instance.pk).update(os_info=mobile_data['os_info'])
            if 'device_info' in mobile_data.keys():
                instance._meta.model.objects.filter(pk=instance.pk).update(device_info=mobile_data['device_info'])
            if 'device_invo' in mobile_data.keys():
                instance._meta.model.objects.filter(pk=instance.pk).update(device_info=mobile_data['device_invo'])
    except ValueError:
        #  print "Session ID {}: Data is not a vaild mobile info. Skipping".format(instance.id)
        pass
    except SyntaxError:
        #  print "Session ID {}: Mobile info syntax error. Skipping".format(instance.id)
        pass


post_save.connect(user_post_save, sender=User)
post_save.connect(profile_post_save, sender=Profile)
post_save.connect(session_post_save, sender=Session)
update_mobile_info_signal.connect(session_post_save, sender=Session)
