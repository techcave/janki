from django.dispatch import Signal

update_mobile_info_signal = Signal(providing_args=["instance", "args", "kwargs"])