# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import logging

from rebateceo.contrib.api.request import prepare_post
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django_utils.exceptions import InputValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rebateceo.contrib.stand.models import Stand
from ..models import Session, Profile, ProfileChannel, Channel
from .serializers import (SessionSerializer, GuestSessionCreateSerializer,
                          ProfileChannelSerializer, ProfileSerializer)

logger = logging.getLogger(__name__)

IS_LOCAL = getattr(settings, 'IS_LOCAL', False)


class GuestSessionView(GenericAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = GuestSessionCreateSerializer
    model = Session

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):

        # Check device
        device = request.POST.get('device')
        if not device:
            raise InputValidationError(u'Brak deviceId urzadzenia')

        # Check stand
        try:
            stand = Stand.objects.get(device=device)
        except Stand.DoesNotExist:
            raise InputValidationError(u'Nie znaleziono urzadzenia')

        return Response(SessionSerializer(instance=Session.objects.create(
            user=request.user,
            stand=stand,
            request=request.DATA
        )).data)


class ProfileChannelView(GenericAPIView):
    if not IS_LOCAL:
        authentication_classes = (TokenAuthentication,)
        permission_classes = (IsAuthenticated,)
    serializer_class = ProfileChannelSerializer

    def validate_serializer(self, data):
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)

    def post(self, request, *args, **kwargs):
        request = prepare_post(request)
        self.validate_serializer(request.data)
        profile = Profile.objects.get(id=request.data.get('id'))
        self.update_channels(profile, request.data)
        return Response(ProfileSerializer(instance=profile).data)

    def update_channels(self, profile, data):
        for channel in data.getlist('channels'):
            channel = eval(channel)
            profile_channel, _ = ProfileChannel.objects.get_or_create(
                profile=profile, channel=Channel.objects.get(pk=channel['id']))
            profile_channel.frequency = channel['frequency']
            profile_channel.save()
