from rebateceo.contrib.authorize.api.serializers import UserSerializer
from rest_framework import serializers
from .. import models


class SegmentSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.Segment
        fields = ['pk', 'title', 'description', 'created_at', 'updated_at']


class ProfileSerializer(serializers.ModelSerializer):
    segment = SegmentSerializer()
    user = UserSerializer()

    class Meta:
        model = models.Profile
        fields = ['id', 'mobile_uuid', 'user', 'segment', 'gender', 'website', 'location', 'birth_date', 'about_me']


class SessionSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = models.Session
        fields = ['id', 'profile', 'created_at', 'updated_at']


class GuestSessionCreateSerializer(serializers.Serializer):
    device = serializers.CharField(max_length=255)

    class Meta:
        fields = ['device', ]


class ChannelSerializer(serializers.Serializer):
    id = serializers.CharField()
    frequency = serializers.CharField()


class ProfileChannelSerializer(serializers.Serializer):
    id = serializers.CharField()
    channels = ChannelSerializer(many=True)

    def validate_id(self, value):
        try:
            models.Profile.objects.get(id=value)
        except models.Profile.DoesNotExist:
            raise serializers.ValidationError("Profile does not exist")
        return value
