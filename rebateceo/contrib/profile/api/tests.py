from rebateceo.contrib.profile.models import Channel, ProfileChannel
from django.test import override_settings
from rest_framework.reverse import reverse
from ...api.tests import APITestBase


class ProfileChannelTest(APITestBase):

    @override_settings(IS_LOCAL=True)
    def test_set_channels(self):
        twitter = Channel.objects.create(name='Twitter')
        facebook = Channel.objects.create(name='Facebook')

        data = {
            'id': self.user.profile.id,
            'channels': [
                {'id': twitter.id, 'frequency': ProfileChannel.NEVER},
                {'id': facebook.id, 'frequency': ProfileChannel.OFTEN}
            ]
        }
        self.client.post(reverse(
            'api:profile:profile-channel'), data=data, **self.headers)

        twitter_frequency = ProfileChannel.objects.get(
            profile=self.user.profile, channel=twitter).frequency
        facebook_frequency = ProfileChannel.objects.get(
            profile=self.user.profile, channel=facebook).frequency

        self.assertEqual(
            twitter_frequency, ProfileChannel.NEVER, "Profile channel test failed")
        self.assertEqual(
            facebook_frequency, ProfileChannel.OFTEN, "Profile channel test failed")

    @override_settings(IS_LOCAL=True)
    def test_incorrect_profile_id(self):
        twitter = Channel.objects.create(name='Twitter')
        facebook = Channel.objects.create(name='Facebook')

        profile_id = 'fake-id'
        data = {
            'id': 'fake-id',
            'channels': [
                {'id': twitter.id, 'frequency': ProfileChannel.NEVER},
                {'id': facebook.id, 'frequency': ProfileChannel.OFTEN}
            ]
        }
        self.client.post(reverse(
            'api:profile:profile-channel'), data=data, **self.headers)

        self.assertEqual(
            ProfileChannel.objects.filter(profile_id=profile_id).count(), 0)
