# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.admin import ModelAdmin, site
from . import models


# Unregister userena's
site.unregister(models.Profile)
class ProfileAdmin(ModelAdmin):
    list_display = ('id', 'mobile_uuid', 'segment', 'created_at', 'updated_at', 'blocked_at', 'notes')
    search_fields = ('mobile_uuid', 'user__username')
    actions = ['assign_profile_card']

    def assign_profile_card(self, request, queryset):
        for x in queryset:
            x.notes = 'assign card'
            x.save()
    assign_profile_card.short_description = "Assign cards"


class SessionAdmin(ModelAdmin):
    list_display = ('id', 'profile', 'stand', 'data', 'request', 'response', 'created_at', 'updated_at')


class SegmentAdmin(ModelAdmin):
    list_filter = ('title', 'description', 'created_at', 'updated_at')


class ProfileCardAdmin(ModelAdmin):
    list_filter = ('profile', 'card', 'created_at', 'updated_at')


site.register(models.Profile, ProfileAdmin)
site.register(models.Session, SessionAdmin)
site.register(models.Segment, SegmentAdmin)
site.register(models.Channel)
site.register(models.ProfileChannel)
