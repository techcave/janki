# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0003_auto_20150224_1033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='mobile_unique_id',
            field=models.CharField(max_length=255, verbose_name='Mobile Unique Id', blank=True),
            preserve_default=True,
        ),
    ]
