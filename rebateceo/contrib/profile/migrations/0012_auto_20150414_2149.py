# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0011_auto_20150402_1457'),
    ]

    operations = [
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfileChannel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('frequency', models.CharField(default=b'none', max_length=255, choices=[(b'none', 'None'), (b'never', 'Never'), (b'rarely', 'Rarely'), (b'normally', 'Normally'), (b'often', 'Often')])),
                ('channel', models.ForeignKey(to='profile.Channel')),
                ('profile', models.ForeignKey(to='profile.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='profile',
            name='channels',
            field=models.ManyToManyField(to='profile.Channel', null=True, through='profile.ProfileChannel', blank=True),
            preserve_default=True,
        ),
    ]
