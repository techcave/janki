# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('profile', '0006_auto_20150313_1241'),
    ]

    operations = [
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at', null=True)),
                ('user', models.OneToOneField(related_name='session', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='mobiledata',
            name='profile',
        ),
        migrations.DeleteModel(
            name='MobileData',
        ),
        migrations.AlterField(
            model_name='profile',
            name='id',
            field=django_extensions.db.fields.UUIDField(primary_key=True, serialize=False, editable=False, blank=True, verbose_name='Profile UUID'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='mobile_uuid',
            field=models.CharField(max_length=250, verbose_name='Mobile identifier (IMEI or iPhone UUID)', blank=True),
            preserve_default=True,
        ),
    ]
