# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0019_merge'),
        ('profile', '0019_session_user'),
    ]

    operations = [
    ]
