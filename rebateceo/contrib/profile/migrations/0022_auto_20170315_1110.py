# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0021_auto_20150713_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='language',
            field=models.CharField(default=b'en', help_text='Default language.', max_length=5, verbose_name='language', choices=[(b'en', b'English')]),
            preserve_default=True,
        ),
    ]
