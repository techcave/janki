# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0013_auto_20150513_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='device_info',
            field=models.CharField(max_length=255, verbose_name='Device info', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='os_info',
            field=models.CharField(max_length=255, verbose_name='OS info', blank=True),
            preserve_default=True,
        ),
    ]
