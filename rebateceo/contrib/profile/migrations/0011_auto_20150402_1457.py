# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0010_session_stand'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='blocked_at',
            field=models.DateTimeField(null=True, verbose_name='Blocked at', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='notes',
            field=models.TextField(verbose_name='Notes', blank=True),
            preserve_default=True,
        ),
    ]
