# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0016_auto_20150519_1023'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='session',
            name='user',
        ),
        migrations.AddField(
            model_name='session',
            name='profile',
            field=models.ForeignKey(related_name='session', blank=True, to='profile.Profile', null=True),
            preserve_default=True,
        ),
    ]
