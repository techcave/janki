# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0002_auto_20150224_1032'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='about_me',
            field=models.TextField(verbose_name='about me', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='birth_date',
            field=models.DateField(null=True, verbose_name='birth date', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='gender',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='gender', choices=[(1, 'Male'), (2, 'Female')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='location',
            field=models.CharField(max_length=255, verbose_name='location', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='website',
            field=models.URLField(verbose_name='website', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='mobile_unique_id',
            field=models.CharField(max_length=255, verbose_name='location', blank=True),
            preserve_default=True,
        ),
    ]
