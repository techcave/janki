# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import easy_thumbnails.fields
import userena.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mugshot', easy_thumbnails.fields.ThumbnailerImageField(help_text='A personal image displayed in your profile.', upload_to=userena.models.upload_to_mugshot, verbose_name='mugshot', blank=True)),
                ('privacy', models.CharField(default=b'registered', help_text='Designates who can view your profile.', max_length=15, verbose_name='privacy', choices=[(b'open', 'Open'), (b'registered', 'Registered'), (b'closed', 'Closed')])),
                ('language', models.CharField(default=b'pl', help_text='Default language.', max_length=5, verbose_name='language', choices=[(b'pl', b'Polish')])),
                ('gender', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='gender', choices=[(1, 'Male'), (2, 'Female')])),
                ('website', models.URLField(verbose_name='website', blank=True)),
                ('location', models.CharField(max_length=255, verbose_name='location', blank=True)),
                ('birth_date', models.DateField(null=True, verbose_name='birth date', blank=True)),
                ('about_me', models.TextField(verbose_name='about me', blank=True)),
                ('user', models.OneToOneField(related_name='profile', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
                'permissions': (('view_profile', 'Can view profile'),),
            },
            bases=(models.Model,),
        ),
    ]
