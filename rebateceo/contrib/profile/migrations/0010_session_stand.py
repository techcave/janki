# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stand', '__first__'),
        ('profile', '0009_auto_20150319_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='stand',
            field=models.ForeignKey(related_name='session', blank=True, to='stand.Stand', null=True),
            preserve_default=True,
        ),
    ]
