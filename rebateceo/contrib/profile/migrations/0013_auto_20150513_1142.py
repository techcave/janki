# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0012_auto_20150414_2149'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='app_version',
            field=models.CharField(max_length=255, verbose_name='App version', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='device_info',
            field=models.CharField(max_length=255, verbose_name='App version', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='os_info',
            field=models.CharField(max_length=255, verbose_name='App version', blank=True),
            preserve_default=True,
        ),
    ]
