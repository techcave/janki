# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0004_auto_20150224_1241'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='id',
            field=django_extensions.db.fields.UUIDField(serialize=False, editable=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
    ]
