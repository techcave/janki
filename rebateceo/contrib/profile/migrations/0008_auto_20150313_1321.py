# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0007_auto_20150313_1259'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='session',
            name='user',
        ),
        migrations.AddField(
            model_name='session',
            name='profile',
            field=models.ForeignKey(related_name='session', blank=True, to='profile.Profile', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='request',
            field=picklefield.fields.PickledObjectField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='response',
            field=picklefield.fields.PickledObjectField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='data',
            field=picklefield.fields.PickledObjectField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
