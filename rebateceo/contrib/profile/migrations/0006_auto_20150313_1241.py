# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('profile', '0005_auto_20150304_1405'),
    ]

    operations = [
        migrations.CreateModel(
            name='MobileData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
                ('profile', models.ForeignKey(related_name='Mobile', verbose_name='mobile', to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='profile',
            name='mobile_unique_id',
        ),
        migrations.AddField(
            model_name='profile',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 13, 12, 41, 54, 674269), verbose_name='Created at', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='mobile_uuid',
            field=models.CharField(max_length=250, verbose_name='Mobile identifier (IMEI or iphone UUID', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated at', null=True),
            preserve_default=True,
        ),
    ]
