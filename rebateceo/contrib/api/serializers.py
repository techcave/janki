# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from rest_framework import serializers
from rebateceo import __version__
from . import models


class InfoSerializer(serializers.Serializer):
    pass


class InfoListSerializer(serializers.Serializer):
    pass


class MobileExtraDataSerializer(serializers.ModelSerializer):
    message_kwargs = {}

    pk = serializers.ReadOnlyField()
    image_url = serializers.SerializerMethodField()
    message = serializers.SerializerMethodField()

    class Meta:
        model = models.MobileExtraData
        fields = ['pk', 'message', 'image_url', 'placeholder', 'behavior', 'created_at', 'updated_at']

    def get_image_url(self, instance):
        return "%s%s" % ("http://%s" % Site.objects.get_current(), instance.image.url) if instance.image else ''

    def get_message(self, instance):
        return instance.message.format(**self.message_kwargs)


class CodeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    mobileextradata = MobileExtraDataSerializer()

    class Meta:
        model = models.Code
        fields = ['pk', 'value', 'description', 'created_at', 'updated_at', 'mobileextradata']

    def __init__(self, *args, **kwargs):
        message_kwargs = kwargs.pop('message_kwargs', {})
        super(CodeSerializer, self).__init__(*args, **kwargs)
        self.fields['mobileextradata'].message_kwargs = message_kwargs


class PingSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    api_version = serializers.SerializerMethodField()
    extra_data = serializers.CharField(allow_null=True, help_text="Additional important data (JSON required)")

    class Meta:
        model = models.Ping
        fields = ['pk', 'created_at', 'tag', 'extra_data', 'api_version']

    def get_api_version(self, instance):
        return __version__


class ContentTypeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = ContentType