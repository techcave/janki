# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf.urls import patterns, url, include
from . import views


urlpatterns = patterns('',
    #  Generic
    url(r'^content-type/list/', views.ContentTypeListView.as_view(), name='contenttype-list'),
    url(r'^info/', views.InfoView.as_view(), name='info'),
    url(r'^ping/', views.PingView.as_view(), name='ping'),
    url(r'^code/list/', views.CodeListView.as_view(), name='code-list'),

    #  Endpoints
    url(r'^map/', include('rebateceo.contrib.map.api.urls', namespace='map')),
    url(r'^tags/', include('rebateceo.contrib.tags.api.urls', namespace='tags')),
    url(r'^shop/', include('rebateceo.contrib.shop.api.urls', namespace='shop')),
    url(r'^trans/', include('rebateceo.contrib.trans.api.urls', namespace='trans')),
    url(r'^stand/', include('rebateceo.contrib.stand.api.urls', namespace='stand')),
    url(r'^nameday/', include('rebateceo.contrib.nameday.api.urls', namespace='nameday')),
    url(r'^mall/', include('rebateceo.contrib.mall.api.urls', namespace='mall')),
    url(r'^advert/', include('rebateceo.contrib.advert.api.urls', namespace='advert')),
    url(r'^contest/', include('rebateceo.contrib.contest.api.urls', namespace='contest')),
    url(r'^contest/bonarka1/', include('rebateceo.contrib.bonarka1.api.urls', namespace='bonarka1')),
    url(r'^backup/', include('rebateceo.contrib.backup.api.urls', namespace='backup')),
    url(r'^news/', include('rebateceo.contrib.news.api.urls', namespace='news')),
    url(r'^promotion/', include('rebateceo.contrib.promotion.api.urls', namespace='promotion')),
    url(r'^purchase/', include('rebateceo.contrib.purchase.api.urls', namespace='purchase')),
    url(r'^receipt/', include('rebateceo.contrib.purchase.api.urls', namespace='receipt')),  # alias
    url(r'^rebate/', include('rebateceo.contrib.rebate.api.urls', namespace='rebate')),
    url(r'^bookmark/', include('rebateceo.contrib.bookmark.api.urls', namespace='bookmark')),
    url(r'^profile/', include('rebateceo.contrib.profile.api.urls', namespace='profile')),
    url(r'^screensaver/', include('rebateceo.contrib.screensaver.api.urls', namespace='screensaver')),
    url(r'^tracking/', include('rebateceo.contrib.tracking.api.urls', namespace='tracking')),
    url(r'^campaign/', include('rebateceo.contrib.campaign.api.urls', namespace='campaign')),
    url(r'^card/', include('rebateceo.contrib.card.api.urls', namespace='card')),
    url(r'^verifier/', include('rebateceo.contrib.verifier.api.urls', namespace='verifier')),
    url(r'^loyalty/', include('rebateceo.contrib.loyalty.api.urls', namespace='loyalty')),
    url(r'^cache/', include('rebateceo.contrib.cache.api.urls', namespace='cache')),

    # url(r'^selfkiosk/', include('rebateceo.contrib.selfkiosk.api.urls', namespace='selfkiosk')),
    # url(r'^lottery/', include('rebateceo.contrib.lottery.api.urls', namespace='lottery')),
    # url(r'^lottery-v2/', include('rebateceo.contrib.lottery_v2.api.urls', namespace='lottery-v2')),
    # url(r'^lottery-v3/', include('rebateceo.contrib.lottery_v3.api.urls', namespace='lottery-v3')),
    # url(r'^lottery-v4/', include('rebateceo.contrib.lottery_v4.api.urls', namespace='lottery-v4')),
    # url(r'^printlog/', include('rebateceo.contrib.printlog.api.urls', namespace='printlog')),
    # url(r'^adv/', include('rebateceo.contrib.adv.api.urls', namespace='adv')),

    # url(r'^backup/', BackupView.as_view(), name='backup'),
)