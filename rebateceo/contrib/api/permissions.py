# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime, timedelta
from rest_framework import permissions
from django.conf import settings
#from rebateceo.contrib.card.api.exceptions import APIIsDisabled


class IsLocalizatorPermission(permissions.BasePermission):
    """
    Global permission check for blacklisted IPs.
    """
    def has_permission(self, request, view):
        device = request.GET.get('device', request.POST.get('device'))
        if device != 'rabatomat':
            return True

        if settings.DEBUG:
            return True

        from rebateceo.contrib.stand.models import Stand
        ip_address = request.META['REMOTE_ADDR']
        return Stand.objects.filter(ip_address=ip_address).exists()


# class WhitelistPermission(permissions.BasePermission):
#     """
#     Global permission check for accessed IPs.
#     """
#     def has_permission(self, request, view):
#         ip_address = request.META['REMOTE_ADDR']
#         return Whitelist.objects.filter(ip_address=ip_address).exists()


class ApiIsEnabledPermission(permissions.BasePermission):
    pass

    # def _is_enabled(self):
    #     now = datetime.now()
    #
    #     # Hardcore
    #     if getattr(settings, 'SCANNING_IS_PERMANENTLY_ENABLED', False):
    #         return True
    #
    #     # Closed days
    #     if hasattr(settings, 'MALL_CLOSED_DAYS') and settings.MALL_CLOSED_DAYS:
    #         for date in settings.MALL_CLOSED_DAYS:
    #             if now.strftime("%Y-%m-%d") == date.strftime("%Y-%m-%d"):
    #                 return False
    #
    #     # Opening hours
    #     if hasattr(settings, 'MALL_OPENING_HOURS') and settings.MALL_OPENING_HOURS:
    #         start_hour = settings.MALL_OPENING_HOURS[now.weekday()][0]
    #         end_hour = settings.MALL_OPENING_HOURS[now.weekday()][1]
    #         start = now.replace(hour=start_hour[0], minute=start_hour[1], second=0)
    #         end = now.replace(hour=end_hour[0], minute=end_hour[1], second=0)
    #         if now > end or now < start:
    #             return False
    #
    #     return True
    #
    # def has_permission(self, request, view):
    #     if not self._is_enabled():
    #         raise APIIsDisabled()
    #     return True