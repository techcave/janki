# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
import reversion
from picklefield.fields import PickledObjectField
from django.utils.encoding import python_2_unicode_compatible, smart_unicode
from django.conf import settings
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django.db import models
from . import codes, codes_choices


def upload_to(instance, filename):
    return '/'.join(['api', str(filename)])


@python_2_unicode_compatible
class Code(models.Model):
    value = models.CharField(unique=True, max_length=255, choices=codes_choices)
    description = models.TextField(blank=True, null=True, help_text=_('Developer notes'))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"{}".format(str(self.value))

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Code')
        verbose_name_plural = _('Codes')
        ordering = ['pk']


@python_2_unicode_compatible
class MobileExtraData(models.Model):
    code = models.OneToOneField(Code)
    message = models.TextField(max_length=255)
    #mobile_model = models.TextField(max_length=255, help_text='Set for custom model')
    image = models.ImageField(upload_to=upload_to, blank=True, null=True)
    placeholder = models.CharField(max_length=64, choices=(('BANNER', _('BANNER')), ('BOTTOM', _('BOTTOM'))), blank=True, null=True)
    behavior = models.CharField(max_length=64, choices=(('ALERT', _('ALERT')), ('POPUP', _('POPUP'))), blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"{} | {} | {} | {}".format(self.placeholder, self.behavior, self.message, self.image)

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Mobile extra data')
        verbose_name_plural = _('Mobile extra datas')
        ordering = ['pk']


@python_2_unicode_compatible
class Ping(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    tag = models.CharField(max_length=255, blank=True, null=True)
    extra_data = PickledObjectField(blank=True, null=True)

    def __str__(self):
        return u"{} {}".format(str(self.created_at), self.tag)

    def __unicode__(self):
        return self.__str__()

    class Meta:
        verbose_name = _('Ping')
        verbose_name_plural = _('Ping')
        ordering = ['pk']


reversion.register(Code)
reversion.register(MobileExtraData)
