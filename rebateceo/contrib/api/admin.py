# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django import forms
from django.contrib import messages
from django.conf import settings
from django.contrib import admin
from . import models


class MobileExtraDataInline(admin.options.TabularInline):
    model = models.MobileExtraData
    extra = 5


class CodeAdmin(admin.ModelAdmin):
    inlines = [MobileExtraDataInline]
    list_display = ('value', 'get_code', 'description', 'created_at', 'updated_at', 'get_mobile_extra_data')
    search_fields = ('value', 'description')

    def get_code(self, instance):
        return instance.value
    get_code.allow_tags = True

    def get_mobile_extra_data(self, instance):
        obj = instance.mobileextradata
        return u"<table><tr>" \
               u"<td width='100'>{}</td>" \
               u"<td width='100'>{}</td>" \
               u"<td width='100'>{}</td>" \
               u"<td width='100'>{}</td>" \
               u"</tr></table>".\
            format(obj.placeholder,
                   obj.behavior,
                   obj.message,
                   "<img src='{}' width='150'/>".format(obj.image.url) if obj.image else '') \
            if obj else '-'
    get_mobile_extra_data.allow_tags = True


class PingAdmin(admin.ModelAdmin):
    list_display = ('pk', 'created_at', 'tag', 'extra_data')
    search_fields = ('tag', 'extra_data',)
    list_filter = ('tag',)


admin.site.register(models.Code, CodeAdmin)
admin.site.register(models.Ping, PingAdmin)