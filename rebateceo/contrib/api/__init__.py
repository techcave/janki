# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.sites.models import Site
from django.contrib.contenttypes.models import ContentType
from django.forms.models import model_to_dict
from reversion import get_registered_models
from reversion.models import Version
from django.conf import settings

import datetime
from rebateceo.contrib.bookmark.api.views import get_registry
from rebateceo import __version__

# TODO: autodiscover from api.exceptions modules
_codes = {
    'rebateceo.contrib.bonarka1': (
        ('#0001', 'bonarka1_PURCHASE_MINIMUM_AMOUNT_REQUIRED'),
        ('#0002', 'bonarka1_PURCHASE_ALREADY_EXISTS'),
        ('#0003', 'bonarka1_PURCHASE_WRONG_SHOP'),
        ('#0004', 'bonarka1_PURCHASE_DATE_NOT_IN_RANGE'),
        ('#0005', 'bonarka1_PURCHASE_USER_BLOCKED'),
    ),
    'rebateceo.contrib.contest': (
        ('#0006', 'CURRENT_CONTEST_NOT_FOUND'),
    ),
}


def choices():
    result = []
    for app, codes in _codes.items():
        for code in codes:
            result.append([code[0], "{} : {}.{}".format( code[0], app, code[1])])
    return result


class APICodes(object):
    def __init__(self):
        for app, codes in _codes.items():
            for code in codes:
                setattr(self, code[1], code[0])

class APIInfo(object):

    @classmethod
    def get_versions(cls):
        changes = {}
        for klass in get_registered_models():
            content_type = ContentType.objects.get_for_model(klass)
            versions = Version.objects.filter(content_type=content_type)
            changes.update({
                "{0}.{1}".format(klass.__module__, content_type.model): {
                    'content_type': model_to_dict(content_type),
                    'last_change': model_to_dict(versions.last(), fields=[],
                                                 exclude=['serialized_data', 'format']) if versions else {}
                }
            })

        last_version = Version.objects.all().last()
        return {
            'last_version': last_version.pk if last_version else None,
            'changes': changes
        }

    @classmethod
    def get_all(cls):
        return {
            'api': {
                'name': 'RebateCEO',
                'server_time': datetime.datetime.now(),
                'media_url': settings.MEDIA_URL,
            },
            'backend': {
                'version': __version__
            },
            'sites': {
                'current': model_to_dict(Site.objects.get_current()),
                'list': [model_to_dict(x) for x in Site.objects.all()]
            },
            'versions': cls.get_versions(),
            'bookmark': {
                'registry': get_registry()
            }
        }


codes = APICodes()
codes_choices = choices()


# Handle reversion
