# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Generate api-auth-tokent'

    def handle(self, *args, **options):
        from rest_framework.authtoken.models import Token
        token, created = Token.objects.get_or_create(user=User.objects.get(username='api'))
        print token.key