# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20150402_1122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='code',
            name='value',
            field=models.CharField(unique=True, max_length=255, choices=[[b'#0001', b'rebateceo.contrib.bonarka1.bonarka1_PURCHASE_MINIMUM_AMOUNT_REQUIRED = #0001'], [b'#0002', b'rebateceo.contrib.bonarka1.bonarka1_PURCHASE_ALREADY_EXISTS = #0002'], [b'#0003', b'rebateceo.contrib.bonarka1.bonarka1_PURCHASE_WRONG_SHOP = #0003'], [b'#0004', b'rebateceo.contrib.bonarka1.bonarka1_PURCHASE_DATE_NOT_IN_RANGE = #0004']]),
            preserve_default=True,
        ),
    ]
