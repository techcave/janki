# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='code',
            name='value',
            field=models.CharField(unique=True, max_length=255, choices=[[b'#0001', b'rebatece.contrib.purchase: #0001: PURCHASE_MINIMUM_AMOUNT_REQUIRED'], [b'#0002', b'rebatece.contrib.purchase: #0002: PURCHASE_EXISTS']]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mobileextradata',
            name='placeholder',
            field=models.CharField(blank=True, max_length=64, null=True, choices=[('BANNER', 'BANNER'), ('BOTTOM', 'BOTTOM')]),
            preserve_default=True,
        ),
    ]
