# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_remove_ping_sentry_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='ping',
            name='extra_data',
            field=picklefield.fields.PickledObjectField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ping',
            name='tag',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
