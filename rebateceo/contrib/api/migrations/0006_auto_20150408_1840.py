# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='code',
            name='value',
            field=models.CharField(unique=True, max_length=255, choices=[[b'#0001', b'#0001 : rebateceo.contrib.bonarka1.bonarka1_PURCHASE_MINIMUM_AMOUNT_REQUIRED'], [b'#0002', b'#0002 : rebateceo.contrib.bonarka1.bonarka1_PURCHASE_ALREADY_EXISTS'], [b'#0003', b'#0003 : rebateceo.contrib.bonarka1.bonarka1_PURCHASE_WRONG_SHOP'], [b'#0004', b'#0004 : rebateceo.contrib.bonarka1.bonarka1_PURCHASE_DATE_NOT_IN_RANGE'], [b'#0005', b'#0005 : rebateceo.contrib.bonarka1.bonarka1_PURCHASE_USER_BLOCKED'], [b'#0006', b'#0006 : rebateceo.contrib.contest.CURRENT_CONTEST_NOT_FOUND']]),
            preserve_default=True,
        ),
    ]
