# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.api.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Code',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(unique=True, max_length=255)),
                ('description', models.TextField(help_text='Developer notes', null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Code',
                'verbose_name_plural': 'Codes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MobileExtraData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField(max_length=255)),
                ('image', models.ImageField(null=True, upload_to=rebateceo.contrib.api.models.upload_to, blank=True)),
                ('placeholder', models.CharField(blank=True, max_length=64, null=True, choices=[('BANNER', 'BANNER'), ('BOTTOM', 'BANNER')])),
                ('behavior', models.CharField(blank=True, max_length=64, null=True, choices=[('ALERT', 'ALERT'), ('POPUP', 'POPUP')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('code', models.OneToOneField(to='api.Code')),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Mobile extra data',
                'verbose_name_plural': 'Mobile extra datas',
            },
            bases=(models.Model,),
        ),
    ]
