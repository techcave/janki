# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20150330_1816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='code',
            name='value',
            field=models.CharField(unique=True, max_length=255, choices=[[b'#0001', b'rebateceo.contrib.purchase: #0001: PURCHASE_MINIMUM_AMOUNT_REQUIRED'], [b'#0002', b'rebateceo.contrib.purchase: #0002: PURCHASE_EXISTS']]),
            preserve_default=True,
        ),
    ]
