# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.forms.models import model_to_dict
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import APIException
from rest_framework import status
from . import models
from .serializers import CodeSerializer


class APIRebateceoException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {}
    extra_message = default_message
    data = {}
    custom = {}
    code = None
    kwargs = {}

    def __init__(self, *args, **kwargs):
        self.detail = {
            'exception': {
                'class': self.__class__.__name__,
                'code': self.code,
                'data': {},
            },
            'messages': {}
        }
        self.kwargs = kwargs

        # Api mess
        api = self.default_detail.format(**kwargs)
        if api:
            self.detail['messages'].update({'api': api})

        # Default
        default = self.get_default_message(**kwargs)
        if default:
            self.detail['messages'].update({'default': default})

        # Extra
        extra = self.get_extra_message(**kwargs)
        if extra:
            self.detail['messages'].update({'extra': extra})

        # Data
        data = self.get_data(**kwargs)
        if data:
            self.detail.update({'data': data})

        # Exc data
        self.get_exception_data()

    def get_default_message(self, **kwargs):
        return self.default_message

    def get_extra_message(self, **kwargs):
        return self.extra_message

    def get_data(self, **kwargs):
        return self.data

    def get_exception_data(self, **kwargs):
        # Custom info from DB!
        try:
            code = models.Code.objects.get(value=self.code)
        except models.Code.DoesNotExist:
            pass
        else:
            self.detail['exception']['data'].update(CodeSerializer(instance=code, message_kwargs=self.kwargs).data)
