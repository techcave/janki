# -*- coding: utf-8 -*-
"""
@licence: MIT License
"""
import csv
from django.core.management import BaseCommand
from rebateceo.contrib.promotion.models import Promotion


class Command(BaseCommand):
    """
        Imports translations of titles, descriptions, and short descriptions for news
    """
    def handle(self, *args, **option):
        help = "Imports translations for news from csv file"

        with open(args[0], "rb") as csvfile:
            data = csv.reader(csvfile, delimiter=",")

            for counter, row in enumerate(data):
                if counter == 0: continue

                promotion_id = row[0]
                title_pl = row[1]
                description_pl = row[2]
                short_pl = row[3]

                instance, created = Promotion.objects.get_or_create(id=promotion_id)

                instance.set_current_language("pl")
                instance.title = title_pl.decode("utf-8")
                instance.descritpion = description_pl.decode("utf-8")
                instance.short = short_pl.decode("utf-8")

                instance.save()

                # print("[ {:03} ] {} -> {}".format(counter, instance, created))
                print("[ {:03} ] {} -> {}".format(counter, instance.id, created))

