# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
from datetime import datetime
import reversion
import datetime
from django.db.models import Q
from django.utils.translation import gettext as _
from django.db import models
from django.utils.text import slugify
from rebateceo.utils.dates import format_date, granular_now
from rebateceo.contrib.shop.models import Shop
from rebateceo.contrib.bookmark.handlers import library as bookmarks
from parler.models import TranslatedFields, TranslatableModel
from parler.managers import TranslationManager


def upload_to(instance, filename):
    return '/'.join(['promotion', str(filename)])


class PromotionManager(TranslationManager):
    def active(self):
        date_filter = Q(date_from__lte=granular_now) & (
        Q(date_to__isnull=True) | Q(date_to__gte=granular_now))
        queryset = self.filter(date_filter, archived_at__isnull=True)
        return queryset


class Promotion(TranslatableModel):
    translations = TranslatedFields(
        title=models.TextField(),
        description=models.TextField(blank=True, null=True),
        short=models.TextField(blank=True, null=True),
    )
    date_from = models.DateTimeField(db_index=True, default=granular_now)
    date_to = models.DateTimeField(blank=True, null=True, db_index=True,
                                   default=datetime.datetime.combine(datetime.date.today(), datetime.time(23, 59, 59)))
    sort = models.IntegerField(blank=True, null=True)
    shops = models.ManyToManyField(Shop, blank=True, null=True)
    mobile_image = models.ImageField(upload_to=upload_to, blank=True, null=True)
    thumb = models.ImageField(upload_to=upload_to, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    archived_at = models.DateTimeField(verbose_name=_('Archived at'), blank=True, null=True)
    objects = PromotionManager()

    class Meta:
        verbose_name = _('Promotion')
        verbose_name_plural = _('Promotion')
        ordering = ['-archived_at', 'sort', '-pk']
    #
    # def delete(self, using=None):
    #     self.archived_at = datetime.datetime.now()
    #     self.save()

    def __str__(self):
        return unicode(self.title)

    def __unicode__(self):
        return unicode(self.title)


class PromotionImage(models.Model):
    promotion = models.ForeignKey(Promotion)
    image = models.ImageField(upload_to=upload_to)

    class Meta:
        verbose_name = _('Promotion image')
        verbose_name_plural = _('Promotion images')

    def __unicode__(self):
        return self.image.url


reversion.register(Promotion)
reversion.register(PromotionImage)

bookmarks.register(Promotion)
