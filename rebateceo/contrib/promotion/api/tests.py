from rest_framework.reverse import reverse
from ..factories import PromotionFactory
from ...api.tests import APITestBase



class PromotionTest(APITestBase):
    def test_list(self):
        items = [PromotionFactory() for x in range(0, self.limit)]
        response = self.client.get(reverse('api:promotion:list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Promotion list test failed")