# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework.generics import ListAPIView
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .serializers import PromotionSerializer
from ..models import Promotion

from django.http import JsonResponse
from django.views.generic import View
from django.contrib.sites.models import Site


class PromotionListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = PromotionSerializer
    model = Promotion

    def get_queryset(self):
        return self.model.objects.active()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)

    def filter_queryset(self, queryset):
        sort = self.request.GET.get('sort')
        query = self.request.GET.get('query')
        queryset = queryset.order_by(sort) if sort else queryset
        queryset = queryset.filter(title__icontains=query) if query else queryset
        return queryset


class PromotionSimpleListView(View):

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)

    def get(self, request, *args, **kwargs):
        # from django.conf import settings
        # settings.DEBUG = True
        # from django.db import connection

        promotions = Promotion.objects.prefetch_related('shops', 'promotionimage_set', 'translations')
        result = []
        for promotion in promotions:
            item = {'pk': promotion.pk}
            item['date_from'] = promotion.date_from
            item['date_to'] = promotion.date_to if promotion.date_to else None
            item['thumb'] = 'http://' + Site.objects.get_current().name + promotion.thumb.url if promotion.thumb else None
            item['shops'] = []
            if promotion.shops:
                for shop in promotion.shops.all():
                    item['shops'].append(shop.id)
            item['promotionimage_set'] = []
            if promotion.promotionimage_set:
                for image in promotion.promotionimage_set.all():
                    item['promotionimage_set'].append('http://' + Site.objects.get_current().name + image.image.url)
            item['translations'] = {}
            item['translations']['pl'] = {'title': promotion.title,
                                          'description': promotion.description,
                                          'short': promotion.short}
            result.append(item)
        # print(len(connection.queries))
        return JsonResponse(result, safe=False)
