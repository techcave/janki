# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.utils.safestring import mark_safe
from django.contrib import admin
from django.contrib.admin.options import TabularInline

from rebateceo.contrib.news.admin import html_decode
from .models import Promotion, PromotionImage
from parler.admin import TranslatableAdmin


class PromotionImageInlines(TabularInline):
    model = PromotionImage
    extra = 5


class PromotionAdmin(TranslatableAdmin):
    inlines = [PromotionImageInlines]
    list_display = (
        'id', 'title', 'get_description', 'short', 'all_languages_column', 'get_image', 'get_mobile_image', 'get_thumb',
        'date_from', 'date_to', 'sort', 'created_at', 'updated_at', 'archived_at')
    list_display_links = ['title']
    search_fields = ('title', 'description', 'short', 'date_from', 'date_to', 'sort')
    list_filter = ('archived_at',)

    def get_description(self, record):
        return mark_safe(html_decode(mark_safe(record.description)))
    get_description.short_description = 'description'

    def get_image(self, record):
        image = record.promotionimage_set.first()
        return mark_safe("<img src='{url}' width='270' />".format(url=image)) if image else '-'

    def get_mobile_image(self, record):
        return mark_safe(
            "<img src='{url}' width='270'/>".format(url=record.mobile_image.url)) if record.mobile_image else '-'

    def get_thumb(self, record):
        return mark_safe("<img src='{url}' width='270'/>".format(url=record.thumb.url)) if record.thumb else '-'


admin.site.register(Promotion, PromotionAdmin)
