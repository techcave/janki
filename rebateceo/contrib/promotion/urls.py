from django.conf.urls import patterns, url
from .views import PromotionsView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = patterns('',
                       url(r'^', csrf_exempt(PromotionsView.as_view()), name='promotions'),
                       )
