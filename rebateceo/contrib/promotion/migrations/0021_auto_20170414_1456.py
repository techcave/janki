# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0020_auto_20170414_1012'),
    ]

    operations = [
        migrations.CreateModel(
            name='PromotionTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_code', models.CharField(max_length=15, verbose_name='Language', db_index=True)),
                ('title', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('short', models.TextField(null=True, blank=True)),
                ('master', models.ForeignKey(related_name='translations', editable=False, to='promotion.Promotion', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'promotion_promotion_translation',
                'db_tablespace': '',
                'default_permissions': (),
                'verbose_name': 'Promotion Translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='promotiontranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.RemoveField(
            model_name='promotion',
            name='description',
        ),
        migrations.RemoveField(
            model_name='promotion',
            name='short',
        ),
        migrations.RemoveField(
            model_name='promotion',
            name='title',
        ),
    ]
