# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.promotion.models


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0007_auto_20151110_1207'),
    ]

    operations = [
        migrations.AddField(
            model_name='promotion',
            name='mobile_image',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.promotion.models.upload_to, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='promotion',
            name='thumb',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.promotion.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
