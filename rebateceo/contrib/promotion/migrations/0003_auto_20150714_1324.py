# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0002_auto_20150527_1839'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promotion',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Stworzony'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='promotion',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name=b'Zaktualizowany'),
            preserve_default=True,
        ),
    ]
