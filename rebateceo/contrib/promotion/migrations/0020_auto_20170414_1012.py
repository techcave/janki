# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0019_auto_20170403_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promotion',
            name='date_to',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 14, 23, 59, 59), null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
    ]
