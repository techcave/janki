# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0021_merge'),
        ('promotion', '0021_auto_20170414_1456'),
    ]

    operations = [
    ]
