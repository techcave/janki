# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.utils.dates


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0010_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promotion',
            name='date_from',
            field=models.DateTimeField(default=rebateceo.utils.dates.granular_now, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='promotion',
            name='date_to',
            field=models.DateTimeField(db_index=True, null=True, blank=True),
            preserve_default=True,
        ),
    ]
