# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0023_auto_20170428_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promotion',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created at'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='promotion',
            name='date_to',
            field=models.DateTimeField(default=datetime.datetime(2018, 8, 20, 23, 59, 59), null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='promotion',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated at'),
            preserve_default=True,
        ),
    ]
