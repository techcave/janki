# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.promotion.models
import rebateceo.utils.dates


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20150302_0024'),
    ]

    operations = [
        migrations.CreateModel(
            name='Promotion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('short', models.TextField(null=True, blank=True)),
                ('date_from', models.DateField(default=rebateceo.utils.dates.granular_now, db_index=True)),
                ('date_to', models.DateField(db_index=True, null=True, blank=True)),
                ('sort', models.IntegerField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name=b'Stworzony')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name=b'Zaktualizowany')),
                ('archived_at', models.DateTimeField(null=True, verbose_name='Archived at', blank=True)),
                ('shops', models.ManyToManyField(to='shop.Shop', null=True, blank=True)),
            ],
            options={
                'ordering': ['-archived_at', 'sort', '-pk'],
                'verbose_name': 'Promotion',
                'verbose_name_plural': 'Promotion',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PromotionImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=rebateceo.contrib.promotion.models.upload_to)),
                ('promotion', models.ForeignKey(to='promotion.Promotion')),
            ],
            options={
                'verbose_name': 'Promotion image',
                'verbose_name_plural': 'Promotion images',
            },
            bases=(models.Model,),
        ),
    ]
