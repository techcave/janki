# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0003_auto_20150930_1009'),
        ('promotion', '0005_merge'),
    ]

    operations = [
    ]
