# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0008_auto_20151118_1030'),
        ('promotion', '0008_merge'),
    ]

    operations = [
    ]
