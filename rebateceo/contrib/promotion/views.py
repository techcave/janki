from django.views.generic import TemplateView
from .models import Promotion
from rebateceo.contrib.news.models import News
from django.conf import settings
from django.core.management import call_command
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site


class PromotionsView(TemplateView):
    template_name = 'dashboard/promotions.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return super(PromotionsView, self).get(request, *args, **kwargs)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        type = request.POST.get('type')
        if type == 'news':
            call_command('news_sync')
        else:
            call_command('promotions_sync')
        return super(PromotionsView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PromotionsView, self).get_context_data(**kwargs)

        f = open('var/log/last_news_logs.txt', 'r')
        file_content_news = f.read()
        f.close()
        f = open('var/log/last_promo_logs.txt', 'r')
        file_content_promo = f.read()
        f.close()
        news_url = settings.REBATECEO_NEWS_SYNC_DATA_URL
        promo_url = settings.REBATECEO_PROMOTIONS_SYNC_DATA_URL
        context.update({'promotions': len(Promotion.objects.all()),
                        'news': len(News.objects.all()),
                        'site_url': Site.objects.get_current().name,
                        'news_url': news_url,
                        'promo_url': promo_url,
                        'file_news': file_content_news,
                        'file_promo': file_content_promo})
        return context
