from rest_framework.reverse import reverse
from ..factories import BannerFactory
from ...api.tests import APITestBase


class BannerTest(APITestBase):
    def test_list(self):
        items = [BannerFactory(active=True) for x in range(0, self.limit)]
        response = self.client.get(reverse('api:advert:banner-list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "Banner list test failed")
