# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advert', '0002_auto_20150223_1718'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='target',
            field=models.CharField(default=b'rab', max_length=3, choices=[(b'rab', 'rabatomat'), (b'mob', 'mobile')]),
            preserve_default=True,
        ),
    ]
