# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advert', '0006_auto_20150401_1318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='target',
            field=models.CharField(default=b'rabatomat', max_length=16, choices=[(b'rabatomat', 'rabatomat'), (b'mobile', 'mobile'), (b'verifier', 'verifier'), (b'website', 'website')]),
            preserve_default=True,
        ),
    ]
