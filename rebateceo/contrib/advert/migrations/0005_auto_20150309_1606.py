# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advert', '0004_auto_20150309_1552'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='banner',
            options={'ordering': ('target', 'pk'), 'verbose_name': 'Banner', 'verbose_name_plural': 'Banners'},
        ),
    ]
