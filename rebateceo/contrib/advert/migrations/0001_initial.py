# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.advert.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('slug', models.SlugField(max_length=255)),
                ('image', models.FileField(max_length=255, upload_to=rebateceo.contrib.advert.models.upload_to)),
                ('active', models.BooleanField(default=False)),
                ('publication_date', models.DateTimeField(default=rebateceo.contrib.advert.models.granular_now, verbose_name='publication date')),
                ('publication_end_date', models.DateTimeField(help_text='Leave empty if the entry should stay active forever.', null=True, verbose_name='publication end date', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
