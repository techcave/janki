# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""

import uuid
import reversion
from actstream import registry
from decimal import Decimal
from datetime import datetime
from django.db import models
from django.db.models import Q
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model as user_model
from rebateceo.utils.dates import format_date, granular_now


def upload_to(instance, filename):
    return '/'.join(['banners', str(filename)])


class SectionManager(models.Manager):
    pass


class Section(models.Model):
    title = models.CharField(_('title'), max_length=255)
    slug = models.SlugField(max_length=255)
    objects = SectionManager()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Section"
        verbose_name_plural = u"Sections"
        ordering = ('pk', )


class BannerManager(models.Manager):
    def active(self):
        date_filter = Q(publication_date__lte=granular_now) & (
        Q(publication_end_date__isnull=True) | Q(publication_end_date__gt=granular_now))
        queryset = self.filter(date_filter, active=True)
        return queryset


class Banner(models.Model):
    TARGETS = (
        ('rabatomat', _('rabatomat')),
        ('mobile', _('mobile')),
        ('verifier', _('verifier')),
        ('website', _('website')),
    )
    section = models.ForeignKey(Section, blank=True, null=True)
    title = models.CharField(_('title'), max_length=255)
    slug = models.SlugField(max_length=255)
    image = models.FileField(max_length=255, upload_to=upload_to)
    target = models.CharField(max_length=16, choices=TARGETS, default='rabatomat')
    active = models.BooleanField(default=False)
    publication_date = models.DateTimeField(_('publication date'), default=granular_now)
    publication_end_date = models.DateTimeField(_('publication end date'), blank=True, null=True,
                                                help_text=_('Leave empty if the entry should stay active forever.'))

    objects = BannerManager()

    def __unicode__(self):
        return str(self.title)

    class Meta:
        verbose_name = u"Banner"
        verbose_name_plural = u"Banners"
        ordering = ('target', 'pk', )

    @property
    def image_link(self):
        return '<a href="%s" target="_blank"><img src="%s"></a>' % (self.image.url, self.image.url)

    def save(self, *args, **kwargs):
        if self.publication_date:
            self.publication_date = granular_now(self.publication_date)
        if self.publication_end_date:
            self.publication_end_date = granular_now(self.publication_end_date)
        super(Banner, self).save(*args, **kwargs)

    def admin_image(self):
        return self.image_link
    admin_image.allow_tags = True
    admin_image.short_description = _('image')

    def admin_datepublisher(self):
        return u'%s &ndash; %s' % (
            format_date(self.publication_date),
            format_date(self.publication_end_date, '&infin;'),
        )
    admin_datepublisher.allow_tags = True
    admin_datepublisher.short_description = _('visible from-to')


reversion.register(Section)
reversion.register(Banner)
