# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import date, datetime, time
from django.contrib import admin
from django.contrib.admin.options import TabularInline
from django.utils.safestring import mark_safe

from . import models, forms
from parler.admin import TranslatableAdmin


class NewsImageInlines(TabularInline):
    model = models.NewsImage
    form = forms.NewsImageForm
    extra = 5


def html_decode(s):
    """
    Returns the ASCII decoded version of the given HTML string. This does
    NOT remove normal HTML tags like <p>.
    """
    for code in (("'", '&#39;'), ('"', '&quot;'), ('>', '&gt;'), ('<', '&lt;'), ('&', '&amp;')):
        s = s.replace(code[1], code[0])
    return s


class NewsAdmin(TranslatableAdmin):
    # form = forms.NewsForm
    inlines = [NewsImageInlines]
    list_display = (
        'id', 'title', 'get_description', 'short', 'all_languages_column', 'get_image', 'get_mobile_image', 'get_thumb',
        'date_from', 'date_to', 'sort', 'created_at', 'updated_at', 'archived_at', 'type')
    list_display_links = ['title']
    exclude = ['mobile_image']
    search_fields = ('title', 'description', 'short', 'date_from', 'date_to', 'sort')
    list_filter = ('archived_at', 'type')

    def get_description(self, record):
        return mark_safe(html_decode(mark_safe(record.description)))
    get_description.short_description = 'description'

    def get_image(self, record):
        image = record.images.first()
        return mark_safe("<img src='{url}' width='270' />".format(url=image)) if image else '-'

    def get_mobile_image(self, record):
        return mark_safe(
            "<img src='{url}' width='270'/>".format(url=record.mobile_image.url)) if record.mobile_image else '-'

    def get_thumb(self, record):
        return mark_safe("<img src='{url}' width='270'/>".format(url=record.thumb.url)) if record.thumb else '-'


class TypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


admin.site.register(models.News, NewsAdmin)
# admin.site.register(models.News)
admin.site.register(models.Type, TypeAdmin)
