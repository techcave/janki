from rest_framework.reverse import reverse
from ..factories import NewsFactory
from ...api.tests import APITestBase


class NewsTest(APITestBase):
    def setUp(self):
        super(NewsTest, self).setUp()
        items = [NewsFactory() for x in range(0, self.limit)]

    def test_list(self):
        response = self.client.get(reverse('api:news:list'), {}, **self.headers)
        self.assertEqual(len(response.data), self.limit, "News list test failed")

    def test_mobile_image(self):
        """
        bin/backend-test test rebateceo.contrib.news.api.tests.NewsTest.test_mobile_image
        """
        response = self.client.get(reverse('api:news:list'), {}, **self.headers)
        self.assertEqual(response.data[0]['mobile_image'], None, "News mobile image field test failed")