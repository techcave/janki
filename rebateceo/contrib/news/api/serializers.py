# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.contrib.sites.models import Site
from rest_framework import serializers
from rebateceo.contrib.shop.api.serializers import ShopSerializer
from ..models import News, NewsImage, Type
from parler_rest.serializers import TranslatedFieldsField


class NewsImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsImage


class NewsSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    images = NewsImageSerializer(many=True)
    translations = TranslatedFieldsField(shared_model=News)

    class Meta:
        model = News
        fields = ['pk', 'translations', 'short', 'date_from', 'date_to', 'sort', 'created_at',
                  'updated_at', 'mobile_image', 'thumb', 'images', 'shops', 'type', 'archived_at']


class NewsForMobileSerializer(NewsSerializer):
    shops = ShopSerializer(many=True)


class TypeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = Type
        fields = ['pk', 'title']


class TypeWithNewsSerializer(TypeSerializer):
    news_set = NewsSerializer(many=True)

    class Meta(TypeSerializer.Meta):
        fields = TypeSerializer.Meta.fields + ['news_set']
