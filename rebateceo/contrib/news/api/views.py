# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rest_framework.generics import ListAPIView
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from ..models import News, Type
from . import serializers

from django.http import JsonResponse
from django.views.generic import View
from django.contrib.sites.models import Site


class NewsListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.NewsSerializer
    model = News

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)

    def get_queryset(self):
        return self.model.objects.active()

    def filter_queryset(self, queryset):
        sort = self.request.GET.get('sort')
        query = self.request.GET.get('query')
        type = self.request.GET.getlist('type')
        queryset = queryset.order_by(sort) if sort else queryset
        queryset = queryset.filter(title__icontains=query) if query else queryset
        queryset.filter(type__in=type) if type else queryset
        return queryset


class NewsForMobileListView(NewsListView):
    serializer_class = serializers.NewsForMobileSerializer

    def get_queryset(self):
        return self.model.objects.active(with_archived=True)


class TypeListView(ListAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.TypeSerializer
    queryset = Type.objects.all()

    def post(self, request, *args, **kwargs):
        """https://techcave.atlassian.net/browse/BOK-181"""
        return self.get(request, args, kwargs)


class NewsListByTypeView(TypeListView):
    serializer_class = serializers.TypeWithNewsSerializer


class NewsSimpleListView(View):

    def post(self, request, *args, **kwargs):
        return self.get(request, args, kwargs)

    def get(self, request, *args, **kwargs):
        # from django.conf import settings
        # settings.DEBUG = True
        # from django.db import connection

        news = News.objects.select_related('type').prefetch_related('shops', 'newsimage_set', 'translations')
        result = []
        for news in news:
            item = {'pk': news.pk}
            item['date_from'] = news.date_from
            item['date_to'] = news.date_to if news.date_to else None
            item['type'] = news.type
            item['thumb'] = 'http://' + Site.objects.get_current().name + news.thumb.url if news.thumb else None
            item['shops'] = []
            if news.shops:
                for shop in news.shops.all():
                    item['shops'].append(shop.id)
            item['images'] = []
            if news.newsimage_set:
                for image in news.newsimage_set.all():
                    item['images'].append('http://' + Site.objects.get_current().name + image.image.url)
            item['translations'] = {}
            item['translations']['pl'] = {'title': news.title,
                                          'description': news.description,
                                          'short': news.short}
            result.append(item)
        # print(len(connection.queries))
        return JsonResponse(result, safe=False)
