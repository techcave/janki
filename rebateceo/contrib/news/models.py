# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from __future__ import unicode_literals
from datetime import date, datetime, time
import reversion
from django.utils.encoding import python_2_unicode_compatible
from django.db.models import Q
from django.utils.translation import gettext as _
from django.db import models
from django.db.models.signals import post_save
from rebateceo.utils.mobileimages_helper import generate_mobileimage
from rebateceo.utils.dates import granular_now
from rebateceo.contrib.shop.models import Shop
from rebateceo.contrib.bookmark.handlers import library as bookmarks

from parler.managers import TranslationManager
from parler.models import TranslatableModel, TranslatedFields


def upload_to(instance, filename):
    return u'/'.join(['news', unicode(filename)])


class NewsManager(TranslationManager):
    def active(self, with_archived=False):
        date_filter = Q(date_from__lte=granular_now) & (
        Q(date_to__isnull=True) | Q(date_to__gt=granular_now))
        queryset = self.filter(date_filter)
        if not with_archived:
            queryset = queryset.filter(archived_at__isnull=True)
        return queryset


class Type(models.Model):
    title = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Type')
        verbose_name_plural = _('Type')

    def __unicode__(self):
        return self.title


@python_2_unicode_compatible
class News(TranslatableModel):
    translations = TranslatedFields(
        title=models.TextField(),
        description=models.TextField(blank=True, null=True),
        short=models.TextField(blank=True, null=True),
    )
    type = models.ForeignKey(Type, blank=True, null=True)
    # short = models.TextField(blank=True, null=True)
    date_from = models.DateTimeField(db_index=True, default=granular_now)
    date_to = models.DateTimeField(blank=True, null=True, db_index=True,
                                   default=datetime.combine(date.today(), time(23, 59, 59)))
    sort = models.IntegerField(blank=True, null=True)
    shops = models.ManyToManyField(Shop, blank=True, null=True)
    mobile_image = models.ImageField(upload_to=upload_to, blank=True, null=True)
    thumb = models.ImageField(upload_to=upload_to, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    archived_at = models.DateTimeField(verbose_name=_('Archived at'), blank=True, null=True)
    objects = NewsManager()

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ['-archived_at', 'sort', '-pk']

    @property
    def images(self):
        return self.newsimage_set.all()

    def __str__(self):
        return unicode(self.title)

    def __unicode__(self):
        return unicode(self.title)


class NewsImage(models.Model):
    news = models.ForeignKey(News)
    image = models.ImageField(upload_to=upload_to)

    class Meta:
        verbose_name = _('News image')
        verbose_name_plural = _('News images')

    def __unicode__(self):
        return unicode(self.image.url)


reversion.register(News)
reversion.register(NewsImage)

bookmarks.register(News)
bookmarks.register(Type)
