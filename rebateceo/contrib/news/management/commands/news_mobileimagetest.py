# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import os
import textwrap
import settings
from PIL import Image, ImageDraw, ImageFont
from django.core.files import File
from django.core.management import BaseCommand
from rebateceo.contrib.news.models import News


class Command(BaseCommand):
    help = u'Tworzy obrazek na podstawie images.first() dodając do niego tekst'

    def handle(self, *args, **options):

        size = 270, 384
        dirname = os.path.dirname
        BG_COLOR = "#777777"
        FONT_COLOR = "white"
        FONT_SIZE = 15
        font = ImageFont.truetype(dirname(dirname(dirname(__file__))) + "/assets/HelveticaNeueLTPro-Lt.ttf", FONT_SIZE)
        _, line_height = font.getsize("Ly")

        for object in News.objects.all():
            # open image and font
            fname, fext = os.path.splitext(object.mobile_image.name)
            img = Image.open(object.mobile_image.path)

            # create small img
            img.thumbnail(size, Image.ANTIALIAS)
            # create canvas
            back = Image.new("RGB", size, BG_COLOR)
            img_width, img_height = img.size
            # merge canvas with img
            back.paste(img, (0, 0))
            img = back
            draw = ImageDraw.Draw(img)

            # draw lines of text below image
            y_text = 0

            for line in adjust_text_length(object.description, font, size[1] - img_height):
                draw.text((5, 5 + img_height + y_text), line, font=font, fill=FONT_COLOR)
                y_text += line_height * 1.1

            # set destination directory
            dest_dir = os.path.join(settings.MEDIA_ROOT, 'mobile_image', 'img')

            # create destination directory if not exists
            if not os.path.isdir(dest_dir):
                os.makedirs(dest_dir)

            filename = 'mobiletest_{}{}'.format(object.pk, fext)
            file_path = "{}".format(os.path.join(dest_dir, filename))
            img.save(file_path)
            object.mobile_image_test = File(open(file_path), name=filename)
            object.save()


def adjust_text_length(text, font, max_height):
    is_bigger = True
    _, line_height = font.getsize("Ly")

    while is_bigger:
        y_text = 0
        lines = textwrap.wrap(text, width=40)
        for line in lines:
            y_text += line_height * 1.1
        if y_text > max_height:
            chars = len(text)
            if y_text > (2 * max_height):
                text = smart_truncate(text, int(chars / 2))
            else:
                text = smart_truncate(text, chars - 10)
        else:
            is_bigger = False

    return lines


def smart_truncate(content, length=100, suffix='...'):
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length + 1].split(' ')[0:-1]) + suffix
