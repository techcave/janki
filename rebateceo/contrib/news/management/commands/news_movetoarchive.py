# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
"""
@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import timedelta
from django.conf import settings
from django.utils.timezone import now
from django.core.management import BaseCommand
from optparse import make_option
from rebateceo.contrib.news.models import News


NEWS_MOVE_TO_ARCHIVE_OFFSET_DAYS = getattr(settings, 'NEWS_MOVE_TO_ARCHIVE_OFFSET_DAYS', 14)


class Command(BaseCommand):
    help = u'https://pm.sizeof.pl/issues/5197'

    def handle(self, *args, **options):
        from_date = now() - timedelta(NEWS_MOVE_TO_ARCHIVE_OFFSET_DAYS)
        items = News.objects.filter(date_to__isnull=True, date_from__lte=from_date, archived_at__isnull=True)
        print 'Moved to archive, ', items.update(archived_at=now())