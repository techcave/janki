from datetime import datetime
import os
import urllib2
import requests
from django.conf import settings
from django.utils import translation, timezone
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand
from rebateceo.utils import getimg2
from ... import models

try:
    _API_URL = settings.REBATECEO_NEWS_SYNC_DATA_URL
except AttributeError:
    raise Warning('REBATECEO_NEWS_SYNC_DATA_URL setting is required!')


class Command(BaseCommand):
    model = models.News
    image_model = models.NewsImage
    image_path = os.path.join(settings.MEDIA_ROOT, 'news')
    today = datetime.now()
    api_url = _API_URL

    def handle(self, *args, **options):
        f = open("var/log/last_news_logs.txt", "w+")
        f.write("Start: {}\n".format(timezone.now()))
        print u"\n\nStart: %s\n" % timezone.now()

        self.model.objects.all().delete()

        user_agent = {
            'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu '
                          'Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36 '
        }
        f.write("\nGetting Data from {}\n".format(self.api_url))
        print "Getting Data from {}\n".format(self.api_url)

        response = requests.get(self.api_url, headers=user_agent, verify=False).json()
        translation.activate('pl')
        for index, child in enumerate(response):
            if child.get('type') == '36':
                # import pdb; pdb.set_trace()
                date_from = datetime.strptime(child.get('date_from'), '%Y-%m-%d') if child.get('date_from') else None

                if child.get('date_to'):
                    date_to = datetime.strptime(child.get('date_to'), '%Y-%m-%d')
                else:
                    date_to = date_from + relativedelta(months=+1)

                if date_from <= self.today <= date_to:
                    pk = int(child.get('pk'))
                    title = child.get('title')
                    description = child.get('description')
                    short = child.get('short')
                    thumb = child.get('thumb_image')

                    if date_from == date_to:
                        date_to = date_to.replace(hour=23, minute=59)

                    obj, created = self.model.objects.get_or_create(pk=pk)
                    obj.description = description
                    obj.date_to = date_to
                    obj.short = short
                    obj.title = title
                    obj.date_from = date_from
                    obj.save()

                    f.write('\nNew news {} {}'.format(index, pk))
                    print u'Object: {}. Created -> {}'.format(title.encode('ascii', 'replace'), created)

                    if thumb:
                        try:
                            thumb, tmp = getimg2(thumb)
                        except urllib2.HTTPError as e:
                            print
                            u'Image: ERROR: {}'.format(str(e), thumb)
                        obj.thumb.save(thumb, tmp, save=True)
                        obj.save()
                        f.write('\n\tThumb: {}'.format(obj.thumb.url))
                        print u'\tThumb: {}'.format(obj.thumb.url)

        f.write("\n\nEnd: {}\n".format(timezone.now()))
        print u"\n\nEnd: {}\n".format(timezone.now())
        f.close()
