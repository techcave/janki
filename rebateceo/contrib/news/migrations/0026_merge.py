# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0024_auto_20170403_1636'),
        ('news', '0025_auto_20170414_1027'),
    ]

    operations = [
    ]
