# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.news.models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=rebateceo.contrib.news.models.upload_to)),
                ('news', models.ForeignKey(to='news.News')),
            ],
            options={
                'verbose_name': 'News image',
                'verbose_name_plural': 'News images',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='newsgallery',
            name='news',
        ),
        migrations.DeleteModel(
            name='NewsGallery',
        ),
    ]
