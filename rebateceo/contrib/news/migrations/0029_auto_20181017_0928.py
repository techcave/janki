# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0028_auto_20180820_0859'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='date_to',
            field=models.DateTimeField(default=datetime.datetime(2018, 10, 17, 23, 59, 59), null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
    ]
