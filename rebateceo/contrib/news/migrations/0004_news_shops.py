# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_auto_20150302_0024'),
        ('news', '0003_auto_20150302_0037'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='shops',
            field=models.ManyToManyField(to='shop.Shop', null=True, blank=True),
            preserve_default=True,
        ),
    ]
