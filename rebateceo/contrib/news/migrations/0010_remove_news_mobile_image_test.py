# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0009_news_mobile_image_test'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='mobile_image_test',
        ),
    ]
