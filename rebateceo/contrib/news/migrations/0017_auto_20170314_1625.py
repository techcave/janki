# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.utils.dates


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0016_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='date_from',
            field=models.DateTimeField(default=rebateceo.utils.dates.granular_now, db_index=True),
            preserve_default=True,
        ),
    ]
