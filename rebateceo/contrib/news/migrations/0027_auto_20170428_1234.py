# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0026_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='type',
            options={'verbose_name': 'Typ', 'verbose_name_plural': 'Typ'},
        ),
        migrations.AlterField(
            model_name='news',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Stworzony'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name='date_to',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 28, 23, 59, 59), null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name=b'Zaktualizowany'),
            preserve_default=True,
        ),
    ]
