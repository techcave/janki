# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0012_auto_20150713_1435'),
        ('news', '0015_news_thumb'),
    ]

    operations = [
    ]
