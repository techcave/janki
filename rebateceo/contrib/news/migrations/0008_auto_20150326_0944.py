# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0007_news_mobile_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='type',
            field=models.ForeignKey(blank=True, to='news.Type', null=True),
            preserve_default=True,
        ),
    ]
