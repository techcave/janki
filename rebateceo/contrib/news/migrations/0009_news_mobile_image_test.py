# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.news.models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0008_auto_20150326_0944'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='mobile_image_test',
            field=models.ImageField(null=True, upload_to=rebateceo.contrib.news.models.upload_to, blank=True),
            preserve_default=True,
        ),
    ]
