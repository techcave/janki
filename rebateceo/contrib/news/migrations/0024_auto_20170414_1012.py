# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0023_auto_20170403_1311'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_code', models.CharField(max_length=15, verbose_name='Language', db_index=True)),
                ('title', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('master', models.ForeignKey(related_name='translations', editable=False, to='news.News', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'news_news_translation',
                'db_tablespace': '',
                'default_permissions': (),
                'verbose_name': 'News Translation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='newstranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.RemoveField(
            model_name='news',
            name='description',
        ),
        migrations.RemoveField(
            model_name='news',
            name='title',
        ),
        migrations.AlterField(
            model_name='news',
            name='date_to',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 14, 23, 59, 59), null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
    ]
