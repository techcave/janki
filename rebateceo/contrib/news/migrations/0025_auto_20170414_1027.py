# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0024_auto_20170414_1012'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='short',
        ),
        migrations.AddField(
            model_name='newstranslation',
            name='short',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
