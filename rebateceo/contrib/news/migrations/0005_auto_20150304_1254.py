# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0004_news_shops'),
    ]

    operations = [
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Typ',
                'verbose_name_plural': 'Typ',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='news',
            name='type',
            field=models.ForeignKey(blank=True, to='news.Type', null=True),
            preserve_default=True,
        ),
    ]
