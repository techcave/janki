# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0005_auto_20150304_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='type',
            field=models.ForeignKey(to='news.Type'),
            preserve_default=True,
        ),
    ]
