# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rebateceo.contrib.news.models
import rebateceo.utils.dates


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('description', models.TextField(null=True, blank=True)),
                ('short', models.TextField(null=True, blank=True)),
                ('date_from', models.DateField(default=rebateceo.utils.dates.granular_now, db_index=True)),
                ('date_to', models.DateField(db_index=True, null=True, blank=True)),
                ('sort', models.IntegerField(null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('archived_at', models.DateTimeField(null=True, verbose_name='Archived at', blank=True)),
            ],
            options={
                'ordering': ['-archived_at', 'sort', '-pk'],
                'verbose_name': 'News',
                'verbose_name_plural': 'News',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsGallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=rebateceo.contrib.news.models.upload_to)),
                ('news', models.ForeignKey(to='news.News')),
            ],
            options={
                'verbose_name': 'News Gallery',
                'verbose_name_plural': 'News Galleries',
            },
            bases=(models.Model,),
        ),
    ]
