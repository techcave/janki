# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import os
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.files.base import File as DjangoFile
from . import models
from . import factories


class ShopAdminTest(TestCase):

    def setUp(self):
        username = 'test_user'
        pwd = 'secret'

        self.u = User.objects.create_user(username, '', pwd)
        self.u.is_staff = True
        self.u.is_superuser = True
        self.u.save()

        self.assertTrue(self.client.login(username=username, password=pwd),
            "Logging in user %s, pwd %s failed." % (username, pwd))

        models.News.objects.all().delete()

    def tearDown(self):
        self.client.logout()
        self.u.delete()

    def test_item_ok(self):
        self.assertEquals(models.News.objects.count(), 0)
        post_data = {
            'title': u'Test OK',

            'newsimage_set-INITIAL_FORMS': u'0',
            'newsimage_set-TOTAL_FORMS': u'7',
            'newsimage_set-MIN_NUM_FORMS': u'0',
            'newsimage_set-MAX_NUM_FORMS': u'1000',

            'mobile_image': DjangoFile(file(u"{}{}".format(unicode(os.path.dirname(__file__)), u'/assets/test-img.gif'), "rb"))
        }

        response = self.client.post(reverse('admin:news_news_add'), post_data)
        self.assertEqual(response.status_code, 200)

    def test_admin_list(self):
        item_1 = factories.NewsFactory()
        self.assertEquals(models.News.objects.count(), 1)

        response = self.client.get(reverse('admin:news_news_changelist'))
        self.assertEqual(response.status_code, 200)