# -*- coding: utf-8 -*-
from django.test import TestCase
from django.conf import settings


REQUIRED_SETTINGS = (

)


class RebateSettingsTest(TestCase):
    def test_required_settings(self):
        for setting in REQUIRED_SETTINGS:
            self.assertTrue(hasattr(settings, setting), msg=setting)