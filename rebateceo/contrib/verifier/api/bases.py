# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.utils import timezone
from rest_framework.views import APIView
from ..models import Verifier, EventLog
from .exceptions import IMEIIsRequired, VerifierDoesNotExist, VerifierIMEIError, VerifierIsDisabled


class VerifierBaseAPIView(APIView):
    imei = None
    verifier = None

    def initial(self, request, *args, **kwargs):
        super(VerifierBaseAPIView, self).initial(request, args, kwargs)

        # Check imei
        self.imei = request.POST.get('imei', request.GET.get('imei', kwargs.get('imei')))
        if not self.imei:
            raise IMEIIsRequired()

        # Check verifier
        try:
            self.verifier = Verifier.objects.get(imei=self.imei)
        except ValueError, e:
            raise VerifierIMEIError(mess=str(e))
        except Verifier.DoesNotExist:
            raise VerifierDoesNotExist(imei=self.imei)
        else:
            if not self.verifier.active:
                raise VerifierIsDisabled()

    def get_timestamp(self):
        tznow = timezone.now()
        return '{:%Y-%m-%d:%H:%M:%S}.{:03d}'.format(tznow, tznow.microsecond // 1000)

    def get_object(self, queryset=None):
        return self.verifier

    def eventlog(self, details):
        return EventLog.objects.create(verifier=self.verifier, details=details)
