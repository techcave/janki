# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from rebateceo.contrib.shop.api.serializers import SellerSerializer
from rebateceo.contrib.shop.models import Seller
from rest_framework.generics import GenericAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from ...models import MasterCode
from ..bases import VerifierBaseAPIView
from ..exceptions import VerifierAuthError, VerifierAuthCodeError, VerifierSellerCodeError
from ..serializers import MasterCodeSerializer


class AuthView(VerifierBaseAPIView, GenericAPIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MasterCodeSerializer
    model = MasterCode
    object = None

    def initial(self, request, *args, **kwargs):
        super(AuthView, self).initial(request, args, kwargs)

        code = self.request.POST.get('code')
        try:
            type, uuid = code.split(';')
        except ValueError:
            exc = VerifierAuthCodeError(data={'POST': self.request.POST})
            # TODO: raven
            self.eventlog(exc)
            raise exc
        else:
            try:
                self.object = MasterCode.objects.get(uuid=uuid)
            except MasterCode.DoesNotExist:
                exc = VerifierAuthError(data={'POST': self.request.POST})
                self.eventlog(exc)
                # TODO: raven
                raise exc
            else:
                self.eventlog(details=dict(mess="Code is OK!", data={'POST': self.request.POST}))

    def post(self, request, *arg, **kwargs):
        return Response(self.serializer_class(self.object).data)


class SellerDetailView(VerifierBaseAPIView, GenericAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = SellerSerializer
    model = Seller
    object = None

    def initial(self, request, *args, **kwargs):
        super(SellerDetailView, self).initial(request, args, kwargs)

        code = self.request.POST.get('code')
        try:
            type, uuid = code.split(';')
        except ValueError:
            exc = VerifierSellerCodeError(data={'POST': self.request.POST})
            # TODO: raven
            self.eventlog(exc)
            raise exc
        else:
            try:
                self.object = Seller.objects.get(uuid=uuid)
            except Seller.DoesNotExist:
                exc = VerifierSellerCodeError(data={'POST': self.request.POST})
                self.eventlog(exc)
                # TODO: raven
                raise exc
            else:
                self.eventlog(details=dict(mess="Code is OK!", data={'POST': self.request.POST}))

    def post(self, request, *arg, **kwargs):
        return Response(self.serializer_class(self.object).data)


class SellerUpdateView(UpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    model = Seller


class SellerDeleteView(DestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    model = Seller
