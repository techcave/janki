# -*- coding: utf-8 -*-
import time

from django.db import transaction
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from rebateceo.contrib.purchase.api.serializers import PurchaseCreateSerializer
from rebateceo.contrib.purchase.models import Purchase, Amount
from rebateceo.contrib.shop.models import Seller
from ...models import VerifierScan, Verification
from ..serializers import PurchaseRegisterSerializer, VerificationSerializer
from ..exceptions import (VerificationScanIdRequired, VerificationScanDoesNotExist,
                          VerifierDoesNotExistForThisScan, CardDoesNotExistForThisScan,
                          SellerIdIsRequired, VerificationSellerDoesNotExist,
                          PurchaseHasAlreadyBeenVerified, AmountIsRequired)

import logging
logger = logging.getLogger(__name__)


class PurchaseRegisterView(CreateAPIView):
    serializer_class = PurchaseRegisterSerializer
    model = Purchase
    scan_id = None
    scan = None
    shop = None
    card = None
    seller_id = None
    seller = None
    coupon = None
    amount = None
    verifier = None
    verification = None

    def initial(self, request, *args, **kwargs):
        super(PurchaseRegisterView, self).initial(request, args, kwargs)
        self.scan_id = request.POST.get('scan_id', request.GET.get('scan_id', kwargs.get('scan_id')))
        self.seller_id = request.POST.get('seller_id', request.GET.get('seller_id', kwargs.get('seller_id')))
        self.amount = request.POST.get('amount', request.GET.get('amount', kwargs.get('amount')))

        if not self.amount:
            raise AmountIsRequired()
        else:
            self.amount = Amount.to_decimal(self.amount)

        if not self.scan_id:
            raise VerificationScanIdRequired()
        try:
            self.scan = VerifierScan.objects.get(pk=self.scan_id)
        except VerifierScan.DoesNotExist:
            raise VerificationScanDoesNotExist(scan_id=self.scan_id)
        else:
            self.verifier = self.scan.verifier
            if not self.verifier:
                raise VerifierDoesNotExistForThisScan(scan_id=self.scan_id)
            else:
                self.shop = self.verifier.shop
                self.coupon = self.scan.coupon
                self.card = self.scan.coupon.get_card if self.coupon else self.scan.card

                def _verified(verification):
                    verification_json = VerificationSerializer(verification).data
                    raise PurchaseHasAlreadyBeenVerified(verification=verification_json)

                # Purhaces from coupon verified?
                if self.coupon and self.coupon.verification:
                    _verified(self.coupon.verification)

                # Purchase by card verified?
                if not self.card:
                    raise CardDoesNotExistForThisScan(scan_id=self.scan_id)
                else:

                    try:
                        verification = self.scan.verifications.get(status=True)
                    except Verification.DoesNotExist:
                        pass
                    else:
                        _verified(verification)

                    # Check for seller confirmation
                    if self.verifier.require_seller_card and not self.seller:
                        if not self.seller_id:
                            raise SellerIdIsRequired()
                        try:
                            self.seller = self.shop.sellers.get(pk=self.seller_id)
                        except Seller.DoesNotExist:
                            raise VerificationSellerDoesNotExist(shop_id=self.shop.pk, seller_id=self.seller_id)

    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            serializer = PurchaseCreateSerializer(data={
                'user': self.card.profile.user.pk,
                'shop': int(self.shop.pk),
                'amount': self.amount,
                'number': 'V-%s' % time.time(),
                'origin': Purchase.ORIGIN_VERIFIER
            })
            if not serializer.is_valid():
                return Response(serializer.errors)

            # OK, Save purchase
            serializer.save()
            response = serializer.data
            status = True

        # Save verification
        verification = Verification.objects.create(
            scan=self.scan, seller=self.seller,
            request={'POST': request.POST}, response=response,
            purchase=serializer.instance, coupon=self.coupon, status=status
        )

        # Logging
        response.update({
            'verification': VerificationSerializer(verification).data,
            'receipt': serializer.instance.pk,
            'card': self.card.pk
        })

        logger.info(self.__class__, exc_info=True, extra={
            'request': request.DATA,
            'meta': request.META,
            'response': response
        })

        # Response
        return Response(response)
