# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from rebateceo.contrib.card.api.serializers import CardSerializer
from rebateceo.contrib.loyalty.api.serializers import AmountSerializer
from rebateceo.contrib.loyalty.models import Program
from rebateceo.contrib.purchase.models import Amount
from rebateceo.contrib.shop.models import Seller
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from django.utils.translation import gettext as _
from ..bases import VerifierBaseAPIView
from ..serializers import (VerificationPostSerializer, VerificationSerializer, CustonMemberSerializer)
from ...models import VerifierScan, Verification
from ..exceptions import (VerificationScanIdRequired, VerificationScanDoesNotExist, VerificationAmountIsRequired,
                          CouponHasAlreadyBeenVerified, VerificationSellerDoesNotExist, VerificationSellerIdRequired,
                          VerificationUpsellingDoesNotExist, UpsellingHasAlreadyBeenVerified,
                          VerificationByCardNotImplementedYet)


import logging
logger = logging.getLogger(__name__)


class VerifierVerificationView(VerifierBaseAPIView, GenericAPIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = VerificationPostSerializer
    model = Verification
    detail = {}
    scan = None
    coupon = None
    seller = None
    seller_id = None
    amount = None
    card = None
    program = None
    program_id = 1  # TODO

    def post(self, request, *args, **kwargs):
        self.program = Program.objects.get(pk=self.program_id)

        response = {}
        instance = self.model.objects.create(request={
            'post': request.POST,
            'get': request.GET,
        })
        instance.save()

        try:
            self.validate(request, *args, **kwargs)
        except Exception, e:
            instance.details = dict(e.__dict__)  # no nie wiem...
            instance.save()
            raise e
        else:

            instance.amount = self.amount
            instance.coupon = self.coupon
            instance.seller = self.seller
            instance.scan = self.scan
            instance.save()

            # ================== WERYFIKACJA KARTA ===========================
            if self.coupon.verification_type == 'code_amount' and self.coupon.verification_required:
                amount, created = Amount.objects.get_or_create(
                    scan=self.scan,
                    seller=self.seller,
                    card=self.coupon.card,
                    amount=self.amount,
                    #program=self.program
                )

                # Update status
                instance.status = True
                instance.save()

                self.detail = {
                    'messages': {
                        'default': {
                            'title': _('You have now {points} points'.format(points=amount.points)),
                            'body': _('You purchase with rebate voucher gives you another\n'
                                      'chance to win 1000 PLN in the drawing at the end of the month.', )
                        },
                    },
                    'data': {
                        'card': CardSerializer(self.coupon.card).data,
                        'amount': AmountSerializer(amount).data
                    }
                }
            else:
                # Update status
                instance.status = True
                instance.save()

                # ==================   ZWYKLA WERYFIKACJA   ======================
                self.detail = {
                    'messages': {
                        'default': {
                            # 'title': _(u'Thank You for using our system'),
                            # 'body': _(u'The more rebate vouchers you use the'
                            #           u'better discounts offers you will receive.'),
                            'title': u'Thank You for using our system',
                            'body': u'The more rebate vouchers you use the'
                                    u'better discounts offers you will receive.',
                        },
                    },
                }

            # Update response
            response.update({
                'detail': self.detail,
                'verification': VerificationSerializer(instance=instance, context={'request': self.request}).data
            })

            # Save to log
            instance.response = response
            instance.save()

            logger.info(self.__class__, exc_info=True, extra={
                'request': request.DATA,
                'meta': request.META,
                'response': response
            })

            return Response(response)

    def validate(self, request, *args, **kwargs):
        # Check scanid
        scan_id = request.POST.get('scan_id')
        if not scan_id:
            raise VerificationScanIdRequired()

        # Check scan
        try:
            self.scan = VerifierScan.objects.get(pk=scan_id)
        except VerifierScan.DoesNotExist:
            raise VerificationScanDoesNotExist(scan_id=scan_id)
        else:
            self.coupon = self.scan.coupon
            if self.coupon:
                verification_json = VerificationSerializer(self.coupon.verification).data


                # ================================================================
                # Upselling valid
                # ================================================================
                # upselling_id = request.POST.get('upselling_id')
                # if upselling_id:
                #     try:
                #         self.upselling = Upselling.objects.get(pk=upselling_id)
                #     except Upselling.DoesNotExist:
                #         raise VerificationUpsellingDoesNotExist(upselling_id=upselling_id)
                #
                #     # Check for upselling verified
                #     if self.upselling.verification:
                #         verification_json = VerificationSerializer(self.upselling.verification).data
                #         raise UpsellingHasAlreadyBeenVerified(verification=verification_json,
                #                                            verification_id=verification_json.get('pk'))
                # else:
                # ================================================================
                # No upselling and verified
                # ================================================================
                if self.coupon.is_used:
                    raise CouponHasAlreadyBeenVerified(verification=verification_json,
                                                       verification_id=verification_json.get('pk'))
                else:

                    # ================================================================
                    # Handle card-amount verification
                    # ================================================================
                    if self.coupon.verification_type == 'code_amount':

                        # Amount valid
                        self.amount = request.POST.get('amount', 0)
                        if not self.amount:
                            raise VerificationAmountIsRequired()
                        else:
                            self.amount = int(self.amount)

                        # Check scanid
                        self.seller_id = request.POST.get('seller_id')
                        if not self.seller_id:
                            raise VerificationSellerIdRequired()

                        # Seller valid
                        if self.seller_id:
                            try:
                                self.seller = Seller.objects.get(pk=self.seller_id)
                            except Seller.DoesNotExist:
                                raise VerificationSellerDoesNotExist(seller_id=self.seller_id)

            else:
                raise VerificationByCardNotImplementedYet()
