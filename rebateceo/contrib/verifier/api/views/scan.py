# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from rebateceo.contrib.card.api.exceptions import CardHasNotMember
from rebateceo.contrib.card.models import Card
from rebateceo.contrib.card.validators import card_exists_validator, card_purchase_limit_validator
from rebateceo.contrib.profile.validators import user_purchase_limit_validator
from rebateceo.contrib.rebate.api.serializers import CouponSerializer
from rebateceo.contrib.rebate.models import Coupon
from rebateceo.contrib.shop.api.serializers import ShopSerializer, ShopForVerifierSerializer
from rebateceo.contrib.shop.models import Seller
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from django_ean13.validators import EAN13_validator

from ..bases import VerifierBaseAPIView
from ... import models
from .. import exceptions, serializers
from rebateceo.contrib.profile.models import Profile


class MixinScanView(object):
    code_type = None
    code_value = None
    force_code_type = 'card'  # Backward
    card = None
    scan_id = None  # Check flow > card scan > seller scan

    def validate(self, request, *args, **kwargs):
        self.scan_id = request.POST.get('scan_id', request.GET.get('scan_id'))
        self.code_type, self.code_value = self._resolve_code(request, args, kwargs)
        func_name = 'validate_%s' % self.code_type
        return getattr(self, func_name)(request, *args, **kwargs)

    def _resolve_code(self, request, *args, **kwargs):
        # Check code
        code = request.POST.get('code')
        if not code:
            raise exceptions.ScanCodeValueRequired()
        if ';' in code:
            code = code.replace(';', '%3B')
        try:
            code_type, code_value = code.split('%3B')  # ;
        except ValueError:
            if self.force_code_type:
                return self.force_code_type, code
            raise exceptions.ScanCodeValueError()
        else:
            if code_type not in [Coupon.QRCODE_TYPE, Card.QRCODE_TYPE, Seller.QRCODE_TYPE,
                                 models.MasterCode.QRCODE_TYPE, Profile.QRCODE_TYPE]:
                raise exceptions.ScanCodeTypeUnrecognized(code_type=code_type)
        return code_type, code_value

    def check_limits(self, card):
        try:
            card_purchase_limit_validator(card)
        except ValidationError, e:
            raise exceptions.PurchaseRegisterLimitsException(mess=e.message)

    def validate_card(self, request, *args, **kwargs):
        # try:
        #     EAN13_validator(self.code_value)
        # except ValidationError:
        #     raise exceptions.CardIsNotEAN13(code_type=self.code_type, code_value=self.code_value)
        # else:
        try:
            card = card_exists_validator(self.code_value)
        except ValidationError:
            raise exceptions.CardDoesNotExist(code_value=self.code_value)
        else:
            self.card = card

            # Raise exc for empty member
            if not self.card.profile:
                raise CardHasNotMember()

            # Validate limits for card
            self.check_limits(self.card.profile)

    def validate_profile(self, request, *args, **kwargs):
        self.validate_card(request, *args, **kwargs)


class VerifierScanView(MixinScanView, VerifierBaseAPIView, GenericAPIView):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    serializer_class = serializers.VerifierPostScanSerializer
    model = models.VerifierScan
    mastercode = None
    session = None
    scan = None
    verifier = None
    coupon = None
    seller = None
    detail = None
    profile = None
    card = None
    upselling = None
    output = {}

    def post(self, request, *args, **kwargs):
        return self._handle_request(request, *args, **kwargs)

    def _handle_upselling(self, request, *args, **kwargs):
        pass

    def _handle_request(self, request, *args, **kwargs):

        self.scan = self.model.objects.create(request={
            'post': request.POST,
            'get': request.GET,
        })

        try:
            self.validate(request, *args, **kwargs)
        except Exception, e:
            self.scan.details = dict(e.__dict__)  # no nie wiem...
            self.scan.save()
            raise e
        else:
            self.scan.response = self.output
            self.scan.status = True
            self.scan.coupon = self.coupon
            self.scan.card = self.card
            self.scan.seller = self.seller
            self.scan.verifier = self.verifier
            self.scan.mastercode = self.mastercode
            self.scan.save()

            self.output.update({
                'detail': self.detail,
                'scan': serializers.VerifierScanSerializer(instance=self.scan, context={'request': self.request}).data
            })

            return Response(self.output)

    # =========================
    # ===== HANDLE TYPES ======
    # =========================
    def check_limits(self, profile):
        if profile:
            try:
                user_purchase_limit_validator(profile.user)
            except ValidationError, e:
                raise exceptions.PurchaseRegisterLimitsException(mess=e.message)

    def validate_coupon(self, request, *args, **kwargs):
        try:
            # check for coupon exists
            self.coupon = Coupon.objects.all().get(uuid=self.code_value)
        except Coupon.DoesNotExist:
            raise exceptions.CouponDoesNotExist(code_value=self.code_value)
        else:

            #
            # TODO: Handle more cards...
            #
            coupon_json = CouponSerializer(self.coupon).data

            # Validate limits for profile in session
            self.card = self.coupon.get_card
            self.profile = self.card.profile if self.card else self.coupon.session.profile
            self.check_limits(self.profile)

            # Check for shop
            if self.coupon.rebate.shop != self.verifier.shop:
                shop = self.coupon.rebate.shop
                coupon_shop_json = ShopSerializer(instance=shop).data
                raise exceptions.CouponFromAnotherStore(shop=coupon_shop_json, coupon=coupon_json,
                                                        code_value=self.code_value, shop_name=shop.name)

            # Check for rebate can display
            if not self.coupon.rebate.is_valid:
                raise exceptions.CouponRebateIsInvalid(coupon=coupon_json, code_value=self.code_value)

            # Check for used coupon
            if self.coupon.verification:  # self.coupon.is_used:
                verification_json = serializers.VerificationSerializer(self.coupon.verification).data
                raise exceptions.CouponRebateIsUsed(coupon=coupon_json, verification=verification_json,
                                                    code_value=self.code_value)

            # Handle upselling
            upselling_id = request.POST.get('upselling_id')
            if upselling_id:
                if not self.coupon.upselling:
                    # TODO: check in DB
                    raise exceptions.UpsellingDoesNotExist(upselling_id=upselling_id)
                # We create coupon and save verification to this
                self.handle_upselling()

    def validate_seller(self, request, *args, **kwargs):
        if not self.scan_id:
            raise exceptions.SellerScanRequiresScanId()
        else:
            parent_scan = self.model.objects.get(pk=self.scan_id)
            if parent_scan.seller:
                raise exceptions.SellerParentScanHasSeller()
            try:
                # check for coupon exists
                seller = Seller.objects.assigned().get(uuid=self.code_value)
            except Seller.DoesNotExist:
                raise exceptions.SellerDoesNotExist(code_value=self.code_value)
            else:
                # Check for shop
                if seller.shop != self.verifier.shop:
                    shop_json = ShopForVerifierSerializer(seller.shop).data
                    raise exceptions.SellerFromAnotherStore(shop=shop_json, code_value=self.code_value)

                # OK!
                self.seller = seller
                self.detail = {
                    'messages': {
                        'default': {
                            'title': _('Accountant code is valid'),
                            'body': ''}
                    },
                }

    def validate_mastercode(self, request, *args, **kwargs):
        try:
            self.mastercode = models.MasterCode.objects.get(uuid=self.code_value)
        except models.MasterCode.DoesNotExist:
            exc = exceptions.VerifierAuthError(data={'POST': self.request.POST})
            self.eventlog(exc)
            raise exc
        else:
            self.eventlog(details=dict(mess="Code is OK!", data={'POST': self.request.POST}))

    def handle_upselling(self):
        new_coupon = Coupon.objects.create(parent=self.coupon, rebate_id=self.coupon.upselling.pk,
                                           origin_id=Coupon.ORIGIN_VERIFIER,) # session=self.session)
        verification = models.Verification.objects.create(scan=self.scan, seller=self.seller, coupon=new_coupon,
                                                          request={'POST': self.request.POST})
        data = {
            'new_coupon': {
                'details': _('Upselling has been saved as as new verified coupon'),
                'pk': new_coupon.pk,
                'verification': {
                    'pk': verification.pk,
                }
            }
        }
        # Update info
        verification.response = data
        verification.save()
        self.output.update(data)  # Append to output
