# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from constance import config
from django.conf import settings
from rest_framework.generics import GenericAPIView, RetrieveAPIView
from rest_framework.response import Response
from django.utils.translation import ugettext as _
from .. import bases, serializers


class ConfigView(GenericAPIView):

    def get_config(self, request, *args, **kwargs):
        varifier_config, prefix = {}, 'verifier_'
        for field in settings.CONSTANCE_CONFIG:
            varifier_config.update({field.lower().replace(prefix, ''): getattr(config, field)})
        return {
            'config': {
                'description': _('This are defaults settings for all verifiers!'),
                'verifier': varifier_config
            }
        }

    def get(self, request, *args, **kwargs):
        return Response(self.get_config(request, *args, **kwargs))


class VerifierConfigView(bases.VerifierBaseAPIView, RetrieveAPIView):
    serializer_class = serializers.VerifierSerializer
