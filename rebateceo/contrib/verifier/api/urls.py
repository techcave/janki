# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from django.conf.urls import patterns, url, include
from .views import adv, scan, verifier, verification, ping, config, eventlog, purchase


urlpatterns = patterns('',
    url(r'^adv/(?P<imei>\d+)/list/$', adv.AdvListView.as_view(), name='adv-list'),
    url(r'^config/$', config.ConfigView.as_view(), name='config'),
    url(r'^(?P<imei>\d+)/config/$', config.VerifierConfigView.as_view(), name='verifier-config'),
    url(r'^(?P<imei>\d+)/detail/$', verifier.VerifierDetailView.as_view(), name='detail'),

    url(r'^list/$', verifier.VerifierListView.as_view(), name='list'),
    url(r'^ping/$', ping.PingView.as_view(), name='ping'),
    url(r'^eventlog/create/$', eventlog.EventLogCreateView.as_view(), name='eventlog'),

    url(r'^scan/$', scan.VerifierScanView.as_view(), name='scan'),
    url(r'^purchase/register/$', purchase.PurchaseRegisterView.as_view(), name='purchase-register'),
    url(r'^verification/$', verification.VerifierVerificationView.as_view(), name='verification'),

    # Other
    url(r'^wizzard/', include('rebateceo.contrib.verifier.api.wizzard.urls', namespace='wizzard')),
)
