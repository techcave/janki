# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import timedelta, datetime
from rebateceo.contrib.card.api.serializers import CardSerializer
from rebateceo.contrib.profile.api.serializers import ProfileSerializer
from rebateceo.contrib.rebate.api.serializers import CouponSerializer
from rebateceo.contrib.shop.api.serializers import ShopForVerifierSerializer, SellerSerializer
from rebateceo.contrib.advert.api.serializers import BannerSerializer
from rest_framework import serializers
from django.conf import settings
from constance import config

from .. import models


class AdvSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    banner = BannerSerializer()

    class Meta:
        model = models.Adv
        fields = ['pk', 'banner', 'shop']


class VerifierSimpleSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.Verifier
        fields = ['pk', 'imei', 'created']


class VerifierSerializer(VerifierSimpleSerializer):
    pk = serializers.ReadOnlyField()
    shop = ShopForVerifierSerializer()
    config = serializers.SerializerMethodField()

    class Meta:
        model = models.Verifier
        fields = ['pk', 'shop', 'imei', 'created', 'modified', 'config']

    def get_default_config(self):
        cfg, prefix = {}, 'verifier_'
        for field in settings.CONSTANCE_CONFIG:
            if hasattr(config, field) and prefix.upper() in field:
                cfg.update({field.lower().replace(prefix, ''): getattr(config, field)})
        return cfg

    def get_config(self, instance):
        cfg = self.get_default_config()
        try:
            custom = instance.config
        except models.Config.DoesNotExist:
            return cfg
        else:
            for field, value in cfg.iteritems():
                #try:
                custom_value = getattr(custom, field)
                #except AttributeError:
                #    cfg.update({field: 'n/a'})
                #else:
                if custom_value and custom_value != value:
                    cfg.update({field: custom_value})
            return cfg


class VerifierPostScanSerializer(serializers.Serializer):
    imei = serializers.CharField(max_length=255)
    code = serializers.CharField(max_length=255)

    class Meta:
        fields = ['imei', 'code']


class MasterCodeSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.MasterCode
        fields = ['pk', 'authtoken']


class VerifierScanSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    verifier = VerifierSerializer()
    card = CardSerializer()
    coupon = CouponSerializer()
    seller = SellerSerializer()
    mastercode = MasterCodeSerializer()
    extra_info = serializers.SerializerMethodField()

    class Meta:
        model = models.VerifierScan
        fields = ['pk', 'verifier', 'card', 'coupon', 'seller', 'mastercode', 'created_at', 'updated_at', 'status',
                  'details', 'extra_info']

    def get_extra_info(self, scan):
        card = scan.card
        if card:
            all_purchases = card.profile.user.purchases.all()
            delta = datetime.now() - timedelta(days=30)
            last_month_purchases = all_purchases.filter(created_at__gte=delta)
            return {
                'member': {
                    'attempts': {
                        "avail": len(all_purchases),
                        "all": len(last_month_purchases)
                    },
                    'purchases': {
                        'all': len(all_purchases),
                        'last_month': len(last_month_purchases),
                    }
                }
            }


class VerificationPostSerializer(serializers.Serializer):
    pk = serializers.ReadOnlyField()
    imei = serializers.CharField(max_length=255)
    scan_id = serializers.CharField(max_length=255)
    seller_id = serializers.CharField(max_length=255)
    amount = serializers.CharField(max_length=255)

    class Meta:
        fields = ['imei', 'scan_id', 'seller_id', 'amount']


class VerificationSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()
    scan = VerifierScanSerializer()

    class Meta:
        model = models.Verification
        fields = ['pk', 'scan', 'seller', 'coupon', 'created_at', 'updated_at', 'status', 'details']


class CustonMemberSerializer(ProfileSerializer):
    class Meta(ProfileSerializer.Meta):
        fields = ['pk', 'first_name', 'last_name', 'points']


class PingSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.Ping


class EventLogPostSerializer(serializers.Serializer):
    imei = serializers.CharField(max_length=255)
    details = serializers.CharField(max_length=3000)

    class Meta:
        fields = ['imei', 'details']


class EventLogSerializer(serializers.ModelSerializer):
    pk = serializers.ReadOnlyField()

    class Meta:
        model = models.EventLog
        fields = ['pk', 'created']


class PurchaseRegisterSerializer(serializers.Serializer):
    scan_id = serializers.IntegerField()
    seller_id = serializers.IntegerField()
    amount = serializers.IntegerField()
