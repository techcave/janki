# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import APIException
from rest_framework import status


class VerifBaseException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {}
    extra_message = default_message
    data = {}
    code = None

    def __init__(self, *args, **kwargs):
        self.detail = {
            'detail': {
            'code': self.code,
            'type': self.__class__.__name__,
            'messages': {}}}

        if kwargs.get('data'):
            self.detail.update({'data': kwargs.get('data')})

        api = self.default_detail.format(**kwargs)
        if api:
            self.detail['detail']['messages'].update({'api': api})

        # Default
        default = self.get_default_message(**kwargs)
        if default:
            self.detail['detail']['messages'].update({'default': default})

        # Extra
        extra = self.get_extra_message(**kwargs)
        if extra:
            self.detail['detail']['messages'].update({'extra': extra})

        # Data
        data = self.get_data(**kwargs)
        if data:
            self.detail['detail'].update({'data': data})

    def get_default_message(self, **kwargs):
        return self.default_message

    def get_extra_message(self, **kwargs):
        return self.extra_message

    def get_data(self, **kwargs):
        return self.data


class VerifAPIException(VerifBaseException):
    code = 'V000'


class IMEIIsRequired(VerifBaseException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('IMEI param is required')
    code = 'V001'


class VerifierDoesNotExist(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Verifier device for IMEI {imei} does not exist!')
    code = 'V002'


class VerifierIMEIError(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Verifier IMEI value error: {mess}!')
    code = 'V002.1'


class ScanCodeValueRequired(VerifBaseException):
    default_detail = _('Scan "code" param is required')
    code = 'V003'


class ScanCodeBaseException(VerifBaseException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_message = {
        'title': _('upss... unknown code'),
        'body': _('only codes from loyalty card\n'
                  'or vouchers are valid.\n'
                  'Try again or tap "exit" button.')
    }
    code = 'V004'


class ScanCodeValueError(ScanCodeBaseException):
    default_detail = _('Wrong format for scan "code"!')
    code = 'V005'


class ScanCodeTypeUnrecognized(ScanCodeBaseException):
    default_detail = _('Unrecognized scan code-type ({code_type})!')
    code = 'V006'


class CardIsNotEAN13(ScanCodeBaseException):
    default_detail = _('Code {code_value} for {code_type} is not EAN13!')
    code = 'V007'


class CardDoesNotExist(ScanCodeBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Card for code {code_value} does not exist!')
    code = 'V008'


class CouponDoesNotExist(ScanCodeBaseException):
    default_detail = _('Coupon for code {code_value} does not exist!')
    code = 'V009'


class CouponFromAnotherStore(ScanCodeBaseException):
    default_detail = _('Coupon for code {code_value} is assigned to another store!')
    code = 'V010'

    def get_default_message(self, **kwargs):
        return {
            'title': _('ups..\nwrong store'),
            'body': _('Your voucher is valid only\n'
                      'at {shop_name}\n'
                      'Scan your loyalty card\n'
                      'or right voucher.').format(**kwargs)
        }

    def get_data(self, **kwargs):
        shop = kwargs.get('shop')
        coupon = kwargs.get('coupon')
        return {
            'shop': shop,
            'coupon': coupon
        }


class CouponRebateIsUsed(ScanCodeBaseException):
    default_detail = _('Coupon for code {code_value} was used!')
    default_message = {
        'title': _('Voucher not longer valid'),
        'body': _('Find new vouchers at Rabatomat.')
    }
    code = 'V011'

    def get_extra_message(self, **kwargs):
        coupon = kwargs.get('coupon')
        created_at = kwargs.get('verification').get('created_at')
        # import locale
        # locale.setlocale(locale.LC_TIME, 'pl_PL.UTF-8')
        # coupon['used_date'] = unicode(timestamp.strftime("%a, %Y %B %d, %H:%M"), 'utf-8')
        created_at = datetime.datetime.strptime(created_at, '%Y-%m-%dT%H:%M:%S.%f')
        coupon['used_date'] = unicode(created_at.strftime("%Y-%m-%d %H:%M"), 'utf-8')
        return {
            'title': _('Welcome back!'),
            'body': _('This voucher was used at\n'
                      '{used_date}\n'
                      'and is no longer valid.').format(**coupon)
        }

    def get_data(self, **kwargs):
        return {
            'coupon': kwargs.get('coupon')
        }


class CouponRebateIsInvalid(ScanCodeBaseException):
    default_detail = _('Coupon for code {code_value} is invalid!')
    default_message = {
        'title': _('Voucher not longer valid'),
        'body': _('Find new vouchers at Rabatomat.')
    }
    code = 'V012'

    def get_extra_message(self, **kwargs):
        rebate = kwargs.get('coupon').get('rebate')
        valid_date_to = rebate.get('valid_date_to')
        now = datetime.datetime.now()
        rebate.update({
            'verb': 'was' if now > valid_date_to else 'is'
        })
        return {
            'title': _('Welcome back!'),
            'body': _('This voucher {verb} valid\n'
                      'from {valid_date_from} to {valid_date_to}\n').format(**rebate)
        }

    def get_data(self, **kwargs):
        coupon = kwargs.get('coupon')
        valid_date_to = coupon.get('rebate').get('valid_date_to')
        now = datetime.datetime.now()
        return {
            'verb': 'was' if now > valid_date_to else 'is',
            'coupon': coupon,
        }


class SellerDoesNotExist(ScanCodeBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Seller for code {code_value} does not exist!')
    code = 'V013'


class SellerFromAnotherStore(ScanCodeBaseException):
    default_detail = _('Seller for code {code_value} is assigned to another store!')
    code = 'V014'

    def get_data(self, **kwargs):
        return {
            'shop': kwargs.get('shop')
        }


class UpsellingDoesNotExist(ScanCodeBaseException):
    default_detail = _('Upselling does not exist ({upselling_id})!')
    code = 'V015'


# =================== Exceptions for Verification ==========================
class VerificationScanIdRequired(VerifBaseException):
    default_detail = _('Scan ID is required!')
    code = 'V015'


class VerificationScanDoesNotExist(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Scan for id={scan_id} does not exist!')
    code = 'V016'


class VerificationAmountIsRequired(VerifBaseException):
    default_detail = _('Amount is required if verification_type="card_amount" !')
    code = 'V017'


class CouponHasAlreadyBeenVerified(VerifBaseException):
    default_detail = _('Coupon has already been verified: #{verification_id}')
    code = 'V018'

    def get_data(self, **kwargs):
        return {
            'verification': kwargs.get('verification')
        }


class VerificationSellerDoesNotExist(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Seller for shop_id={shop_id} and seller_id={seller_id} does not exist')
    code = 'V019'


class VerificationUpsellingDoesNotExist(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Upselling for id={upselling_id} does not exist!')
    code = 'V020'


class UpsellingHasAlreadyBeenVerified(VerifBaseException):
    default_detail = _('Upselling has already been verified #{verification_id}')
    code = 'V021'

    def get_data(self, **kwargs):
        return {
            'verification': kwargs.get('verification')
        }


class VerificationSellerIdRequired(VerifBaseException):
    default_detail = _('SellerID is required if verification_type="card_amount" !')
    code = 'V021'


class VerificationByCardNotImplementedYet(VerifBaseException):
    default_detail = _('Verification by card is not implemented yet!')
    code = 'V022'


class VerifierAuthError(VerifBaseException):
    default_detail = _('Master code auth error!')
    code = 'V023'


class VerifierAuthCodeError(VerifBaseException):
    default_detail = _('Master code empty or wrong!')
    code = 'V024'


class VerifierSellerCodeError(VerifBaseException):
    default_detail = _('Seller code empty or wrong!')
    code = 'V025'


class VerifierIsDisabled(VerifBaseException):
    default_detail = _('Verifier is disabled!')
    code = 'V026'


class VerifierDoesNotExistForThisScan(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Verifier device does not exist for scan_id "{scan_id}"!')
    code = 'V0027'


class CardDoesNotExistForThisScan(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Member card does not exist for scan_id "{scan_id}"!')
    code = 'V0027'


class SellerIdIsRequired(VerifBaseException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Seller id is required for purchases registration on this device!')
    code = 'V0028'


class PurchaseHasAlreadyBeenVerified(VerifBaseException):
    default_detail = _('Purchase has already been verified')
    code = 'V029'

    def get_data(self, **kwargs):
        return {
            'verification': kwargs.get('verification')
        }


class UpsellingCouponCreatingError(VerifBaseException):
    default_detail = _('Upselling coupon creating error')
    code = 'V030'

    def get_data(self, **kwargs):
        return {
            'verification': kwargs.get('verification')
        }


class AmountIsRequired(VerifBaseException):
    default_detail = _('Amount is required')
    code = 'V031'


class SellerScanRequiresScanId(ScanCodeBaseException):
    default_detail = _('Seller card requires scan id!')
    code = 'V032'


class SellerParentScanHasSeller(ScanCodeBaseException):
    default_detail = _('Seller parent scan has seller!')
    code = 'V033'


class PurchaseRegisterLimitsException(ScanCodeBaseException):
    code = 'V044'
    default_detail = '{mess}'
