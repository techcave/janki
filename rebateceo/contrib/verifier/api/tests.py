from rest_framework.reverse import reverse_lazy
from ...api.tests import APITestBase
from .. import factories


class VerifierBaseTest(APITestBase):
    def setUp(self):
        super(VerifierBaseTest, self).setUp()


class VerifierTest(VerifierBaseTest):
    """
    bin/backend-test test rebateceo.contrib.verifier.api.tests.VerifierTest
    bin/py.test -v rebateceo/contrib/rebate --nomigrations --reuse-db
    """
    def test_adv_list(self):
        """
        http://127.0.0.1:8000/api/verifier/adv/1234567890/list/

        [
            {
                "pk": 1,
                "banner": {
                    "pk": 3,
                    "section": 1,
                    "title": "Baner1",
                    "slug": "baner1",
                    "image": "http://127.0.0.1:8000/media/banners/baner1.jpg",
                    "publication_date": "2015-03-19T09:35:00",
                    "publication_end_date": null,
                    "active": true
                },
                "shop": 765
            }
        ]
        """

    def test_config(self):
        """
        http://127.0.0.1:8000/api/verifier/config/

        {
            "config": {
                "verifier": {
                    "conn_type": "GSM",
                    "user_action_timeout": 10,
                    "ping_timeout": 70,
                    "enabled_to_hour": "21:00",
                    "enabled_from_hour": "09:00",
                    "require_seller_card": false
                },
                "description": "To sa standardowe ustawienia dla wszystkich "
            }
        }
        """

    def test_config_for_verifier(self):
        """
        http://127.0.0.1:8000/api/verifier/1234567890/config/

        {
            "pk": 1,
            "shop": {
                "id": 765,
                "name": "4F",
                "floor": 1,
                "email": "info@otcf.pl",
                "www": "http://4f.com.pl/",
                "phone": "12 298-68-39",
                "logo_url": "http://dev-lomianki.techcave.pl/media/shop/4F_logo.JPG",
                "has_verifier": "n/a"
            },
            "imei": 1234567890,
            "created": "2015-05-26T14:05:48.682560",
            "modified": "2015-05-26T14:05:48.682584",
            "config": {
                "conn_type": "GSM",
                "user_action_timeout": "n/a",
                "ping_timeout": 1,
                "enabled_to_hour": "2",
                "enabled_from_hour": "1",
                "require_seller_card": false
            }
        }
        """

    def test_detail(self):
        """
        http://127.0.0.1:8000/api/verifier/1234567890/detail/

        {
            "pk": 1,
            "shop": {
                "id": 765,
                "name": "4F",
                "floor": 1,
                "email": "info@otcf.pl",
                "www": "http://4f.com.pl/",
                "phone": "12 298-68-39",
                "logo_url": "http://dev-lomianki.techcave.pl/media/shop/4F_logo.JPG",
                "has_verifier": "n/a"
            },
            "imei": 1234567890,
            "created": "2015-05-26T14:05:48.682560",
            "modified": "2015-05-26T14:05:48.682584",
            "config": {
                "conn_type": "GSM",
                "user_action_timeout": "n/a",
                "ping_timeout": 1,
                "enabled_to_hour": "2",
                "enabled_from_hour": "1",
                "require_seller_card": false
            }
        }
        """

    def test_list(self):
        """
        http://127.0.0.1:8000/api/verifier/list/

        [
            {
                "pk": 1,
                "shop": {
                    "id": 765,
                    "name": "4F",
                    "floor": 1,
                    "email": "info@otcf.pl",
                    "www": "http://4f.com.pl/",
                    "phone": "12 298-68-39",
                    "logo_url": "http://dev-lomianki.techcave.pl/media/shop/4F_logo.JPG",
                    "has_verifier": "n/a"
                },
                "imei": 1234567890,
                "created": "2015-05-26T14:05:48.682560",
                "modified": "2015-05-26T14:05:48.682584",
                "config": {
                    "conn_type": "GSM",
                    "user_action_timeout": "n/a",
                    "ping_timeout": 1,
                    "enabled_to_hour": "2",
                    "enabled_from_hour": "1",
                    "require_seller_card": false
                }
            }
        ]
        """

    def test_ping(self):
        """
        http://127.0.0.1:8000/api/verifier/ping/
        curl -X POST -d imei=1234567890 http://127.0.0.1:8000/api/verifier/ping/

        {
            "id":1,
            "pk":1,
            "version":null,
            "created":"2015-05-27T12:21:08.406405",
            "verifier":1
        }
        """

    def test_eventlog_create(self):
        """
        http://127.0.0.1:8000/api/verifier/eventlog/create/

        {
            "pk": 2,
            "created": "2015-05-27T12:21:55.012199"
        }
        """

    def test_verification(self):
        """

        http://127.0.0.1:8000/api/verifier/verification/

        """





        # [factories.VerifierFactory() for x in range(0, self.limit)]
        # response = self.client.get(reverse_lazy('verifier-list'), **self.headers)
        # self.assertEqual(len(response.data), self.limit)

    # def test_adv_list(self):
    #     imei = 123456789
    #     factories.VerifierFactory(imei=imei)
    #     [factories.AdvFactory() for x in range(0, self.limit)]
    #
    #     response = self.client.get(reverse('verifier-adv-list'), kwargs={
    #         'imei': imei
    #     }, **self.headers)
    #     self.assertEqual(len(response.data), self.limit)


# class VerifierConfigTest(APITestBase):
#     """
#     bin/backend-test test rebateceo.contrib.verifier.api.tests.VerifierConfigTest
#     """
#
#     def setUp(self):
#         super(VerifierConfigTest, self).setUp()
#
#     def test_config_view(self):
#         response = self.client.get(reverse('api:verifier:config'), {}, **self.headers)
#         self.assertEqual(response.status_code, 200)

    # def test_verifier_config_view(self):
    #     response = self.client.get(reverse('api:verifier:verifier-config'), {}, **self.headers)
    #     print response
    #     self.assertEqual(response.status_code, 201)


# class ConfigView(GenericAPIView):
#
#     def get_config(self, request, *args, **kwargs):
#         varifier_config, prefix = {}, 'verifier_'
#         for field in settings.CONSTANCE_CONFIG:
#             varifier_config.update({field.lower().replace(prefix, ''): getattr(config, field)})
#         return {
#             'config': {
#                 'description': _('This are defaults settings for all verifiers!'),
#                 'verifier': varifier_config
#             }
#         }
#
#     def get(self, request, *args, **kwargs):
#         return Response(self.get_config(request, *args, **kwargs))
#
#
# class VerifierConfigView(bases.VerifierBaseAPIView, RetrieveAPIView):
#     serializer_class = serializers.VerifierSerializer