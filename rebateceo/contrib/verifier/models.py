# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
import datetime
from constance import config
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings
from django_extensions.db.fields import UUIDField
from picklefield.fields import PickledObjectField
from rest_framework.authtoken.models import Token

QRCODE_STR = "{type};{uuid}"
QRCODE_IMG = getattr(settings, 'QRCODE_IMG',
                     "http://chart.apis.google.com/chart?cht=qr&chs={size}x{size}&chl={qrcode_str}")


class VerifierManager(models.Manager):
    def for_selfkiosk(self):
        qs = super(VerifierManager, self).get_queryset()
        return qs.exclude(deleted=True)

    def get_queryset(self):
        qs = super(VerifierManager, self).get_queryset()
        return qs.exclude(deleted=True)

    def active(self):
        qs = super(VerifierManager, self).get_queryset()
        return qs.filter(active=True)

    def skipped(self):
        qs = super(VerifierManager, self).get_queryset()
        return qs.filter(skip_lock=True)

    def for_shop(self, shop):
        return self.active().filter(shop=shop)


class Verifier(models.Model):
    STATUS_OFFLINE = 'OFFLINE'
    STATUS_READY = 'READY'
    OFFLINE_REASONS = {
        'TIMEOUT': _(u'Ostatni ping {last} sekund temu (wymagany jest co {timeout} sekund)'),
        'NO_PING': _(u'Urządzenie nie wysłalo jeszcze żadnego sygnału PING'),
        'DISABLED': _(u'Urządzenie wyłączone na poziomie panelu administracyjnego'),
    }
    _offline_reason = None
    # DB fields
    shop = models.ForeignKey('shop.Shop', related_name='verifiers', blank=True, null=True)
    imei = models.BigIntegerField(unique=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    skip_lock = models.BooleanField(verbose_name=_('Skip lock'), default=False)
    deleted = models.BooleanField(verbose_name=_('Mark as deleted'), default=False)
    active = models.BooleanField(verbose_name=_('Active'), default=False)
    description = models.TextField(blank=True, null=True)
    objects = VerifierManager()

    class Meta:
        verbose_name = _("Verifier")
        verbose_name_plural = _("Verifiers")
        ordering = ('-pk',)
        unique_together = ('shop', 'imei')

    def __unicode__(self):
        return u"%s %s" % (self.imei, unicode(self.shop))

    def _get_config(self, key):
        try:
            value = getattr(self.config, key)
        except Config.DoesNotExist:
            return getattr(config, 'VERIFIER_%s' % key.upper(), -1)
        else:
            return value

    def delete(self, using=None):
        self.deleted = True
        self.save()

    @property
    def last_ping(self):
        return self.pings.last()

    @property
    def last_event(self):
        return self.eventlogs.last()

    @property
    def status(self):
        # Check permanently active
        if not self.active:
            self.offline_reason = self.OFFLINE_REASONS['DISABLED']
            return self.STATUS_OFFLINE
        if not self.last_ping:
            self.offline_reason = self.OFFLINE_REASONS['NO_PING']
            return self.STATUS_OFFLINE

        diff = datetime.datetime.now() - self.last_ping.created
        timeout = 60 #config.VERIFIER_PING_TIMEOUT
        if diff.total_seconds() > timeout:
            self.offline_reason = self.OFFLINE_REASONS['TIMEOUT'].format(last=int(diff.total_seconds()), timeout=timeout)
            return self.STATUS_OFFLINE
        else:
            return self.STATUS_READY

    @property
    def require_seller_card(self):
        return self._get_config('require_seller_card')

    @property
    def ready(self):
        return self.status == self.STATUS_READY

    @property
    def offline_reason(self):
        return self._offline_reason

    @offline_reason.setter
    def offline_reason(self, value):
        self._offline_reason = value


class Config(models.Model):
    DEFAULT_HELP_TEXT = 'WARNING! This field overrides global conf for verifiers'

    CONN_GSM = 'GSM'
    CONN_WIFI = 'WIFI'
    CONN_TYPE = (
        (CONN_GSM, CONN_GSM),
        (CONN_WIFI, CONN_WIFI),
    )

    verifier = models.OneToOneField(Verifier, primary_key=True)
    ping_timeout = models.IntegerField(blank=True, null=True, help_text=DEFAULT_HELP_TEXT)
    user_action_timeout = models.IntegerField(default=10, blank=True, null=True, help_text=DEFAULT_HELP_TEXT)
    conn_type = models.CharField(choices=CONN_TYPE, max_length=4, blank=True, null=True, help_text=DEFAULT_HELP_TEXT)
    enabled_from_hour = models.CharField(max_length=5, blank=True, null=True, help_text=DEFAULT_HELP_TEXT + ' E.g: 09:00')
    enabled_to_hour = models.CharField(max_length=5, blank=True, null=True, help_text=DEFAULT_HELP_TEXT + ' E.g: 21:00')
    require_seller_card = models.BooleanField(verbose_name=_('Purchase register requires '
                                                             'seller card confirmation'), default=False)

    class Meta:
        verbose_name = _("Config")
        verbose_name_plural = _("Configs")
        ordering = ('-pk',)

    def __unicode__(self):
        return "Config for %s" % (self.verifier,)


class ConfigDate(models.Model):
    from_time = models.DateTimeField(verbose_name=_('from time'))
    to_time = models.DateTimeField(verbose_name=_('to_time'))
    description = models.TextField()

    class Meta:
        verbose_name = _("Config date")
        verbose_name_plural = _("Config dates")
        ordering = ('from_time',)
        unique_together = ('from_time', 'to_time')

    def __unicode__(self):
        return "%s - %s from %s to %s" % (self.get_date, self.description, self.get_from_time, self.get_to_time)

    def clean(self):
        if self.from_time.date() != self.to_time.date():
            raise ValidationError(_(u'Dzień, miesiąc i rok muszą być w obydwu polach jednakowe!'))

        if self.from_time >= self.to_time:
            raise ValidationError(_(u'Data początkowa musi być mniejsza od daty końcowej'))

        diff = self.to_time - self.from_time
        hours = int(diff.total_seconds() / 3600)
        if hours == 0:
            raise ValidationError(_(u'Między podanymi datami musi być minimum godzina odstępu'))

    @property
    def get_date(self):
        return self.from_time.date()

    @property
    def get_from_time(self):
        return self.from_time.strftime("%H:%M")

    @property
    def get_to_time(self):
        return self.to_time.strftime("%H:%M")


class Ping(models.Model):
    verifier = models.ForeignKey(Verifier, related_name='pings')
    version = models.CharField(max_length=255, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Ping")
        verbose_name_plural = _("Pings")

    def __unicode__(self):
        return "%s | %s | %s" % (self.verifier, self.created, self.version or 'b/d')


class EventLog(models.Model):
    verifier = models.ForeignKey(Verifier, related_name='eventlogs')
    details = PickledObjectField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Event Log")
        verbose_name_plural = _("Event Logs")
        ordering = ('-pk',)

    def __unicode__(self):
        return "%s" % self.created


class VerifierScan(models.Model):
    verifier = models.ForeignKey('verifier.Verifier', related_name='verifier_scans', blank=True, null=True)
    seller = models.ForeignKey('shop.Seller', related_name='verifier_sellers', blank=True, null=True)
    card = models.ForeignKey('card.Card', related_name='verifier_cards', blank=True, null=True)
    coupon = models.ForeignKey('rebate.Coupon', related_name='verifier_coupons', blank=True, null=True)
    mastercode = models.ForeignKey('verifier.MasterCode', related_name='verifier_mastercodes', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), blank=True, null=True, auto_now=True)
    status = models.BooleanField(verbose_name=_('Status'), default=False)
    details = models.TextField(verbose_name=_('Details'), blank=True, null=True)
    request = PickledObjectField(verbose_name=_('Request'), blank=True, null=True)
    response = PickledObjectField(verbose_name=_('Response'), blank=True, null=True)

    class Meta:
        verbose_name = _("VerifierScan")
        verbose_name_plural = _("VerifierScans")
        ordering = ['-created_at']

    def __unicode__(self):
        return u"#%s [%s] %s" % (self.pk, unicode(self.verifier), self.created_at)


class Verification(models.Model):
    NORMAL = 'normal'
    SELLER = 'seller'

    scan = models.ForeignKey('verifier.VerifierScan', related_name='verifications', blank=True, null=True)
    seller = models.ForeignKey('shop.Seller', related_name='verifications', blank=True, null=True)
    purchase = models.ForeignKey('purchase.Purchase', related_name='verifications', blank=True, null=True)
    coupon = models.ForeignKey('rebate.Coupon', related_name='verifications', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Updated at'), auto_now=True)
    status = models.BooleanField(verbose_name=_('Status'), default=False)
    details = models.TextField(verbose_name=_('Details'), blank=True, null=True)
    request = PickledObjectField(verbose_name=_('Request'), blank=True, null=True)
    response = PickledObjectField(verbose_name=_('Response'), blank=True, null=True)

    def __unicode__(self):
        return u"%s, %s, %s" % (self.pk, self.scan.pk if self.scan else 'b.d', self.seller)

    class Meta:
        verbose_name = _("Verification")
        verbose_name_plural = _("Verifications")
        ordering = ('-pk',)

    @property
    def type(self):
        return self.SELLER if self.seller else self.NORMAL


class Adv(models.Model):
    shop = models.ForeignKey('shop.Shop')
    banner = models.ForeignKey('advert.Banner')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"%s %s" % (self.shop, self.banner)

    class Meta:
        verbose_name = _("Adv")
        verbose_name_plural = _("Advs")
        ordering = ('-pk',)


class MasterCodeManager(models.Manager):
    def get_queryset(self):
        qs = super(MasterCodeManager, self).get_queryset()
        return qs.exclude(deleted_at__isnull=False)


class MasterCode(models.Model):
    QRCODE_TYPE = 'mastercode'
    QRCODE_SIZE = 300

    authtoken = models.OneToOneField(Token)
    uuid = UUIDField(db_index=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    deleted_at = models.DateTimeField(help_text=_('Mark as deleted'), blank=True, null=True)
    objects = MasterCodeManager()

    @property
    def qrcode_str(self):
        return QRCODE_STR.format(type=self.QRCODE_TYPE, uuid=self.uuid)

    @property
    def qrcode_img(self):
        return QRCODE_IMG.format(size=self.QRCODE_SIZE, qrcode_str=self.qrcode_str)

    def __unicode__(self):
        return "%s" % (self.authtoken.user,)

    def delete(self, using=None):
        self.deleted_at = datetime.datetime.now()
        self.save()

    class Meta:
        verbose_name = _('Master code')
        verbose_name_plural = _('Master codes')