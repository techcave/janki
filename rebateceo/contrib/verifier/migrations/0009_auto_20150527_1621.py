# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0008_amount'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='amount',
            name='card',
        ),
        migrations.RemoveField(
            model_name='amount',
            name='program',
        ),
        migrations.RemoveField(
            model_name='amount',
            name='scan',
        ),
        migrations.RemoveField(
            model_name='amount',
            name='seller',
        ),
        migrations.DeleteModel(
            name='Amount',
        ),
    ]
