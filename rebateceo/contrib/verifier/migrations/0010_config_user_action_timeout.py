# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0009_auto_20150527_1621'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='user_action_timeout',
            field=models.IntegerField(default=10, help_text=b'WARNING! This field overrides global conf for verifiers', null=True, blank=True),
            preserve_default=True,
        ),
    ]
