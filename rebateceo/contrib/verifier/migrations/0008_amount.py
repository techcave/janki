# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0002_auto_20150525_0952'),
        ('rebate', '0021_auto_20150527_1604'),
        ('shop', '0028_auto_20150527_1253'),
        ('verifier', '0007_auto_20150527_1539'),
    ]

    operations = [
        migrations.CreateModel(
            name='Amount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('card', models.ForeignKey(related_name='amounts', verbose_name='Karta', to='card.Card')),
                ('program', models.ForeignKey(related_name='amounts', verbose_name='Program', to='rebate.Program')),
                ('scan', models.ForeignKey(related_name='amounts', verbose_name='Skan', to='verifier.VerifierScan')),
                ('seller', models.ForeignKey(related_name='amounts', verbose_name='Sprzedawca', to='shop.Seller')),
            ],
            options={
                'ordering': ('pk',),
                'verbose_name': 'Amount',
                'verbose_name_plural': 'Amounts',
            },
            bases=(models.Model,),
        ),
    ]
