# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('authtoken', '0001_initial'),
        ('profile', '0016_auto_20150519_1023'),
        ('card', '0002_auto_20150525_0952'),
        ('shop', '0027_auto_20150519_1023'),
        ('purchase', '0005_auto_20150413_1638'),
        ('advert', '0007_auto_20150522_1201'),
        ('rebate', '0019_coupon_reservation_date_to'),
    ]

    operations = [
        migrations.CreateModel(
            name='Advert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('banner', models.ForeignKey(to='advert.Banner')),
                ('shop', models.ForeignKey(to='shop.Shop')),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Advert',
                'verbose_name_plural': 'Adverts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConfigDate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('from_time', models.DateTimeField(verbose_name='from time')),
                ('to_time', models.DateTimeField(verbose_name='to_time')),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ('from_time',),
                'verbose_name': 'Config date',
                'verbose_name_plural': 'Config dates',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('details', picklefield.fields.PickledObjectField(null=True, editable=False, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Event Log',
                'verbose_name_plural': 'Event Logs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MasterCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', django_extensions.db.fields.UUIDField(db_index=True, unique=True, editable=False, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('deleted_at', models.DateTimeField(help_text='Mark as deleted', null=True, blank=True)),
                ('authtoken', models.OneToOneField(to='authtoken.Token')),
            ],
            options={
                'verbose_name': 'Master code',
                'verbose_name_plural': 'Master codes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version', models.CharField(max_length=255, null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Ping',
                'verbose_name_plural': 'Pings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Verification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Updated')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
                ('details', models.TextField(null=True, verbose_name='Details', blank=True)),
                ('request', picklefield.fields.PickledObjectField(verbose_name='Request', null=True, editable=False, blank=True)),
                ('response', picklefield.fields.PickledObjectField(verbose_name='Response', null=True, editable=False, blank=True)),
                ('coupon', models.ForeignKey(related_name='verifications', blank=True, to='rebate.Coupon', null=True)),
                ('purchase', models.ForeignKey(related_name='verifications', blank=True, to='purchase.Purchase', null=True)),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Verification',
                'verbose_name_plural': 'Verifications',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Verifier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imei', models.BigIntegerField(unique=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('skip_lock', models.BooleanField(default=False, verbose_name='Skip lock')),
                ('deleted', models.BooleanField(default=False, verbose_name='Mark as deleted')),
                ('active', models.BooleanField(default=False, verbose_name='Active')),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Verifier',
                'verbose_name_plural': 'Verifiers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Config',
            fields=[
                ('verifier', models.OneToOneField(primary_key=True, serialize=False, to='verifier.Verifier')),
                ('ping_timeout', models.IntegerField(help_text=b'WARNING! This field overrides global conf for verifiers', null=True, blank=True)),
                ('conn_type', models.CharField(blank=True, max_length=4, null=True, help_text=b'WARNING! This field overrides global conf for verifiers', choices=[(b'GSM', b'GSM'), (b'WIFI', b'WIFI')])),
                ('enabled_from_hour', models.CharField(help_text=b'WARNING! This field overrides global conf for verifiers E.g: 09:00', max_length=5, null=True, blank=True)),
                ('enabled_to_hour', models.CharField(help_text=b'WARNING! This field overrides global conf for verifiers E.g: 21:00', max_length=5, null=True, blank=True)),
                ('require_seller_card', models.BooleanField(default=False, verbose_name='Purchase register requires seller card confirmation')),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Config',
                'verbose_name_plural': 'Configs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VerifierScan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Updated', null=True)),
                ('timestamp', models.CharField(max_length=32, verbose_name='Timestamp')),
                ('status', models.BooleanField(default=False, verbose_name='Status')),
                ('details', models.TextField(null=True, verbose_name='Details', blank=True)),
                ('request', picklefield.fields.PickledObjectField(verbose_name='Request', null=True, editable=False, blank=True)),
                ('response', picklefield.fields.PickledObjectField(verbose_name='Response', null=True, editable=False, blank=True)),
                ('card', models.ForeignKey(related_name='verifier_cards', blank=True, to='card.Card', null=True)),
                ('coupon', models.ForeignKey(related_name='verifier_coupons', blank=True, to='rebate.Coupon', null=True)),
                ('mastercode', models.ForeignKey(related_name='verifier_mastercodes', blank=True, to='verifier.MasterCode', null=True)),
                ('seller', models.ForeignKey(related_name='verifier_sellers', blank=True, to='shop.Seller', null=True)),
                ('session', models.ForeignKey(related_name='verifier_sessions', blank=True, to='profile.Session', null=True)),
                ('verifier', models.ForeignKey(related_name='verifier_scans', blank=True, to='verifier.Verifier', null=True)),
            ],
            options={
                'ordering': ['-created'],
                'verbose_name': 'VerifierScan',
                'verbose_name_plural': 'VerifierScans',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='verifier',
            name='shop',
            field=models.ForeignKey(related_name='verifiers', blank=True, to='shop.Shop', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='verifier',
            unique_together=set([('shop', 'imei')]),
        ),
        migrations.AddField(
            model_name='verification',
            name='scan',
            field=models.ForeignKey(related_name='verifications', blank=True, to='verifier.VerifierScan', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='verification',
            name='seller',
            field=models.ForeignKey(related_name='verifications', blank=True, to='shop.Seller', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ping',
            name='verifier',
            field=models.ForeignKey(related_name='pings', to='verifier.Verifier'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='eventlog',
            name='verifier',
            field=models.ForeignKey(related_name='eventlogs', to='verifier.Verifier'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='configdate',
            unique_together=set([('from_time', 'to_time')]),
        ),
    ]
