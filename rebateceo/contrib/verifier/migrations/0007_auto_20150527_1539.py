# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0006_verifierscan_card'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='verifierscan',
            options={'ordering': ['-created_at'], 'verbose_name': 'VerifierScan', 'verbose_name_plural': 'VerifierScans'},
        ),
        migrations.RemoveField(
            model_name='verification',
            name='created',
        ),
        migrations.RemoveField(
            model_name='verification',
            name='updated',
        ),
        migrations.RemoveField(
            model_name='verifierscan',
            name='created',
        ),
        migrations.RemoveField(
            model_name='verifierscan',
            name='timestamp',
        ),
        migrations.RemoveField(
            model_name='verifierscan',
            name='updated',
        ),
        migrations.AddField(
            model_name='verification',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 27, 15, 39, 12, 335310), verbose_name='Created at', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='verification',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 27, 15, 39, 17, 951357), verbose_name='Updated at', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='verifierscan',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 27, 15, 39, 27, 391222), verbose_name='Created at', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='verifierscan',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated at', null=True),
            preserve_default=True,
        ),
    ]
