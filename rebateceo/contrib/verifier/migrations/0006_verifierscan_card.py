# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0002_auto_20150525_0952'),
        ('verifier', '0005_auto_20150527_1309'),
    ]

    operations = [
        migrations.AddField(
            model_name='verifierscan',
            name='card',
            field=models.ForeignKey(related_name='verifier_cards', blank=True, to='card.Card', null=True),
            preserve_default=True,
        ),
    ]
