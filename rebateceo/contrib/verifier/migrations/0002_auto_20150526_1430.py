# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advert', '0007_auto_20150522_1201'),
        ('shop', '0027_auto_20150519_1023'),
        ('verifier', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Adv',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('banner', models.ForeignKey(to='advert.Banner')),
                ('shop', models.ForeignKey(to='shop.Shop')),
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Adv',
                'verbose_name_plural': 'Advs',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='advert',
            name='banner',
        ),
        migrations.RemoveField(
            model_name='advert',
            name='shop',
        ),
        migrations.DeleteModel(
            name='Advert',
        ),
    ]
