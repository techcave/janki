# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0003_auto_20150527_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verifierscan',
            name='card',
            field=models.ForeignKey(related_name='verifier_cards', blank=True, to='card.Card', null=True),
            preserve_default=True,
        ),
    ]
