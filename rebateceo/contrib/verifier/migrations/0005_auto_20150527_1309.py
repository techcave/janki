# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0004_auto_20150527_1256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='verifierscan',
            name='card',
        ),
        migrations.RemoveField(
            model_name='verifierscan',
            name='session',
        ),
    ]
