# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('verifier', '0002_auto_20150526_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verifierscan',
            name='card',
            field=models.ForeignKey(related_name='verifier_cards', blank=True, to='profile.Session', null=True),
            preserve_default=True,
        ),
    ]
