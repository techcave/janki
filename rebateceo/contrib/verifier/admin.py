# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.contrib.admin import ModelAdmin, site
from . import models


class VerifierAdmin(ModelAdmin):
    list_display = ['imei', 'shop', 'active', 'skip_lock', 'get_ready', 'get_status', 'get_offline_reason',
                    'get_last_ping', 'get_version', 'last_event_data', 'created', 'modified', 'config']

    def last_event_data(self, instance):
        return '{created} / {details}'.format(created=instance.last_event.created,
                                              details=instance.last_event.details or 'b/d') \
            if instance.last_event else 'b/d'

    last_event_data.short_description = _('Last event')
    last_event_data.allow_tags = True

    def get_ready(self, instance):
        return '<img alt="True" src="/static/admin/img/icon-%s.gif">' % ('yes' if instance.ready else 'no')

    get_ready.short_description = _('Ready')
    get_ready.allow_tags = True

    def get_status(self, instance):
        return '<span style="color: %s">%s</span>' % ('green' if instance.ready else 'red', instance.status)

    get_status.short_description = _('Status')
    get_status.allow_tags = True

    def get_last_ping(self, instance):
        return instance.last_ping.created if instance.last_ping else 'b/d'

    get_last_ping.short_description = _('Last ping')
    get_last_ping.allow_tags = True

    def get_version(self, instance):
        return instance.last_ping.version if instance.last_ping else 'b/d'

    get_version.short_description = _('soft. version')
    get_version.allow_tags = True

    def get_offline_reason(self, instance):
        return instance.offline_reason

    get_offline_reason.short_description = _('Offline reason')
    get_offline_reason.allow_tags = True


class VerifierScanAdmin(ModelAdmin):
    list_display = ('pk', 'verifier', 'coupon', 'card', 'seller', 'mastercode', 'created_at', 'updated_at', 'status',
                    'details', 'request', 'response')
    list_filter = ('verifier', 'created_at', 'status',)


class VerificationAdmin(ModelAdmin):
    list_display = ('pk', 'scan', 'seller', 'purchase', 'coupon', 'created_at',
                    'updated_at', 'status', 'details', 'request', 'response')
    list_filter = ('scan', 'created_at', 'status',)


class AdvAdmin(ModelAdmin):
    list_display = ['created', 'id', 'shop', 'banner_image']

    def banner_image(self, record):
        return mark_safe(
            "<img src='{url}' width='200' />".format(url=record.banner.image.url)) if record.banner.image else '-'


class PingAdmin(ModelAdmin):
    list_display = ['pk', 'verifier', 'version', 'created']


class ConfigAdmin(ModelAdmin):
    list_display = ['verifier', 'ping_timeout', 'conn_type', 'enabled_from_hour', 'enabled_to_hour',
                    'require_seller_card']
    list_editable = ['require_seller_card']


class ConfigDateAdmin(ModelAdmin):
    list_display = ['get_date', 'get_from_time', 'get_to_time', 'description', 'is_today']

    def is_today(self, obj):
        is_today = datetime.now().today().date() == obj.from_time.date()
        return '<img alt="True" src="/static/admin/img/icon-yes.gif">' if is_today else ''

    is_today.allow_tags = True


class EventLogAdmin(ModelAdmin):
    list_display = ['verifier', 'created', 'details']


class MasterCodeAdmin(ModelAdmin):
    list_display = ['authtoken', 'uuid', 'created_at', 'user', 'qrcode']
    readonly_fields = ['deleted_at']

    def user(self, obj):
        return obj.authtoken.user

    def qrcode(self, obj):
        return '<a href="{src}" target="_blank"><img src="{src}" /></a><br><input value="{qrcode_str}"/>'. \
            format(src=obj.qrcode_img, qrcode_str=obj.qrcode_str)

    qrcode.allow_tags = True


site.register(models.MasterCode, MasterCodeAdmin)
site.register(models.EventLog, EventLogAdmin)
site.register(models.ConfigDate, ConfigDateAdmin)
site.register(models.Config, ConfigAdmin)
site.register(models.Ping, PingAdmin)
site.register(models.Adv, AdvAdmin)
site.register(models.Verification, VerificationAdmin)
site.register(models.Verifier, VerifierAdmin)
site.register(models.VerifierScan, VerifierScanAdmin)
