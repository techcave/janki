��    T      �  q   \            !     :     A     P     W  7   _  2   �     �  *   �  /   �     ,     3     ?     L  ,   T  :   �  (   �  4   �     	     "	     )	     1	  	   6	  
   @	     K	     k	     �	     �	     �	     �	     �	     �	  3   �	     
  3   '
     [
     a
     i
     r
     w
     �
  %   �
     �
  ,   �
  :     @   T  9   �     �  	   �     �  N   �  -   L  <   z  D   �  	   �  *        1     9  *   Y  /   �  6   �  2   �       ,   +     X     f  "   o  /   �     �     �     �  	   �     �          $     B  q   _  R   �  	   $  S   .     �     �     �  �  �     W     v     ~     �     �  <   �  3   �       3     9   Q     �     �     �     �  )   �  =   �  +   +  4   W  	   �     �     �     �  	   �  	   �  $   �     �            
   !  "   ,  %   O     u  8   �  	   �  9   �     �  	     
             !     :  2   Z  *   �  �   �  D   J  D   �  ?   �       !     +   <  U   h  -   �  @   �  A   -     o  &   x     �  "   �  &   �  +   �  7   $  8   \     �  8   �     �     �     �  3        <     Y  
   ^     i  "   v     �     �  #   �  _   �  d   F     �  �   �     5     8     L     $   '                  >       C   -       %          7           :       2   M   ;   6   P   G                    E                  0   (             
           !   8   B   	      )   T   .      +      F   &   I   "   1             <                 *   R       3       K   Q   D         #      =       L       S   N   ?              H      @   J   9         /      4   ,                 O         A       5           Accountant code is valid Active Add new device Advert Adverts Amount is required if verification_type="card_amount" ! Are you sure, that you want to delete this record? Cancel Card for code {code_value} does not exist! Code {code_value} for {code_type} is not EAN13! Config Config date Config dates Configs Coupon for code {code_value} does not exist! Coupon for code {code_value} is assigned to another store! Coupon for code {code_value} is invalid! Coupon has already been verified: #{verification_id} Created Delete Details Edit Event Log Event Logs Find new vouchers at Rabatomat. IMEI param is required Imei Mark as deleted Master code Master code auth error! Master code empty or wrong! Master codes Member card does not exist for scan_id "{scan_id}"! Offline reason Purchase register requires seller card confirmation Ready Request Response Save Scan "code" param is required Scan ID is required! Scan for id={scan_id} does not exist! Seller code empty or wrong! Seller for code {code_value} does not exist! Seller for code {code_value} is assigned to another store! Seller id is required for purchases registration on this device! SellerID is required if verification_type="card_amount" ! Shop Skip lock Thank You for using our system The more rebate vouchers you use the
better discounts offers you will receive. This are defaults settings for all verifiers! This voucher was used at
{used_date}
and is no longer valid. This voucher {verb} valid
from {valid_date_from} to {valid_date_to}
 Timestamp Unrecognized scan code-type ({code_type})! Updated Upselling coupon creating error Upselling does not exist ({upselling_id})! Upselling for id={upselling_id} does not exist! Upselling has already been verified #{verification_id} Upselling has been saved as as new verified coupon Verification Verification by card is not implemented yet! Verifications Verifier Verifier IMEI value error: {mess}! Verifier device for IMEI {imei} does not exist! Verifier is disabled! VerifierScan VerifierScans Verifiers Voucher not longer valid Welcome back! Wrong format for scan "code"! You have now {points} points You purchase with rebate voucher gives you another
chance to win 1000 PLN in the drawing at the end of the month. Your voucher is valid only
at {shop_name}
Scan your loyalty card
or right voucher. from time only codes from loyalty card
or vouchers are valid.
Try again or tap "exit" button. to_time ups..
wrong store upss... unknown code Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-22 12:19+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Kod użytkownika jest poprawny Aktywny Dodaj nowe urządzenie Reklama Reklama Kwota jest wymagana jeśli verification_type="card_amount" ! Czy jesteś pewien, że chcesz usunąć ten rekord? Anuluj Karta dla takiego kodu '{code_value}' nie istnieje! Kod '{code_value}' dla typu '{code_type}' nie jest EAN13! Konfguracja Konfguracja dat Konfguracje dat Konfguracje Kupon o kodzie {code_value} nie istnieje! Kupon o kodzie {code_value} jest przypisany do innego sklepu! Kod kuponu '{code_value}' jest niepoprawny! Kupon został już zweryfikowany: #{verification_id} Utworzono Usuń Szczegóły Edytuj Zdarzenie Zdarzenia Szukaj nowych Kuponów w Rabatomacie Parametr 'imei' jest wymagany Imei Oznacz jako usunięte Kod master Bład autoryzacji kodu typu master Kod master jest pusty lub niepoprawny Kody master Karta członkowska nie istnieje dla scan_id "{scan_id}"! Przyczyna Rejestracja zakupu wymaga potwierdzenia kartą sprzedawcy Gotowy Zapytanie Odpowiedź Zapisz Wymagany parametr 'code' Parametr scan_id jest wymagany! Obiekt skanu dla wartości {scan_id} nie istnieje! Kod sprzedawcy jest pusty lub niepoprawny! Nie znaleziono konta sprzedawcy dla podanego kodu '{code_value}'. Konto nie istnieje, jest nieaktywne lub nie jest przypisane do żadnego sklepu! Ta karta sprzedawcy '{code_value}' przypisana jest do innego sklepu! Id sprzedawcy jest wymagane do rejestracji zakupu na tym urządzeniu SellerID jest wymagany jeśli verification_type="card_amount" ! Sklep Omiń blokadę rejestracji zakupu Dziękujemy za użytwkoanie naszego systemu Im więcej voucherów wykorzystujesz 
tym lepsze będą oferty wyprzedaży dla Ciebie To są standardowe ustawienia dla wszystkich  Ten Kupon został użyty
{used_date}
i nie jest dłużej ważny. Ten kupon {verb} aktywny
od {valid_date_from} do {valid_date_to}
 Znacznik Nierozpoznano typu kodu ({code_type})! Zaktualizowano Błąd utworzenia kuponu upselling Upselling {upselling_id} nie istnieje! Upselling o id={upselling_id} nie istnieje! Upselling został już zweryfikowany #{verification_id} Upselling został zapisany jako nowy zweryfikowany kupon Weryfikacja Veryfikacja kartą nie została jeszcze zaimplementowana Weryfikacje Weryfikator Błędny imei: {mess} Nie znaleziono urządzenia do podanego imei {imei}! Weryfikator jest wyłączony Skan Skanowania Weryfikatoru Ważność tego Kuponu upłynęła Witaj ponownie! Niepoprawny format kodu! Posiadasz obecnie {points} punktów Zakup z voucherem daje Ci kolejną 
szansę na wygranie 1000 PLN w loteri pod koniec miesiąca. Twój Kupon jest ważny tylko
w sklepie {shop_name}
Skanuj swoją Kartę Klubu lub
właściwy Kupon. od Akceptujemy tylko kody z Kart Klubu Galerii Echo
oraz z Kuponów Rabatowych wydrukowanych z Rabatomatu.
Spróbuj ponownie lub Zakończ do Niewłaściwy Kupon Nieznany kod 