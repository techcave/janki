from django.http import HttpResponse


def xml(request):
    """ Render a default, permissive, crossdomain.xml. """
    crossdomain_str = """<?xml version="1.0"?>
<!DOCTYPE cross-domain-policy SYSTEM "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">
<cross-domain-policy>
 <site-control permitted-cross-domain-policies="all"/>
 <allow-http-request-headers-from domain="*" headers="*" secure="true"/>
 <allow-access-from domain="*" secure="true" />
 </cross-domain-policy>
"""
    return HttpResponse(crossdomain_str, content_type='application/xml')


