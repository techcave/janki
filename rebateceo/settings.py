# -*- coding: utf-8 -*-
# !/usr/bin/env python
#
# Copyright (c) 2010-2014 Mariusz Smenzyk <mariusz.smenzyk@sizeof.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""

@author: Mariusz Smenzyk
@license: MIT License
@contact: mariusz.smenzyk@sizeof.pl
"""
from datetime import datetime
from decimal import Decimal as D
import os
from django_utils.datetimestub import DatetimeStub

_ = lambda s: s


#################################################################
class FakeDatetime(DatetimeStub):
    class datetime(DatetimeStub.datetime):
        year = 2015
        month = 1
        day = 1

# sys.modules['datetime'] = FakeDatetime()
#################################################################


ROOT = os.path.dirname(os.path.abspath(__file__))
path = lambda *a: os.path.join(ROOT, *a)

os.environ['REUSE_DB'] = "1"

ADMINS = (
    ('Mariusz', 'msmenzyk@sizeof.pl'),
)

ALLOWED_HOSTS = (
    '*'
)

MANAGERS = ADMINS

TIME_ZONE = 'Europe/Warsaw'

gettext = lambda s: s
LANGUAGES = (
    ('en', gettext('English')),
    #('pl', gettext('Polish')),
)

LANGUAGE_CODE = 'en'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

MEDIA_ROOT = os.path.join(ROOT, "media")
MEDIA_URL = "/media/"

STATIC_ROOT = os.path.join(ROOT, "static")
STATIC_URL = "/static/"

SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_PREFIX = 'session'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(ROOT, "assets"),
)

# Don't share this with anybody.
SECRET_KEY = 'md@jzj9hh8-_f-xtoo-fhfj06+&_5$+e#==&rl6p*0!pz9zl&v'

MIDDLEWARE_CLASSES = (
    'django_utils.middleware.BasicAuthMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django_tables2_reports.middleware.TableReportMiddleware',
    'django_utils.middleware.ForceLangMiddleware',
    'reversion.middleware.RevisionMiddleware',
    # 'newrelicextensions.middleware.NewRelicMiddleware'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "rebateceo.core.context_processors.conf",
    "rebateceo.utils.context_processors.version",
    "ws4redis.context_processors.default",
)

ROOT_URLCONF = 'rebateceo.urls'

# Set your DSN value
RAVEN_CONFIG = {
    'dsn': '',
}

INSTALLED_APPS = [
    'grappelli',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.webdesign',
    'django.contrib.humanize',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'bootstrap3',
    'constance',
    'constance.backends.database',
    'captcha',
    'crispy_forms',
    'chartit',
    'dbbackup',
    'django_nose',
    'django_extensions',
    'django_filters',
    'django_ean13',
    'django_utils',
    'django_tables2',
    'django_tables2_reports',
    'easy_thumbnails',
    'endless_pagination',
    'localflavor',
    'raven.contrib.django.raven_compat',
    'rest_framework',
    'rest_framework.authtoken',
    'sizeof_cpanel',
    'sizeof_cpanel_v2',
    'sizeof_qrcode',
    'widget_tweaks',
    'ws4redis',
    'reversion',
    'guardian',
    'userena',
    'userena.contrib.umessages',
    'adminsortable',
    'test_without_migrations',
    'rest_framework_swagger',
    'parler',

    # Rebateceo apps
    'rebateceo.contrib.api',
    'rebateceo.contrib.dashboard',
    'rebateceo.contrib.advert',
    'rebateceo.contrib.map',
    'rebateceo.contrib.tags',
    'rebateceo.contrib.shop',
    'rebateceo.contrib.trans',
    'rebateceo.contrib.stand',
    'rebateceo.contrib.nameday',
    'rebateceo.contrib.mall',
    'rebateceo.contrib.authorize',
    'rebateceo.contrib.bookmark',
    'rebateceo.contrib.profile',
    'rebateceo.contrib.backup',
    'rebateceo.contrib.news',
    'rebateceo.contrib.promotion',
    'rebateceo.contrib.purchase',
    'rebateceo.contrib.contest',
    #'rebateceo.contrib.bonarka1',
    'rebateceo.contrib.rebate',
    'rebateceo.contrib.screensaver',
    'rebateceo.contrib.tracking',
    'rebateceo.contrib.panel',
    'rebateceo.contrib.campaign',
    'rebateceo.contrib.websocket',
    'rebateceo.contrib.card',
    'rebateceo.contrib.verifier',
    'rebateceo.contrib.loyalty',
    'rebateceo.contrib.cache',

    # 'rebateceo.contrib.account',
    # 'rebateceo.contrib.chat',
    # 'rebateceo.contrib.code',
    # 'rebateceo.contrib.card',
    # 'rebateceo.contrib.demo',
    # 'rebateceo.contrib.lottery',
    # 'rebateceo.contrib.tools'
    # 'rebateceo.contrib.member',

    # 'rebateceo.contrib.manager',
    # 'rebateceo.contrib.operator',
    # 'rebateceo.contrib.stat',
    # 'rebateceo.contrib.report',
    # 'rebateceo.contrib.printlog',
    # 'rebateceo.contrib.campaigns',
    # 'rebateceo.contrib.selfkiosk'
]

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), "templates"),
)

LOCALE_PATHS = [
    os.path.join(os.path.dirname(__file__), 'locale')
]

SITE_ID = 1

TEST_RUNNER = 'rebateceo.utils.testrunner.NoDbTestRunner'

# ==== LOGGING ====
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'INFO',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'INFO',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'socketio': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

GRAPPELLI_ADMIN_URL = '/'

GRAPPELLI_ADMIN_TITLE = 'Rebate CEO'

ANONYMOUS_USER_ID = -1

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'ws4redis.django_runserver.application'

# ===== WEB SOCKETS =====
WEBSOCKET_URL = '/ws/'
WS4REDIS_PREFIX = 'ws'
WS4REDIS_EXPIRE = None
# WS4REDIS_HEARTBEAT = '--heartbeat--'

# ===== AUTH ======
AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_PROFILE_MODULE = 'profile.Profile'
USERENA_DISABLE_PROFILE_LIST = False
USERENA_USE_MESSAGES = False
USERENA_ACTIVATION_REQUIRED = False
USERENA_SIGNIN_REDIRECT_URL = _('/account/%(username)s/')
LOGIN_URL = _('/account/signin/')
LOGOUT_URL = _('/account/signout/')

# ==== Crispy forms ====
CRISPY_TEMPLATE_PACK = 'bootstrap3'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rebateceo.contrib.api.renderers.CustomJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'EXCEPTION_HANDLER': 'rebateceo.contrib.api.utils.custom_exception_handler'
}

SOUTH_TESTS_MIGRATE = False

BASICAUTH_ENABLED = False

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

QRCODE_STR = "{type};{uuid}"
QRCODE_IMG = "http://chart.apis.google.com/chart?cht=qr&chs={size}x{size}&chl={qrcode_str}"

EXCEL_SUPPORT = 'xlwt'  # or 'openpyxl' or 'pyexcelerator'

GOOGLE_ANALYTICS_UA = ''

PROJECT_NAME = 'Rebate CEO'

PROJECT_OWNER_NAME = 'Rebate CEO'

PHONENUMBER_DEFAULT_REGION = 'PL'



CONSTANCE_CONFIG = {
    'VERIFIER_PING_TIMEOUT': (70, 'ping in seconds'),
    'VERIFIER_CONN_TYPE': ('GSM', 'connection type'),
    'VERIFIER_ENABLED_FROM_HOUR': ('09:00', 'enabled from'),
    'VERIFIER_ENABLED_TO_HOUR': ('21:00', 'enabled from'),
    'VERIFIER_REQUIRE_SELLER_CARD': (False, 'Purchase register requires seller card confirmation?'),
    'VERIFIER_USER_ACTION_TIMEOUT': (10, 'Time for user action (in seconds)'),

    # 'SELFKIOSK_TEXT_CARD_NO_ACTIVE_TITLE': (u'Karta nieaktywna', ''),
    # 'SELFKIOSK_TEXT_CARD_NO_ACTIVE_HEADER': (u'Istnieja dwa sposoby rejestracji Twojej Karty', ''),
    # 'SELFKIOSK_TEXT_CARD_NO_ACTIVE_SMS': (u'Wyslij <strong>SMS</strong> zawierający numer karty na nr <strong>xxx yyy zzz<strong>', ''),
    # 'SELFKIOSK_TEXT_CARD_NO_ACTIVE_FILLFORM': (u'Wypelnij formularz w <strong>Punkcie Informacyjnym</strong>', '')
}

MALL_ID = 1

AUTHORIZE = {
    'LOGIN_AFTER_REGISTRATION': True,
}

MOBILE_IMAGE_SETTINGS = {
    'USE': False,
    'IMAGE_SIZE': (270, 384),
    'BG_COLOR': "#777777",
    'FONT_COLOR': "white",
    'FONT_SIZE': 15,
    'FONT_PATH': os.path.join(ROOT, "assets") + "/fonts/HelveticaNeueLTPro-Lt.ttf",
    'LINE_HEIGHT': 15,
}


# Chances for purchases amount range
RECEIPT_CHANCES_RULES = [
    { # from 2015 to ...
        'from': datetime(year=2014, month=12, day=31, hour=17, minute=0, second=0),
        'to': datetime(year=2017, month=12, day=31, hour=15, minute=0, second=0),
        'rules': [
            [D('20.00'), D('20.00'), 1],
        ],
        'func': 'calc_from_multiplier'
    },
    { # 2014
        'from': datetime(year=2013, month=9, day=1, hour=9, minute=0, second=0),
        'to': datetime(year=2014, month=12, day=31, hour=15, minute=0, second=0),
        'rules': [
            [D('0.00'), D('19.99'), 0],
            [D('20'), D('49.99'), 1],
            [D('50'), D('99.99'), 2],
            [D('100'), D('199.99'), 3],
            [D('200'), D('299.99'), 4],
            [D('300'), D('999.99'), 5],
            [D('1000'), D('100000000.00'), 10]
        ],
        'func': 'calc_from_thresholds'
    },
]