# -*- coding: utf-8 -*-
from rabatomat.conf.server import *

DEBUG = False

DEMO = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '<DB-NAME>',
        'USER': '<DB-USER>',
        'PASSWORD': '<DB-PASSWORD>',
        'HOST': '<DB-HOST>',
        'PORT': '<DB-PORT>',
    },
}