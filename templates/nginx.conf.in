server {
    server_name ${deploy:server_name};
    listen ${deploy:listen};
    root ${buildout:directory}/${deploy:project};
    index index.html index.htm index.php;
    access_log ${buildout:directory}/var/log/${deploy:server_name}_access_log;
    error_log ${buildout:directory}/var/log/${deploy:server_name}_error_log;
    client_max_body_size 20M;
    fastcgi_read_timeout 1000;
    fastcgi_param GATEWAY_INTERFACE CGI/1.1;
    fastcgi_param SERVER_SOFTWARE nginx;
    fastcgi_param QUERY_STRING $query_string;
    fastcgi_param REQUEST_METHOD $request_method;
    fastcgi_param CONTENT_TYPE $content_type;
    fastcgi_param CONTENT_LENGTH $content_length;
    fastcgi_param SCRIPT_FILENAME ${buildout:directory}/${deploy:project}$fastcgi_script_name;
    #fastcgi_param SCRIPT_NAME $fastcgi_script_name;
    fastcgi_param REQUEST_URI $request_uri;
    fastcgi_param DOCUMENT_URI $document_uri;
    fastcgi_param DOCUMENT_ROOT ${buildout:directory}/${deploy:project};
    fastcgi_param SERVER_PROTOCOL $server_protocol;
    fastcgi_param REMOTE_ADDR $remote_addr;
    fastcgi_param REMOTE_PORT $remote_port;
    fastcgi_param SERVER_ADDR $server_addr;
    fastcgi_param SERVER_PORT $server_port;
    fastcgi_param SERVER_NAME $server_name;
    fastcgi_param HTTPS $https;
 
    location /public {
        root ${buildout:directory}/${deploy:project};
        access_log      off;
        expires 30d;
    }

    location /maintenance.html {
        root ${buildout:directory}/${deploy:project};
        access_log      off;
        expires 30d;
    }

    location /static {
        root ${buildout:directory}/${deploy:project};
        access_log      off;
        expires max;
    }

    location /media {
        root ${buildout:directory}/${deploy:project};
        access_log      off;
        expires max;
    }

    location /media/purchases/ {
        auth_basic "Restricted area";
        auth_basic_user_file ${buildout:directory}/.htpasswd;
        root ${buildout:directory}/${deploy:project};
        access_log      off;
        expires max;
    }
   
    location /favicon.ico {
        root ${buildout:directory}/${deploy:project}/public;
        access_log      off;
        expires 30d;
    }

    location /apple-touch-icon.png {
        root ${buildout:directory}/${deploy:project}/public;
        access_log      off;
        expires 30d;
    }

    location /robots.txt {
        root ${buildout:directory}/${deploy:project}/public;
        access_log      off;
        expires 30d;
    }

    location / {
        auth_basic "Restricted area";
        auth_basic_user_file ${buildout:directory}/.htpasswd;

        if (-f $document_root/../maintenance) {
            return 503;
        }

        if ($request_filename ~* ^.*?\.(eot)|(ttf)|(woff)|(svg)|(otf)$) {
            add_header Access-Control-Allow-Origin *;
        }

        error_page 404 = /public/404.html;
        error_page 500 501 = /public/50x.html;
        error_page 502 503 504 = /public/maintenance.html;

        fastcgi_pass ${deploy:fastcgi_pass_host}:${deploy:fastcgi_pass_port};
        #fastcgi_param HTTPS on;
        #include ${buildout:directory}/${deploy:project}/etc/nginx_fastcgi_django;
    }

    location /api/ {
        auth_basic          "off";
        fastcgi_pass ${deploy:fastcgi_pass_host}:${deploy:fastcgi_pass_port};
    }

    location /ws/ {
        auth_basic          "off";
        include /etc/nginx/uwsgi_params;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        uwsgi_pass ${deploy:uwsgi_socket};
        #proxy_pass http://unix:/${buildout:directory}/var/web.socket;
    }

    location /crossdomain.xml {
        auth_basic          "off";
        fastcgi_pass ${deploy:fastcgi_pass_host}:${deploy:fastcgi_pass_port};
    }

    if ($scheme = http) {
        #  return 301 https://$server_name$request_uri;
    }

    error_page 503 @maintenance;
    location @maintenance {
        rewrite ^(.*)$ /maintenance.html break;
    }

    listen ${deploy:listen}:${deploy:ssl_port} ssl;

    #ssl_certificate ${buildout:directory}/${deploy:project}/etc/cert.pem;
    #ssl_certificate_key ${buildout:directory}/${deploy:project}/etc/cert.key;

    gzip off;
}
