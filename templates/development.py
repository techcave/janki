# -*- coding: utf-8 -*-
from backend.settings import *

DEBUG = True

DEMO = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "<DB-NAME>",
        "USER": "<DB-USER>",
        "PASSWORD": "<DB-PASSWORD>",
        "HOST": "<DB-HOST>",
        "PORT": "<DB-PORT>",
    },
}

SECRET_KEY =

# Titles
PROJECT_NAME =
PROJECT_OWNER_NAME = PROJECT_NAME
GRAPPELLI_ADMIN_TITLE = PROJECT_NAME