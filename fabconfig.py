from fabric.decorators import task
from fabric.state import env
from fabric.colors import green


def notify(msg, color=green):
    bar = '+' + '-' * (len(msg) + 2) + '+'
    print color('')
    print color(bar)
    print color("| %s |" % msg)
    print color(bar)
    #print color('')


@task
def dev(name_gallery=None):
    """Setup env"""
    if not name_gallery:
        print "Not name gallery!!!"
        exit(0)
    env.hosts = ['192.162.248.15']
    env.user = '{0}_dev'.format(name_gallery)
    env.package = 'backend'
    env.project = 'backend'
    env.env = "dev"
    env.code_dir = '/home/{0}_dev/www/{0}'.format(
        name_gallery,
        name_gallery)
    env.virtualenv = ''
    env.settings = 'backend.conf.development'
    env.use_db_backup = False


@task
def prod(name_gallery):
    """Setup env"""
    if not name_gallery:
        print "Not name gallery!!!"
        exit(0)
    env.hosts = ['192.162.248.15']
    env.user = '{0}'.format(name_gallery)
    env.package = 'backend'
    env.project = 'backend'
    env.env = "dev"
    env.code_dir = '/home/{0}/www/{0}'.format(
        name_gallery,
        name_gallery)
    env.virtualenv = ''
    env.settings = 'backend.conf.production'
    env.use_db_backup = False