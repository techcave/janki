# Basis for all new panels
# Version 1.0.0

1. Create fork with name gallery (https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) 
2. git clone 
3. cd <name-gallery>/
4. cp deploy/local.cfg-dist llocal.cfg && nano local.cfg
 """
 [deploy]
 server_name = dev/prod-<name-gallery>.techcave.pl
 listen = 192.162.248.15
 fastcgi_pass_host = 127.0.0.1
 fastcgi_pass_port = 3005 (change)
 uwsgi_socket = 0.0.0.0:4005 (change)
 autostart_apps = true
 dj_settings = backend.conf.development/production
 project = backend
 ssl_port = 443
 env = dev/prod
 """
5. make dev
