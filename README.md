# Basis for all new panels
# Version 1.0.0
Create fork with name mall (https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) 

Connect to server over ssh:
```
$ ssh <name_mall>@192.162.248.15
```

Create ssh key:
```
$ ssh-keygen -t rsa -b 4096 -C '<name_mall>'
```

Copy key and add to repo
```
$ cat ~/.ssh/id_rsa.pub
```
* copy ssh-key and add kaey to bitbacket.org -> <name_mall> -> Settings -> Access keys -> add key
* after that go to https://bitbucket.org/techcave/script_deploy_panel
