import random
import string

from contextlib import contextmanager
from fabric.context_managers import cd, prefix
from fabric.operations import require, run, sudo, local, put, get
from fabric.state import env
from fabric.colors import green, white
from fabric.api import task, settings
from fabric.contrib.files import exists, sed
from fabconfig import dev, prod
from datetime import datetime


def notify(msg, color=green):
    bar = '+' + '-' * (len(msg) + 2) + '+'
    print color('')
    print color(bar)
    print color("| %s |" % msg)
    print color(bar)
    print color('')


@contextmanager
def virtualenv():
    with cd(env.code_dir):
        yield


def django_manage(cmd):
    run('bin/%s-%s %s' % (env.project, env.env, cmd))


@task
def collectstatic():
    require('hosts')
    notify("Collecting static")
    with cd(env.code_dir):
        django_manage("collectstatic --noinput -v 0")


@task
def restart():
    require('hosts')
    notify("Restarting application")
    with cd(env.code_dir):
        run("bin/supervisorctl restart all")


@task
def update_code():
    require('hosts')
    notify("Updating code")
    with cd(env.code_dir):
        run('git reset --hard')
        run('git pull')
        run('make %s' % (env.env))


@task
def css():
    require('hosts')
    notify("Compile css")
    with virtualenv():
        run('make css')

@task
def clear_pyc():
    notify("Clearing pyc files")
    with cd(env.code_dir):
        run("find . -name '*.pyc' -print0|xargs -0 rm")


@task
def syncdb():
    notify("Synchronize db")
    with cd(env.code_dir):
        django_manage("syncdb --noinput > /dev/null")
        django_manage("migrate")


@task
def createsuperuser():
    notify("Create super user")
    with cd(env.code_dir):
        django_manage("createsuperuser")


@task
def shell():
    with cd(env.code_dir):
        django_manage("shell")


def backup_pg(settings='project.settings', alias='default', file_name='backup.sql'):
    """Create pg dump using django db settings"""

    def get_db_param(param, alias, settings):
        return ("python -c \"import %(settings)s as settings; "
                "print settings.DATABASES['%(alias)s']['%(param)s']\"" %
                {'param': param.upper(), 'alias': alias, 'settings': settings})

    param_kwargs = {'alias': alias, 'settings': settings}
    run("""
        DBHOST=`%(host)s`
        DBUSER=`%(user)s`
        DBPASS=`%(password)s`
        DBNAME=`%(name)s`
        DBPORT=`%(port)s`
        mkdir -p data
        export PGPASSWORD=$DBPASS
        pg_dump -U $DBUSER -h $DBHOST $DBNAME | gzip > data/%(file_name)s
        """ % {'host': get_db_param('host', **param_kwargs),
               'user': get_db_param('user', **param_kwargs),
               'password': get_db_param('password', **param_kwargs),
               'name': get_db_param('name', **param_kwargs),
               'port': get_db_param('port', **param_kwargs),
               'file_name': file_name})


@task
def backup():
    with cd(env.code_dir):
        file_name = 'db_%s.sql' % datetime.now().strftime('%Y%m%d_%H%M%S')
        backup_file = 'data/%s' % file_name

        notify("Backup database")
        backup_pg(file_name=file_name)

        notify("Download backup")
        get(backup_file, backup_file)



@task
def db_backup():
    """Backup database to remote data directory"""
    require('hosts', provided_by=[dev])
    with cd(env.code_dir):
        file_name = 'db_%s.sql.gz' % datetime.now().strftime('%Y%m%d_%H%M%S')
        backup_file = 'data/%s' % file_name

        notify("Backup database...", white)
        backup_pg(settings=env.settings, file_name=file_name)
        notify("Backup was created: %s" % backup_file, green)


@task
def get_db_backup(backup_file):
    """Get specified database backup from remote to local"""
    require('hosts', provided_by=[dev])
    with cd(env.code_dir):
        notify("Download backup")
        get(backup_file, backup_file)


@task
def deploy():
    require('hosts', provided_by=[dev, prod])
    with settings(warn_only=True):
       result = tests()
       if result.return_code == 0:
          quick_deploy()
       elif result.return_code == 2:
            pass
       else: #print error to user
          print result
          raise SystemExit()


def _extractstatic():
    _backend_dir = env.code_dir + '/backend'
    if not exists(_backend_dir) and exists(_backend_dir + 'static.tar'):
        run("cd %s && tar -xvf static.tar && rm static.tar" % _backend_dir)


def _create_main_dir(name_gallery):
    if not exists('www'):
        run('mkdir -p www')
    with cd('www'):
        if not exists(name_gallery):
            run('git clone git@bitbucket.org:techcave/rabatomat-%s.git %s'
                % (name_gallery, name_gallery))


def _build(name_gallery=None):
    with cd(env.code_dir):
        loc = 'local.cfg'
        run('cp deploy/local.cfg-dist %s' % loc)
        if env.env == "dev":
            run('cp deploy/dev.cfg-dist dev.cfg')
            sed('%s' % loc,
                'server_name =',
                'server_name = dev-%s.techcave.pl' % name_gallery)
        else:
            run('cp deploy/prod.cfg-dist prod.cfg')
            sed('%s' % loc,
                'server_name =',
                'server_name = %s.techcave.pl' % name_gallery)
        sed('%s' % loc,
            'fastcgi_pass_port = 3005',
            'fastcgi_pass_port = %s' % random.randint(3000, 3999)
            )
        sed('%s' % loc,
            'uwsgi_socket = 0.0.0.0:4005',
            'uwsgi_socket = 0.0.0.0:%s' % random.randint(4000, 4999)
            )
        sed('%s' % loc,
            'dj_settings =',
            'dj_settings = %s' % env.settings
            )
        sed('%s' % loc,
            'env =',
            'env = %s' % env.env
            )
        run('make %s' % env.env)


def _configure_db(name_gallery=None):
    settings_db = 'development.py' if env.env == 'dev' else 'production.py'
    with cd(env.code_dir + '/backend/conf/'):
        DATABASE =  {
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'pg68886_%s%s' % (name_gallery, '_dev' if env.env == "dev" else ''),
                'USER': 'pg68886_%s%s' % (name_gallery, '_dev' if env.env == "dev" else ''),
                'PASSWORD': '7FrlUiA3.X',
                'HOST': '68886.p.tld.pl',
                'PORT': '5432',
            },
        }
        sed(settings_db, "DATABASES = ''", str(DATABASE))


def _set_name_and_key(name_gallery=None):
    line_chars = string.ascii_letters + string.digits + string.punctuation
    key = ''.join(random.SystemRandom().choice(line_chars) for _ in range(50))
    with cd(env.code_dir + '/backend'):
        sed('settings.py', "SECRET_KEY = ''", "SECRET_KEY = '%s'" % key)
        sed('settings.py', "PROJECT_NAME = ''", "PROJECT_NAME = '%s'" % name_gallery)



@task
def quick_deploy(name_gallery=None, servers='all'):
    # require('hosts', provided_by=[dev, prod])
    ser = []
    if servers == 'all':
        ser = [dev, prod]
    else:
        ser = [dev if servers == dev.__name__ else prod]
    for fun in ser:
        fun.__call__(name_gallery)
        _create_main_dir(name_gallery)
        _build(name_gallery)
        _configure_db(name_gallery)
        _set_name_and_key(name_gallery)
        # update_code()
        _extractstatic()
        #css()
        #collectstatic()
        syncdb()
        restart()


@task
def deploy_quick():
    """Alias"""
    quick_deploy()

@task
def tests():
    notify("Run tests")
    return local('bin/backend-test test rebateceo --verbosity 2')


@task
def unittests():
    tests = [
        'rebateceo.contrib.api.tests',
        'rebateceo.contrib.shop.api.tests',
        'rebateceo.contrib.advert.api.tests',
        'rebateceo.contrib.contest.api.tests',
        'rebateceo.contrib.map.api.tests',
        'rebateceo.contrib.bookmark.api.tests',
        'rebateceo.contrib.rebate.api.tests',
        'rebateceo.contrib.promotion.api.tests',
        'rebateceo.contrib.news.api.tests',
        'rebateceo.contrib.screensaver.api.tests',
        'rebateceo.contrib.stand.api.tests',
        'rebateceo.contrib.bonarka1.api.tests',
        #'rebateceo.contrib.profile.api.tests',
        #'rebateceo.contrib.tracking.api.tests',
        #'rebateceo.contrib.tags.api.tests',
        #'rebateceo.contrib.trans.api.tests',
        #'rebateceo.contrib.authorize.api.tests',
    ]
    local('bin/backend-test test {}'.format(' '.join(tests)))